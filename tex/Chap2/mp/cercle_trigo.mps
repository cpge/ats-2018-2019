%!PS
%%BoundingBox: -57 -57 64 72 
%%HiResBoundingBox: -56.97636 -56.97636 63.1252 71.02899 
%%Creator: MetaPost 2.000
%%CreationDate: 2018.01.24:1447
%%Pages: 1
%*Font: cmmi10 9.96265 9.96265 4d:a000000c00004
%*Font: cmmi7 6.97385 6.97385 12:8
%*Font: cmr9 8.96637 8.96637 28:c000000000000010431
%*Font: cmmi9 8.96637 8.96637 12:8
%*Font: cmsy10 9.96265 9.96265 43:8
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0 0 setrgbcolor 0 0.56693 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath -56.6929 0 moveto
56.6929 0 lineto stroke
 0.56693 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 -56.6929 moveto
0 56.6929 lineto stroke
 0 1 dtransform truncate idtransform setlinewidth pop
newpath 0 0 moveto
54.99219 0 lineto stroke
newpath 51.29662 -1.53079 moveto
54.99219 0 lineto
51.29662 1.53079 lineto
 closepath
gsave fill grestore stroke
 1 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 0 moveto
0 54.99219 lineto stroke
 0 1 dtransform truncate idtransform setlinewidth pop
newpath 1.53079 51.29662 moveto
0 54.99219 lineto
-1.53079 51.29662 lineto
 closepath
gsave fill grestore stroke
 0 0.56693 dtransform truncate idtransform setlinewidth pop
newpath 56.6929 0 moveto
56.6929 15.03656 50.71877 29.45546 40.08711 40.08711 curveto
29.45546 50.71877 15.03656 56.6929 0 56.6929 curveto
-15.03656 56.6929 -29.45546 50.71877 -40.08711 40.08711 curveto
-50.71877 29.45546 -56.6929 15.03656 -56.6929 0 curveto
-56.6929 -15.03656 -50.71877 -29.45546 -40.08711 -40.08711 curveto
-29.45546 -50.71877 -15.03656 -56.6929 0 -56.6929 curveto
15.03656 -56.6929 29.45546 -50.71877 40.08711 -40.08711 curveto
50.71877 -29.45546 56.6929 -15.03656 56.6929 0 curveto closepath stroke
 0.56693 0 dtransform exch truncate exch idtransform pop setlinewidth
 [1.5 1.5 ] 0 setdash
newpath 43.42886 0 moveto
43.42886 36.44173 lineto stroke
 0 0.56693 dtransform truncate idtransform setlinewidth pop
newpath 43.42886 36.44173 moveto
0 36.44173 lineto stroke
 0 0.2 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 0 0 moveto
43.42886 36.44173 lineto stroke
-9.976 -8.90779 moveto
(O) cmmi10 9.96265 fshow
 0 3 dtransform truncate idtransform setlinewidth pop
newpath 0 0 moveto 0 0 rlineto stroke
45.52885 40.03612 moveto
(M) cmmi10 9.96265 fshow
55.19406 38.54172 moveto
(\022) cmmi7 6.97385 fshow
newpath 43.42886 36.44173 moveto 0 0 rlineto stroke
31.39111 -9.7248 moveto
(cos\() cmr9 8.96637 fshow
47.31322 -9.7248 moveto
(\022) cmmi9 8.96637 fshow
51.88292 -9.7248 moveto
(\)) cmr9 8.96637 fshow
newpath 43.42886 0 moveto 0 0 rlineto stroke
-26.0516 34.20012 moveto
(sin\() cmr9 8.96637 fshow
-11.15341 34.20012 moveto
(\022) cmmi9 8.96637 fshow
-6.58371 34.20012 moveto
(\)) cmr9 8.96637 fshow
newpath 0 36.44173 moveto 0 0 rlineto stroke
-48.51166 -48.75839 moveto
(C) cmsy10 9.96265 fshow
58.1522 -2.41835 moveto
(~) cmmi10 9.96265 fshow
59.6929 -4.69945 moveto
(i) cmmi10 9.96265 fshow
-3.2569 63.9112 moveto
(~) cmmi10 9.96265 fshow
-2.3365 61.6301 moveto
(j) cmmi10 9.96265 fshow
showpage
%%EOF
