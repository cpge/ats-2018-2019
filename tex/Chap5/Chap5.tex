\objectif{Ce chapitre est l'occasion de revenir sur un concept vu en Physique et en S.I : les équations différentielles linéaires. On verra différentes méthodes de résolution, et le lien avec les autres matières scientifiques.
}

\large \textbf{Objectifs} \normalsize

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
	\item Concernant les équations différentielles du premier ordre :
			\begin{itemize}[label=\textbullet]
				\item Savoir trouver des primitives simples\dotfill $\Box$
				\item Savoir résoudre des équations du premier ordre  sans second membre\dotfill $\Box$ 
				\item Savoir trouver la forme générale des équations du premier ordre avec second membre\dotfill $\Box$
				\item Savoir déterminer une solution particulière par la variation de la constante\dotfill $\Box$
				\item Savoir déterminer une solution particulière simple\dotfill $\Box$
				\item Savoir déterminer une solution particulière par le principe de superposition\dotfill $\Box$
			\end{itemize}
		\item Concernant les équations différentielles du second ordre :
			\begin{itemize}[label=\textbullet]
				\item Savoir résoudre les équations sans second membre dans $\R$ et dans $\C$\dotfill $\Box$
				\item Savoir trouver la forme générale des équations du premier ordre avec second membre\dotfill $\Box$
				\item Savoir trouver une solution particulière avec second membre exponentielle-polynôme\dotfill $\Box$
			\end{itemize}
		\item Savoir résoudre un problème de Cauchy\dotfill $\Box$
\end{numerote}

\newpage

Dans l'ensemble de ce cours, les fonctions sont à valeurs dans $\R$ ou $\C$. On notera de manière générique $\mathbb{K}$ le corps d'arrivée des fonctions.

\section{Notions d'équation différentielle}

	\subsection{Généralités}

\definition{On appelle \textbf{équation différentielle} d'ordre $n$ une égalité liant la variable réelle $x$, une fonction inconnue $y$, définie sur un intervalle $I$ de $\R$, et ses dérivées successives $y', y'',\dots,y^{(n)}$.}

\remarque{Si la fonction inconnue est à valeurs dans $\R$ ou $\C$, on dit que l'équation est scalaire. C'est l'objectif de ce chapitre. On verra plus tard dans l'année les équations différentielles vectorielles, où la fonction inconnue est à valeurs dans $\R^p$ ou $\C^p$.}

\exemple{Les équations suivantes sont des équations différentielles scalaires du premier ordre
\[ y'+xy = 3x, \quad \quad y'=\frac{1}{x}, \quad \quad y'=y^2+y, \quad \quad y'=ay~(a\in \R) \]
$y''+3y'+4y=\eu{x}$ est une équation différentielle scalaire du second ordre.}

\definition{On appelle \textbf{solution maximale} d'une équation différentielle une fonction $f$ satisfaisant l'équation différentielle, et définie sur un intervalle $I$ maximal, c'est-à-dire une fonction ne pouvant être prolongée sur un intervalle plus grand.}

\remarque{Résoudre une équation différentielle, c'est trouver \textbf{l'ensemble} de ses solutions maximales.\\On appellera alors \textbf{courbe intégrale} la courbe représentative d'une fonction maximale.}

\remarque{Nous allons cette année apprendre à résoudre certaines équations différentielles. Il arrive cependant très souvent qu'on arrive pas à exprimer les solutions d'une équation différentielle avec les fonctions usuelles. On essaie alors de représenter graphiquement une solution.}


	\subsection{Problème de Cauchy}

\definition{Soit $(E)$ une équation différentielle d'ordre $n$. On appelle \textbf{problème de Cauchy} la donnée de l'équation différentielle, et de $n$ conditions initiales sur les valeurs de la fonction $y$ et de ses dérivées en un réel $x_0$.}

\exemple{Par exemple, $y'=3xy+4$, avec $y(0)=1$, est un problème de Cauchy.}


	\subsection{Equations différentielles linéaires}

\definition{Une équation différentielle d'ordre $n$ est dite linéaire si elle s'écrit sous la forme

\[ (E)~:~a_ny^{(n)}+a_{n-1}y^{(n-1)}+\cdots + a_1y'+a_0y=f \]
où $a_0,\cdots, a_n$ et $f$ sont des fonctions continues sur un intervalle $I$.\\
$f$ est appelé le \textbf{second membre} de l'équation différentielle linéaire.\\Si $f$ est la fonction nulle, on dit que l'équation linéaire est \textbf{homogène} ou \textbf{sans second membre}.\\
Si $a_0,\cdots,a_n$ sont des constantes, on dit que l'équation est à \textbf{coefficients constants}.
}

\exemple{Les équations différentielles suivantes sont linéaires : \[ y'+2xy=\E^x\quad \quad y''+2xy'+\ln(x)y=1 \quad \quad y^{(3)}+y'=\frac{1}{x} \]
En revanche, $y'=y^2$ n'est pas linéaire.}

\section{Equations différentielles linéaires du premier ordre}

	\subsection{Généralités}
	
\definition{Une équation différentielle du premier ordre est une équation différentielle de la forme \[ y'=a(x)y+b(x) \]
où $a$ et $b$ sont deux fonctions définies et continues sur un intervalle $I$ de $\R$.}

\exemple{L'équation $y'=xy+\E^x$ est une équation différentielle linéaire du premier ordre.\\L'équation $y'=3y+4x$ est une équation différentielle linéaire du premier ordre, à coefficients constants.}

\definition{Soit $(E)~y'=a(x)y+b(x)$ une équation différentielle linéaire du premier ordre. On appelle \textbf{équation homogène} associée à $(E)$ l'équation différentielle $y'=a(x)y$. Il s'agit également d'une équation différentielle linéaire du premier ordre.}

\exemple{Les équations homogènes associées aux deux exemples précédents sont $y'=xy$ et $y'=3y$.}

\begin{theoreme}
Soit $(E)$ une équation différentielle $y'=a(x)y+b(x)$, avec $y(x_0)=y_0$. Alors il existe une unique solution à ce problème de Cauchy.	
\end{theoreme}


\remarque{Ainsi, il s'agira de trouver la seule solution si on dispose d'un problème de Cauchy.}

	\subsection{Résolution d'une équation homogène}

\begin{theoreme}
Soit $a$ une fonction continue sur un intervalle $I$ de $\R$. Soit $(E)$ l'équation différentielle $y'=a(x)y$.
L'ensemble des solutions de $(E)$ sur l'intervalle $I$ est
\[ \mathcal{S}=\left \{ x\mapsto k\eu{A(x)},\quad k\in \R \right \} \]
où $A$ est une primitive de $a$ sur $I$.	
\end{theoreme}


\preuve{$a$ étant continue sur $I$, elle y admet des primitives. On note $A$ une primitive de $a$. Remarquons alors que $f$ est solution de $(E)$ si et seulement si, pour tout $x\in I$ :
\begin{eqnarray*}
	f'(x)=a(x)f(x) &\Leftrightarrow& f'(x)e^{-A(x)}=a(x)f(x)e^{-A(x)} \quad \text{car } \eu{-A(x)}>0 \\
	 &\Leftrightarrow& f'(x)e^{-A(x)} + f(x)\left(-a(x)e^{-A(x)}\right)=0 \\
	 &\Leftrightarrow& (fe^{-A})'(x) = 0
\end{eqnarray*}
Ainsi, la fonction $x\mapsto f(x)\eu{-A(x)}$ est une fonction constante sur $I$. Il existe donc $k\in \R$ tel que 
\[ \forall~x\in I,\quad f(x)\eu{-A(x)}=k \Leftrightarrow f(x)=k\eu{A(x)} \]
Réciproquement, une fonction de la forme $x\mapsto k\eu{A(x)}$ est solution de $(E)$.
}

\exemple{Déterminer l'ensemble des solutions sur $\R$ de l'équation différentielle $y'=xy$.}

\begin{prof}
\solution{Soit $a$ la fonction définie sur $\R$ par $a(x)=x$. Une primitive de $a$ est la fonction définie sur $\R$ par $A:x\mapsto \dfrac{x^2}{2}$. Ainsi, l'ensemble des solutions de l'équation $y'=xy$ sur $\R$ est 
	\[ \mathcal{S} = \left \{ x\mapsto k\eu{\frac{x^2}{2}},\quad k\in \R \right \} \]
}	
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{5}{\textwidth}
\end{solu_eleve}
\end{eleve}

\exercice{Déterminer l'ensemble des solutions sur $\R^*_+$ de l'équation différentielle $y'=\frac{y}{x}$, avec $y(1)=1$.}

\begin{prof}
\solution{On note $a$ la fonction définie sur $\R^*_+$ par $a(x)=\frac{1}{x}$. Une primitive de $a$ sur $\R^*_+$ est $A:x\mapsto \ln(x)$. Alors, l'ensemble des solutions de l'équation $y'=\frac{y}{x}$ sur $\R^*_+$ est 
\[ \mathcal{S} = \left \{ x\mapsto k\eu{\ln(x)}=kx,\quad k\in \R \right \} \]
Avec la condition $y(1)=1$, on obtient $k\times 1 = 1$, c'est-à-dire $k=1$. Ainsi, l'unique solution du problème de Cauchy est 
\[ \left \{ \begin{array}{lcl}\R^*_+ & \rightarrow & \R \\ x &\mapsto& x  \end{array} \right. \]
}	
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{7}{\textwidth}
\end{solu_eleve}
\end{eleve}

	\subsection{Cas général}
	
Pour résoudre une équation différentielle linéaire du premier ordre, on se ramène à une équation homogène, selon deux méthodes possibles.

\begin{proposition}
Soit $(E)~y'=a(x)y+b(x)$ une équation différentielle linéaire, avec $a$ et $b$ deux fonctions continues sur $I$. \\Soit $(E_0)$ l'équation différentielle homogène associée : $(E_0)~y'=a(x)y$. Ses solutions sont les fonctions $x\mapsto k\eu{A(x)}$, où $A$ est une primitive de $a$ et $k\in \R$.\\
	Soit $g$ une solution particulière de $(E)$ : pour tout $x\in I$, $g'(x)=a(x)g(x)+b(x)$. \\
	Alors l'ensemble des solutions de $(E)$ sur $I$ est 
	\[ \mathcal{S} = \left \{ x\mapsto g(x)+ k \eu{A(x)},\quad k\in \R \right \} \]	
\end{proposition}


\preuve{Soit $g$ une solution particulière de $(E)$. 
\begin{itemize}[label=\textbullet]
	\item Soit $f$ une solution de $(E)$. Notons $h=f-g$. Alors, pour tout réel $x\in I$
	\begin{eqnarray*}
		h'(x)&=&f'(x)-g'(x) \\
			 &=& (a(x)f(x)+b(x)) - (a(x)g(x)+b(x)) \\
			 &=& a(x) (f(x)-g(x))\\
			 &=& a(x) h(x)
	\end{eqnarray*} 
	Ainsi $h$ est une solution de l'équation homogène associée $(E_0)$.
	\item Réciproquement, soit $h$ une solution de l'équation $(E_0)$. On note $f=h+g$. Alors, pour tout réel $x\in I$ :
	\begin{eqnarray*}
		f'(x) &=& h'(x)+g'(x) \\
			  &=& a(x)h(x) + (a(x)g(x)+b(x)) \\
			  &=& a(x) (h(x)+g(x)) + b(x) \\
			  &=& a(x)f(x)+b(x)
	\end{eqnarray*} 
	et donc $f$ est solution de $(E)$.
\end{itemize}
Ainsi, $f$ est solution de $(E)$ si et seulement si $f-g$ est solution de $(E_0)$, ce qui donne le théorème.
}

\remarque{Ainsi, connaissant une solution particulière de l'équation générale, il suffit de résoudre l'équation homogène associée, et d'ajouter à ses solutions la solution particulière.}

\begin{methode}
Pour résoudre une équation différentielle du premier ordre $(E)~:y'=a(x)y+b(x)$ sur un intervalle $I$.
\begin{numerote}
	\item on résout l'équation différentielle homogène associée : $(E_0)~:y'=a(x)y$.
	\item on cherche une solution particulière de $(E)$.
	\item on conclut quant à l'ensemble des solutions de $(E)$.	
\end{numerote}	
\end{methode}


\exemple{[Fondamental] Soit $(a,b) \in \R^2$, avec $a\neq 0$. Résoudre sur $\R$ l'équation différentielle $y'=ay+b$.}

\begin{prof}
\solution{On $(E)$ l'équation $y'=ay+b$ et $(E_0)$ l'équation homogène associée $y'=ay$.
\begin{itemize}[label=\textbullet]
	\item Une primitive de la fonction $x\mapsto a$ est $x\mapsto ax$. L'ensemble des solutions de l'équation homogène
		\[ \mathcal{S}_0=\left \{ x\mapsto k\eu{ax},\quad k\in \R\right\} \]
	\item Cherchons une solution particulière constante : soit $f:x\mapsto p$. Alors
		\begin{eqnarray*}
			f' = af+b &\Leftrightarrow& 0 = a\times p +b \\ &\Leftrightarrow& p=-\frac{b}{a}
		\end{eqnarray*}
		Ainsi, la fonction $f:x\mapsto -\frac{b}{a}$ est une solution particulière de $(E)$.
\end{itemize}
\textbf{Bilan} : l'ensemble des solutions de $(E)$ est donc
		\[ \mathcal{S}=\left \{ x\mapsto k\eu{ax}-\frac{b}{a},\quad k\in \R\right\} \]
}	
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{10}{\textwidth}
\end{solu_eleve}
\end{eleve}

\exercice{Déterminer l'ensemble des solutions sur $\R$ de l'équation $y'=2y+x$, avec $y(0)=1$.}



\begin{prof}
\solution{Comme précédemment, l'ensemble des solutions de l'équation $y'=2x$ est 
		\[ \mathcal{S}_0=\left \{ x\mapsto k\eu{2x},\quad k\in \R\right\} \]
Le second membre étant $x\mapsto x$, cherchons une solution particulière de la forme $x\mapsto ax+b$. $f$ est donc solution si et seulement si, pour tout $x\in \R$
\begin{eqnarray*} f'(x)=2f(x)+x &\Leftrightarrow& a = 2(ax+b)+x \\ &\Leftrightarrow& (2a+1)x+2b-a = 0 \end{eqnarray*}
Ainsi, on a $2a+1=0$ et $2b-a=0$, c'est-à-dire $a=-\frac{1}{2}$ et $b=-\frac{1}{4}$. Une solution particulière est donc $x\mapsto -\frac{1}{2}x-\frac{1}{4}$.\\
\textbf{Bilan} : l'ensemble des solutions sur $\R$ de l'équation $y'=2y+x$ est donc
		\[ \mathcal{S}=\left \{ x\mapsto k\eu{2x}-\frac{1}{2}x-\frac{1}{4},\quad k\in \R\right\} \]
Avec l'hypothèse $y(0)=1$, cela donne $k-\frac{1}{4}=0$, et donc $k=\frac{1}{4}$.\\
\textbf{Bilan} : la solution de l'équation $y'=2y+x$, avec $y(0)=1$ est
\[ \boxed{x\mapsto \frac{1}{4}\eu{2x}-\frac{1}{2}x-\frac{1}{4}} \]

}	
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{12}{\textwidth}
\end{solu_eleve}
\end{eleve} 

\begin{methode}[Principe de superposition]
Pour déterminer une solution particulière :
 Si on souhaite résoudre l'équation $y'+ay=f+g$, on peut chercher une solution particulière $y_1$ de $y'+ay=f$ et une autre $y_2$ de $y'+ay=g$. Alors, $y_1+y_2$ est solution de $y'+ay=f+g$.	
\end{methode}


	\subsection{Méthode de variation de la constante}

\begin{methode}[Variation de la constante] 
Soit $(E)$ l'équation différentielle $y'=a(x)y+b(x)$. Les solutions de l'équation $y'=a(x)y$ sont $\ds{x\mapsto k\eu{A(x)}}$ où $A$ est une primitive de $a$.\\
Pour déterminer une solution particulière de $(E)$, on cherche une solution sous la forme $\ds{x\mapsto k(x) \eu{A(x)}}$, où $k$ est cette fois-ci une fonction dérivable sur l'intervalle, et non une constante.	
\end{methode}


\exemple{Soit $(E)$ l'équation différentielle $y'=2xy+x$. Les solutions sur $\R$ de l'équation homogène sont $x\mapsto k\eu{x^2}$. Cherchons une solution particulière sous la forme $f:x\mapsto k(x)\eu{x^2}$, où $k$ est une fonction dérivable sur $\R$. $f$ vérifie donc, pour tout réel $x$, 
\begin{eqnarray*}
	f'(x)=2xf(x)+x &\Leftrightarrow& k'(x)\eu{x^2}+k(x)(2x\eu{x^2}) = 2xk(x)\eu{x^2} + x\\
			&\Leftrightarrow& k'(x)=x\eu{-x^2}
\end{eqnarray*}
Ainsi, en prenant $k:x\mapsto -\frac{1}{2}\eu{-x^2}$, on obtient ainsi une solution particulière de $(E)$ :
\[ x\mapsto -\frac{1}{2}\eu{-x^2} \eu{x^2} = -\frac{1}{2} \]
et les solutions de $(E)$ sont alors
\[ \mathcal{S}=\left \{ x\mapsto k\eu{x^2} -\frac{1}{2},\quad k\in \R \right \} \]
}

\remarque{Dans le cas précédent, on aurait pu chercher une solution particulière sous la forme $x\mapsto ax+b$, ce qui était plus efficace.}

\exercice{Résoudre dans $\R$ l'équation \[(\E^x+1)y'=\E^xy+2x(\E^x+1)^2 \]
}

\begin{prof}
\solution{Pour tout réel $x$, $\E^x+1>0$, l'équation est donc équivalente à l'équation \[ (E)\quad y' = \frac{\E^x}{\E^x+1}y+2x(\E^x+1) \]
\begin{itemize}[label=\textbullet]
	\item La fonction $\ds{x\mapsto \frac{\E^x}{\E^x+1}}$ admet comme primitive sur $\R$ $\ds{x\mapsto \ln(\E^x+1)}$. Ainsi, l'équation homogène $\ds{y'=\frac{\E^x}{\E^x+1}y}$ admet comme solutions
		\[ \mathcal{S}_0 = \left \{ x\mapsto  k\eu{\ln(\E^x+1)} = k(\E^x+1),\quad k\in \R \right \} \]
	\item Cherchons une solution particulière par variation de la constante. On cherche une solution $f$ de la forme $f:x\mapsto k(x)(\E^x+1)$, où $k$ est une fonction dérivable sur $\R$. On a alors $f':x\mapsto k'(x)(\E^x+1)+k(x)\E^x$ et l'équation différentielle $(E)$ s'écrit
		\begin{eqnarray*}
			f'(x)=\frac{\E^x}{\E^x+1}f(x)+2x(\E^x+1) &\Leftrightarrow&
				k'(x)(\E^x+1)+k(x)\E^x=k(x)(\E^x+1)\frac{\E^x}{\E^x+1} + 2x(\E^x+1) \\
				&\Leftrightarrow& k'(x)(\E^x+1)=2x(\E^x+1) \\
				&\Leftrightarrow& k'(x)=2x
		\end{eqnarray*}
		En prenant comme primitive $k:x\mapsto x^2$, on obtient donc une solution particulière de $(E$) : $f:x\mapsto k(x)(\E^x+1)=x^2(\E^x+1)$.
\end{itemize}
\textbf{Bilan} : l'ensemble des solutions de $(E)$ est donc
\[ \mathcal{S}=\left \{ x\mapsto x^2(\E^x+1)+k(\E^x+1), \quad k\in \R \right \} \]
}	
\end{prof}


\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{15}{\textwidth}
\end{solu_eleve}
\end{eleve}

	\subsection{En physique et S.I.}

En électronique, il y a deux cas classiques d'équation différentielle du premier ordre : le circuit RL et le circuit RC.

\begin{proposition}
Soit le circuit RL suivant :
\begin{center}
	\includegraphics[width=6cm]{tex/Chap5/pic/RL}
\end{center}
En régime transitoire, la tension $U_L$ vérifie $U_L=L\frac{\dd i}{\dd t}$ et la tension $U_R$ vérifie $U_R=R_ti$. D'après la loi des mailles, $U=U_R+U_L$, et donc $i$ vérifie l'équation différentielle sur $\R^+$
\[ U = L\frac{\dd i}{\dd t}+R_ti = L i' + R_ti \]
Ainsi, si $U=U_0$ est constante, et en supposant comme condition initiale $i(0)=0$, on obtient 
\[ \forall~t\geq 0,\quad  i(t)=\frac{U_0}{R_t}\left(1-\eu{-\frac{R_tt}{L}}\right) \]
On pose alors $\tau=\frac{L}{R_t}$ la constante de temps du circuit.	
\end{proposition}


\begin{proposition}
Soit le circuit RC suivant :
\begin{center}
	\includegraphics[width=6cm]{tex/Chap5/pic/RC}
\end{center}
En régime transitoire, la tension $U_R$ vérifie $U_R=Ri$, et la tension $U_C$ vérifie 
$i = C\frac{\dd U_C}{\dd t}$. D'après la loi des mailles, $E=U_R+U_C$, et donc $U_C$ vérifie l'équation différentielle
\[ E = RC \frac{\dd U_C}{\dd t} + U_C \]
Ainsi, si $E$ est constante, et en supposant comme condition initiale $U_C(0)=0$ (condensateur déchargé), on obtient 
\[ \forall~t\geq 0,\quad  U_C(t)=E\left(1-\eu{-\frac{t}{RC}}\right) \]
On pose alors $\tau=RC$ la constante de temps du circuit.	
\end{proposition}




\section{Equations différentielles du second ordre à coefficients constants}

	\subsection{Généralités}
	
\definition{Une équation différentielle du second ordre à coefficients constants est une équation différentielle de la forme \[ ay''+by'+cy=d(x) \]
où $a$, $b$ et $c$ sont des réels ou des complexes, et $d$ une fonction définie et continue sur un intervalle $I$ de $\R$.}

\exemple{L'équation $y''+3y'-y=\E^x$ est une équation différentielle linéaire du second ordre à coefficient constant.\\L'équation $y''+xy'+y=4x$ est une équation différentielle linéaire du second ordre, mais pas à coefficients constants.}

\definition{Soit $(E)~ay''+by'+cy=d(x)$ une équation différentielle linéaire du second ordre à coefficients constants. On appelle \textbf{équation homogène} associée à $(E)$ l'équation différentielle $ay''+by'+cy=0$. Il s'agit également d'une équation différentielle linéaire du second ordre à coefficients constants.}

\exemple{L'équation homogène associée à l'équation $y''+3y'-y=\E^x$ est $y''+3y'-y=0$.}

\begin{theoreme}
Soit $(E)$ une équation différentielle $ay''+by'+cy=d(x)$ ($a\neq 0$),  avec $y(x_0)=y_0$ et $y'(x_0)=y'_0$. Alors il existe une unique solution à ce problème de Cauchy.	
\end{theoreme}


\remarque{Ainsi, il s'agira de trouver la seule solution si on dispose d'un problème de Cauchy.}


	\subsection{Résolution de l'équation homogène}

\definition{Soit $(E):~a y''+by'+cy=0$ une équation du second ordre à coefficients constants sans second membre. On appelle \textbf{équation caractéristique} de l'équation $(E)$ l'équation du second degré à coefficients complexes $az^2+bz+c=0$.}

Dans le cas général complexe, les solutions sont faciles à obtenir :

\begin{theoreme}
Soit $(E):~ay''+by'+cy=0$ une équation du second ordre à coefficients constants sans second membre, et $az^2+bz+c=0$ son équation caractéristique. Soit $\Delta=b^2-4ac$ son discriminant.
\begin{itemize}[label=\textbullet]
	\item Si $\Delta\neq 0$, on note $z_1$ et $z_2$ les deux racines complexes de l'équation caractéristique. Alors les solutions de $(E)$ sont les fonctions de la forme 
		\[ x\mapsto A\eu{z_1x} + B\eu{z_2x} \]
		où $A$ et $B$ sont des complexes.
	\item Si $\Delta=0$, on note $z_0$ la racine double de l'équation caractéristique. Alors les solutions de $(E)$ sont les fonctions de la forme
		\[ x\mapsto (Ax+B)\eu{z_0x} \]
		où $A$ et $B$ sont des complexes. 
\end{itemize}	
\end{theoreme}


Dans le cas où $a, b$ et $c$ sont réels, on souhaite obtenir les solutions à valeurs réelles. On obtient alors le théorème suivant :

\begin{theoreme}
Soit $(E)$ l'équation $ay''+by'+cy=0$, avec $(a,b,c)\in \R^3$ et $a\neq 0$. On note $\Delta=b^2-4ac$ le discriminant de l'équation caractéristique.
\begin{itemize}[label=\textbullet]
	\item Si $\Delta>0$, les solutions de $(E)$ sont de la forme $\ds{x\mapsto A\eu{\alpha x}+B\eu{\beta x}}$, où $\alpha$ et $\beta$ sont les racines de l'équation caractéristique et $(A,B)\in \R^2$.
	\item Si $\Delta=0$, les solutions de $(E)$ sont de la forme $\ds{x\mapsto (Ax+B)\eu{\alpha x}}$, où $\alpha$ est la racine double de l'équation caractéristique et $(A,B)\in \R^2$.
	\item Si $\Delta<0$, on note $z_1=k+i\omega$ et $z_2=k-i\omega$ les racines complexes de l'équation caractéristique. Alors, les solutions de $(E)$ sont de la forme $\ds{x\mapsto (A\cos(\omega x)+B\sin(\omega x))\eu{kx} }$.
\end{itemize}	
\end{theoreme}


\exemple{Résoudre dans $\R$ l'équation $y''-2y'-3y=0$.}

\begin{prof}
\solution{L'équation caractéristique de l'équation est $x^2-2x-3=0$. Son discriminant vaut $\Delta=(-2)^2-4\times 1 \times (-3)=16 >0$. Ains, l'équation caractéristique possède deux racines réelles :
\[ x_1=\frac{-(-2)-\sqrt{16}}{2}=-1 \qeq x_1=\frac{-(-2)+\sqrt{16}}{2}=3 \]
Ainsi, l'ensemble des solutions de l'équation $y''-2y'-3y=0$ s'écrit
\[ \mathcal{S}=\left \{x\mapsto A\eu{-x}+B\eu{3x},\quad\quad (A,B)\in \R^2 \right \}\]
}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{10}{\textwidth}
\end{solu_eleve}
\end{eleve}

\exercice{[Fondamental] Résoudre dans $\R$ l'équation $y''+\omega^2y=0$.}

\begin{prof}
\solution{Son équation caractéristique est $x^2+\omega^2x=0$, de discriminant $\Delta=-4\omega^2 < 0$. Elle possède donc deux racines complexes conjuguées
\[ z_1=\frac{-2i\omega}{2}=-\omega i \qeq z_2=\frac{2i\omega}{2}=i\omega \]
L'ensemble des solutions de l'équation $y''+\omega^2y=0$ est donc
\[ \mathcal{S}=\left \{ x\mapsto  (A\cos(\omega x)+B\sin(\omega x))\eu{0}=A\cos(\omega x)+B\sin(\omega x),\quad \quad (A,B)\in \R^2 \right \} \]

}	
\end{prof}


\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{10}{\textwidth}
\end{solu_eleve}
\end{eleve}


\exercice{Résoudre dans $\R$ l'équation $y''+2y'+y=0$.}

\begin{prof}
\solution{Son équation caractéristique est $x^2+2x+1=0$, de racine double $x=-1$. Ainsi, l'ensemble des solutions de l'équation $y''+2y'+y=0$ est donc
\[ \mathcal{S}=\left \{ x\mapsto (Ax+B)\eu{-x},\quad \quad (A,B)\in \R^2 \right \} \]

}	
\end{prof}


\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{10}{\textwidth}
\end{solu_eleve}
\end{eleve}

	\subsection{Cas général}
	
Pour résoudre une équation différentielle linéaire du second ordre à coefficients constants, on se ramène à une équation homogène.

\begin{proposition}
Soit $(E)~ay''+by'+cy=d(x)$ une équation différentielle linéaire, avec $(a,b,c)\in \R^3$ et $d$ une fonction continue sur $I$. \\Soit $(E_0)$ l'équation différentielle homogène associée : $(E_0)~ay''+by'+cy=0$.\\
	Soit $g_0$ une solution particulière de $(E)$ : pour tout $x\in I$, $ag''(x)+bg'(x)+cg(x)=d(x)$. \\
	Alors l'ensemble des solutions de $(E)$ sur $I$ s'écrit $g+g_0$, avec $g$ une solution de l'équation homogène associée :
	\[ \mathcal{S} = \left \{ x\mapsto g_0(x)+g(x),\quad g \text{ solution de } (E_0) \right \} \]	
\end{proposition}



\begin{methode}
Pour déterminer une solution particulière dans le cas où le second membre est de la forme $P(x)\eu{\gamma x}$, avec $P$ un polynôme de degré $n$, on cherche une solution de la forme 
\begin{itemize}[label=\textbullet]
	\item $x\mapsto Q(x)\eu{\gamma x}$ si $\gamma$ n'est pas solution de l'équation caractéristique;
	\item $x\mapsto xQ(x)\eu{\gamma x}$ si $\gamma$ est racine simple de l'équation caractéristique;
	\item $x\mapsto x^2Q(x)\eu{\gamma x}$ si $\gamma$ est racine double de l'équation caractéristique;
\end{itemize}
 où $Q$ est un polynôme de degré $n$.\\
 On peut utiliser cette méthode si le second membre est un polynôme ($\gamma=0)$ ou une exponentielle ($P$ de degré $0$).	
\end{methode}


\remarque{De manière générale, on cherche une solution particulière de la même forme que le second membre, éventuellement en augmentant le degré d'un polynôme.}

\exemple{Déterminer les solutions de l'équation $y''+y=x+1$.}

\begin{prof}
\solution{L'équation homogène s'écrit $y''+y=0$, qui a comme solution 
\[ \mathcal{S}_0=\left \{ x\mapsto A\cos(x)+B\sin(x),\quad (A,B)\in \R^2 \right \} \]
(cas vu précédemment, où $\omega=1$). Puisque $x+1=(x+1)\eu{0x}$, et que $0$ n'est pas racine de l'équation caractéristique, cherchons une solution particulière de la même forme, c'est-à-dire cherchons $c$ et $d$ deux réels tels que $f:x\mapsto cx+d$ soit solution de l'équation. En injectant dans l'équation (et puisque $f''=0$), on obtient
\[ cx+d=x+1 \Leftrightarrow c=1 \qeq d=1 \]
Ainsi, la fonction $x\mapsto x+1$ est solution particulière.\\
\textbf{Bilan} : l'ensemble des solutions de l'équation $y'+y=x+1$ est donc
\[ \mathcal{S}=\left \{ x\mapsto x+1+A\cos(x)+B\sin(x),\quad (A,B)\in \R^2 \right \} \]
}	
\end{prof}


\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{13}{\textwidth}
\end{solu_eleve}
\end{eleve}

\exercice{Déterminer les solutions de l'équation $y''-4y'+4y=\eu{2x}$.}

\begin{prof}
\solution{Notons $(E):\quad y''-4y'+4y=\eu{2x}$ et $(E_0)$ son équation homogène $y''-4y'+4y=0$.
\begin{itemize}[label=\textbullet]
	\item L'équation caractéristique de l'équation homogène est $r^2-4r+4=(r-2)^2$ qui admet donc $2$ comme racine double. Ainsi, l'ensemble des solutions de $(E_0)$ est
		\[ \mathcal{S}_0=\left \{ x\mapsto (Ax+B)\eu{2x},\quad (A,B)\in \R^2 \right \} \]
	\item Puisque $2$ est racine double de $(E_0)$, on ne peut pas chercher une solution particulière de la forme $x\mapsto c\eu{2x}$, ni même $x\mapsto (cx+d)\eu{2x}$ puisque celles-ci sont solutions de l'équation homogène. On cherche alors une solution de la forme $f:x\mapsto (ax^2+bx+c)\eu{2x}$. On obtient
		\[ f':x\mapsto (2ax^2 + (2b+2a)x + 2c +b)\eu{2x} \qeq 
		f'':x\mapsto ( 4ax^2 +(8a+4b)x+2a+4c+4b)\eu{2x}\]
		En injectant dans l'équation $(E)$, on a alors
		\[ ( 4ax^2 +(8a+4b)x+2a+4c+4b)\eu{2x}-4 (2ax^2 + (2b+2a)x + 2c +b)\eu{2x}+4(ax^2+bx+c)\eu{2x} = \eu{2x} \]
		Soit, après simplification
		 $2a = 1$ et donc $a=\frac{1}{2}$. $b$ et $c$ sont quelconques, et c'est normal puisque $x\mapsto (cx+d)\eu{2x}$ est une solution de l'équation homogène. Ainsi, la fonction $x\mapsto \frac{1}{2}x^2\eu{2x}$ est solution particulière de $(E)$.
\end{itemize}
\textbf{Bilan} : l'équation $(E)$ admet comme solutions sur $\R$ :
\[ \mathcal{S}=\left \{ x\mapsto \frac{1}{2}x^2\eu{2x}+(Ax+B)\eu{2x},\quad (A,B)\in \R^2 \right \} \]
}	
\end{prof}


\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{13}{\textwidth}
\end{solu_eleve}
\end{eleve}

\remarque{Le principe de superposition s'applique aussi dans le cas d'une équation différentielle linéaire du second ordre à coefficients constant : si on cherche une solution particulière de l'équation $ay''+by'+cy=d_1+d_2$, il suffit de trouver une solution particulière $y_1$ de $ay''+by'+cy=d_1$ et une solution particulière $y_2$ de $ay''+by'+cy=d_2$; une solution de l'équation de départ est alors $y_1+y_2$. 
}

\begin{methode}
Si le second membre est lié aux fonctions trigonométriques, on écrit $\cos(x)=\Re(\eu{ix})$ et $\sin(x)=\Im(\eu{ix})$, et on se ramène à une équation dont le second membre est une exponentielle.	
\end{methode}


\exemple{Résoudre dans $\R$ l'équation différentielle $y'+y=\cos(x)$.} 

\begin{prof}
\solution{L'équation homogène associée est $y'+y=0$, de solution
\[ \mathcal{S}_0=\left \{ x\mapsto k\eu{-x},\quad k\in \R \right \} \]
Cherchons une solution particulière de l'équation $y'+y=\eu{ix}$, de la forme $f:x\mapsto a\eu{ix}$ (puisque $x\mapsto \eu{ix}$ n'est pas solution de l'équation homogène). En remplaçant dans l'équation, on obtient
\begin{eqnarray*}
	f'+f=\eu{ix} &\Leftrightarrow& ai\eu{ix}+a\eu{ix}=\eu{ix} \\&\Leftrightarrow& a(1+i)=1\\ 
	     &\Leftrightarrow& a=\frac{1}{1+i}=\frac{1}{2}-i\frac{1}{2}
\end{eqnarray*}
Ainsi, une solution particulière de $y'+y=\eu{ix}$ est 
\begin{eqnarray*}
	x\mapsto \left(\frac{1}{2}-\frac{1}{2}i\right)\eu{ix}&=&\left(\frac{1}{2}-\frac{1}{2}i\right)(\cos(x)+i\sin(x))\\
	&=& \frac{1}{2}\cos(x)+\frac{1}{2}\sin(x)+i\left( \frac{1}{2}\sin(x)-\frac{1}{2}\cos(x)\right)
\end{eqnarray*} 
La partie réelle de la solution précédente (puisque $\cos(x)=\Re(\eu{ix})$) donne alors une solution particulière de $y'+y=\cos(x)$.
\\\textbf{Bilan} : l'ensemble des solutions de l'équation $y'+y=\cos(x)$ est donc
\[ \mathcal{S}=\left \{ x\mapsto \frac{1}{2}\cos(x)+\frac{1}{2}\sin(x)+k\eu{-x},\quad k\in \R \right \} \]
}	
\end{prof}



\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{13}{\textwidth}
\end{solu_eleve}
\end{eleve}

\remarque{Le second membre étant trigonométrique, on aurait pu chercher une solution particulière de la forme $x\mapsto A\cos(x)+B\sin(x)$, en identifiant les parties "cosinus" et les parties "sinus".}


	\subsection{En physique et S.I.}
	
Les cas des circuits LC et RLC mènent à la résolution d'équations différentielles linéaires du second ordre.

\begin{proposition}
Soit le circuit LC suivant :
\begin{center}
	\includegraphics[width=6cm]{tex/Chap5/pic/LC1}
	\includegraphics[width=6cm]{tex/Chap5/pic/LC2}
\end{center}
Après avoir chargé le condensateur (première image), on bascule l'interrupteur pour déclencher la décharge de celui-ci à travers la bobine.\\
En régime transitoire, la tension $U_L$ vérifie $U_L=L\frac{\dd i}{\dd t}$ et la tension $U_C$ vérifie $U_C=\frac{q}{c}$ avec $i=\frac{\dd q}{\dd t}$. D'après la loi des mailles, $0=U_C+U_L$, et donc, en dérivant cette équation par rapport au temps, $i$ vérifie l'équation différentielle sur $\R^+$
\[  L\frac{\dd^2 i}{\dd t^2}+\frac{1}{C}i = 0 \]
qui s'écrit encore
\[ \frac{\dd^2i}{\dd t^2} + \frac{1}{LC}i = 0 \]
En notant $\omega=\frac{1}{\sqrt{LC}}$, l'équation s'écrit $\ds{\frac{\dd^2i}{\dd t^2}+\omega^2 i = 0 }$, qui a comme solutions 
\[ \forall~t\geq 0,\quad i(t)=A\cos(\omega t)+B\sin(\omega t) = C \cos(\omega t + \varphi) \]
On détermine alors les constantes grâce aux conditions initiales.\\$T=\frac{2\pi}{\omega}$ représente la période.
\end{proposition}


\begin{proposition}
Soit le circuit RLC suivant :
\begin{center}
	\includegraphics[width=6cm]{tex/Chap5/pic/RLC1}
	\includegraphics[width=6cm]{tex/Chap5/pic/RLC2}
\end{center}
Après avoir chargé le condensateur (première image), on bascule l'interrupteur pour déclencher la décharge de celui-ci à travers la bobine et la résistance.\\
En régime transitoire, la tension $U_L$ vérifie $U_L=L\frac{\dd i}{\dd t}$, la tension $U_C$ vérifie $U_C=\frac{q}{c}$ avec $i=\frac{\dd q}{\dd t}$ et $U_R=Ri$. D'après la loi des mailles, $0=U_R+U_C+U_L$, et donc, en dérivant cette équation par rapport au temps, $i$ vérifie l'équation différentielle sur $\R^+$
\[  L\frac{\dd^2 i}{\dd t^2}+R\frac{\dd i}{\dd t} + \frac{1}{C}i = 0 \]
qui s'écrit encore
\[ \frac{\dd^2i}{\dd t^2} +\frac{R}{L}\frac{\dd i}{\dd t} + \frac{1}{LC}i = 0 \]
En notant $\omega_0=\frac{1}{\sqrt{LC}}$ la pulsation propre du circuit et $\frac{\omega_0}{Q}=\frac{R}{L}$, où $Q=\frac{1}{R}\sqrt{\frac{L}{C}}$ est le facteur de qualité, l'équation s'écrit $\ds{\frac{\dd^2i}{\dd t^2}+\frac{\omega_0}{Q}\frac{\dd i}{\dd t} + \omega_0^2 i = 0 }$. \\On dispose de trois cas, selon le signe du discriminant $\Delta=\frac{\omega_0^2}{Q^2}-4\omega_0^2=\omega_0^2\left(\frac{1}{Q^2}-4\right)$ :
\begin{itemize}[label=\textbullet]
	\item Si $\Delta>0$, c'est-à-dire si $Q<\frac{1}{2}$, on obtient des solutions de la forme 
		\[ \forall~t\geq 0,\quad i(t)=A\eu{k_1t}+B\eu{k_2t} \]
		avec $k_1,k_2$ solutions de l'équation caractéristique. Le régime est dit \textbf{apériodique}. $k_1$ et $k_2$ sont strictement négatives, et sont généralement notés $-\frac{1}{\tau_+}$ et $-\frac{1}{\tau_-}$.
	\item Si $\Delta=0$, c'est-à-dire si $Q=\frac{1}{2}$, on obtient des solutions de la forme \[ \forall~t\geq 0,\quad i(t)=(At+b)\eu{k_0t} \]
		 où $k_0=-\omega_0=-\frac{1}{\tau}$ est la racine double de l'équation caractéristique. Ce régime est appelé \textbf{régime critique}.
	\item Enfin si $\Delta<0$, c'est-à-dire si $Q>\frac{1}{2}$, on obtient des solutions de la forme 
		\[ \forall~t\geq 0,\quad i(t)=(A\cos(\omega t)+B\sin(\omega t))\eu{-\frac{t}{\tau}} \]
		avec $\ds{\omega=\omega_0\sqrt{1-\frac{1}{4Q^2}}}$, appelée pseudo-pulsation.
		Le régime est dit \textbf{pseudo-périodique}.
\end{itemize}
\begin{center}
	\includegraphics[width=10cm]{tex/Chap5/pic/RLC}
\end{center}	
\end{proposition}

