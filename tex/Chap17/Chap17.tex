\objectif{Dans ce chapitre, on revient sur la notion de développement limité, vue dans le chapitre Dérivabilité, que l'on étend au cas général des développements limités d'ordre $n$. On utilise ces développements limités pour déterminer des limites, asymptotes et positions relatives.}

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
	\item Connaître la définition de négligeable et équivalence :
			\begin{itemize}[label=\textbullet]
				\item connaître la définition\dotfill $\Box$
				\item connaître les différentes propriétés usuelles\dotfill $\Box$
				\item connaître les négligeabilités et équivalences usuelles\dotfill $\Box$
			\end{itemize}
	\item Concernant les développements limités :
			\begin{itemize}[label=\textbullet]
				\item connaître la définition d'un développement limité\dotfill $\Box$
				\item savoir obtenir des développements limités (somme, produit, composée, troncature, quotient, intégration)\dotfill $\Box$
				\item connaître les développements limités usuels\dotfill $\Box$
			\end{itemize}
	\item Savoir utiliser la formule de Taylor-Young pour déterminer des développements limités\dotfill $\Box$
	\item Savoir utiliser les développements limités : :
			\begin{itemize}[label=\textbullet]
				\item pour déterminer des limites et des équivalents\dotfill $\Box$
				\item pour déterminer des positions relatives\dotfill $\Box$
				\item pour déterminer des asymptotes et position relatives\dotfill $\Box$
			\end{itemize}
\end{numerote}

\newpage 

Dans l'ensemble de chapitre, $\K$ désignera $\R$ ou $\C$.

%%%%%%%%%%%%%%%%%%%%%
\section{Comparaison locale}
%%%%%%%%%%%%%%%%%%%%%

Dans toute cette partie, $x_0$ désignera ou bien un nombre réel, ou bien $+\infty$, ou bien $-\infty$.

\definition{
On appelle \textbf{voisinage} de $x_0$ :
\begin{itemize}[label=\textbullet]
	\item un intervalle de la forme $]x_0-\eps, x_0+\eps[$, $]x_0-\eps, x_0[$ ou $]x_0,x_0+\eps[$ si $x_0$ est un réel.
	\item $]A;+\infty[$, avec $A$ réel, si $x_0=+\infty$.
	\item $]-\infty; A[$, avec $A$ réel, si $x_0=-\infty$.
\end{itemize}	
}

	%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Négligeable}
	%%%%%%%%%%%%%%%%%%%%%%%%
	
\definition{
Soient $f$ et $g$ définies au voisinage de $x_0$, avec $g$ ne s'annulant pas au voisinage de $x_0$. Alors, on dit que $f$ est \textbf{négligeable} devant $g$ au voisinage de $x_0$, et on note $f=o_{x_0}(g)$, si
\[ \lim_{x\rightarrow x_0} \frac{f(x)}{g(x)} = 0 \]
$f=o_{x_0}(g)$ peut se noter également $f(x)=o_{x_0}(g(x))$ et se lit ``$f$ est un petit $o$ de $g$ au voisinage de $x_0$''.
}
	
\exemple{Par exemple, $x=o_{+\infty}(x^2)$ puisque \[ \lim_{x\rightarrow +\infty} \frac{x}{x^2}=\lim_{x\rightarrow +\infty} \frac{1}{x}=0 \]}

\begin{proposition}$f=o_{x_0}(1)$ si et seulement si $\ds{\lim_{x\rightarrow x_0} f(x)=0}$.	
\end{proposition}

\begin{prof}
\preuve{En effet, la fonction constante égale à $1$ ne s'annule pas au voisinage de $x_0$ et on a $f=o_{x_0}(1)$ par définition si et seulement si
\[ \lim_{x\rightarrow x_0} \frac{f(x)}{1}=0 \Longleftrightarrow \lim_{x\rightarrow x_0} f(x)=0 \] 
}
\end{prof}

\lignes{3}


\begin{theoreme}[Négligeabilités usuelles] On a 
\begin{itemize}[label=\textbullet]
	\item Si $0\leq \alpha < \beta$, on a
	\[ x^\alpha = o_{+\infty}\left( x^\beta\right) \qeq x^\beta = o_0\left(x^\alpha\right) \]
	\item Si $\alpha \geq 0$ et $\beta >0$, on a \[ x^\alpha=o_{+\infty}\left(\eu{\beta x}\right) \]
	et en particulier $x^{\alpha}=o_{+\infty}(\E^x)$.
	\item Si $\alpha \geq 0$ et $\beta >0$, on a \[ \left(\ln(x)\right)^\alpha = o_{+\infty}\left(x^\beta\right) \]
	\item Si $\alpha \in \N$ et $\beta >0$, on a \[  \left(\ln(x)\right)^\alpha=o_0\left(\frac{1}{x^\beta}\right) \]
\end{itemize} 
\end{theoreme}

\preuve{Cela découle des croissances comparées :
\begin{eqnarray*}
	\lim_{x\rightarrow +\infty} \frac{x^\alpha}{\eu{\beta x}}=0 &\Longleftrightarrow& x^\alpha = o_{+\infty}\left(\eu{\beta x}\right) \\
	\lim_{x\rightarrow +\infty} \frac{(\ln x)^\alpha}{x^\beta}=0 &\Longleftrightarrow& (\ln x)^\alpha = o_{+\infty}\left(x^\beta\right) \\
	\lim_{x\rightarrow 0} x^\beta (\ln x)^\alpha = 0 &\Longleftrightarrow& (\ln x)^\alpha = o_0 \left(x^\beta\right)
\end{eqnarray*}
De plus, $\ds{\frac{x^\alpha}{x^\beta} = x^{\alpha-\beta}}$ et le premier résultat découle des limites des fonctions puissances.
}	

\propriete{Soient $f, g$ et $h$ des fonctions définies au voisinage de $x_0$ :
\begin{itemize}[label=\textbullet]
	\item Transitivité : si $f=o_{x_0}(g)$ et $g=o_{x_0}(h)$, alors $f=o_{x_0}(h)$.
	\item Linéarité : si $f=o_{x_0}(h)$ et $g=o_{x_0}(h)$, alors pour tous réels $a$ et $b$, on a $af+bg=o_{x_0}(h)$
\end{itemize}
}

\preuve{Si $f=o_{x_0}(g)$ et $g=o_{x_0}(h)$ alors $g$ et $h$ ne s'annulent pas au voisinage de $x_0$, et on peut écrire 
\[ \frac{f}{h} = \frac{f}{g}\times \frac{g}{h} \]
et ainsi $\ds{\lim_{x\rightarrow x_0} \frac{f(x)}{h(x)} = 0}$ par produit des limites.\\
De même, si $f=o_{x_0}(h)$ et $g=o_{x_0}(h)$ alors $h$ ne s'annule pas au voisinage de $x_0$ et on a
\[ \frac{af+bg}{h} = a\frac{f}{h}+b\frac{g}{h} \]
Par somme et produit des limites, on en déduit donc que $\ds{\lim_{x\rightarrow x_0} \frac{(af+bg)(x)}{h(x)} = 0}$.
}	

\remarque{On peut dans certains cas diviser. Le plus classique : si $f(x) = o_0(x^n)$, alors pour tout $p\leq n$, $\frac{f(x)}{x^p} = o_0(x^{n-p})$. En effet
\[ \frac{\frac{f(x)}{x^p}}{x^{n-p}} = \frac{f(x)}{x^n} \underset{x\rightarrow 0}{\longrightarrow} 0 \]}


	\subsection{Equivalence}
	
\definition{
Soient $f$ et $g$ deux fonctions définies au voisinage de $x_0$. On dit que $f$ est \textbf{équivalente} à $g$ en $x_0$ si, au voisinage de $x_0$, on a
\[ f-g = o_{x_0}(g) \]
ce qu'on note $f\sim_{x_0} g$. Ainsi, puisque $g$ ne s'annule pas au voisinage de $x_0$, on a
\[ f\sim_{x_0} g \quad\text{si et seulement si}\quad \lim_{x\rightarrow x_0} \frac{f(x)}{g(x)} = 1 \]	
}


\begin{theoreme}[Polynômes] 
Soient $\alpha$ et $\beta$ deux réels tels que $0\leq \alpha < \beta$. Alors
\[ \left(x^\alpha + x^\beta\right) \sim_{+\infty} x^\beta \qeq \left(x^\alpha + x^\beta \right) \sim_{0} x^\alpha. \]
De plus, si $\alpha$ et $\beta$ sont des entiers, alors
\[ \left(x^\alpha + x^\beta\right) \sim_{-\infty} x^\beta \]
Ainsi, on obtient que 
\begin{itemize}[label=\textbullet]
	\item un polynôme non nul est équivalent à son monôme de plus haut degré au voisinage de $+\infty$ ou $-\infty$;
	\item un polynôme non nul est équivalent à son monôme de plus bas degré au voisinage de $0$. 
\end{itemize}	
\end{theoreme}

\preuve{En effet, puisque $\alpha-\beta<0$ et $\beta-\alpha>0$ :
\[ \frac{x^\alpha+x^\beta}{x^\beta}= x^{\alpha-\beta} + 1 \underset{x\rightarrow +\infty}{\longrightarrow} 0+1 =1 \]
et
\[ \frac{x^\alpha+x^\beta}{x^\alpha}=1+x^{\beta-\alpha} \underset{x\rightarrow 0}{\longrightarrow} 1+0=1 \]
}


\begin{theoreme}[Equivalences usuelles]
Soit $u$ une fonction définie au voisinage de $x_0$ telle que\\ $\ds{\lim_{x\rightarrow x_0} u(x)=0}$.
\begin{itemize}[label=\textbullet]
	\item $\ds{\ln(1+u(x)) \sim_{x_0} u(x)}$, et en particulier, $\ds{\ln(1+x)\sim_0 x}$.
	\item $\ds{\left(\eu{u(x)}-1\right) \sim_{x_0} u(x)}$, en particulier $\ds{\left(\E^x-1\right) \sim_0 x}$.
	\item $\ds{\left((1+u(x))^\alpha-1\right) \sim_{x_0} \alpha u(x)}$ si $\alpha\neq 0$. En particulier $\ds{\left((1+x)^\alpha-1\right) \sim_0 \alpha x}$.
\end{itemize}	
\end{theoreme}


\preuve{Théorème admis - repose sur la limite d'un taux d'accroissement.}

\propriete{Soient $f, g$ et $h$ trois fonctions définies au voisinage de $x_0$, avec $h$ ne s'annulant pas au voisinage de $x_0$.
\begin{itemize}[label=\textbullet]
	\item Si $f\sim_{x_0} g$ et $g\sim_{x_0} h$ alors $f\sim_{x_0} h$.
	\item Si $f\sim_{x_0} g$, alors $f\times h \sim_{x_0} g\times h$.
	\item Si $f\sim_{x_0} g$ alors pour tout entier $n$, $f^n \sim_{x_0} g^n$.
	\item Si $f\sim_{x_0} g$ et si $f$ et $g$ ne s'annulent pas au voisinage de $x_0$, alors $\ds{\frac{1}{f}\sim_{x_0} \frac{1}{g}}$.
\end{itemize}
}

\remarque{Ainsi, on peut multiplier et diviser des équivalents.\\
\danger On ne peut ni ajouter, ni composer des équivalents (sauf par une fonction puissance d'exposant entier).}

\exemple{Soit $f:x\mapsto x^3+x^2$ et $g:x\mapsto x^2-x^3$.  Déterminer un équivalent de $f$, de $g$ et de $f+g$ au voisinage de $+\infty$.}

\solution[4]{Puisque $f$ et $g$ sont des polynômes, on a rapidement que $f(x)\sim_{+\infty} x^3$ et $g(x)\sim_{+\infty} -x^3$. Enfin, $(f+g)(x)=2x^2 \sim_{+\infty} 2x^2$. Ainsi, on ne peut pas additionner les équivalents.}	

\begin{proposition}[Equivalent et limite] 
Soient $f$ et $g$ deux fonctions définies au voisinage de $x_0$. On suppose que $f\sim_{x_0} g$ et que $\ds{\lim_{x\rightarrow x_0} g(x)=\ell}$. Alors $\ds{\lim_{x\rightarrow x_0} f(x)=\ell}$.
\end{proposition}

\begin{prof}
\preuve{Remarquons que $\ds{f=\frac{f}{g}\times g}$. Puisque $f\sim_{x_0} g$, on a $\ds{\lim_{x\rightarrow x_0} \frac{f(x)}{g(x)} = 1 }$ et par hypothèse $\ds{\lim_{x\rightarrow x_0} g(x)=l}$. Par produit des limites, $f$ admet une limite en $x_0$ qui vaut $l$.}	
\end{prof}

\lignes{4}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Développements limités}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Définition}
	%%%%%%%%%%%%%%%%%%%%%%%
	
\definition{
Soit $f$ une fonction définie au voisinage de $x_0$, et $n$ un entier naturel. On dit que $f$ admet un \textbf{développement limité} à l'ordre $n$ au voisinage de $x_0$ s'il existe des réels $a_0, a_1\cdots, a_n$ tels qu'au voisinage de $x_0$ on ait
\[ f(x) = a_0 + a_1(x-x_0)+a_2(x-x_0)^2+\cdots +a_n(x-x_0)^n + o_{x_0}\left ( (x-x_0)^n\right)\]
La partie $a_0 + a_1(x-x_0)+a_2(x-x_0)^2+\cdots +a_n(x-x_0)^n$ est appelée \textbf{partie régulière} du développement limité, et $o_{x_0}\left ( (x-x_0)^n\right)$ le \textbf{terme complémentaire}.\\
On abrégera régulièrement ``développement limité à l'ordre $n$ au voisinage de $x_0$'' en $\text{DL}_n(x_0)$.	
}


\remarque{L'idée d'un développement limité est de trouver une approximation locale d'une fonction $f$ par un polynôme. Le terme complémentaire est essentiel, puisqu'il permet d'indiquer l'ordre du développement limité.}

\exemple{On a le développement limité à l'ordre $1$ suivant : $\ln(1+x)=x+o_0(x)$. En effet, 
\[ \lim_{x\rightarrow 0} \frac{\ln(1+x)-x}{x}=\lim_{x\rightarrow 0} \frac{\ln(1+x)}{x}-1=0 \]
}

\exercice{Montrer qu'on a le développement limité suivant : 
$\ds{\E^x=1+x+o_0(x)}$.}

\begin{prof}
\solution{En effet
\[ \frac{\E^x-1-x}{x} = \frac{\E^x-1}{x}-1 \underset{x\rightarrow 0}{\longrightarrow} 1-1=0 \]}	
\end{prof}

\lignes{3}

\remarque{$f$ admet un développement limité en $x_0$ si et seulement si la fonction $h\mapsto f(x_0+h)$ admet un développement limité en $0$ (par rapport à la variable $h$). Dans la suite, on n'énoncera ainsi que les résultats pour les développements limités en $0$.}

\begin{proposition}
Soit $f$ une fonction admettant un développement limité à l'ordre $n$ au voisinage de $x_0$. Alors la partie régulière est unique.	
\end{proposition}


\preuve{Montrons-le dans le cas d'un voisinage de $0$. Ecrivons $f(x)=P(x)+o_0(x^n) =Q(x)+o_0(x^n)$ avec $P,Q$ deux polynômes de degrés inférieurs ou égaux à $n$. Mais alors
\[ P(x)-Q(x)=o_0(x^n) \quad\text{soit}\quad \lim_{x\rightarrow 0} \frac{P(x)-Q(x)}{x^n}=0 \]
Or, puisque $\deg(P-Q)\leq n$, ce résultat est impossible, sauf si $P-Q=0$. Ainsi, $P=Q$ et la partie régulière est unique.
}

\exercice{Déterminer un $\text{DL}_n(0)$ de $f:x\mapsto \frac{1}{1-x}$ (on pourra s'intéresser à $1+x+\cdots +x^n$).}

\begin{prof}
\solution{Remarquons que, pour $x\neq 1$ : \[1+x+\cdots + x^n = \frac{1-x^{n+1}}{1-x}= \frac{1}{1-x}-\frac{x^{n+1}}{1-x} \]
Ainsi, pour $x\neq 1$ :
\[ \frac{1}{1-x} = 1+x+\cdots + x^n + \frac{x^{n+1}}{1-x} \]
Or
\[ \lim_{x\rightarrow 0} \frac{ \frac{x^{n+1}}{1-x}}{x^n} = \lim_{x\rightarrow 0} \frac{x}{1-x} = 0 \]
Ainsi, $\ds{\frac{x^{n+1}}{1-x} = o_0(x^n)}$ et donc
\[ \frac{1}{1-x} = 1+x+\cdots + x^n + o_0(x^n) \]
}	
\end{prof}

\lignes{5}


	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Propriétés des DL}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
\begin{proposition}[Troncature] 
Soit $f$ une fonction admettant un développement limité à l'ordre $n$ au voisinage de $x_0$ de la forme 
\[ f(x) = a_0 + a_1(x-x_0)+a_2(x-x_0)^2+\cdots +a_n(x-x_0)^n + o_{x_0}\left ( (x-x_0)^n\right)\]
et soit $0\leq p<n$. Alors $f$ admet un développement limite à l'ordre $p$, donné par
\[ f(x) = a_0 + a_1(x-x_0)+a_2(x-x_0)^2+\cdots +a_p(x-x_0)^p + o_{x_0}\left ( (x-x_0)^p\right)\] 	
\end{proposition}

\exemple{Ainsi, si $f(x)=1+x+x^2+x^3+o_0(x^3)$ alors $f(x)=1+x+x^2+o(x^2)$.}

\begin{proposition}[Opérations sur les DL]
Soient $f$ et $g$ deux fonctions admettant un développement limité à l'ordre $n$ au voisinage de $0$. On écrit 
\[ f(x)=P(x)+o_0(x^n) \qeq g(x)=Q(x)+o_0(x^n) \]
avec $P,Q$ deux polynômes de $\R_n[X]$.
\begin{itemize}[label=\textbullet]
	\item Pour tout réel $\lambda$, $\lambda f+g$ admet un développement limité à l'ordre $n$ au voisinage de $0$, dont la partie régulière est $\lambda P+Q$.
	\item $f\times g$ admet un développement limité à l'ordre $n$ au voisinage de $0$, dont la partie régulière est la troncature du polynôme $P\times Q$ à l'ordre $n$.
	\item Si $f(0)=0$, $g\circ f$ admet un développement limité à l'ordre $n$ au voisinage de $0$, dont la partie régulière est la troncature du polynôme $Q\circ P$ à l'ordre $n$. 
\end{itemize}	
\end{proposition}


\preuve{Le premier point se fait aisément, en utilisant les règles de prépondérance :
\[ \lambda f(x)+g(x)=\lambda P(x)+o_0(x^n)+Q(x)+o_0(x^n) = (\lambda P+Q)(x) + o_0(x^n) \]
Pour le deuxième point, on écrit astucieusement :
\[ f\times g-P\times Q=f\times g-f\times Q + f\times Q-Q\times P = f\times (g-Q) + Q\times (f-P) \]
Mais alors
\[ f\times g-P\times Q = fo_0(x^n) + Qo_0(x^n) = o_0(x^n) \]
Le dernier point est admis.
}

\remarque{\danger~Pour $f\times g$, $P\times Q$ a un degré $2n$, mais n'apporte qu'une information jusqu'à l'ordre $n$, et donc tout monôme de degré strictement supérieur à $n$ sont à supprimer.\\
Ces résultats sont valables de manière générale en $x_0$, quitte à poser $f(x_0+h)$.}
  
\exercice{Déterminer un $\text{DL}_n(0)$ de $x\mapsto \frac{1}{1+x}$ et $x\mapsto \frac{1}{1+x^2}$. }

\begin{prof}
\solution{On part du développement limité 
\[ \frac{1}{1-x}= 1+x+\cdots + x^n + o_0(x^n) \]
et on compose $f:x\mapsto -x$ (qui vérifie bien $f(0)=0$) par $x\mapsto\frac{1}{1-x}$  :
\[ \frac{1}{1+x}=\frac{1}{1-(-x)} = 1-x+x^2+\cdots +(-1)^n x^n + o_0(x^n) \]
De même, on compose $x\mapsto x^2$ par le précédent et on tronque le résultat à l'ordre $n$ :
\[ \frac{1}{1+x^2} = 1-x^2+x^4+\cdots + (-1)^px^{2p} + \cdots + o_0(x^n) \]
}	
\end{prof}

\lignes{5}


 
\begin{proposition}[Parité]
Soit $f$ une fonction admettant un développement limité à l'ordre $n$ au voisinage de $0$.
\begin{itemize}[label=\textbullet]
	\item Si $f$ est paire, alors sa partie régulière ne contient que des termes de degrés pairs.
	\item Si $f$ est impaire, alors sa partie régulière ne contient que des termes de degrés impairs.
\end{itemize}	
\end{proposition}


\preuve{Ecrivons $f(x)=P(x)+I(x)+o(x^n)$, où $P$ contient les termes pairs, et $I$ les termes impairs du développement limité. Mais alors $f(-x)=P(x)-I(x)+o(x^n)$. Par unicité du développement limité, par exemple si $f$ est paire, on a nécessairement $I(x)=0$.}

\subsection{Quotient}
	
\begin{methode}
Pour déterminer le développement limité d'un quotient $\frac{f}{g}$, on écrira $\frac{f}{g}=f\times \frac{1}{g}$ et on se ramènera au développement limité de $x\mapsto \frac{1}{1+x}$ par composée.	
\end{methode}

\exercice{Déterminer un $\text{DL}_3(0)$ de $x\mapsto \frac{1}{-x^2+x+1}$.}


\solution[5]{On utilise le développement limité à l'ordre $3$ de $\frac{1}{1+x}$ et on compose $x\mapsto x-x^2$ par celui-ci, en tronquant à l'ordre $3$ :
$\ds{\frac{1}{1+x} = 1-x+x^2-x^3 =o_0(x^3)}$ donc 
\[ \frac{1}{1+x-x^2} = 1-(x-x^2)+(x-x^2)^2 -(x-x^2)^3 + o_0(x^3) \]
soit, après développement et troncature
\[ \frac{1}{1+x-x^2} = 1-x+x^2 + (x^2-2x^3) - x^3 + o_0(x^3) = 1-x+2x^2-3x^3 +o_0(x^3) \]}

	\subsection{Développement limité, continuité et dérivabilité}

\begin{proposition}
$f$ admet un développement limité à l'ordre $0$ au voisinage de $x_0$ de la forme $f(x)=l+o_{x_0}(1)$ si et seulement si $f$ est continue (ou prolongeable par continuité) en $x_0$ et $f(x_0)=l$.	
\end{proposition}


\preuve{En effet, $f(x)-l=o_{x_0}(1) \Longleftrightarrow \ds{\lim_{x\rightarrow x_0} \frac{f(x)-l}{1}=0}$.}

\rappel{
$f$ admet un développement limité à l'ordre $1$ au voisinage de $x_0$ de la forme $f(x)=a+b(x-x_0)+o_{x_0}(x-x_0)$ si et seulement si $f$ est dérivable en $x_0$ et $f(x_0)=a$ et $f'(x_0)=b$.	
}


\preuve{En effet, par troncature, $f$ est continue en $x_0$ et $f(x_0)=a$. Enfin
\[ f(x)=a+bx+o_{x_0}(x-x_0) \Longleftrightarrow  \frac{f(x)-a}{x-x_0}=b+o_{x_0}(1)\stackrel{x\rightarrow x_0}{\longrightarrow} b \]
}

\remarque{\danger~ Ce résultat, vrai si $f$ est continue ou dérivable, n'est plus vrai pour les dérivées d'ordre supérieur. Ainsi, une fonction peut avoir un développement limité d'ordre $2$ au voisinage de $0$ sans être deux fois dérivable en $0$.}

\exemple{Soit $f:x\mapsto x^3\sin\left(\frac{1}{x}\right)$. Montrer que $f(x)=o_0(x^2)$ mais que $f$ n'est pas deux fois dérivable en $0$.} 

\begin{prof}
\solution{Rapidement, pour $x\neq 0$, on a $\frac{f(x)}{x^2}=x\sin\left(\frac{1}{x}\right) \longrightarrow 0$ en $0$ (par encadrement classique). Donc $f$ est dérivable en $0$ et $f'(0)=0$. $f$ est de classe $\CC^1$ sur $]-\infty;0[$ et $]0;+\infty[$. Pour $x\neq 0$, on a
\[ f'(x)=3x^2\sin\left(\frac{1}{x}\right) + x^3 \left(-\frac{1}{x^2}\right) \cos\left(\frac{1}{x}\right) =  3x^2\sin\left(\frac{1}{x}\right) - x\cos\left(\frac{1}{x}\right) \] 
Calculons alors le taux d'accroissement de $f'$ en $0$ :
\[ \frac{f'(x)-f'(0)}{x-0}=\frac{f'(x)}{x}= 3x\sin\left(\frac{1}{x}\right) - \cos\left(\frac{1}{x}\right) \]
Le premier terme admet une limite en $0$ (qui vaut $0$). En revanche, le deuxième terme n'en admet pas (car $\cos$ n'admet pas de limite en $+\infty$ ou en $-\infty$. Ainsi, $f'$ ne peut être dérivable en $0$, et $f$ n'est donc pas deux fois dérivable en $0$.
}
\end{prof}

\lignes{12}

	\subsection{Intégration}
	
\begin{proposition}
Soit $f$ une fonction continue sur un intervalle contenant $0$. On suppose que $f$ admet un développement limité à l'ordre $n$ au voisinage de $0$ :
\[ f(x)=\underbrace{a_0+a_1x+a_2x^2+\cdots +a_nx^n}_{=P(x)} + o_0(x^n) \]
Alors une primitive de $f$ admet un développement limité à l'ordre $n+1$ au voisinage de $0$, dont la partie régulière est la primitive de $P$ nulle en $F(0)$ :
\[ F(x)=F(0)+a_0x+\frac{a_1}{2}x^2+\frac{a_2}{3}x^3+\cdots +\frac{a_n}{n+1}x^{n+1}+o_0\left(x^{n+1}\right) \]	
\end{proposition}


\preuve{Posons $Q(x)=F(0)+a_0x+\frac{a_1}{2}x^2+\frac{a_2}{3}x^3+\cdots +\frac{a_n}{n+1}x^{n+1}$. La fonction $t\mapsto F(t)-Q(t)$ est de classe $\CC^1$ sur $[0;x]$ (pour $x$ bien choisi), donc par inégalité des accroissements finis
\[ \left | \frac{F(x)-Q(x) - (F(0)-Q(0))}{x-0} \right| \leq \underset{t\in [0;x]}{\max} |(F-Q)'(t)|=\underset{t\in [0;x]}{\max} |f(t)-P(t)| \]
Ainsi,
\[ \left | \frac{F(x)-Q(x)}{x^{n+1}} \right| \leq \frac{\underset{t\in [0;x]}{\max}|f(t)-P(t)|}{x^n}\stackrel{x\rightarrow 0}{\longrightarrow}0 \]
Puisque $f(x)-P(x)=o_0(x^n)$.
}

\exercice{Déterminer un $\text{DL}_{n+1}(0)$ de $x\mapsto \ln(1+x)$ et un $\text{DL}_{2n+1}(0)$ de $x\mapsto \arctan(x)$.}

\begin{prof}
\solution{On part d'un $\text{DL}_n(0)$ de $x\mapsto \frac{1}{1+x}$ que l'on intègre :
\[ \frac{1}{1+x} = 1-x+x^2+\cdots + (-1)^n x^n +o_0(x^n) \]
et donc
\[ \ln(1+x) = \underbrace{\ln(1+0)}_{=0} +x-\frac{x^2}{2}+\frac{x^3}{3}+\cdots + (-1)^n \frac{x^{n+1}}{n+1} +o_0(x^{n+1}) \]
De même, on écrit un $\text{DL}_{2n}(0)$ de $x\mapsto \frac{1}{1+x^2}$ que l'on intègre :
\[ \frac{1}{1+x^2} = 1-x^2+x^4+\cdots +(-1)^n x^{2n} + o_0(x^{2n}) \]
et donc
\[ \arctan(x)=\underbrace{\arctan(0)}_{=0} +x - \frac{x^3}{3}+\frac{x^5}{5}+\cdots + (-1)^n \frac{x^{2n+1}}{2n+1} + o_0(x^{2n+1}) \]
}	
\end{prof}

\lignes{5}


%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Formule de Taylor-Young}
%%%%%%%%%%%%%%%%%%%%%%%%%%%

	\subsection{Formule de Taylor-Young}
	
\begin{theoreme}[Formule de Taylor-Young] 
Soit $f\in \CC^n(I)$ où $I$ est un intervalle contenant $x_0$. Alors $f$ admet un développement limité à l'ordre $n$ au voisinage de $x_0$ et 
\[ f(x)=\sum_{k=0}^n \frac{f^{(k)}(x_0)}{k!}(x-x_0)^k + o_{x_0}((x-x_0)^n) \]	
\end{theoreme}


\preuve{La preuve se fait par récurrence et est admise.}

\exercice{Déterminer le $\text{DL}_n(0)$ de $\exp$.}

\begin{prof}
\solution{$\exp$ est de classe $\CC^n(\R)$ pour tout entier $n$ (puisqu'en réalité, $\exp$ est de classe $\CC^{\infty}$) et on a \[ \forall~k\in \N,\quad \exp^{(k)} = \exp \quad\text{et donc}\quad \forall~k\in \N,\quad \exp^{(k)}(0)=\exp(0)=1 \]
Ainsi, $f$ admet un développement limité à l'ordre $n$ au voisinage de $0$ et 
\[ \exp(x)=\sum_{k=0}^n \frac{\exp^{(k)}(0)}{k!} x^k + o_0(x^n) = \sum_{k=0}^n \frac{1}{k!} x^k + o_0(x^n) \]
}	
\end{prof}

\lignes{5}

	\subsection{Développements limités usuels}
	
En utilisant la formule de Taylor-Young, on obtient les développements limités au voisinage de $0$ suivants, qui sont à \emph{connaître par c\oe ur}. 

\begin{itemize}[label=\textbullet]
	\item Les développements limités liés à $x\mapsto \frac{1}{1-x}$ :
	\begin{eqnarray*}
	\frac{1}{1-x} &=& 1+x+x^2+\cdots +x^n +o_0(x^n) \\
	\frac{1}{1+x} &=& 1-x+x^2+\cdots +(-1)^nx^n +o_0(x^n) \\
	\ln(1+x) &=& x-\frac{x^2}{2}+\cdots +(-1)^{n-1}\frac{x^n}{n} +o_0(x^n) \\
	\ln(1-x) &=& -x-\frac{x^2}{2}-\cdots -\frac{x^n}{n} +o_0(x^n)\\
	\frac{1}{1+x^2} &=& 1-x^2+x^4+\cdots +(-1)^n x^{2n} +o_0(x^{2n}) \\
	\arctan(x)&=&x-\frac{x^3}{3}+\frac{x^5}{5}+\cdots +(-1)^n \frac{x^{2n+1}}{2n+1} + o_0\left(x^{2n+1}\right)
\end{eqnarray*}
	\item Les développements limités liés à l'exponentielle :
	\begin{eqnarray*}
	\exp(x) &=& 1+x+\frac{x^2}{2!}+\frac{x^3}{3!}+\cdots + \frac{x^n}{n!} + o_0(x^n) \\
	\cosh(x) &=& 1+\frac{x^2}{2!}+\frac{x^4}{4!}+\cdots + \frac{x^{2n}}{(2n)!} + o_0(x^{2n})\\
	\sinh(x) &=& x+\frac{x^3}{3!}+\frac{x^5}{5!}+\cdots + \frac{x^{2n+1}}{(2n+1)!} + o_0(x^{2n+1})\\
	\cos(x) &=& 1-\frac{x^2}{2!}+\frac{x^4}{4!}+\cdots (-1)^n\frac{x^{2n}}{(2n)!} + o_0(x^{2n}) \\
	\sin(x) &=& x-\frac{x^3}{3!}+\frac{x^5}{5!}+\cdots +(-1)^n\frac{x^{2n+1}}{(2n+1)!} + o_0(x^{2n+1}) \\
	\tan(x) &=& x+\frac{x^3}{3}+\frac{2x^5}{15}+o_0(x^5)
\end{eqnarray*}
	\item Puissance :
	\[ (1+x)^\alpha = 1+\alpha x + \frac{\alpha(\alpha-1)}{2}x^2+\cdots + \frac{\alpha(\alpha-1)\cdots(\alpha-n+1)}{n!}x^n + o_0(x^n) \] 
	et en pratique 
	\[ \sqrt{1+x} = 1+\frac{1}{2}x-\frac{1}{8}x^2+\cdots +o_0(x^n)\]
\end{itemize}

\section{Applications}

	\subsection{Calcul de limite}

On peut utiliser les développements limités pour lever des formes indéterminées.

\exercice{Calculer 
\[ \lim_{x\rightarrow 0} \frac{(x+1)(\E^x +1) - 3(\ln(1+x)+1)+1}{x^2} \qeq \lim_{x\rightarrow 0} \frac{\ln(1+x)-x}{x^2} \]
}

\begin{prof}
\solution{Dans le premier cas, on utilise le développement limité de $\exp$ et de $x\mapsto \ln(1+x)$ à l'ordre $2$ (car le $x^2$ au dénominateur nous donne l'intuition) :
\[ \frac{(x+1)(1+x+\frac{x^2}{2}+o_0(x^2) + 1)-3(x-\frac{x^2}{2}+o_0(x^2) +1)+1}{x^2} = \frac{3x^2+o_0(x^2)}{x^2} \underset{x\rightarrow 0}{\longrightarrow} 3 \]
De même, en effectuant un développement limité à l'ordre $2$ de $x\mapsto \ln(1+x)$ :
\[ \frac{\ln(1+x)-x}{x^2} = \frac{x-\frac{x^2}{2}+o_0(x^2)-x}{x^2}=\frac{-\frac{x^2}{2}+o_0(x^2)}{x^2}\underset{x\rightarrow 0}{\longrightarrow} -\frac{1}{2} \] 
}	
\end{prof}

\lignes{7}

	\subsection{Recherche d'équivalents}
	
\begin{proposition}
Soit $f$ une fonction admettant un développement limité à l'ordre $n$ au voisinage de $0$ :
\[ f(x)=a_0+a_1x+\cdots +a_nx^n+o_0(x^n) \]
Soit $p$ l'indice du premier coefficient $a_p$ non nul. On appelle \textbf{forme normalisée} du développement limité l'écriture
\[ f(x)=x^p\left(a_p+a_{p+1}x+\cdots +a_nx^{n-p} + o_0(x^{n-p})\right) \]
et dans ce cas
\[ f(x) \sim_0 a_px^p \]	
\end{proposition}


\remarque{On peut raisonner en dehors de $0$, en faisant un développement limité à l'ordre $n$ de $x\mapsto f(x_0+x)$.}
	
\exercice{Déterminer un équivalent simple en $0$ de \[ f:x\mapsto \frac{\sinh(x)-\sin(x)}{1-\cos(x)}\]}

\begin{prof}
\solution{On utilise un développement limité à l'ordre 3 au voisinage de $0$ de chacune des fonctions :
\[ \frac{\sinh(x)-\sin(x)}{1-\cos(x)} = \frac{x+\frac{x^3}{3!}-\left(x-\frac{x^3}{3!}\right) + o_0(x^3)}{1-\left(1-\frac{x^2}{2!}+o_0(x^3)\right)} = \frac{\frac{x^3}{3}+o_0(x^3)}{\frac{x^2}{2}+o_0(x^3)} = \frac{\frac{x}{3}+o_0(x)}{\frac{1}{2}+o_0(x)} \]
Ainsi,
\[ f(x) \sim_0 \frac{2}{3} x \]
}	
\end{prof}

\lignes{7}

	\subsection{Position relative de courbe}
	
\begin{methode}
Supposons que $f$ admette un $\text{DL}_1(x_0)$. Alors on obtient l'équation de la tangente à la courbe de $f$ au point d'abscisse $x_0$ en prenant la partie régulière du développement limité. En appliquant le résultat précédent, et sous réserve qu'un DL d'ordre suffisant existe, on peut écrire
\[ f(x)-(a+bx) \sim_{x_0} a_p(x-x_0)^p \]
Alors le signe de $a_p(x-x_0)^p$ permet de donner la position relative de la courbe et de la tangente au voisinage de $x_0$ : s'il est positif, la courbe est au dessus de sa tangente; sinon elle est au-dessous.	
\end{methode}


\remarque{Attention : le résultat n'est vrai que localement autour de $x_0$.}

\exemple{Soit $f:x\mapsto \frac{\ln(1+x)-x}{x^2}$. Montrer que $f$ est dérivable en $0$, et étudier la position relative de la courbe de $f$ et de sa tangente au point d'abscisse $0$.}

\begin{prof}
\solution{Utilisons le développement limité de $x\mapsto \ln(1+x)$ au voisinage de $x_0$ :
\[ f(x)=\frac{x-\frac{x^2}{2}+\frac{x^3}{3}-\frac{x^4}{4} + o_0(x^4)-x}{x^2}=-\frac{1}{2}+\frac{1}{3}x-\frac{1}{4}x^2+o_0(x^2) \]
D'une part, puisque $f$ admet (par troncature) un $\text{DL}_1(0)$, $f$ est dérivable en $0$ et $f'(0)=\frac{1}{3}$, mais de plus, $f(x)-\left(-\frac{1}{2}+\frac{1}{3}x\right) = -\frac{1}{4}x^2+o_0(x^2)$.\\
Puisque $-\frac{1}{4}x^2<0$ au voisinage de $0$, on en déduit que la tangente à la courbe de $f$ au voisinage de $0$ est localement toujours au-dessus de la courbe de $f$. 
}	
\end{prof}

\lignes{10}

 
	\subsection{Asymptote}

\definition{
On dit qu'une fonction $f$ admet un \textbf{développement asymptotique} à l'ordre $n$ au voisinage de $+\infty$ si on peut écrire
\[ f\left(t\right) = a_0+\frac{a_1}{t}+\cdots + \frac{a_n}{t^n} + o_{+\infty}\left(\frac{1}{t^n}\right) \]	
}



\begin{methode}
Pour obtenir un développement asymptotique au voisinage de $+\infty$ de $f(x)$, on cherche un développement limité au voisinage de $0$ de $f\left(\frac{1}{t}\right)$, puis on pose $t=\frac{1}{x}$.	
\end{methode}


\exercice{Déterminer un développement asymptotique à l'ordre $2$ de \[ f:x\mapsto \frac{\sqrt{1+\frac{1}{x}}}{x} \]}

\begin{prof}
\solution{Notons $u=\frac{1}{x}$. On a alors $f(u)=u\sqrt{1+u}$. Faisons un développement limité au voisinage de $0$ :
\[ f(u) =u\left(1+\frac{1}{2}u-\frac{1}{8}u^2+o_0(u^2)\right) = u+\frac{1}{2}u^2+u_0(u^2) \]
On obtient alors de développement asymptotique
\[ f(x) = \frac{1}{x}+\frac{1}{2x^2}+o_{+\infty}\left(\frac{1}{x^2}\right) \]
}
\end{prof}


\lignes{8}

\begin{proposition}
Soit $f$ une fonction, telle que $\frac{f(x)}{x}$ admet un développement asymptotique au voisinage de $+\infty$ à l'ordre $n$ :
\[ \frac{f(x)}{x} = a_0+\frac{a_1}{x}+\cdots + \frac{a_n}{x^n} + o_{+\infty}\left(\frac{1}{x^n}\right)  \]
soit encore
\[ f(x)= a_0x+a_1+\frac{a_2}{x}+\cdots + \frac{a_n}{x^{n-1}}+o_{+\infty}\left(\frac{1}{x^{n-1}}\right) \]
Soit $p\geq 2$ l'indice du premier coefficient $a_p$ non nul. Alors $y=a_0x+a_1$ est asymptote à la courbe de $f$ au voisinage de $+\infty$, et le signe de $\frac{a_p}{x^{p-1}}$ donne la position de la courbe et de son asymptote au voisinage de $+\infty$. 	
\end{proposition}


\exercice{Déterminer l'asymptote au voisinage de $+\infty$ et la position relative avec la courbe de la fonction \[ f:x\mapsto x\sqrt{1+\frac{1}{x}}
\]
}

\begin{prof}
\solution{En procédant comme précédemment, posons $u=\frac{1}{x}$. $f(u)=\frac{1}{u}\sqrt{1+u}$ et faisons un développement limité à l'ordre $2$ de $u\mapsto\sqrt{1+u}$ :
\[ f(u)=\frac{1}{u}\left(1+\frac{1}{2}u-\frac{1}{8}u^2+o_0(u^2)\right) = \frac{1}{u}+\frac{1}{2}-\frac{1}{8}u+o_0(u) \]
Soit, en revenant à $x$ :
\[ f(x)=x+\frac{1}{2}-\frac{1}{8x}+o_{+\infty}\left(\frac{1}{x}\right) \]
Ainsi, la courbe de $f$ admet la droite d'équation $y=x+\frac{1}{2}$ comme asymptote oblique, et puisque $-\frac{1}{8x}< 0$ au voisinage de $+\infty$, la courbe de $f$ est localement en dessous de son asymptote.
}	
\end{prof}


\lignes{10}
