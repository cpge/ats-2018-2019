\objectif{Ce chapitre est souvent délaissé par les élèves au concours. \\
Ce chapitre repose sur les bases connues des années précédentes, en les consolidant.
}

\large \textbf{Objectifs} \normalsize

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
	\item Concernant le produit scalaire :
			\begin{itemize}[label=\textbullet]
				\item Connaître la définition du produit scalaire\dotfill $\Box$
				\item Savoir calculer un produit scalaire dans le cas des coordonnées cartésiennes\dotfill $\Box$ 
			\end{itemize}
	\item Concernant le produit vectoriel :
			\begin{itemize}[label=\textbullet]
				\item Connaître la notion de produit vectoriel et les différentes méthodes de calcul\dotfill $\Box$
				\item Savoir utiliser le produit vectoriel pour déterminer l'aire d'un parallélogramme\dotfill $\Box$
			\end{itemize}
	\item Concernant le produit mixte :
			\begin{itemize}[label=\textbullet]
				\item Connaître la notion de produit mixte et les différentes méthodes de calcul\dotfill $\Box$
				\item Savoir montrer que des vecteurs sont coplanaires\dotfill $\Box$
				\item Savoir utiliser le produit mixte pour déterminer le volume d'un parallélépipède\dotfill $\Box$
			\end{itemize}
	\item Concernant les plans :
		\begin{itemize}[label=\textbullet]
			\item savoir déterminer une équation cartésienne de plan\dotfill $\Box$
			\item savoir déterminer un système d'équations paramétriques de plan\dotfill $\Box$
			\item savoir déterminer les intersections éventuelles de deux plans\dotfill $\Box$
			\item savoir déterminer la distance d'un point à un plan\dotfill $\Box$
		\end{itemize}
	\item Concernant les droites :
		\begin{itemize}[label=\textbullet]
			\item savoir déterminer un système d'équations cartésiennes de droite\dotfill $\Box$
			\item savoir déterminer un système d'équations paramétriques de droite\dotfill $\Box$
			\item savoir déterminer la distance d'un point à une droite\dotfill $\Box$
		\end{itemize}
	\item Concernant les sphères :
		\begin{itemize}[label=\textbullet]
			\item savoir déterminer une équation cartésienne de sphère\dotfill $\Box$
			\item savoir déterminer les caractéristiques d'une sphère connaissant une équation cartésienne\dotfill $\Box$
		\end{itemize}
\end{numerote}

\newpage

\section{Repérage dans l'espace}

Dans cette partie, on revient sur les bases de la géométrie de l'espace.

	\subsection{Base de l'espace et repère}

\rappel{Trois vecteurs de l'espace sont dits \textbf{coplanaires} si et seulement si on peut trouver trois représentants de ces vecteurs situés dans un même plan.}

\remarque{On dira plus tard que si trois vecteurs sont coplanaires, la famille composée de ces trois vecteurs est \textbf{liées}. Sinon, on dira qu'elle est \textbf{libre}.}

\definition{On appelle \textbf{base} de l'espace la donnée de trois vecteurs $\base[3]$ non coplanaires.\\On dit que la base est 
\begin{itemize}[label=\textbullet]
	\item \textbf{orthogonale} si et seulement si $\vecti$, $\vectj$ et $\vectk$ sont deux à deux orthogonaux.	\item \textbf{orthonormée} ou orthonormale si et seulement si elle est orthogonale, et si $||\vecti||=||\vectj||=||\vectk||=1$. 
\end{itemize}
%\begin{center}\input{tex/Chap10/pic/base}\end{center}
}

\begin{proposition}
Soit $\base[3]$ une base de l'espace. Alors pour tout vecteur $\vectu$ du plan, il existe trois réels uniques $x$, $y$ et $z$ tels que \[ \vectu=x\vecti+y\vectj+z\vectk \]
$x$ est appelé \textbf{abscisse} de $\vectu$, $y$ est appelé \textbf{ordonnée}, $z$ est appelé la \textbf{côte} et $(x,y,z)$ représente les \textbf{coordonnées cartésiennes} du vecteur $\vectu$ dans la base $\base[3]$.\\On notera 
\[ \vectu\matrice{x\\y\\z} \quad \text{ou}\quad \vectu(x;y;z) \]
%\begin{center}\input{tex/Chap10/pic/coordvec}\end{center}	
\end{proposition}


\definition{[Repère] On appelle \textbf{repère} de l'espace la donnée d'un point $O$, appelé \textbf{origine} du repère, et d'une base $\base[3]$.\\Le repère est dit orthogonal si la base $\base[3]$ est orthogonale, et orthonormé si la base $\base[3]$ est orthonormée.}

\begin{proposition}
Soit $\repere[3]$ un repère de l'espace. Pour tout point $A$ de l'espace, il existe trois réels $x$, $y$ et $z$ uniques vérifiant \[ \V{OA}=x\vecti+y\vectj+z\vectk \]
$x$ est appelé abscisse du point $A$, $y$ ordonnée du point $A$ et $z$ la côte du point $A$. Le triplet $(x;y;z)$ représente les \textbf{coordonnées cartésiennes} de $A$, et on note $A(x;y;z)$.	
\end{proposition}


\remarque{Toutes les formules de géométrie du plan ont leur équivalent dans l'espace. Ainsi, si on se donne un repère $\repere[3]$ de l'espace, et $A(x_A;y_A;z_A)$, $B(x_B;y_B;z_B)$ deux points du plan, alors
\[ \V{AB}\matrice{x_B-x_A\\y_B-y_A\\z_B-z_A},\quad \text{si $I$ milieu de $[AB]$},\quad I\left(\frac{x_A+x_B}{2};\frac{y_A+y_B}{2};\frac{z_A+z_B}{2}\right) \]}

\exemple{Soient $A(1;2;1)$ et $B(2;-1;1)$ deux points de l'espace. Déterminer les coordonnées du milieu de $[AB]$ et les coordonnées de $\V{AB}$.
}

\begin{prof}
\solution{On a donc, en notant $I$ le milieu de $[AB]$ : \[ \V{AB}\matrice{1\\-3\\0} \qeq I\left(\frac{3}{2};\frac{1}{2};1\right) \]
}
\end{prof}


\lignes{5}

	\subsection{Orientation}
	
On souhaite orienter un repère de l'espace.

\remarque{Si on se donne un plan et un vecteur  $\V{AB}$ non contenu dans ce plan, le sens direct est le sens trigonométrique si on regarde le plan depuis le point $B$, extrémité du vecteur extérieur.
\begin{center}
\input{tex/Chap10/pic/orientation}	
\end{center}
}

\definition{Une base $\base[3]$ est dite \textbf{directe} si $\sin(\vecti;\vectj)>0$, dans le plan contenant $\vecti$ et $\vectj$, orienté par le vecteur $\vectk$.}

	\subsection{Produit scalaire dans l'espace}

\definition{On se donne une base \textbf{orthonormée} $\base[3]$. On définit la \textbf{norme} d'un vecteur $\vectu\matrice{x\\y\\z}$ par \[ ||\vectu ||=\sqrt{x^2+y^2+z^2} \]
Ainsi, la distance entre deux points $A(x_A;y_A;z_A)$ et $B(x_B;y_B;z_B)$ est donnée par 
\[ AB=||\V{AB}|| = \sqrt{(x_B-x_A)^2+(y_B-y_A)^2+(z_B-z_A)^2} \]
}


\exemple{Soient $A(1;2;1)$ et $B(2;-1;1)$ deux points de l'espace dans un repère orthonormé. Déterminer la longueur $AB$.}

\begin{prof}
\solution{On a 
\[ AB=\sqrt{(2-1)^2+(-1-2)^2+(1-1)^2} = \sqrt{10} \]
}
\end{prof}

\lignes{5}

\definition{Soient $\vectu$ et $\vectv$ deux vecteurs de l'espace. On définit le produit scalaire de $\vectu$ et $\vectv$ par 
\[ \vectu\cdot \vectv = \frac{||\vectu+\vectv||^2-||\vectu||^2-||\vectv||^2}{2} \]
}

\exemple{Soient $\vectu\matrice{1\\2\\1}$ et $\vectv\matrice{2\\-1\\-2}$. Déterminer $\vectu\cdot \vectv$.}
\begin{prof}
\solution{On a $\vectu+\vectv\matrice{3\\1\\-1}$, puis
\[ ||\vectu+\vectv||^2=11,\quad ||\vectu||^2=6 \qeq ||\vectv||^2=9 \]
et donc
\[ \vectu\cdot \vectv=\frac{11-6-9}{2}=-2 \]
}	
\end{prof}

\lignes{6}

\propriete{Le produit scalaire dans l'espace vérifie les même propriétés que dans le plan; pour tous vecteurs $\vectu, \vectv, \V{w}$ et réel $\lambda$, on a :
\begin{itemize}[label=\textbullet]
	\item homogénéité : $(\lambda \vectu)\cdot \vectv = \vectu\cdot (\lambda \vectv) = \lambda (\vectu\cdot \vectv)$.
	\item bilinéarité : $(\vectu+\vectv)\cdot \V{w}=\vectu\cdot \V{w}+\vectv\cdot \V{w}$ et $\vectu\cdot(\vectv+\V{w}) = \vectu\cdot \vectv+\vectu\cdot \V{w}$.
	\item symétrie : $\vectu\cdot\vectv=\vectv\cdot \vectu$.
	\item positivité : $\vectu\cdot \vectu=||\vectu||^2$.
	\item caractère défini : $\vectu\cdot \vectu=0$ si et seulement si $\vectu=\V{0}$.
	\item $\vectu$ et $\vectv$ sont orthogonaux si et seulement si $\vectu\cdot \vectv=0$.
\end{itemize}
}

Dans le cas où on dispose d'un repère orthonormé, on dispose d'une formule simple pour calculer le produit scalaire dans l'espace.

\begin{proposition}
On se donne un repère orthonormé $\repere[3]$. Soient $\vectu\matrice{x\\y\\z}$ et $\vectv\matrice{x'\\y'\\z'}$ deux vecteurs de l'espace. Alors
\[ \vectu\cdot \vectv =xx'+yy'+zz' \]	
\end{proposition}


\exemple{Si $\vectu\matrice{1\\2\\3}$ et $\vectv\matrice{-1\\2\\2}$ dans un repère orthonormé, alors
\[ \vectu\cdot \vectv = 1\times (-1)+2\times 2 + 3\times 2 = 9 \]
}

%% ANGLE DANS L'ESPACE ? Projeté orthongonal ?

%
%\proposition{Soient $\vectu$ et $\vectv$ deux vecteurs non nuls. Soient $A$ et $B$ deux points du plan, tels que $\V{OA}=\vectu$ et $\V{OB}=\vectv$. On note $H$ le projeté orthogonal de $B$ sur $(OA)$. 
%\begin{center}
%\input{tex/Chap10/pic/ortho}	
%\end{center}
%
%Alors, $\vectu\cdot \vectv = OA \times OH$ si $\V{OA}$ et $\V{OH}$ sont dans le même sens, $\vectu\cdot \vectv=-OA\times OH$ sinon. \\On notera \[ \vectu\cdot \vectv = \overline{OA}\times \overline{OH} \]
%}
%


\section{Produit vectoriel}

	\subsection{Définition}

\definition{Soient $\vectu$ et $\vectv$ deux vecteurs non colinéaires de l'espace. On appelle \textbf{produit vectoriel} de $\vectu$ et $\vectv$, et on note $\vectu \wedge \vectv$, le \textbf{vecteur} vérifiant :
	\begin{itemize}[label=\textbullet]
		\item la norme de $\vectu \wedge \vectv$ est
			\[ ||\vectu\wedge \vectv|| = ||\vectu||.||\vectv||.\left|\sin(\vectu;\vectv)\right| \]
		\item la direction de $\vectu\wedge \vectv$ est orthogonale à $\vectu$ et $\vectv$.
		\item le triplet $(\vectu;\vectv;\vectu\wedge\vectv)$ est dans le sens direct
	\end{itemize}
Si $\vectu$ et $\vectv$ sont colinéaires, on pose $\vectu\wedge \vectv=\V{0}$.
\begin{center}
	\input{tex/Chap10/pic/prodvect}
\end{center}
}

\exercice{Soit $\base[3]$ une base orthonormée directe de l'espace. Calculer les $9$ produits vectoriels possibles en prenant deux couples ordonnées de vecteurs de la base.}

\begin{prof}
\solution{On rapidement 
\[ \vecti\wedge \vecti=\V{0},\quad \vecti\wedge \vectj=\vectk,\quad\vecti\wedge \vectk=-\vectj \]
\[ \vectj \wedge \vecti=-\vectk,\quad \vectj\wedge \vectj=\V{0}, \quad \vectj\wedge \vectk=\vecti \]
\[ \vectk \wedge \vecti=\vectj,\quad \vectk\wedge \vectj=-\vecti, \quad \vectk\wedge \vectk=\V{0} \]
}	
\end{prof}

\lignes{14}

	\subsection{Propriétés}

\propriete{Soient $\vectu, \vectv$ et $\V{w}$ trois vecteurs de l'espace, et $\lambda$ un réel.
\begin{itemize}[label=\textbullet]
	\item antisymétrie : $\vectu\wedge \vectv = -\vectv\wedge \vectu$.
	\item homogénéité : $(\lambda \vectu)\wedge \vectv= \vectu\wedge (\lambda \vectv) = \lambda (\vectu\wedge \vectv)$.
	\item bilinéarité :
		\[ (\vectu+\vectv)\wedge \V{w} = \vectu\wedge \V{w} + \vectv\wedge \V{w} \qeq \vectu \wedge (\vectv+\V{w}) = \vectu\wedge \vectv + \vectu\wedge \V{w} \]
\end{itemize}
}

\preuve{On utilise la définition du produit vectoriel.}

\remarque{\danger~Le produit vectoriel n'est pas associatif. En effet, 
\[ \vectu\wedge(\vectv\wedge \V{w}) \neq (\vectu\wedge \vectv)\wedge \V{w} \]}

On dispose d'une caractérisation des vecteurs colinéaires.

\begin{proposition}
$\vectu$ et $\vectv$ sont colinéaires si et seulement si $\vectu\wedge \vectv=\V{0}$.	
\end{proposition}


\preuve{En effet, s'ils ne sont pas colinéaires, leur angle n'est pas égal à $0$ ou $\pi$ modulo $2\pi$, et le sinus est donc non nul. S'ils sont colinéaires, par définition, leur produit vectoriel est nul.}

\begin{proposition}[Coordonnées cartésiennes] 
Soient $\vectu\matrice{x\\y\\z}$ et $\vectv\matrice{x'\\y'\\z'}$ deux vecteurs de l'espace dans une base orthonormée directe. Alors

\[ \vectu\wedge \vectv \matrice{yz'-y'z\\zx'-z'x\\xy'-x'y} \quad \text{soit}\quad \vectu\wedge \vectv \matrice{
	\small \left|\begin{array}{cc}y&y'\\z&z'\end{array}\right|\\~\\
	\small \left|\begin{array}{cc}z&z'\\x&x'\end{array}\right| \\~\\
	\small \left|\begin{array}{cc}x&x'\\y&y'\end{array}\right|
} \]	
\end{proposition}



\exemple{Soient $\vectu\matrice{2\\1\\3}$ et $\vectv\matrice{3\\4\\-1}$ deux vecteurs de l'espace dans une base orthonormée directe. Déterminer les coordonnées de $\vectu\wedge \vectv$.
}

\begin{prof}
\solution{On a \[\vectu\wedge \vectv\matrice{1\times (-1) - 3\times 4 \\ 3\times 3 - 2\times (-1) \\ 2\times 4-1\times 3} \]
soit $\vectu\matrice{-13\\11\\5}$.
}
\end{prof}

\lignes{4}

\remarque{On constate que, dans le plan contenant $\vectu$ et $\vectv$, 
\[ || \vectu\wedge \vectv|| =\left| [\vectu;\vectv] \right| \]
Ainsi, l'aire du parallélogramme $ABCD$ peut être calculée par $|| \V{AB}\wedge \V{AD} ||$.}

%% CF EXO TRIRECTANGLE
%$OA^2OC^2 + OB^2OA^2+OB^2OC^2 = OA^2(OC^2+OB^2) + OB^2OC^2 = BC^2OA^2+OB^2OC^2$

\section{Produit mixte}

La notion de produit mixte étend la définition de déterminant à l'espace.

	\subsection{Définition}
	
\definition{Soient $\vectu, \vectv$ et $\V{w}$ trois vecteurs de l'espace. On appelle \textbf{produit mixte} des trois vecteurs, et on note $[\vectu;\vectv;\V{w}]$ ou $\det(\vectu;\vectv;\V{w})$ le nombre réel
\[ [\vectu;\vectv;\V{w}] = (\vectu \wedge \vectv) \cdot \V{w} \]
}

\remarque{Ainsi, le produit mixte, contrairement au produit vectoriel, est \textbf{un réel}.}

\exemple{Soit $\base[3]$ une base orthonormée directe. Alors
\[ [\vectk;\vectj;\vecti] = (\vectk\wedge \vectj)\cdot \vecti = -\vecti\cdot \vecti = -1 \]
}

\exercice{Déterminer de même $[\vecti;\vectk;\vectj]$ et $[\vectj;\vectk;\vectj]$.}

\begin{prof}
\solution{On a rapidement
\[ [\vecti;\vectk;\vectj] = (\vecti\wedge \vectk)\cdot \vectj = -\vectj\cdot \vectj=-1 \qeq [\vectj;\vectk;\vectj]=(\vectj\wedge\vectk)\cdot \vectj=\vecti\cdot \vectj = 0 \]
}
\end{prof}

\lignes{5}

	%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Propriétés}
	%%%%%%%%%%%%%%%%%%%%%%%

En utilisant les différentes propriétés du produit vectoriel et du produit scalaire, on obtient les propriétés suivantes :

\propriete{Soient $\vectu, \vectv, \V{w}$ et $\V{a}$ quatre vecteurs de l'espace, et $\lambda$ un réel. 
\begin{itemize}[label=\textbullet]
	\item on a $[\vectu;\vectv;\V{w}]=[\vectv;\V{w};\vectu]=[\V{w};\vectu;\vectv]$.
	\item homogénéité : $[\lambda \vectu;\vectv;\V{w}] = [\vectu;\lambda \vectv;\V{w}] = [\vectu;\vectv;\lambda \V{w}] = \lambda [\vectu;\vectv;\V{w}]$.
	\item trilinéarité : $[\vectu+\V{a};\vectv;\V{w}] = [\vectu;\vectv;\V{w}] +[\V{a};\vectv;\V{w}]$, et de même pour les deux autres variables.
	\item alterné : $[\vectv;\vectu;\V{w}]=-[\vectu;\vectv;\V{w}]$ et de même en inversant deux vecteurs consécutifs.
\end{itemize}
}

On dispose, comme pour le produit vectoriel avec la colinéarité, d'une caractérisation de la coplanarité :

\begin{proposition}
Soient $\vectu, \vectv, \V{w}$ trois vecteurs. Alors $\vectu, \vectv$ et $\V{w}$ sont coplanaires si et seulement si $[\vectu;\vectv;\V{w}] = 0$.	
\end{proposition}


\exercice{Montrer que les vecteurs $\vectu\matrice{0\\2\\3}, \vectv\matrice{-2\\4\\-1}$ et $\V{w}\matrice{8\\-14\\7}$ sont coplanaires.}

\begin{prof}
\solution{On constate que
\[ \vectu\wedge \vectv \matrice{-14\\-6\\4} \]
Ainsi
\[ [\vectu;\vectv;\V{w}] = -14\times 8+(-6)\times (-14)+4\times 7 = 0 \]
Les vecteurs $\vectu, \vectv$ et $\V{w}$ sont donc coplanaires.
}	
\end{prof}

\lignes{6}

\begin{proposition}[Coordonnées cartésiennes] 
Soient $\vectu\matrice{x\\y\\z}, \vectv\matrice{x'\\y'\\z'}$ et $\V{w}\matrice{x''\\y''\\z''}$ trois vecteurs de l'espace dans une base orthonormée directe. Alors
\[ [\vectu;\vectv;\V{w}] = \left| \begin{array}{ccc}x&x'&x''\\y&y'&y''\\z&z'&z''\end{array}\right| = x(y'z''-y''z') - y(x'z''-z'x'')+z(x'y''-y'x'') \]	
\end{proposition}


\preuve{Cela découle des définition du produit scalaire et du produit vectoriel.}

\remarque{Tout comme le déterminant dans le plan permet de calculer l'aire d'un parallélogramme, le produit mixte dans l'espace permet de calculer le volume du parallélépipède construit sur les trois vecteurs. Ainsi, si $\vectu,\vectv$ et $\V{w}$ sont trois vecteurs non colinéaires, le parallélépipède construit sur $\vectu,\vectv$ et $\V{w}$ a pour volume 
\begin{center}
	\input{tex/Chap10/pic/parall}
\end{center}
\[ \mathcal{V}_P = \left| [\vectu;\vectv;\V{w}] \right| \]
De même, le tétraèdre construit à partir de ces trois vecteurs a pour volume
\begin{center}
\input{tex/Chap10/pic/tetra}
\end{center}
\[ \mathcal{V}_T =\frac{1}{6} \left| [\vectu;\vectv;\V{w}] \right| \]
}

\section{Plans}

On se donne dans l'ensemble de cette section un \ron[3] de l'espace.

	\subsection{Système d'équations paramétriques d'un plan}
	
\definition{Soient $A$ un point de l'espace, et $\vectu,\vectv$ deux vecteurs non colinéaires de l'espace. On appelle \textbf{plan affine} $(A;\vectu;\vectv)$, ou encore plan passant par $A$ et dirigé par $\vectu$ et $\vectv$, l'ensemble des points $M$ de l'espace qui vérifient
\[ \V{AM}=t\vectu+t'\vectv \quad \text{avec}\quad (t,t')\in \R^2 \]
c'est-à-dire que les vecteurs $\V{AM}, \vectu$ et $\vectv$ sont coplanaires.
} 

\definition{Soient $A$ et $B$ deux points de l'espace. L'ensemble des points $M$ de l'espace à égale distance de $A$ et $B$ est un plan, appelé \textbf{plan médiateur} du segment $[AB]$.}

\begin{proposition}
Un point $M(x;y;z)$ est dans le plan passant par $A(x_A;y_A;z_A)$ et dirigé par $\vectu\matrice{a\\b\\c}$ et $\vectv\matrice{a'\\b'\\c'}$ non colinéaires si et seulement s'il existe $(t,t')\in \R^2$ vérifiant
	\[ \left \{ \begin{array}{ccccccc}x&=&x_A&+&ta&+&t'a' \\
	 	y&=&y_A&+&tb&+&t'b' \\
		z&=&z_A&+&tc&+&t'c' \\	
 \end{array}\right.\]	
\end{proposition}


\definition{On appelle \textbf{système d'équations paramétriques} du plan $(A;\vectu;\vectv)$ le système 
	\[ \left \{ \begin{array}{ccccccc}x&=&x_A&+&ta&+&t'a' \\
	 	y&=&y_A&+&tb&+&t'b' \\
		z&=&z_A&+&tc&+&t'c' \\	
 \end{array}\right.\quad \quad (t,t')\in \R^2 \]
} 

\exemple{Déterminer un système d'équations paramétriques de plan passant par $A(1;2;3)$ et dirigés par $\vectu\matrice{1\\-1\\0}$ et $\vectv\matrice{2\\1\\-1}$.}

\begin{prof}
\solution{On a rapidement le système d'équations paramétriques suivant :
\[ \left \{ \begin{array}{ccccccc}x&=&1&+&t&+&2t'\\y&=&2&-&t&+&t'\\z&=&3&&&-&t'\end{array}\right. \quad \quad (t,t')\in \R^2 \]
}	
\end{prof}

\lignes{5}


\remarque{Trois points non alignés $A, B, C$ définissent un plan $(A;\V{AB};\V{AC})$.}

	\subsection{Équation cartésienne d'un plan}
	
\definition{Soit $\mathcal{P}$ un plan de l'espace. Un vecteur $\V{n}$ est un vecteur \textbf{normal} du plan $\mathcal{P}$ si et seulement s'il est orthogonal à tout vecteur inclus dans $\mathcal{P}$.}

\remarque{Il suffit qu'il soit orthogonal à deux vecteurs non colinéaires du plan, en utilisant la définition d'un plan vue précédemment.}

\begin{proposition}
Soient $A$ un point de l'espace et $\V{n}\matrice{a\\b\\c}$ un vecteur non nul. \\L'ensemble des points $M$ de l'espace vérifiant $\V{AM}$ orthogonal à $\V{n}$ est le plan, passant par $A$ et de vecteur normal $\V{n}$. Les points $M(x;y;z)$ de ce plan vérifient une équation de la forme $ax+by+cz+d=0$, appelée \textbf{équation cartésienne du plan}.\\
Réciproquement, l'ensemble des points $M(x;y;z)$ de l'espace vérifiant $ax+by+cz+d=0$ avec $a,b$ et $c$ non tous nuls, est un plan de vecteur normal $\V{n}\matrice{a\\b\\c}$.	
\end{proposition}


\remarque{\danger~L'équation cartésienne d'un plan dans l'espace ressemble à l'équation cartésienne d'une droite dans un plan. Mais attention : une droite est donnée par deux équations cartésiennes de plan. Nous le verrons plus tard.}

\begin{methode}
Pour déterminer l'équation cartésienne d'un plan
\begin{itemize}[label=\textbullet]
	\item si on connait un vecteur $\V{n}\matrice{a\\b\\c}$ et un point $A$, on écrit que $M(x;y;z)$ appartient au plan si et seulement si $ax+by+cz+d=0$, où $d$ est à déterminer en utilisant les coordonnées de $A$.
	\item si on connait deux vecteurs directeurs non colinéaires $\vectu$ et $\vectv$ ainsi qu'un point $A$ :
	\begin{itemize} 	
		 \item soit on prend $\vectu\wedge \vectv$ comme vecteur normal et on applique la méthode précédente;
		 \item soit on écrit que $M(x;y;z)$ appartient au plan si et seulement si $[\V{AM};\vectu;\vectv] = 0$;
		 \item soit on forme un système d'équations paramétriques du plan et on élimine $t$ et $t'$.
	\end{itemize}
\end{itemize}	
\end{methode}


\exemple{Soient $A(1;0;0)$, $B(0;1;0)$, $C(0;0;1)$ et $\V{n}\matrice{0\\1\\-1}$. Déterminer une équation du plan $\mathcal{P}_1$ passant par l'origine et de
vecteur normal $\V{n}$, du plan $(ABC)$, de $\mathcal{P}_2$ plan perpendiculaire à $(ABC)$ passant par $C$, et du plan médiateur de $[AB]$.}

\begin{prof}
\solution{$\mathcal{P}_1$ a une équation de la forme $y-z+d=0$, avec $O \in \mathcal{P}_1$, c'est-à-dire $d=0$. Ainsi \[ \mathcal{P}_1 : y-z=0 \]
Le plan $(ABC)$ passe par $A$ et est dirigé par $\V{AB}\matrice{-1\\1\\0}$ et $\V{AC}\matrice{-1\\0\\1}$ non colinéaires. Ainsi, $\V{n'}=\V{AB}\wedge\V{AC}$ est un vecteur normal. Puisque $\V{n'}\matrice{1\\1\\1}$, $(ABC)$ a une équation de la forme $x+y+z+d=0$. Or $A\in (ABC)$ donc $d=-1$: \[ (ABC): x+y+z-1=0 \]
Un plan $\mathcal{P}_2$ est perpendiculaire à $(ABC)$ donc a pour vecteur normal (par exemple) $\V{CA}$. Ainsi, $\mathcal{P}_2$ a une équation de la forme $x-z+d=0$, avec $C\in \mathcal{P}_2$, et donc $d=1$. \[ \mathcal{P}_2:x-z+1=0 \]
Enfin, le plan médiateur $\mathcal{P}_3$ de $[AB]$ passe par $I$ milieu de $[AB]$ et a comme vecteur normal $\V{AB}$. Ainsi, $\mathcal{P}_3$ a une équation de la forme $-x+y+d=0$, et passe par $I\left(\frac{1}{2};\frac{1}{2};0\right)$. Donc $d=0$ et $\mathcal{P}_3:-x+y=0$.

}	
\end{prof}

\lignes{18}

\begin{methode}
Pour déterminer des vecteurs non colinéaires d'un plan ainsi qu'un point $A$ :
\begin{itemize}[label=\textbullet]
	\item si on connait un système d'équation paramétrique, il suffit de prendre des valeurs quelconques pour $t$ et $t'$ pour obtenir un point, et de prendre les coefficients de $t$ (respectivement de $t'$) pour obtenir des vecteurs directeurs.
	\item si on connait un point $A$ , un vecteur directeur $u$ et un vecteur normal $n$, on obtient un deuxième vecteur directeur en prenant $\vectv=\vectu\wedge \V{n}$.
	\item si on connait deux points distincts du plan $A$ et $B$, et un vecteur normal, on dispose de deux vecteurs directeurs : $\V{AB}$ et $\V{AB}\wedge \V{n}$.
	\item si on connait une équation cartésienne, on se ramène au cas précédent en récupérant un vecteur normal et deux points du plan.
\end{itemize}	
\end{methode}


\exemple{Donner un point et deux vecteurs directeurs du plan d'équation $x-z+1=0$.}

\begin{prof}
\solution{Le vecteur $\V{n}\matrice{1\\0\\-1}$ est un vecteur normal au plan. De plus, $A(1;0;2)$ et $B(-1;0;0)$ sont deux points du plan. Ainsi, $\V{AB}\matrice{-2\\0\\-2}$ est un vecteur directeur du plan, et $\V{AB}\wedge \V{n}\matrice{0\\-4\\0}$ est également un vecteur directeur du plan.\\\textbf{Bilan} : $A(1;0;2)$ est un point du plan et $\V{AB}\matrice{-2\\0\\-2}$ et $\vectv\matrice{0\\-4\\0}$ sont des vecteurs directeurs.
}	
\end{prof}

\lignes{12}

	\subsection{Distance d'un point à un plan}
	
La formule est similaire à la distance d'un point à une droite dans le plan :

\begin{proposition}
Soit $\mathcal{P}$ un plan d'équation cartésienne $ax+by+cz+d=0$ (avec $a,b,c$ non tous nuls), et $A(x_A;y_A;z_A)$ un point. La distance de $A$ au plan $\mathcal{P}$ est la distance de $A$ à son projeté orthogonal sur $\mathcal{P}$. Elle est donnée par
\[ d(A;\mathcal{P}) = \frac{|ax_A+by_A+cz_A+d|}{\sqrt{a^2+b^2+c^2}} \]	
\end{proposition}


\preuve{Même idée que pour la distance d'un point à une droite dans le plan.}

\exemple{Déterminer la distance du point $A(1;2;1)$ au plan $\mathcal{P}$ d'équation cartésienne $x+2y-3z+2=0$.}

\begin{prof}
\solution{En appliquant la formule précédente
\[ d(A;\mathcal{P}) = \frac{|1+2\times 2-3\times 1+2|}{\sqrt{1^2+2^2+(-3)^2}} = \frac{4}{\sqrt{14}}=\frac{2\sqrt{14}}{7}\]
}	
\end{prof}

\lignes{5}

\section{Droites}

On se donne dans l'ensemble de cette section un \ron[3] de l'espace.


	\subsection{Définitions}

\definition{Soit $\vectu$ un vecteur non nul de l'espace, et $A$ et $B$ deux points distincts de l'espace. On appelle :
\begin{itemize}[label=\textbullet]
	\item \textbf{droite vectorielle} dirigée par $\vectu$ l'ensemble des vecteurs colinéaires à $\vectu$ :
		\[ \mathcal{D}_{\vectu} = \left \{ \lambda \vectu, ~\lambda \in \R \right \} \]
	\item \textbf{droite} (ou \textbf{droite affine}) passant par $A$ et dirigée par $\vectu$ l'ensemble des points $M$ de l'espace tels que $\V{AM}$ et $\vectu$ sont colinéaires :
		\[ \mathcal{D}_{A,\vectu} = \left \{ M~\text{du plan},\quad\exists~\lambda \in \R,\quad \V{AM}=\lambda \vectu \right \} \]
		$\vectu$ est appelé \textbf{vecteur directeur} de la droite.
	\item droite passant par $A$ et $B$ la droite passant par $A$ et dirigée par $\V{AB}$.
\end{itemize}
}

\definition{[Vocabulaire] ~\begin{itemize}[label=\textbullet]
	\item Deux droites sont dites \textbf{parallèles} si leurs vecteurs directeurs sont colinéaires.
	\item Deux droites sont \textbf{perpendiculaires} si leurs vecteurs directeurs sont orthogonaux.
\end{itemize}
}

\definition{Soit $\mathcal{D}$ la droite passant par $A$ et de vecteur directeur $\vectu$. On appelle \textbf{vecteur normal} de la droite $\mathcal{D}$ tout vecteur orthogonal à $\vectu$.}

	\subsection{Représentations paramétriques}

\begin{theoreme}
Soit $\mathcal{D}$ la droite passant par $A(x_A;y_A;z_A)$ et de vecteur directeur $\vectu\matrice{a\\b\\c}$ non nul. Alors $M(x;y;z)$ est sur la droite $\mathcal{D}$ si et seulement 
\[ \left \{ \begin{array}{ccl} x&=&x_A+ta\\y&=&y_A+tb\\z&=&z_A+tc \end{array}\right. \quad \text{avec}\quad t\in \R \]
Cette représentation est appelée \textbf{système d'équations paramétriques}.	
\end{theoreme}


\preuve{$M(x;y)$ est sur la droite $\mathcal{D}$ si et seulement si $\V{AM}$ et $\vectu$ sont colinéaires, c'est-à-dire s'il existe $t\in \R$ tel que $\V{AM}=t\vectu$, ce qui s'écrit 
\[ \left \{ \begin{array}{ccl} x&=&x_A+ta\\y&=&y_A+tb\\z&=&z_A+tc \end{array}\right. \quad \text{avec}\quad t\in \R \]
}

\exemple{Soit $\mathcal{D}$ la droite, passant par $A(1;2;3)$ et de vecteur directeur $\vectu\matrice{1\\-1\\1}$. Alors $\mathcal{D}$ a comme système d'équations paramétriques
\[ \left \{ \begin{array}{ccl} x&=&1+t\\y&=&2-t\\z&=&3+t \end{array}\right. \quad t\in \R \]
}

	\subsection{Intersection de plans et représentation cartésienne}

\begin{proposition}
Soient deux plans $\mathcal{P}$ et $\mathcal{P}'$ de vecteur normal respectif $\V{n}$ et $\V{n'}$. Alors, on dispose de trois cas possibles :
\begin{itemize}[label=\textbullet]
	\item soit les deux plans sont d'intersection vide : ils sont strictement parallèles, et $\V{n}$ et $\V{n}'$ sont colinéaires et $\V{n}\wedge \V{n'}=\V{0}$.
	\item soit $\mathcal{P}$ et $\mathcal{P}'$ sont confondus, et $\V{n}$ et $\V{n}'$ sont colinéaires et $\V{n}\wedge \V{n'}=\V{0}$.
	\item soit $\mathcal{P}$ et $\mathcal{P}'$ se coupent en une droite dirigée par $\V{n}\wedge \V{n'} \neq \V{0}$.
\end{itemize}	
\end{proposition}

\definition{Un système d'équations cartésiennes d'une droite est formé des équations $ax+by+cz+d=0$ et $a'x+b'y+c'z+d=0$ de deux plans non parallèles contenant cette droite.}

\begin{methode}
Pour déterminer l'intersection de deux plans, on s'intéresse à leurs vecteurs normaux. S'ils ne sont pas colinéaires, leur intersection est une droite, dont on obtient un système d'équations cartésiennes.	
\end{methode}


\exemple{Soient $\mathcal{P}_1:4x-2z+1=0,\quad \mathcal{P}_2:  -2x+z=0$ et $\mathcal{P}_3:x+y+z=2$. Déterminer les intersections deux à deux de ces trois plans.}

\begin{prof}
\solution{Les vecteurs normaux respectifs de $\mathcal{P}_1,\mathcal{P}_2$ et $\mathcal{P}_3$ sont $\V{n}_1\matrice{4\\0\\-2}$, $\V{n}_2\matrice{-2\\0\\1}$ et $\V{n}_3\matrice{1\\1\\1}$.
\begin{itemize}[label=\textbullet]
	\item $\V{n}_1=-2\V{n}_2$. Donc les plans $\mathcal{P}_1$ et $\mathcal{P}_2$ sont parallèles (et non confondues).
	\item $\V{n}_1$ et $\V{n}_3$ ne sont pas colinéaires, donc les plans $\mathcal{P}_1$ et $\mathcal{P}_3$ sont sécants en une droite.
	\item $\V{n}_2$ et $\V{n}_3$ ne sont pas colinéaires, donc les plans $\mathcal{P}_2$ et $\mathcal{P}_3$ sont sécants en une droite.
\end{itemize}

}	
\end{prof}


\lignes{18}

\begin{methode}
Pour déterminer un système d'équations cartésiennes d'une droite passant par $A$ et de vecteur directeur $\vectu$, on écrit que $M(x;y;z)$ est sur cette droite si et seulement si $\V{AM}\wedge \vectu=\V{0}$. On obtiendra trois équations cartésiennes, dont l'une est redondante. On en choisit deux pour obtenir le système d'équations cartésiennes.	
\end{methode}


\exercice{Déterminer un système d'équations cartésiennes de la droite passant par $A(1;0;1)$ et de vecteur directeur $\vectu\matrice{1\\1\\1}$.}

\begin{prof}
\solution{$M(x;y;z)$ est sur la droite passant par $A(1;0;1)$ et dirigée par $\vectu\matrice{1\\1\\1}$ si et seulement si $\V{AM}\wedge\vectu=\V{0}$, soit
\[ \left \{\begin{array}{lll} y-(z-1)&=&0\\z-1-(x-1)&=&0\\x-1-y&=&0\end{array} \right. \]
Ainsi, un système d'équations cartésiennes de la droite cherchée est
\[ \left \{\begin{array}{lll}y-z+1&=&0\\z-x&=&0 \end{array}\right. \]
}	
\end{prof}

\lignes{10}



	\subsection{Distance d'un point à une droite}

La formule de la distance d'un point à une droite est légèrement différente :	
	
\begin{proposition}
Soit une droite $\mathcal{D}$, passant par $A$ et de vecteur directeur $\vectu$. Soit $M$ un point du plan. Alors,
	\[ d(M,\mathcal{D}) = \frac{||\vectu\wedge \V{AM}||}{||\vectu||} \]	
\end{proposition}


\exemple{Soient $\mathcal{D}$ la droite passant par $A(1;2;3)$ et de vecteur directeur $\vectu\matrice{1\\-1\\1}$, et $M(1;0;2)$. Déterminer la distance de $M$ à la droite $\mathcal{D}$.}

\begin{prof}
\solution{Alors
\[ \V{AM}\matrice{0\\-2\\-1} \qeq \vectu\wedge \V{AM}\matrice{3\\1\\-2}  \]
et donc
\[ d(A;\mathcal{D}) = \frac{||\vectu\wedge \V{AM}||}{||\vectu||}=\frac{\sqrt{3^2+1^2+(-2)^2}}{\sqrt{1^2+(-1)^2+1^2}}=\frac{\sqrt{14}}{\sqrt{3}}=\sqrt{\frac{14}{3}} \]
}

\end{prof}

\lignes{8}

\section{Sphères}

Dans l'ensemble de cette section, on se place dans un \ron[3].

	\subsection{Définition}

\definition{Soit $A$ un point de l'espace. La \textbf{sphère} $\mathcal{S}_{A,r}$ de centre $A$ et de rayon $r\geq 0$ est l'ensemble des points de l'espace à une distance $r$ de $A$ :
\[ \mathcal{C}_{A,r} = \left \{ M ~ \text{de l'espace},~AM=r \right \} \]
}

\remarque{On ne confondra pas la sphère avec la boule. La boule de centre $A$ et de rayon $r$ est l'ensemble des points $M$ de l'espace à une distance inférieure ou égale à $r$ de $A$.\\On rappelle que le volume d'une boule est $\mathcal{V}_\mathcal{B} = \frac{4}{3}\pi r^3$.}

\remarque{Soit $A(x_A;y_A;z_A)$ un point de l'espace. $M(x;y;z)$ est sur la sphère, de centre $A$ et de rayon $r$ si et seulement si $AM=r$, c'est-à-dire
\[ (x-x_A)^2+(y-y_A)^2+(z-z_A)^2=r^2 \]
Cette écriture est appelée \textbf{équation cartésienne} de la sphère.}

\definition{[Equation cartésienne] Toute sphère du plan admet une équation du type $x^2+y^2+z^2+ax+by+cz+d=0$. Réciproquement, tout ensemble admettant une équation du type $x^2+y^2+z^2+ax+by+cz+d=0$ est une sphère.}

\exemple{La sphère de centre $A(1;1;3)$ et de rayon $1$ a pour équation cartésienne $(x-1)^2+(y-1)^2+(z-3)^2=1^2$, c'est-à-dire $x^2+y^2+z^2-2x-2y-6y+10=0$.}

\begin{methode}
Connaissant l'équation cartésienne d'une sphère, on obtient le centre et le rayon en mettant les différents carrés sous forme canonique, comme pour l'équation cartésienne d'un cercle.	
\end{methode}


\exercice{Déterminer le centre et le rayon de la sphère d'équation cartésienne $x^2+y^2+z^2-2x+4y+2z-10=0$.}

\begin{prof}
\solution{En mettant sous forme canonique :
\begin{eqnarray*}
	x^2+y^2+z^2-2x+4y+2z-9=0 &\Leftrightarrow& x^2-2x + y^2+4y + z^2+2z - 10 = 0 \\
		&\Leftrightarrow& (x-1)^2-1 + (y+2)^2-4+(z+1)^2-1 - 10 = 0 \\
		&\Leftrightarrow& (x-1)^2+(y+2)^2+(z+1)^2 = 16=4^2
\end{eqnarray*}
Il s'agit donc de la sphère de centre $A(1;-2;-1)$ et de rayon $4$.}
\end{prof}


\lignes{10}

