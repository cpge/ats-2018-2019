\objectif{Dans ce chapitre, on reprend le chapitre \emph{Equations différentielles} déjà vu en ajoutant des méthodes pour des équations différentielles particulières.}

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
	\item Concernant les équations différentielles d'ordre $2$  :
			\begin{itemize}[label=\textbullet]
				\item connaître la définition\dotfill $\Box$
				\item connaître les conditions d'application du théorème de Cauchy\dotfill $\Box$
				\item savoir résoudre une équation homogène connaissant une solution particulière\dotfill $\Box$
			\end{itemize}
	\item Concernant les systèmes d'équations différentielles :
			\begin{itemize}[label=\textbullet]
				\item connaître la structure des solutions\dotfill $\Box$
				\item savoir résoudre dans le cas où la matrice associée est diagonalisable\dotfill $\Box$
				\item connaître le lien entre équation linéaire d'ordre $n$ et système d'équations différentielles\dotfill $\Box$
			\end{itemize}
\end{numerote}

\newpage 

Dans l'ensemble de ce chapitre, on note $\K=\R$ ou $\C$.

\section{Equations différentielles scalaire d'ordre 2}

	\subsection{Définition}
	
\definition{Soit $I$ un intervalle de $\R$, et $a, b, c$ trois fonctions continues sur $I$ à valeurs dans $\K$.
L'équation $(E)\quad y''+a(x)y'+b(x)y=c(x)$ est appelée équation différentielle linéaire scalaire d'ordre $2$.

L'équation $(H)\quad y''+a(x)y'+b(x)y=0$ est appelée équation linéaire homogène associée à $(E)$.
}

\remarque{Les solutions de $(E)$ ou $(H)$ sont donc nécessairement de classe $\CC^2$.}

\exemple{L'équation $y''+xy'+x^2y=\cos(x)$ est une équation différentielle linéaire scalaire d'ordre $2$, d'équation homogène associée $y''+xy'+x^2y=0$.}

	\subsection{Solutions de l'équation homogène}
	
\begin{proposition}[Principe de superposition] 
Soit $I$ un intervalle de $\R$, et $a, b, c$ trois fonctions continues sur $I$ à valeurs dans $\K$.
Soit $(E)\quad y''+a(x)y'+b(x)y=c(x)$  et $(H)$ son équation homogène associée.

Alors on obtient toutes les solutions $(E)$ en ajoutant à une solution générale de l'équation $(H)$ une solution particulière quelconque de $(E)$.
\end{proposition}

\remarque{Ainsi, pour résoudre $(E)$, il ``suffit'' de résoudre $(H)$ de manière générale, et de trouver une solution particulière quelconque de $(E)$.}

\begin{proposition}
Soit $I$ un intervalle de $\R$, et $a, b, c$ trois fonctions continues sur $I$ à valeurs dans $\K$.

L'ensemble des solutions définie sur $I$ de l'équation homogène $(H)\quad y''+a(x)y'+b(x)y=0$ est un espace vectoriel de dimension $2$.	
\end{proposition}


\preuve{Admis.}

\remarque{On ne dispose pas de méthode générale pour résoudre l'équation homogène associée. Nous verrons, avec les séries entières, une méthode pour obtenir certaines solutions.}

	\subsection{Résolution connaissant une solution de $(H)$}
	
Dans le cas où on  arrive à obtenir une solution particulière ne s'annulant pas de l'équation homogène, on peut obtenir l'ensemble des solutions de l'équation homogène.

\begin{methode}[Méthode de Lagrange]
Soit $I$ un intervalle de $\R$, et $a, b, c$ trois fonctions continues sur $I$ à valeurs dans $\K$.
Soit $(H)\quad y''+a(x)y'+b(x)y=0$ et $h$ une solution de $(H)$ ne \textbf{s'annulant pas} sur $I$.

\begin{numerote}
	\item On pose $y(x) = h(x) z(x)$.
	\item On remplace $y$, $y'$ et $y''$ dans l'équation $(E)$.
	\item On obtient alors une équation de fonction inconnue $z$, qu'on peut résoudre en posant $Z=z'$.
\end{numerote}
\end{methode}

\exercice{Soit $(H)\quad : x^2y''(x)+xy'(x)-4y(x)=0$ définie sur $I=]0;+\infty[$.
\begin{enumerate}
	\item Montrer que $h:x\mapsto x^2$ est solution de $(H)$.
	\item Déterminer alors toutes les solutions de $(H)$.
\end{enumerate}
}

\begin{prof}
\solution{$h$ est bien de classe $\CC^2$ sur $I$.
\begin{enumerate}
	\item On constate que, pour tout $x>0$ : $x^2h''(x)+xh'(x)-4h(x)=x^2(2)+x(2x)-4(x^2)=0$. Ainsi $h$ est bien solution de $(H)$.
	\item On pose alors $y(x)=z(x)x^2$, où $z$ est de classe $\CC^2$ sur $I$. On a alors
\[ y'(x)=z'(x)x^2+2xz(x) \qeq y''(x)=z''(x)x^2+2z'(x)x+2z(x)+2xz'(x)=z''(x)x^2+4z'(x)x+2z(x) \]
et en remplaçant dans $(H)$ :
\begin{eqnarray*}
	x^2y''(x)+xy'(x)-4y(x)&=&x^2(z''(x)x^2+4z'(x)x+2z(x)) + x(z'(x)x^2+2xz(x))-4z(x)x^2 \\
	&=& x^4z''(x)+5z'(x)x^3
\end{eqnarray*}
Ainsi, $y$ est solution de $(H)$ si et seulement si $x^4z''(x)+5z'(x)x^3=0$, soit encore $Z'+\frac{5}{x}Z=0$, en posant $Z=z'$. L'ensemble des solutions de cette équation est 
\[ S_1 = \left \{ x\mapsto  k\eu{-5\ln(x)} = \frac{k}{x^5},\quad k\in \R \right \} \]
Ainsi, $y$ est solution de $(H)$ si et seulement si $Z \in S_1$, soit si et seulement si
\[ z \in  S_2 = \left \{ x\mapsto -\frac{k}{4x^4} +k', \quad (k,k') \in \R^2 \right \} \]
et donc si et seulement si
\[ y \in S_3 = \left \{ x\mapsto x^2( -\frac{k}{4x^4} +k'), \quad (k,k') \in \R^2 \right \} \]

\textbf{Bilan} : l'ensemble des solutions de $(H)$ est donc
\[ \boxed{S_3= \left \{ x\mapsto -\frac{k}{4x^2} +k'x^2, \quad (k,k') \in \R^2 \right \} }\]
\end{enumerate}
}	
\end{prof}

\lignes{34}
\lignes{10}


	\subsection{Problème de Cauchy}
	
\begin{proposition}
Soit $I$ un intervalle de $\R$, et $a, b, c$ trois fonctions continues sur $I$ à valeurs dans $\K$. Soit $(E)$ l'équation $y''+a(x)y'+b(x)y=c(x)$. On se donne $x_0\in I$, et $(a,b)\in \K^2$.

Alors il existe une unique solution $y$ de $E$, définie sur $I$, vérifiant $y(x_0)=a$ et $y'(x_0)=b$.	
\end{proposition}


\remarque{Ainsi, si on impose une valeur et le coefficient directeur en un point, la courbe intégrale définie sur $I$ est unique.}


\section{Systèmes différentiels linéaires d'ordre 1 à coefficients constants}

Dans la suite, $\K$ désigne $\R$ ou $\C$.

	\subsection{Définition}

\definition{Soit $I$ un intervalle de $\R$, et $n\geq 2$. On appelle \textbf{système différentiel linéaire} d'ordre $1$ homogène à coefficients constants un système du type
\[ (S)\quad X' = A X \text{ avec } X=\matrice{x_1\\\vdots\\x_n} \qeq X'=\matrice{x_1'\\\vdots\\v_n} \]
où $A$ est une matrice de $\MM_n(\K)$
 et $x_1,\hdots,x_n$ sont des fonctions dérivables sur $I$ à valeurs dans $\K$.}

\exemple{Le système suivant est un système différentiel linéaire d'ordre $1$ homogène à coefficients constants :
\[ \left \{ \begin{array}{ccccc} x'(t)&=&2x(t)&+&3y(t)\\ y'(t)&=&-3x(t)&+&y(t)\end{array}\right. \]
En effet, celui-ci peut s'écrire $X'=AX$ avec $X=\matrice{x\\y}$ et $A=\matrice{2&3\\-3&1}$.
}

\definition{On appelle \textbf{solution} de $(S)$ une fonction $X:I\rightarrow \K^n$ vérifiant, dérivable sur $I$, vérifiant \[ \forall~t\in I,\quad X'(t)=AX(t) \]}

Ainsi, résoudre un système différentiel linéaire d'ordre $1$, c'est trouver toutes les fonctions $X:I\rightarrow \K^n$ dérivable vérifiant le système d'équations différentielles.

	\subsection{Structure des solutions}

Tout comme dans le cas des équations du premier ordre homogène, les solutions forment un espace vectoriel.

\begin{theoreme}
Soit $I$ un intervalle de $\R$ et $n\geq 2$. Soit $(S):X'=AX$ avec $X=\matrice{x_1\\\vdots\\x_n}$ et $A\in \MM_n(\K)$. Alors l'ensemble des solutions $\mathcal{S}$ de $(S)$ forme un espace vectoriel de dimension $n$.	
\end{theoreme}


\preuve{Montrons que $\mathcal{S}$ est un sous-espace vectoriel de $\mathcal{F}(I,\K^n)$. Soient $X,  Y$ deux solutions de $(S)$, et $\lambda$ un réel. Alors, on a
\[ A(\lambda X+Y)=\lambda AX+AY = \lambda X'+Y'= (\lambda X+Y)' \]
Ainsi, $\lambda X + Y\in \mathcal{S}$.

On admet que ce sous-espace vectoriel est de dimension $n$.}

Puisque celui-ci est de dimension $n$, on dispose alors d'une unicité de la solution en imposant $n$ valeur :

\begin{theoreme}[Théorème de Cauchy] 
Soit $(S): X'=AX$, avec $X:I\rightarrow \K^n$ et $A\in \MM_n(\K)$. Soit $(\alpha_1,\hdots,\alpha_n) \in \K^n$ $n$ éléments fixés, et $t_0\in I$. Alors, il existe une unique solution $X=\matrice{x_1\\\vdots\\x_n}$ au problème de Cauchy $(S)$ vérifiant $x_1(t_0)=\alpha_1,\hdots,x_n(t_0)=\alpha_n$.	
\end{theoreme}


\preuve{En effet, l'ensemble des solutions de $(S)$ étant de dimension $n$, il y a une unique solution si on impose $n$ conditions distinctes.}

	\subsection{Résolution dans le cas où $A$ est diagonalisable}

Pour résoudre un système différentiel linéaire d'ordre $1$ homogène à coefficients constants, on va effectuer un changement de fonctions inconnues en diagonalisant la matrice $A$.

\begin{proposition}
Soit $a_1,\cdots,a_n \in \K^n$. Soit $(S)$ le système $X'=DX$ avec $D=\matrice{a_1&0&\cdots&0 \\0&a_2&\cdots&0\\\vdots&\vdots & & \vdots\\0&0&\cdots&a_n}$, c'est-à-dire 
\[ (S): \left \{ \begin{array}{ccc}x_1'&=&a_1x_1\\x_2'&=&a_2x_2\\\cdots & & \\x_n'&=&a_nx_n \end{array}\right. \]
Alors $(S)$ se résout facilement en résolvant chacune des équations :
\[ x_1:t\mapsto \alpha_1\eu{a_1t},x_2:t\mapsto \alpha_2\eu{a_2t},\cdots,x_n:t\mapsto \alpha_n\eu{a_nt} \quad \text{avec} \quad (\alpha_1,\cdots,\alpha_n)\in \K^n \]
et donc
\[ \mathcal{S}=\left \{ t\mapsto \matrice{\alpha_1\eu{a_1t}\\\vdots\\\alpha_n\eu{a_nt}},\quad (\alpha_1,\cdots, \alpha_n)\in \K^n \right \} \]	
\end{proposition}

	
\preuve{En effet, dans ce cas, il s'agit de $n$ équations du premier ordre à coefficients constants, que l'on sait résoudre.}

\exercice{Résoudre le système $(S):X'=AX$, avec $A=\matrice{1&0\\0&2}$. Trouver l'unique solution vérifiant $X(0)=\matrice{1\\-1}$.
}

\begin{prof}
\solution{On note $X=\matrice{x\\y}$ avec $(x,y)$ de classe $\CC^1$ sur $\R$. Le système $(S)$ s'écrit 
	\[\systeme*{x'=x,y'=2y}\]
	et a donc comme solution
	\[ \mathcal{S} = \left \{ t\mapsto \matrice{a\E^t\\b\eu{2t}},\quad (a,b)\in \R^2 \right \} \]
	Avec $X(0)=\matrice{1\\-1}$, cela donne donc $a=1$ et $b=-1$. Ainsi, l'unique solution de $(S)$ vérifiant $X(0)=\matrice{1\\-1}$ est 
	\[ t\mapsto \matrice{\E^t\\-e^{2t}}\]
}	
\end{prof}

\lignes{15}

L'idée, dans le cas général, est donc de se ramener à un système diagonale comme précédemment.

\begin{proposition}
Soit $A \in \MM_n(\K)$ que l'on suppose diagonalisable, et $(S):X'=AX$. On écrit $A=PDP^{-1}$, où $D$ est une matrice diagonale. Alors, en posant $Y=P^{-1}X$, $Y$ est dérivable si et seulement si $X$ et dérivable et on a l'équivalence
\[ X \text{ solution de } (S) \Leftrightarrow Y \text{ solution de } Y'=DY \]
Ainsi, résoudre $(S)$ revient à résoudre le système diagonal $Y'=DY$.	
\end{proposition}


\begin{methode}
Supposons $A$ diagonalisable.
\begin{numerote}
	\item On diagonalise $A$ sous la forme $A=PDP^{-1}$, où $D$ est diagonale et en ne calculant pas $P^{-1}$.
	\item On pose $Y=P^{-1}X$ et on résout le système équivalent $Y'=DY$ de fonction inconnue $Y$.
	\item On revient à $X$ : $Y$ est solution de $Y'=DY$ si et seulement si $X=PY$ est solution de $X'=A$. Ainsi
\[ \mathcal{S} = \left \{ PY,\quad Y \text{ solution de }Y'=DY \right \} \]	
\end{numerote}	
\end{methode}


\exercice{Résoudre le système \[ (S) : \left \{ \begin{array}{ccccc} x'&=&4x&-&2y\\y'&=&x&+&y\end{array}\right. \]
}

%\begin{prof}
%AFIRE	
%\end{prof}

\lignes{40}
\lignes{27}


	\subsection{Exemple dans le cas où $A$ est trigonalisable}

Si la matrice n'est pas diagonalisable, mais qu'elle est trigonalisable, on peut tout de même résoudre, on ``remontant'' le système d'équations différentielles.

\exercice{Soit $(S)$ le système \[ (S) : \left \{ \begin{array}{ccccccc} x' &=& 2x& -&y&+& 2z \\y'&=&10x& -&5y&+& 7z \\z'&=&4x& -&2y&+ &2z \end{array}\right. \]
\begin{enumerate}
	\item Montrer que $-1$ et $0$ sont valeurs propres. Déterminer une base $e_1$ de $E_{-1}$ et $e_2$ de  $E_0$, (on prendra des vecteurs dont la première coordonnée non nulle vaut $1$).
	\item Montrer que $\mathcal{B}=(e_1,e_2,e_3)$ avec $e_3=\matrice{0\\1\\1}$ forme une base de $\R^3$.
	\item En notant $P$ la matrice de passage de la base canonique à $\mathcal{B}$, montrer que $T=P^{-1}AP$ est triangulaire.
	\item Résoudre le système $Y'=TY$.
	\item En déduire les solutions de $(S)$.
\end{enumerate}
}

\solution{~\begin{enumerate}
	\item On note $A=\matrice{2&-1&2\\10&-5&7\\4&-2&2}$. Puisqu'on nous donne des valeurs propres, on déterminer directement $E_{-1}$ et $E_0$. Après calcul, on obtient :
		\[ E_{-1} =\Vect\left( \matrice{1\\-1\\-2}\right) \qeq E_0=\Vect\left( \matrice{1\\2\\0}\right) \]
	\item On note donc $e_1=\matrice{1\\-1\\-2}$ et $e_2=\matrice{1\\2\\0}$.
	Puisque le cardinal de $(e_1,e_2,e_3)$ est égal à la dimension de $\R^3$, il suffit de montrer, par méthode classique, que la famille $(e_1,e_2,e_3)$ est libre. Or, si on prend $a,b,c\in \R^3$ tels que $ae_1+be_2+ce_3=0$, on obtient alors \[ \matrice{a+b\\-a+2b+c\\-2a+c}=\matrice{0\\0\\0} \]
	ce qui donne rapidement $a=b=c=0$. La famille est libre de bon cardinal, donc forme une base de $\R^3$.
	\item On a donc $P=\matrice{1&1&0\\-1&2&1\\-2&0&1}$. Après calcul, \[ P^{-1}=\matrice{2&-1&1\\-1&1&-1\\4&-2&3} \]
	 et après calculs \[ P^{-1}\times A \times B = \matrice{-1&0&0\\0&0&1\\0&0&0}\]
	 \item On pose $Y=\matrice{u\\v\\w}$. Le système $Y'=TY$ s'écrit alors \[ \left \{ \begin{array}{ccc} u' &=& -u \\ v' &=& w \\ w' &=& 0\end{array}\right.\]
	 On résout par remontées successives : $w:t\mapsto a$, avec $a\in \R$. Puis $v'=a$ et donc $v:t\mapsto at+b$ avec $b\in \R$. Enfin, $u'=-u$ nous donne $u:t\mapsto c\eu{-t}$, avec $c\in \R$. 
	 \bilan{L'ensemble des solutions de $Y'=TY$ est \[ \mathcal{S}_Y=\left \{ t\mapsto \matrice{c\eu{-t}\\at+b\\a},\quad (a,b,c)\in \R^3 \right \} \]}
	 \item En posant $Y=P^{-1}X$, le système $X'=AX$ s'écrit $Y'=TY$, que l'on vient de résoudre. Pour trouver l'ensemble des solutions de $(S)$, il suffit alors de calculer $X=PY$. On obtient alors :
	  \[ \boxed{\mathcal{S} = \left \{ t\mapsto \matrice{c\eu{-t}+at+b\\-c\eu{-t}+2at+2b+a\\-2c\eu{-t}+a},\quad (a,b,c)\in \R^3\right \}} \]
\end{enumerate}}	


\lignes{50}
\lignes{23}

	\subsection{Relation entre équation différentielle d'ordre $n$ et système d'équations différentielles d'ordre $1$}
	
On peut interpréter une équation différentielle homogène à coefficients constants d'ordre $n$ comme un système d'équations différentielles d'ordre $1$ que l'on peut donc résoudre par les méthodes précédentes.

\begin{proposition}
Soit $(E)$ l'équation différentielle $a_ny^{(n)}+a_{n-1}y^{(n-1)}+\hdots+a_1y'+a_0y=0$, avec \\$(a_0,\hdots, a_n)\in \K^{n+1}$ et $a_n\neq 0$. Alors, résoudre $(E)$ revient à résoudre le système $X'=AX$ avec
\[ X=\matrice{y^{(n-1)}\\y^{(n-2)}\\\vdots\\y} \qeq A=\matrice{-\frac{a_{n-1}}{a_n}&-\frac{a_{n-2}}{a_n}& -\frac{a_{n-3}}{a_n}&\hdots &-\frac{a_1}{a_n} &-\frac{a_0}{a_n}\\1&0&0&\hdots&0&0\\0&1&0&\hdots&0&0\\\vdots&\vdots&\vdots&&\vdots\\ 0&0&0&\hdots&1&0} \in \MM_n(\K) \]	
\end{proposition}


\exemple{[Important] Soit l'équation différentielle homogène à coefficients constants $(E) : ay''+by'+cy=0$, avec $a\neq 0$.
\begin{enumerate}
	\item Montrer que $y$ est solution de $(E)$ si et seulement si $X=\matrice{y'\\y}$ est solution de $X'=AX$, avec $A \in \MM_2(\R)$ à déterminer.
	\item Montrer que le polynôme caractéristique est un multiple de $aX^2+bX+c$.
	\item Dans le cas où $\Delta=b^2-4ac>0$, terminer la résolution du système $X'=AX$, et retrouver le résultat connu sur les équations du second ordre homogène à coefficients constants.
\end{enumerate}
}

%\begin{prof}
%\solution{A faire}	
%\end{prof}

\lignes{50}
\lignes{32}

\section{Equations non linéaires}

Dans certains cas, et avec certaines méthodes, on peut résoudre des équations non linéaires, par exemple une équation du type $y'=y^2$.

\begin{proposition}
[Equations de Bernoulli]	
Dans le cas d'une équation du type $(E) : y'=ay+by^\alpha$ (où $a$ et $b$ sont des fonctions), la seule fonction s'annulant est la fonction nulle (d'après le théorème de Cauchy). Ainsi, toute solution non nulle ne s'annule pas, et on peut écrire, en divisant par $y^\alpha$ :
\[ (E) : y^{-\alpha}y' = ay^{1-\alpha}+b \]
que l'on peut résoudre en changeant de fonction inconnue et en posant $z=y^{1-\alpha}$.	
\end{proposition}


\exercice{Résoudre l'équation $xy'+xy+y^2\E^x=0$ sur $]0;+\infty[$.}

\begin{prof}
\solution{On utilise la méthode proposée. On peut écrire sur $\R^*_+$ l'équation précédente sous la forme $\ds{y'=-y-y^2\frac{\E^x}{x}}$. Posons alors $z=y^{1-2}=\frac{1}{y}$. On a alors $y=\dfrac{1}{z}$, $y'=-\dfrac{z'}{z^2}$. Ainsi, $y$ est solution sur $\R^*_+$ si et seulement si 
	\[ -\frac{z'}{z^2} = -\frac{1}{z}-\frac{1}{z^2}\frac{\E^x}{x}\]
	soit encore
	\[ z'=z+\frac{\E^x}{x} \] 
	L'équation homogène a pour solution $x\mapsto k\E^x$, avec $k\in \R$. Utilisons la méthode de la variation de la constante pour déterminer une solution particulière de l'équation générale. On prend $k$ de classe $\CC^1$ et on cherche une solution sous la forme $x\mapsto k(x)\E^x$. En injectant, on obtient 
	\[ k'(x)\E^x=\frac{\E^x}{x} \Leftrightarrow k'(x)=\frac{1}{x} \]
	Ainsi, une solution particulière est $x\mapsto \ln(x)\E^x$.
	
	\textbf{Bilan} : les solutions de l'équation $\ds{z'=z+\frac{\E^x}{x} }$ sont \[ \mathcal{S}_z=\left \{ x\mapsto \ln(x)\E^x+k\E^x,\quad k \in \R \right \} \]
	Et puisque $y=\dfrac1z$ on en déduit que les solutions de l'équation de départ sont :
	\[ \mathcal{S}=\left \{ x\mapsto \frac{1}{\ln(x)\E^x+k\E^x}=\frac{\eu{-x}}{\ln(x)+k},\quad k\in \R \right \} \] 
}
\end{prof}

\lignes{50}
\lignes{40}

\section{Méthode d'Euler}

La méthode d'Euler permet de déterminer une représentation graphique approchée d'une solution d'une équation différentielle en calculant des points de proche en proche.

\begin{methode}
Soit l'équation différentielle $y'=g(x,y)$. Soit $f$ une solution de cette équation différentielle, avec une condition intiale $f(0)=b$.
On construite alors de proche en proche $f$ en utilisant l'approximation affine
\[ f(a+h)\approx f(a)+hf'(a) \]
pour $h$ suffisamment petit.

Par exemple, $f(0+h)=f(h)\approx f(0)+hf'(0)=b+hg(0,b)$. On obtient ainsi $f(h)$. On peut alors calculer $f(h+h)=f(2h) \approx f(h)+hf'(h)$ et continue ainsi.	
\end{methode}


En Scilab, on peut utiliser une boucle  \lstinline{for} (lorsqu'on impose le nombre de points que l'on veut), ou \lstinline{while} si l'on souhaite s'arrêter à une certaine abscisse :

\begin{scilab}[Méthode d'Euler]
{\small \lstinputlisting[]{tex/Chap23/sce/Methode_euler.sce}}
\end{scilab}

ce qui donne la figure suivante :
\begin{center}\includegraphics[width=10cm]{tex/Chap23/sce/23_courbe_euler}\end{center}