\objectif{Dans ce chapitre, on s'intéresse aux fonctions non pas d'une variable, mais de plusieurs variables. On introduira la notion d'application $\CC^1, \CC^2$, de gradient, et on étudiera plus en détail les fonctions de deux variables. On verra enfin le lien avec les surfaces.}

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
	\item Sur les généralités  :
			\begin{itemize}[label=\textbullet]
				\item \hyperref[objectif-26-1]{connaître les notions de base de topologie}\dotfill $\Box$
				\item \hyperref[objectif-26-2]{connaître les définitions de limite et de continuité dans le cas de fonctions de plusieurs variables}\dotfill $\Box$
				\item \hyperref[objectif-26-3]{connaître la notion de lignes de niveaux}\dotfill $\Box$
			\end{itemize}
	\item Concernant la notion de classe $\CC^1$ :
			\begin{itemize}[label=\textbullet]
				\item \hyperref[objectif-26-4]{connaître la définition des dérivées partielles}\dotfill $\Box$
				\item \hyperref[objectif-26-5]{savoir déterminer les dérivées d'une somme, d'une composée, d'un produit}\dotfill $\Box$
				\item \hyperref[objectif-26-6]{connaître la notion de point critique et de plan tangent}\dotfill $\Box$
			\end{itemize}
	\item Concernant la notion de classe $\CC^2$ :
			\begin{itemize}[label=\textbullet]
				\item \hyperref[objectif-26-7]{connaître la définition de fonctions de classe $\CC^2$}\dotfill $\Box$
				\item \hyperref[objectif-26-8]{connaître le théorème de Schwarz}\dotfill $\Box$

			\end{itemize}
	\item \hyperref[objectif-26-9]{Savoir étudier les points critiques d'une fonction de deux variables réelles}\dotfill $\Box$
	\item Concernant les surfaces :
			\begin{itemize}[label=\textbullet]
			\item \hyperref[objectif-26-10]{Connaître la définition d'une surface}\dotfill $\Box$
			\item \hyperref[objectif-26-11]{Savoir déterminer le plan tangent et la normale d'une surface}\dotfill $\Box$
	\end{itemize}
\end{numerote}

\newpage 

\section{Généralités}

	\label{objectif-26-1}\subsection{Topologie de $\R^2$}

Dans l'ensemble de ce chapitre, l'espace vectoriel $\R^n$ est muni du produit scalaire usuel, ainsi que de la norme et la distance associées :
\[ \forall~x\in \R^n,\quad \|x\|=\sqrt{x_1^2+\cdots+x_n^2} \qeq \forall~(x,y)\in (\R^n)^2,\quad d(x,y)=\|y-x\| = \sqrt{(y_1-x_1)^2+\cdots+(y_n-x_n)^2} \]

\definition{Soit $a\in \R^n$ et $r\in \R^+$. 
	\begin{itemize}[label=\textbullet]
		\item 	On appelle boule ouverte de centre $a$ et de rayon $r$, notée $B(a,r)$, l'ensemble
\[ B(a,r)=\left \{ x\in \R^n,\quad \|x-a\| < r \right \}\]
		\item On appelle boule fermée de centre $a$ et de rayon $r$, notée $B_f(a,r)$, l'ensemble
\[ B(a,r)=\left \{ x\in \R^n,\quad \|x-a\| \leq r \right \}\]
	\end{itemize}
}

\exemple{Dans $\R^2$, la boule ouverte et la boule fermée de centre $A\in \R^2$ et de rayon $r$ se représentent de la manière suivante :
\begin{center}
	\begin{tabular}{cc}
	\input{tex/Chap26/pic/bouleouverte} & \input{tex/Chap26/pic/boulefermee}\\
	$B(A,r)$ & $B_f(A,r)$
	\end{tabular}
\end{center}
}

\remarque{La boule ouverte $B(A,r)$ est incluse dans la boule fermée $B_f(A,r)$. Si $s<r$, alors $B(A,s) \subset B(A,r)$.}

\definition{Une partie $O$ de $\R^n$ est dite ouverte si elle est vide, ou si, pour tout $x\in O$, il existe $r>0$ telle que $B(x,r)\subset O$. Ainsi, un ouvert est une réunion, éventuellement infinie, de boules ouvertes.}

\remarque{Ainsi, si $O$ est un ouvert, pour tout $x\in O$ on peut trouver une boule incluse dans $O$. C'est ce qu'on va utiliser régulièrement dans ce chapitre, de manière équivalente aux intervalles ouverts dans le cas des fonctions d'une variable.}

\exemple{Toute boule ouverte de $\R^n$ est un ouvert de $\R^n$.}

\definition{Un ensemble $F$ de $\R^n$ est dit fermé si son complémentaire dans $\R^n$, $\R^n\backslash F$ est un ouvert.}

\exemple{Toute boule fermée de $\R^n$ est un fermé de $\R^n$.}

\remarque{L'ensemble vide $\emptyset$ et $\R^n$ tout entier sont à la fois des ouverts et des fermés.}

\definition{Un ensemble $A$ de $\R^n$ est dit borné s'il existe une boule de rayon $r$ contenant $A$ : $A\subset B(a,r)$.}

\exemple{Toute boule (ouverte ou fermée) de $\R^n$ est bornée dans $\R^n$.}

	\subsection{Fonctions de plusieurs variables}

Dans la suite de ce chapitre, on va s'intéresser aux fonctions de plusieurs variables, c'est-à-dire aux fonctions définies sur $\R^n$ (ou une partie de $\R^n$) et à valeurs dans $\R$.

\definition{Soit $f$ une fonction définie sur un ouvert $U$ de $\R^n$ et à valeurs dans $\R$. On appelle graphe de $f$ l'ensemble des points $\mathcal{G}$ de $\R^{n+1}$ définie par 
\[ \mathcal{G} = \left \{ (x_1,\cdots, x_n,z)\in U\times \R,\quad z=f(x_1,x_2,\cdots,x_n) \right \} \]
}

\remarque{Ainsi, le graphe d'une fonction de $2$ variables réelles est une partie de l'espace.}

\exemple{Par exemple, le graphe de la fonction définie sur $\R^2$ par $f(x,y)=x^2-y^2$ est le suivant :
\begin{center}
\includegraphics[width=10cm]{tex/Chap26/pic/exempleselle}	
\end{center}

}

\definition{On appelle \textbf{fonctions polynomiales} de $\R^n$ toute somme et produit finis de fonction $(x_1,\cdots, x_n)\mapsto x_i^k$, où $k$ est un entier.}

\exemple{Les fonctions $(x,y)\mapsto x^2-y^2$ et $(x_1,x_2,x_3)\mapsto x_1^2x_2+x_2x_3^2$ sont polynomiales.} 

	\label{objectif-26-3}\subsection{Lignes de niveaux}

\definition{Soit $f$ une fonction définie sur un ouvert $U$ de $\R^n$ à valeurs réelles. Pour tout réel $\lambda$, on appelle \textbf{courbe de niveau} $\lambda$ l'ensemble des éléments $a\in U$ vérifiant $f(a)=\lambda$.}

\exemple{En reprenant l'exemple de la fonction $f:(x,y)\mapsto x^2-y^2$, voici les lignes de niveaux pour $\lambda \in \{-0.5,-0.4,-0.3,-0.2,-0.1,0.1,0.2,0.3,0.4,0.5 \}$ :

\begin{center}
	\begin{tabular}{cc}
		\includegraphics[width=8cm]{tex/Chap26/pic/niveau1} & \includegraphics[width=8cm]{tex/Chap26/pic/niveau2}	
	\end{tabular}
\end{center}

}

En Scilab, on utilise les instructions \verb-plot3d- et \verb-contour- pour tracer le graphe d'une fonction de deux variables, ainsi que les lignes de niveaux.

\begin{scilab}[Représentation graphique fonction de deux variables]
{\small \lstinputlisting[]{tex/Chap26/pic/plotetcontour.sci}}
\end{scilab}


	\label{objectif-26-2}\subsection{Limite et continuité sur $\R^n$}
	
L'idée est de généraliser la définition de continuité à des fonctions définies non pas sur $\R$ ou un intervalle de $\R$, mais à des fonctions définies sur $\R^n$ ou un ouvert de $\R^n$.

\definition{[Limite d'une fonction]Soit $f$ une fonction définie sur un ouvert $U$ de $\R^n$ à valeurs réelles. Soit $a \in U$. On dit que $f$ \textbf{tend vers $0$ lorsque $x\in U$ tend vers $a$} si et seulement si
\[ \forall~\eps>0,~\exists~\delta>0,\quad \|x-a\|<\delta \implies |f(x)|<\eps \]
On note alors $\ds{\lim_{x\rightarrow a} f(x)=0}$. \\
On dit également que $f$ tend vers $l\in \R$ quand $x$ tend vers $a$ si $f-l$ tend vers $0$.}

\remarque{Il s'agit exactement de la même définition que celle des fonctions d'une variable réelle, en utilisant la norme euclidienne sur $\R^n$.}

\definition{[Continuité d'une fonction] Soit $f$ une fonction définie sur un ouvert $U$ de $\R^n$ à valeurs réelles. Soit $a \in U$. $f$ est \textbf{continue} en $a$ si et seulement si $\ds{\lim_{x\rightarrow a} f(x)=f(a)}$.\\La fonction $f$ est dite \textbf{continue} sur $U$ si elle est continue en tout point de $U$.}

\begin{proposition}
Les fonctions polynomiales sont continues sur $\R^n$.	
\end{proposition}

 
\propriete{La somme et le produit de deux fonctions continues sur $U$ sont continues sur $U$. \\
Le quotient de deux fonctions continues sur $U$ dont le dénominateur ne s'annule pas est continue sur $U$.}

\propriete{Soit $U$ un ouvert de $\R^n$ et $I$ un intervalle de $\R$. Soient $f:U\rightarrow I$ et $g:I\rightarrow \R$ deux fonctions continues, respectivement sur $U$ et sur $I$. Alors $g\circ f$ est continue sur $U$.}

Les fonctions de plusieurs variables continues vérifient le même résultat que les fonctions continues sur un segment : elles sont bornées et atteignent leurs bornes.

\begin{theoreme}[Théorème de Weierstrass] 
Toute fonction continue sur un ensemble fermé et borné de $\R^n$ est bornée et atteint ses bornes.	
\end{theoreme}


\remarque{Un ensemble fermé et borné de $\R^n$ est appelé un \textbf{compact} de $\R^n$.}

\section{Applications $\CC^1$}

	\label{objectif-26-4}\subsection{Dérivées partielles d'ordre 1}	

On va utiliser la notion de dérivée vue dans le cas des fonctions d'une variable pour les fonctions de plusieurs variables.

\definition{[Dérivée partielle d'ordre 1]Soit $U$ un ouvert de $\R^n$, et $f:U\rightarrow \R$ une fonction. Soit $a=(a_1,\cdots,a_n)$ un point de $U$. On appelle \textbf{dérivée partielle d'ordre $1$ de $f$ en $a$  par rapport à la $i$-ième variable} la dérivée, si elle existe, de la fonction $f_i:x\mapsto f(a_1,\cdots,a_{i-1},x,a_{i+1},\cdots,a_n)$, que l'on note $\partial_i(f)(a)$ . Ainsi, si elle existe
\[ \partial_i(f)(a) = \lim_{x\rightarrow a_i} \frac{f(a_1,\cdots,a_{i-1},x,a_{i+1},\cdots a_n) - f(a_1,\cdots, a_n)}{x-a_i} \]
On la note également $\ds{\frac{\partial f}{\partial x_i}(a)}$. Dans le cas d'une fonction de deux variables, on notera souvent $\partial_1 f$ sous la forme $\ds{\frac{\partial f}{\partial x}}$ et $\partial_2 f$ sous la forme $\ds{\frac{\partial f}{\partial y}}$.
}

\definition{[Dérivées partielles d'ordre $1$] La fonction qui à tout $a\in U$ associe $\partial_i f(x,y)$, lorsqu'il existe, est appelée \textbf{dérivée partielle d'ordre $1$} de $f$ par rapport à la $i$-ième variable.}

\begin{methode}
En pratique, pour calculer les dérivées partielles d'ordre $1$ par rapport à la $i$-ième variable, on considère que toutes les autres variables sont des constantes, et on dérive par rapport à $x_i$.	
\end{methode}


\exemple{\label{ex1} Soit $f:\R^2\rightarrow \R$ définie par $f(x,y)=x^2y+xy^2$. Calculer les dérivées partielles d'ordre $1$ de $f$.}

\begin{prof}
\solution{$f$ est polynomiale en toutes les variables, donc admet des dérivées partielles en tout point. On a alors, pour tout $(x,y)\in \R^2$ :
\[ \partial_1 f(x,y)=\frac{\partial f}{\partial x}(x,y)=2xy+y^2 \qeq \partial_2 f(x,y)=\frac{\partial f}{\partial y}(x,y) = x^2+2xy \]
}
\end{prof}

\lignes{8}

\exercice{Soit $f$ la fonction définie par $f(x,y)=\sqrt{x^2+y^2}$.
\begin{enumerate}
	\item Montrer que $f$ est définie sur $\R^2$ et y est continue.
	\item Déterminer les dérivées partielles de $f$ en tout point de $\R^2\backslash \{0,0\}$.
\end{enumerate} 
}

\begin{prof}
\solution{à faire}	
\end{prof}


\lignes{15}

	\subsection{Gradient}

\definition{Soit $f$ une fonction définie sur un ouvert $U$ de $\R^n$, à valeurs dans $\R$ et admettant des dérivées partielles d'ordre $1$. On appelle \textbf{gradient} de $f$ en $a\in U$ le vecteur colonne de $\MM_{n,1}(\R)$, noté $\vv{\nabla} f(a)$ ou $\vv{\text{grad}} f(a)$, défini par
\[ \vv{\nabla} f(a)=\matrice{\partial_1f(a)\\\partial_2f(a)\\\vdots\\\partial_nf(a) } \]
}

\exemple{Dans l'exemple \ref{ex1}, pour tout $(x,y)\in \R^2$, on a
\[ \vv{\nabla} f (x,y) = \matrice{2xy+y^2\\x^2+2xy} \]}

Le vecteur gradient joue le même rôle que le vecteur directeur de la tangente dans le cas d'une fonction d'une variable réelle : il est perpendiculaire à la courbe.

\begin{proposition}
Soit $f$ une fonction définie sur un ouvert $U$ de $\R^n$, à valeurs dans $\R$ et admettant des dérivées partielles d'ordre $1$. Soit $a \in U$ vérifiant $\vv{\nabla}f(a)\neq \vv{0}$. \\Alors le vecteur $\vv{\nabla}f(a)$ est orthogonal à la courbe de niveau $f(a)$ au point $a$.	
\end{proposition}


\remarque{\danger~On ne peut rien affirmer quant au comportement local lorsque le gradient est nul, contrairement au cas des fonctions d'une variable.}


	\subsection{Fonctions de classe $\CC^1$}

\definition{Soit $f$ une fonction définie sur un ouvert $U$ de $\R^n$, à valeurs dans $\R$. On dit que $f$ est de classe $\CC^1$ si elle admet des dérivées partielles d'ordre $1$, et que chacune de ces dérivées partielles est continue sur $U$.}


On obtient le même théorème que pour les fonctions d'une variable réelle :

\begin{proposition}
Toute fonction de classe $\CC^1$ est continue.	
\end{proposition}

\exercice{Soit $f$ la fonction définie par $\ds{f(x,y)=\frac{\ln(x)}{y}+\frac{\ln(y)}{x}}$. Déterminer le domaine de définition $U$ de $f$, puis montrer que $f$ est de classe $\CC^1$ sur $U$.}

\begin{prof}
\solution{a faire}	
\end{prof}

\lignes{20}

On dispose des opérations suivantes :

\begin{proposition}[Somme, produit, quotient] 
Toute combinaison linéaire et tout produit de fonctions de classe $\CC^1$ sur un ouvert $U$ de $\R^n$ sont des fonctions de classe $\CC^1$ sur $U$.\\Le quotient de deux fonctions de classe $\CC^1$ sur $U$ dont le dénominateur ne s'annule pas est de classe $\CC^1$ sur $U$.	
\end{proposition}

	\label{objectif-26-5}\subsection{Composition de fonctions}
	
On dispose de deux résultats sur la composition de fonctions :

\begin{proposition}[Composition] 
$f$ une fonction de classe $\CC^1$ sur un ouvert $U$ de $\R^2$, à valeurs réelles. Soient $x$ et $y$ deux fonctions de classe $\CC^1$ sur un intervalle $I$ de $\R$, et telle que pour tout $t\in I$, on a $(x(t), y(t))\in U$.

Alors la fonction $g:I \rightarrow I$ définie pour tout $t\in I$ par $g(t)=f(x(t),y(t))$ est de classe $\CC^1$ sur $I$, et on a 
\[ \forall~t\in I,\quad g'(t)=x'(t)\frac{\partial f}{\partial x}(x(t),y(t))
+y'(t)\frac{\partial f}{\partial y}(x(t),y(t)) \]	
\end{proposition}


\remarque{Ainsi, avec les notations de Leibniz et un peu d'abus de notation 
\[ \frac{df}{dt} = \frac{dx}{dt}\cdot \frac{\partial f}{\partial x} + \frac{dy}{dt}\cdot \frac{\partial f}{\partial y} \]
}

\exemple{Soit $f:\R^2\rightarrow \R$ une fonction de classe $\CC^2$. Pour tout réel $t$, on note $g(t)=f(t,t^2)$. Montrer que $g$ est de classe $\CC^1$ sur $\R$ et déterminer $g'$.}

\begin{prof}
\solution{a faire}	
\end{prof}

\lignes{20} 

Ce résultat se généralise au cas de composition plus complexe :

\begin{proposition}
$f$ une fonction de classe $\CC^1$ sur un ouvert $U$ de $\R^2$, à valeurs réelles. Soient $x$ et $y$ deux fonctions de classe $\CC^1$ sur un ouvert $V$ de $\R^2$, telles que pour tout $(u,v)\in V$, on a $(x(u,v),y(u,v))\in U$. Alors la fonction $g:(u,v)\mapsto f(x(u,v), y(u,v))$ est de classe $\CC^1$ sur $V$, et on a
\[ \forall~(u,v)\in V,\quad \frac{\partial g}{\partial u}= \frac{\partial x}{\partial u}\cdot \frac{\partial f}{\partial x} +  \frac{\partial y}{\partial u}\cdot \frac{\partial f}{\partial y} \qeq 
\frac{\partial g}{\partial v}= \frac{\partial x}{\partial v}\cdot \frac{\partial f}{\partial x} +  \frac{\partial y}{\partial v}\cdot \frac{\partial f}{\partial y} \]	
\end{proposition}


\remarque{En notant $dg =\matrice{\frac{\partial g}{\partial u}\\\frac{\partial g}{\partial v}}$ et $df =\matrice{\frac{\partial f}{\partial x}\\\frac{\partial f}{\partial y}}$ les deux gradients, on a
\[ dg = J df \quad \text{avec}\quad J=\matrice{\frac{\partial x}{\partial u}&\frac{\partial x}{\partial v}\\\frac{\partial y}{\partial u}&\frac{\partial y}{\partial v} }\]
}

\definition{La matrice $J=\matrice{\frac{\partial x}{\partial u}&\frac{\partial x}{\partial v}\\\frac{\partial y}{\partial u}&\frac{\partial y}{\partial v}}$ est appelé la matrice jacobienne du changement de variables $x=x(u,v)$ et $y=y(u,v)$.}

\exemple{[Changement en coordonnées polaires] Soit $f$ une fonction de classe $\CC^1$ sur $\R^2$. On pose $g(r,\theta)=f(r\cos \theta, r\sin \theta)$ pour $(r,\theta)\in \R^+\times \R$. Déterminer $\vv{\nabla} g$ en fonction de $\vv{\nabla} f$, et exprimer
 $\frac{\partial f}{\partial x}(r\cos(\theta),r\sin(\theta))$ et $\frac{\partial f}{\partial y}(r\cos(\theta),r\sin(\theta))$ en fonction de $\frac{\partial g}{\partial r}$ et $\frac{\partial g}{\partial \theta}$.}

\begin{prof}
\solution{Les fonctions $x:(r,\theta)\mapsto r\cos(\theta)$ et $y:(r,\theta)\mapsto r\sin(\theta)$ sont de classe $\CC^1$ par produits de fonctions de classe $\CC^1$. Par théorème de composition:
\begin{eqnarray*}
	\frac{\partial g}{\partial r} &=& \frac{\partial x}{\partial r}\cdot \frac{\partial f}{\partial x}+\frac{\partial y}{\partial r}\cdot \frac{\partial f}{\partial y} \\
	&=& \cos(\theta)\frac{\partial f}{\partial x}+\sin(\theta)\frac{\partial f}{\partial y}\\
	\frac{\partial g}{\partial \theta} &=& \frac{\partial x}{\partial \theta}\cdot \frac{\partial f}{\partial x}+\frac{\partial y}{\partial \theta}\cdot \frac{\partial f}{\partial y} \\
	&=& -r\sin(\theta)\frac{\partial f}{\partial x} + r\cos(\theta)\frac{\partial f}{\partial y}
\end{eqnarray*}
Ainsi
\[ \vv{\nabla} g(r,\theta) = \matrice{\cos(\theta)&-r\sin(\theta)\\\sin(\theta)&r\cos(\theta)} \vv{\nabla f} (r\cos(\theta),r\sin(\theta))\]
Puis, en exprimant $\frac{\partial f}{\partial x}$ et $\frac{\partial f}{\partial y}$:

\[ \frac{\partial f}{\partial x}(r\cos(\theta),r\sin(\theta)) = \cos(\theta) \frac{\partial g}{\partial r} - \frac{1}{r}\sin(\theta) \frac{\partial g}{\partial \theta} \]
\[ \frac{\partial f}{\partial y}(r\cos(\theta),r\sin(\theta)) = \sin(\theta) \frac{\partial g}{\partial r} + \frac{1}{r}\cos(\theta) \frac{\partial g}{\partial \theta} \]
ce qui s'écrit encore, en prenant comme base $\vv{u}=\cos(\theta)\vv{i}+\sin(\theta)\vv{j}$ et $\vv{v}=-\sin(\theta)\vv{i}+\cos(\theta)\vv{j}$
\[ \vv{\nabla}f(r\cos(\theta),r\sin(\theta)) = \frac{\partial g}{\partial r}(r,\theta)\vv{u} +\frac{1}{r} \frac{\partial g}{\partial \theta}(r,\theta) \vv{v} \]
}	
\end{prof}

\lignes{20}


	\subsection{Développement limité d'ordre $1$}
	
On obtient un développement limité d'ordre $1$ de la même forme que pour les fonctions d'une variable réelle :

\begin{theoreme}
Soit $f$ une fonction de classe $\CC^1$ sur un ouvert $U$ de $\R^n$. Soit $a=(a_1,\cdots,a_n)\in U$. Alors pour tout $h=(h_1,\cdots, h_n) \in \R^n$ tels que $(a_1+h_1,\cdots, a_n+h_n)\in U$, on a
\[ f(a+h) = f(a_1,\cdots,a_n) + \sum_{i=1}^n \partial_if(a)\cdot h_i + o_0(\|h\|) \]	
\end{theoreme}


Le résultat précédent se ré-écrit :

\definition{L'égalité précédente est le développement limité à l'ordre $1$ de $f$ au point $a$. Il est \textbf{unique} et peut également s'écrire 
\[ f(a+h)=f(a) + \,^t\vv{\nabla}f(a)\cdot \matrice{h_1\\\vdots\\h_n} + o_0(\| h \|) \]
}

\remarque{Si le gradient est nul en un point, le développement limité précédent n'apporte aucune information. Dans ce cas, il faut obtenir des informations à l'ordre $2$.}

\definition{Soit $f$ une fonction définie sur un \textbf{ouvert} $U$ de $\R^n$ à valeurs dans $\R$ et admettant des dérivées partielles d'ordre $1$. On dit que $a\in U$ est un \textbf{point critique} de $f$ si $\vv{\nabla}f(a)=\vv{0}$, c'est-à-dire
\[ \forall~i \in \ll 1,n\rr,\quad \partial_if(a)=0 \]}

	\label{objectif-26-6}\subsection{Plan tangent}

Dans le cas d'une surface définie implicitement par $z=f(x,y)$, si le point n'est pas critique, on a vu que le gradient au point $(x,y)$ est orthogonal à la ligne de niveau $f(x,y)$. Tout comme, dans le cas $y=f(x)$, on a introduit la notion de tangente, on va introduire ici la notion de plan tangent :
   
\definition{Soit $f$ une fonction définie sur un \textbf{ouvert} $U$ de $\R^2$ à valeurs dans $\R$ de classe $\CC^1$. Soit $(a,b)\in U$. On appelle \textbf{plan tangent} à la surface $\mathcal{S}$ d'équation $z=f(x,y)$ au point $(a,b)$ le plan, passant par $(a,b,f(a,b))$ et de vecteur normal $\matrice{\partial_1 f(a,b)\\\partial_2 f(a,b)\\-1}$ si celui-ci n'est pas nul.

Une équation du plan tangent au point $(a,b)$ est alors \[ z=\partial_1 f(a,b)(x-a)+\partial_2 f(a,b)(y-b)+f(a,b) \]}

\exemple{Pour $f:(x,y)\mapsto x^2+y^2$, au point $(1,1)$, le gradient vaut $(2,2)$, et le plan tangent a donc pour équation 
\[ z=2(x-1)+2(y-1)+f(1,1)=2x+2y-2 \]

\begin{center}
	\includegraphics[width=10cm]{tex/Chap26/pic/tangent}
\end{center}
}
\section{Applications $\CC^2$}

	\subsection{Dérivées partielles d'ordre $2$.}
	
\definition{Soit $f$ une fonction définie sur un \textbf{ouvert} $U$ de $\R^n$ à valeurs dans $\R$ et admettant des dérivées partielles d'ordre $1$.

Si la fonction $\partial_i(f)$ admet elle-même une dérivée partielle d'ordre $1$ par rapport à la $j$-ième variable sur $U$, alors on dit que $f$ possède une dérivée partielle d'ordre $2$ par rapport à la $i$-ième variable puis par rapport à la $j$-ième variable, et on la note $\partial_{j,i}^2(f)$ ou $\ds{\frac{\partial^2 f}{\partial x_j \partial x_i}}$.

Dans le cas d'une fonction de deux variables, on notera 
\[ \partial^2_{1,1}f = \frac{\partial^2 f}{\partial x^2},\quad \partial^2_{2,1}f = \frac{\partial^2 f}{\partial y \partial x},\quad \partial^2_{1,2}f = \frac{\partial^2 f}{\partial x \partial y} \qeq \partial^2_{2,2}f = \frac{\partial^2 f}{\partial y^2} \]
}

\exemple{Soit $f$ la fonction définie par $f(x,y)=(x+y)\left( \frac1x + \frac1y \right)$ pour tout couple $(x,y)$ dans l'ouvert $U=]0,+\infty[^2$. Montrer que $f$ admet des dérivées partielles d'ordre 2 et les déterminer.}

\begin{prof}
\solution{ a faire}	
\end{prof}

\lignes{10}

\definition{[Matrice Hessienne]
Soit $f$ une fonction définie sur un \textbf{ouvert} $U$ de $\R^n$, à valeurs dans $\R$ et admettant des dérivées partielles d'ordre $2$. On appelle \emph{hessienne} de $f$ en $(a_1,\cdots,a_n)$ la matrice de $\MM_{n}(\R)$ notée $\nabla^2(f)(a)$, de coefficient $h_{ij}=\partial_{ij}f(a)$.
}

	\label{objectif-26-7}\subsection{Fonctions de classe $\CC^2$}
	
\definition{Soit $f$ une fonction définie sur un ouvert $U$ de $\R^n$, à valeurs dans $\R$. On dit que $f$ est de classe $\CC^2$ si elle admet des dérivées partielles d'ordre $2$, et que chacun de ces quatre dérivées partielles est continue sur $U$.}

\remarque{Ainsi $f$ est de classe $\CC^2$ si et seulement si elle possède des dérivées partielles d'ordre $1$ sur $U$, et que celles-ci sont de classe $\CC^1$ sur $U$.}

Les fonctions de classe $\CC^2$ vérifient une propriété très importante :

\label{objectif-26-8}\begin{theoreme}[Théorème de Schwarz] 
Soit $f$ une fonction de classe $\CC^2$ sur un ouvert $U$ de $\R^n$. Alors, pour tout $i,j \in \ll 1,n \rr^2$, on a
\[ \partial^2_{i,j}f = \partial^2_{j,i}f \]	
\end{theoreme}

\remarque{\danger~Le théorème de Schwarz est faux si $f$ n'est pas une fonction de classe $\CC^2$ !}

Si la fonction $f$ est de classe $\CC^2$, la matrice hessienne vérifie une propriété essentielle :

\begin{proposition}
Soit $f$ une fonction de classe $\CC^2$ sur un ouvert $U$ de $\R^2$, à valeurs dans $\R$. Alors sa matrice hessienne en tout point est symétrique, et donc diagonalisable.	
\end{proposition}


\remarque{Nous utiliserons ce résultat pour étudier les points critiques.}

\definition{[Laplacien] Soit $f$ une fonction de classe $\CC^2$ sur un ouvert $U$ de $\R^n$, à valeurs dans $\R$. On appelle \textbf{laplacien} de $f$ en $a\in U$ le réel
\[ \Delta f(a) = \sum_{i=1}^n \frac{\partial^2 f}{\partial x_i^2}(a) \]
}

\remarque{Le laplacien correspond à la trace de la matrice hessienne.}

	\subsection{Développement limité d'ordre $2$}
	
\begin{theoreme}
Soit $f$ une fonction de classe $\CC^2$ sur un ouvert $U$ de $\R^n$. Soit $a=(a_1,\cdots,a_n)\in U$. Alors pour tout $h=(h_1,\cdots, h_n) \in \R^n$ tels que $(a_1+h_1,\cdots, a_n+h_n)\in U$, on a
\[ f(a+h)=f(a)+\sum_{i=1}^n h_i\frac{\partial f}{\partial x_i}(a) + \frac{1}{2}\sum_{i=1}^n \sum_{j=1}^n h_ih_j\frac{\partial^2 f}{\partial x_i\partial x_j}(a)+ o_0(\|h\|^2) \]	
\end{theoreme}


\remarque{Si $a$ est un point critique, le résultat précédent permet d'obtenir des informations locales quant au comportement de $f$ au voisinage de ce point.}

\section{Extrema d'une fonction de deux variables réelles}

	\subsection{Extremum local d'une fonction}
	
\definition{Soit $f$ une fonction définie sur un ouvert $U$ de $\R^2$. Soit $M_0 \in U$. On dit que $f$ admet
\begin{itemize}[label=\textbullet]
	\item un maximum local en $M_0$ s'il existe un réel $r>0$ tel que \[ f(x,y) \leq f(M_0) \quad \text{pour tout }(x,y)\in B(M_0,r) \]
	Le maximum est dit global si l'inégalité précédente est vraie sur tout $U$.
	\item un minimum local en $M_0$ s'il existe un réel $r>0$ tel que \[ f(x,y) \geq f(M_0) \quad \text{pour tout }(x,y)\in B(M_0,r) \]
	Le minimum est dit global si l'inégalité précédente est vraie sur tout $U$.
\end{itemize}}

Comme dans le cas réel, un extremum local est un point critique :

\begin{proposition}[Condition nécessaire d'extremum] 
Soit $f$ une fonction définie sur un ouvert $U$ de $\R^2$, et y admettant des dérivées partielles d'ordre $1$. \\Si $f$ admet un extremum local en $M_0$, alors $M_0$ est un point critique de $f$.	
\end{proposition}

\remarque{\danger~Attention : comme pour le cas d'une variable réelle, cette condition est nécessaire, mais n'est pas suffisante.}

\exemple{Soit $f:\R^2 \rightarrow \R$ définie par $f(x,y)=x^3$. Montrer que $(0,0)$ est un point critique de $f$, mais que $f$ n'admet pas d'extremum local en $0$.}

\begin{prof}
$f$ est $\CC^1$ sur $\R^2$ car polynomiale. On a rapidement, pour tout $(x,y)\in \R^2$ : \[ \frac{\partial f}{\partial x}(x,y) = 3x^2 \qeq \frac{\partial f}{\partial x}(x,y) = 0 \]
En $(0,0)$ les dérivées s'annulent et $(0,0)$ est donc bien un point critique. Or, pour tout $(x,y)\in \R^2$ : \[ f(x,y)>0 \text{ si } x >0, \quad f(x,y)<0 \text{ si } x<0 \]
Ainsi, il ne peut y avoir en $(0,0)$ ni un maximum local, ni un minimum local.
\end{prof}

\lignes{8}

	\subsection{Etude des points critiques}
	
Dans le cas de fonctions d'une variable réelle, on avait le résultat ``si $f'$ s'annule en $x_0$ en changeant de signe, alors $f$ admet un extremum local en $x_0$''. On va obtenir le même genre de résultat dans le cas de fonctions de deux variables réelles.

\remarque{Si $f$ est de classe $\CC^2$ sur un ouvert $U$ de $\R^2$, et si $M_0=(x_0,y_0)\in U$ est un point critique,  alors le développement limité à l'ordre $2$ au voisinage de $M_0$ s'écrit =
\[ f(x_0+h,y_0+k)=f(x_0,y_0)+0+\frac{1}{2}\left(h^2\frac{\partial^2f}{\partial x^2} + hk \frac{\partial^2f}{\partial x\partial y} + kh \frac{\partial^2f}{\partial y\partial x}+k^2\frac{\partial^2f}{\partial y^2} \right)+o_0(\| (h,k) \|^2) \]
soit, puisque $f$ est de classe $\CC^2$, en utilisant le théorème de Schwarz :
\[ f(x_0+h,y_0+k)=f(x_0,y_0)+\frac{1}{2}\left(h^2\frac{\partial^2f}{\partial x^2} + 2hk \frac{\partial^2f}{\partial x\partial y} +k^2\frac{\partial^2f}{\partial y^2}\right) +o_0(\| (h,k) \|^2) \]
}

\notation{[Notations de Monge] Si $f$ est de classe $\CC^2$ sur un ouvert $U$ de $\R^2$, et si $M_0=(x_0,y_0)\in U$ est un point critique,  on note en général
\[ r=\frac{\partial^2 f}{\partial x^2}(x_0,y_0),\quad s=\frac{\partial^2 f}{\partial x\partial y}(x_0,y_0) \qeq t=\frac{\partial^2 f}{\partial y^2}(x_0,y_0) \]
}

\remarque{Avec les notations de Monge, le développement limité s'écrit 
\[ f(x_0+h,y_0+k)=f(x_0,y_0)+\frac{1}{2}(h^2r + 2hks +k^2t) +o_0(\| (h,k) \|^2) \]
En prenant $k\neq 0$, on peut alors écrire
\[ f(x_0+h,y_0+k)=f(x_0,y_0)+\frac{1}{2}k^2\left(\left(\frac{h}{k}\right)^2r + 2\frac{h}{k}s +t\right) +o_0(\| (h,k) \|^2) \]
On peut alors s'intéresser au polynôme $rX^2+2sX+t$, de discriminant $\Delta=4s^2-4rt$. On obtient alors le résultat suivant :
}

\begin{theoreme}
Soit $f$ une fonction de classe $\CC^2$ sur un ouvert $U$ de $\R^2$, et $M_0=(x_0,y_0)\in U$  un point critique. On note
\[ r=\frac{\partial^2 f}{\partial x^2}(x_0,y_0),\quad s=\frac{\partial^2 f}{\partial x\partial y}(x_0,y_0) \qeq t=\frac{\partial^2 f}{\partial y^2}(x_0,y_0) \]\begin{itemize}[label=\textbullet]
	\item Si $rt-s^2>0$ :
		\begin{itemize}
			\item Si $r>0$, alors $\left(\frac{h}{k}\right)^2r + 2\frac{h}{k}s +t>0$ : $(x_0,y_0)$ est un minimum local.
			\item Si $r<0$, alors $\left(\frac{h}{k}\right)^2r + 2\frac{h}{k}s +t<0$ : $(x_0,y_0)$ est un maximum local.
		\end{itemize}
	\item Si $rt-s^2<0$ : $\left(\frac{h}{k}\right)^2r + 2\frac{h}{k}s +t$ change de signe : $(x_0,y_0)$ n'est ni minimum, ni maximum local. $(x_0,y_0)$ est appelé \textbf{point selle} ou \textbf{point col}.
	\item Si $rt-s^2=0$, on ne peut pas conclure.
\end{itemize}	
\end{theoreme}


\begin{center}
	\begin{tabular}{ccc}
		\includegraphics[width=5cm]{tex/Chap26/pic/minimumlocal}&		\includegraphics[width=5cm]{tex/Chap26/pic/maximumlocal} & 
					\includegraphics[width=5cm]{tex/Chap26/pic/selle}\\
		$rt-s^2>0 \qeq r>0$ & $rt-s^2>0\qeq r<0$ & $rt-s^2<0$
	\end{tabular}
\end{center}


\label{objectif-26-9}\begin{methode}
Pour étudier la nature d'un point critique :
\begin{numerote}
	\item On détermine les points critiques en cherchant les points d'annulation du gradient.
	\item On calcule, pour chacun des points critiques, $r, s$ et $t$, puis on calcule $rt-s^2$.
	\item On conclut en fonction du signe quant à la nature locale du point critique.	
\end{numerote}	
\end{methode}


\exercice{Soit $f:\R^2\rightarrow \R$ définie par $f(x,y)=x^2+y^2$. Etudier les points critiques de $f$.}

\solution[20]{
$f$ est de classe $\CC^2$ sur $\R^2$ car polynomiale. Pour tout $(x,y)$ : \[ \frac{\partial f}{\partial x}(x,y)=2x \qeq \frac{\partial f}{\partial y}(x,y)=2y \]
Il n'y a donc qu'un seul point critique : $(0,0)$. Or \[ \frac{\partial^2 f}{\partial x^2}(x,y)=2,\quad \frac{\partial^2 f}{\partial y\partial x}(x,y)=\frac{\partial^2 f}{\partial x\partial y}(x,y)=0 \qeq \frac{\partial^2 f}{\partial y^2}(x,y)=2 \]
Ainsi, en $(0,0)$ : $r=2, s=0$ et $t=2$. Alors $rt-s^2=4>0$ et $r>0$ : il y a donc un minimum local en $(0,0)$ qui vaut $f(0,0)=0$. D'ailleurs, celui-ci est un minimum global car pour tout $(x,y)\in \R^2$, $f(x,y)\geq 0=f(0,0)$.
}

\exercice{Soit $f:\R^2\rightarrow \R$ définie par $f(x,y)=x^2-y^2$. Etudier les points critiques de $f$.}

\solution[20]{
$f$ est de classe $\CC^2$ sur $\R^2$ car polynomiale. Pour tout $(x,y)$ : \[ \frac{\partial f}{\partial x}(x,y)=2x \qeq \frac{\partial f}{\partial y}(x,y)=-2y \]
Il n'y a donc qu'un seul point critique : $(0,0)$. Or \[ \frac{\partial^2 f}{\partial x^2}(x,y)=2,\quad \frac{\partial^2 f}{\partial y\partial x}(x,y)=\frac{\partial^2 f}{\partial x\partial y}(x,y)=0 \qeq \frac{\partial^2 f}{\partial y^2}(x,y)=-2 \]
Ainsi, en $(0,0)$ : $r=2, s=0$ et $t=-2$. Alors $rt-s^2=-4<0$ : il y a donc un point selle en $(0,0)$.
}

\exercice{Soit $f:\R^2\rightarrow \R$ définie par $f(x,y)=xy$. Etudier les points critiques de $f$.}

\solution[20]{
$f$ est de classe $\CC^2$ sur $\R^2$ car polynomiale. Pour tout $(x,y)$ : \[ \frac{\partial f}{\partial x}(x,y)=y \qeq \frac{\partial f}{\partial y}(x,y)=x \]
Il n'y a donc qu'un seul point critique : $(0,0)$. Or \[ \frac{\partial^2 f}{\partial x^2}(x,y)=0,\quad \frac{\partial^2 f}{\partial y\partial x}(x,y)=\frac{\partial^2 f}{\partial x\partial y}(x,y)=1 \qeq \frac{\partial^2 f}{\partial y^2}(x,y)=0 \]
Ainsi, en $(0,0)$ : $r=0, s=1$ et $t=à$. Alors $rt-s^2=-1<0$ : il y a donc un point selle en $(0,0)$.
}
	\subsection{Extrema globaux}

Le théorème de Weierstrass garantit qu'une fonction continue sur un compact est bornée et atteint ses bornes. Ainsi, une fonction continue sur un compact admet un minimum  et un maximum global.

\begin{methode}
Pour démontrer qu'un extremum local atteint en $(x_0,y_0)$ est global ou non, on calcule $f(x,y)-f(x_0,y_0)$ et on essaie de factoriser pour obtenir le signe de ce terme.	
\end{methode}

\exercice{Soit $f$ la fonction définie par $f(x,y)=\sqrt{1-x^2-y^2}$ sur $D=B_f(O,1)$. On rappelle que $D$ est fermé et borné. 
\begin{enumerate}
\item
Montrer que $f$ est bornée et atteint ses bornes sur $D$.
\item
Déterminer l'unique point critique de $f$ sur $B(O,1)$, et étudier sa nature.
\item
Quel est l'ensemble de points $\Gamma$ pour lesquels $f$ est susceptible d'admettre un  minimum local ?
\item
Vérifier que $f$ admet un minimum global en tous les points de $\Gamma$.
\end{enumerate}
}

\solution[66]{~
\begin{enumerate}
	\item $(x,y)\mapsto 1-x^2-y^2$ est continue sur $D$ car polynomiale et positive sur ce domaine. De plus, la fonction racine est continue sur $\R^+$. Par composée, $f$ est continue sur $D$, qui est un compact. D'après le théorème de Weierstrass, $f$ est bornée et atteint ses bornes sur $D$.
	\item $f$ est de classe $\CC^2$ sur $B(O,1)$ (par composée, et car $(x,y)\mapsto 1-x^2-y^2 > 0$ sur la boule ouverte). Pour tout $(x,y)\in B(O,1)$ : 
		\[ \frac{\partial f}{\partial x}(x,y) = \frac{-x}{\sqrt{1-x^2-y^2}} \qeq  \frac{\partial f}{\partial y}(x,y) = \frac{-y}{\sqrt{1-x^2-y^2}}  \]
	Le seul point critique est donc en $(0,0)$.\\ 
	Déterminons les valeurs de $r, s$ et $t$ en $(0,0)$. Après calcul, on obtient : \[ r=\frac{\partial^2 f}{\partial x^2}(0,0)=-1,\quad s= \frac{\partial^2 f}{\partial x\partial y}(0,0)=0 \qeq t=\frac{\partial^2 f}{\partial y^2}(0,0)=-1 \]
	On a alors $rt-s^2=1>0$ et $r<0$ : il y a donc un maximum local qui vaut $1$ au point $(0,0)$. On voit rapidement, de part la définition de $f$ qu'il s'agit d'un maximum global.
	\item Puisqu'il n'y a pas d'autres points critiques sur $B(O,1)$, les seuls points en lesquels il peut y avoir un minimum local est sur la sphère unité, c'est-à-dire : \[ \Gamma = \left \{ (x, y)\in \R^2,\quad x^2+y^2=1 \right \} \]
	\item Si $(x,y)\in \Gamma$, alors $f(x,y)=0$. Clairement, pour tout $(x,y)\in D$, $f(x,y)\geq 0$ donc en tout point de $\Gamma$, il y a un minimum global.  
\end{enumerate}
}	


\section{Surface}

	\subsection{Définition}
	
\label{objectif-26-10}\definition{Soient $x, y$ et $z$ trois fonctions de classe $\CC^2$ sur un ouvert $U$ de $\R^2$.

L'ensemble des points $\mathcal{S}$ de l'espace vérifiant
\[ \left \{ \begin{array}{lcl} x &=& x(u,v)\\y&=&y(u,v)\\z&=&z(u,v) \end{array}\right.,\quad (u,v)\in U \]
est une surface, appelée \textbf{surface paramétrée} ou \textbf{nappe}.}

\exemple{L'ensemble des points $\mathcal{T}$ de l'espace paramétrée par 
\[ \left \{ \begin{array}{lcl} x &=& -\sin(\theta)(4+\cos(\phi))\\y&=&\cos(\theta)(4+\cos(\phi)) \\ z&=&\sin(\phi)\end{array} \right.,\quad (\theta, \phi)\in \R^2 \]
est un tore, de représentation :
\begin{center}
	\includegraphics[width=10cm]{tex/Chap26/pic/tore}
\end{center}
}

\definition{[Equation implicite] Soit $F$ une application de classe $\CC^2$ de $U$, ouvert de $\R^3$ à valeurs dans $\R$. L'ensemble $\mathcal{S}$ des points $M(x,y,z)$ vérifiant $F(x,y,z)=0$ est une surface, d'équation implicite $F(x,y,z)=0$.}

\remarque{En général, connaissant une équation implicite, on ne peut pas forcément obtenir une définition explicite comme pour le tore.}

\exemple{La surface d'équation $x^2+y^2-z^2=1$ est appelée  hyperboloïde à une nappe.
\begin{center}
\includegraphics[width=10cm]{tex/Chap26/pic/hyperboloide}
\end{center}
}

	\label{objectif-26-11}\subsection{Plan tangent, normale}
	
\definition{Soit $\mathcal{S}$ une surface paramétrée par \[ \left \{ \begin{array}{lcl} x &=& x(u,v)\\y&=&y(u,v)\\z&=&z(u,v) \end{array}\right.,\quad (u,v)\in U \]
Soit $(u,v)\in U$, et $M=x(u,v),y(u,v),z(u,v))$. On note \[\vv{n}(u,v)=\matrice{\frac{\partial x}{\partial u}(u,v)\\\frac{\partial y}{\partial u}(u,v)\\\frac{\partial z}{\partial u}(u,v)}\wedge \matrice{\frac{\partial x}{\partial v}(u,v)\\\frac{\partial y}{\partial v}(u,v)\\\frac{\partial z}{\partial v}(u,v)} \]
Si $\vv{n}(u,v)\neq \vv{0}$, alors le point $(u,v)$ de $\mathcal{S}$ est dit \textbf{régulier}, et dans ce cas, le vecteur $\vv{n}(u,v)$ est un \textbf{vecteur normal} à la surface de $\mathcal{S}$ en $(u,v)$.

On appelle alors :
\begin{itemize}[label=\textbullet]
	\item \textbf{plan tangent} à $\mathcal{S}$ en $M$ le plan, passant par $M$ et de vecteur normal $\vv{n}(u,v)$.
	\item \textbf{normale} à $\mathcal{S}$ en $M$ la droite, passant par $M$ et dirigée par $\vv{n}(u,v)$.
\end{itemize}
}

\exercice{Montrer que tout point du  tore est régulier, et déterminer une équation du plan tangent à $\mathcal{T}$ en $M(x(\theta, \phi),y(\theta, \phi),z(\theta, \phi))$.}

\solution[30]{Soit $(\theta,\phi)\in \R^2$ et calculons $\vec{n}(\theta,\phi)$ :
\begin{align*}
  \vec{n}(\theta,\phi) &= \matrice{-\cos(\theta)(4+\cos(\phi))\\-\sin(\theta)(4+\cos(\phi))\\0} \wedge \matrice{\sin(\theta)\sin(\phi)\\-\cos(\theta)\sin(\phi)\\\cos(\phi)} 	\\
  &= \matrice{-\sin(\theta)(4+\cos(\phi))\cos(\phi)\\ \cos(\theta)(4+\cos(\phi))\cos(\phi) \\ (4+\cos(\phi))\sin(\phi)}\\
  &= (4+\cos(\phi))\matrice{-\sin(\theta)\cos(\phi)\\\cos(\theta)\cos(\phi)\\\sin(\phi)}
\end{align*}
Remarquons que ce vecteur n'est jamais nul. En effet, si la dernière ligne est nulle, alors $\phi \equiv 0 [\pi]$ mais alors l'une des deux premières lignes ne s'annule pas.

Le plan tangent en $M(x(\theta, \phi),y(\theta, \phi),z(\theta, \phi))$ a alors pour équation
\[ -\sin(\theta)(4+\cos(\phi))\cos(\phi) x + \cos(\theta)(4+\cos(\phi))\cos(\phi) y +  (4+\cos(\phi))\sin(\phi) z +d = 0 \]
On déterminer $d$ en disant que le point $M$ est dans le plan, et donc que 
\begin{gather*} -\sin(\theta)(4+\cos(\phi))\cos(\phi) (-\sin(\theta)(4+\cos(\phi))) + \newline\cos(\theta)(4+\cos(\phi))\cos(\phi) (\cos(\theta)(4+\cos(\phi))) + \\ (4+\cos(\phi))\sin(\phi) \sin(\phi) +d = 0 \end{gather*}
 et donc $d=-(4+\cos(\phi))(1+4\cos(\phi))$.
 
 En divisant par $(4+\cos(\phi))$ qui ne s'annule jamais : 
 
 \[ -\sin(\theta)\cos(\phi)x + \cos(\theta)\cos(\phi)y + \sin(\phi)z-(1+4\cos(\phi)) = 0 \]
}

\remarque{Dans le cas où une surface $\mathcal{S}$ est définie implicitement par $F(x,y,z)=0$, alors un point $(x,y,z)$ est régulier si et seulement si $\vv{\text{grad}}F(x,y,z)\neq \vv{0}$. Dans ce cas, le gradient est un vecteur normal au plan tangent à la surface $\mathcal{S}$ en $M$, ou un vecteur directeur de la normale à $\mathcal{S}$ en $M$
}