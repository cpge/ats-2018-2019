function z=mini(x,y)
    z=x^2+y^2
endfunction

function z=plan(x,y)
    z=2*x+2*y-2
endfunction

x=(-2:0.1:2)';y=x


// Minimum local
clf;delete(gcf());
a=get("current_axes");a.axes_visible="off";  
a.auto_ticks(1)="off"
a.auto_ticks(3)="off"
a.auto_ticks(2)="off"
z=feval(x,y,mini)
plot3d(x,y,z,theta=120,alpha=30); 
z=feval(x,y,plan)
plot3d(x,y,z,theta=120,alpha=30); 
orig=[0 0 0];
//theta=70,alpha=80,
//title("Minimum local",'fontsize',5);xlabel('');ylabel('');zlabel('');
xs2pdf(0,"tangent.pdf")
