function z=v1(x,y)
    z=10*sqrt(x^2+y^2-1)
endfunction

function z=v2(x,y)
    z=-10*sqrt(x^2+y^2-1)
endfunction


x=(-3:0.1:3)';y=x


// Hyperboloide
clf;
a=get("current_axes");a.axes_visible="off"; 

z=feval(x,y,v1)
plot3d1(x,y,z,theta=70,alpha=40,flag=[0 2 1]); 

z=feval(x,y,v2)
plot3d1(x,y,z,theta=70,alpha=40,flag=[0 2 1]); 

F=gcf();
F.color_map=graycolormap(64);
  a=gca();//"axes" courant
  p=a.children(1);//Plot3d 
  p.color_mode=1;//dessus bleu
  p=a.children(2);//Plot3d 
  p.color_mode=0;//dessus bleu

xlabel("");ylabel("");zlabel("")
xs2pdf(0,"hyperboloide.pdf")

