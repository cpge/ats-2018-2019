function z=mini(x,y)
    z=x^2+y^2
endfunction

function z=maxi(x,y)
    z=-x^2-y^2
endfunction

function z=sellee(x,y)
    z=x^2-y^2
endfunction

x=(-2:0.1:2)';y=x


// Minimum local
z=feval(x,y,mini)
clf;
a=get("current_axes");a.axes_visible="off"; 
plot3d1(x,y,z,theta=70,alpha=80,flag=[0 2 1]); 
title("Minimum local",'fontsize',5);xlabel('');ylabel('');zlabel('');
xs2pdf(0,"minimumlocal.pdf")

// Maximum local
z=feval(x,y,maxi)
clf;
a=get("current_axes");a.axes_visible="off"; 
plot3d1(x,y,z,theta=70,alpha=80,flag=[0 2 1]); 
title("Maximum local",'fontsize',5);xlabel('');ylabel('');zlabel('');
xs2pdf(0,"maximumlocal.pdf")

// Selle
z=feval(x,y,sellee)
clf;
a=get("current_axes");a.axes_visible="off"; 
plot3d1(x,y,z,theta=70,alpha=90,flag=[0 2 1]); 
title("Point selle",'fontsize',5);
xlabel('');ylabel('');zlabel('');
xs2pdf(0,"selle.pdf")
