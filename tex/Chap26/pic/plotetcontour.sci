// Auteur : Crouzet
// Date : 15/04/2017
// Résumé : montre comment tracer le graphe 
//          d'une fonction de 2 variables ainsi
//          que les lignes de niveaux

// On définit la fonction f(x,y)=x^2-y^2
function z=mafonction(x,y)
    z=x^2-y^2
endfunction

// On efface l'interface graphique
clf

// On définit les vecteurs x (abscisses) et y (ordonnées)
x=[-1:0.05:1]';
y=x;

// On calcule les images qu'on met dans z
z=feval(x,y,mafonction);

// On utilise plot3d pour tracer la courbe
plot3d(x,y,z);

// On utilise contour pour tracer les lignes de niveaux
// contour(x,y,z,ligne,flag)
// où ligne est un vecteur donnant les réels l tels que f(x,y,z)=l
// et flag permet de tracer dans l'espace (sinon les lignes de niveaux
// sont tracées dans le plan)
contour(x,y,z,[-.5,-.4,-.3,-.2,-.1,.1,.2,.3,.4,.5], flag=[0 2 4])
