function [x,y,z]=tore(theta,phi)
  x=-(4+1*cos(phi)).*sin(theta) 
  y=(4+1*cos(phi)).*cos(theta)
  z=sin(phi)
endfunction

clf;
 phi=[0:0.1:2*3.15];
  theta=[2*3.15:-0.05:0];
  [x,y,z]=eval3dp(tore,theta,phi); 
  plot3d(x,y,z,alpha=65,theta=45,flag=[0 2 1])
 // a=gca();//"axes" courant
 // p=a.children(1);//Plot3d 
//  p.color_mode=10;//dessus bleu
 title("Tore",'fontsize',5);xlabel('');ylabel('');zlabel('');

xs2pdf(0,"tore2.pdf")
