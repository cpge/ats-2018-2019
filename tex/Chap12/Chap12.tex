\objectif{On étend dans ce chapitre la notion de limite de suite au cas plus général des limites de fonctions. Cela permettra de commencer les études de fonctions, en étudiant le comportement asymptotique de celles-ci. On définit rigoureusement la notion déjà vue de continuité. C'est l'occasion de revoir le théorème des valeurs intermédiaires, et un corollaire important - le théorème de la bijection.}

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
	\item Connaître la définition des limites :
			\begin{itemize}[label=\textbullet]
				\item Connaître les limites usuelles et les croissances comparées\dotfill $\Box$
				\item Savoir utiliser les théorèmes d'addition, multiplication, quotient de limites\dotfill $\Box$
				\item Savoir calculer la limite d'une composée de fonction\dotfill $\Box$ 
				\item Reconnaître les limites liées au taux d'accroissement\dotfill $\Box$
			\end{itemize}
	\item Savoir lever les indéterminations classiques :
			\begin{itemize}[label=\textbullet]
				\item polynômes et fractions rationnelles\dotfill $\Box$
				\item fonctions avec des radicaux\dotfill $\Box$
				\item les cas ``$\infty/\infty$'' ou ``$0/0$''\dotfill $\Box$
			\end{itemize}
	\item Savoir appliquer les théorèmes d'existence de limites (théorème d'encadrement, de comparaison)\dotfill $\Box$
	\item Savoir déterminer le comportement asymptotique d'une fonction (limites, asymptotes)\dotfill $\Box$
	\item Concernant la continuité :
			\begin{itemize}[label=\textbullet]
				\item Savoir montrer qu'une fonction est continue en un point\dotfill $\Box$
				\item Savoir prolonger de manière continue une fonction en un point\dotfill $\Box$
				\item Savoir utiliser la continuité pour déterminer la limite d'une suite récurrente\dotfill $\Box$
			\end{itemize}
	\item Savoir utiliser le théorème des valeurs intermédiaires pour déterminer l'existence d'une solution à une équation  du type $f(x)=a$\dotfill $\Box$
	\item Savoir utiliser le théorème de la bijection pour montrer qu'une fonction est bijective, et étudier le sens de variations d'une fonction réciproque \dotfill $\Box$ 
\end{numerote}



\newpage

\section{Limites à l'infini}

\subsection{Limites nulles}

\definition{
Si, pour tout $\eps > 0$ (aussi petit qu'on veut), la fonction $f$ est comprise entre $-\eps$ et $+\eps$ (soit $|f(x)| \leq \eps$) lorsque $x$ est suffisamment grand , on dit que $f$ \textbf{a pour limite $0$} quand $x$ tend vers $+\infty$. On note
 $$\lim_{x \rightarrow +\infty} f(x)=0~~~~\textrm{ ou }~~~~\lim_{+\infty} f =0$$}
 
\begin{center}
    \includegraphics[width=8cm]{./tex/Chap12/limitenulle}
\end{center}
 
 
\remarque{Mathématiquement, on écrit donc 
$$\forall~\eps>0,~\exists~M,~\forall~x, x>M\Rightarrow |f(x)|<\eps$$}  
 
\remarque{On définit de même $\displaystyle{\lim_{x \rightarrow -\infty}f(x) = 0}$}

\exemple{$\displaystyle{\lim_{x\rightarrow +\infty} \frac{1}{x}=0}$}

    \subsection{Limites finies : $l \in \R$}

\definition{Soit $l \in \R$. On dit qu'une fonction $f$ \textbf{a pour limite $l$} quand $x$ tend vers $+\infty$ (ou $-\infty$) si $f(x)-l$ tend vers $0$ quand $x$ tend vers $+\infty$ (ou $-\infty$).}

\begin{center}
	\includegraphics[width=18cm]{./tex/Chap12/limitel}
\end{center}

\remarque{Mathématiquement, on écrit donc 
$$\forall~\eps>0,~\exists~M,~\forall~x, x>M\Rightarrow |f(x)-l|<\eps$$}  

\begin{histoire}
Cette définition rigoureuse est due à \textbf{Karl Weierstrass}, considéré comme le ``père de l'analyse moderne'', même si \textbf{Bernard Bolzano} avait déjà défini la notion de limite, certes de manière moins rigoureuse.
\end{histoire}

\exemple{$\displaystyle{\lim_{x\rightarrow +\infty} 2+\frac{1}{x}=2}$}

\definition{\textit{(Asymptote horizontale)}
Soit $f$ une fonction telle que $\displaystyle{\lim_{x \rightarrow +\infty} f(x) = l}$ (où $l \in \R$). Alors, la droite d'équation $y=l$ est appelée \textbf{asymptote horizontale} à la courbe de $f$ au voisinage de $+\infty$ (même chose en $-\infty$).
}

\begin{center}
\includegraphics[width=7cm]{tex/Chap12/ashoriz}\\
On dispose ici d'une asymptote horizontale d'équation $y=l$.
\end{center}

\remarque{
Pour étudier la position de la courbe de $f$ par rapport à l'asymptote $y=l$, on étudie le signe de $f(x)-l$. 
\begin{itemize}
	\item[$\bullet$] Si $f(x)-l \geq 0$, la courbe est au-dessus de son asymptote.
	\item[$\bullet$] Si $f(x)-l \leq 0$, la courbe est en dessous de son asymptote.
\end{itemize}}

\subsection{Limites infinies}

\definition{Si, pour tout nombre $A$ (aussi grand qu'on veut), la fonction $f$ est toujours supérieure ou égale à $A$ dès que $x$ est suffisamment grand, on dit que \textbf{$f$ a pour limite $+\infty$} quand $x$ tend vers $+\infty$. On note 
 $$\lim_{x \rightarrow +\infty} f(x)=+\infty~~~~\textrm{ ou }~~~~\lim_{+\infty} f =+\infty$$}

\begin{center}
    \includegraphics[width=6cm]{tex/Chap12/limiteinfinie}
\end{center}

\remarque{Mathématiquement, on écrit donc 
$$\forall~A>0,~\exists~M,~\forall~x, x>M\Rightarrow f(x)>A$$}
  
\remarque{On définit de même $\displaystyle{\lim_{x \rightarrow -\infty} f(x)=+\infty}$, $\displaystyle{\lim_{x \rightarrow +\infty} f(x)=-\infty}$ et $\displaystyle{\lim_{x \rightarrow -\infty} f(x)=-\infty}$}


\section{Limites en $a \in \R$}

Ici, on s'intéresse au comportement d'une fonction $f$ quand $x$ tend vers $a \in \R$. 

    \subsection{Limite réelle en un point}
    
 \definition{Soient $x_0$ et $l$ deux réels.\\
Si $f(x)$ est aussi proche que l'on veut de $l$ dès lors que $x$ est proche de $x_0$, on dit que $f$ \textbf{a pour limite $l$} quand $x$ tend vers $x_0$. On note
 $$\lim_{x \rightarrow x_0} f(x)=l~~~~\textrm{ ou }~~~~\lim_{x_0} f =l$$}
  

\begin{center}
    \includegraphics[width=8cm]{tex/Chap12/limiteenreel}
\end{center}

\remarque{Mathématiquement, on écrit donc 
$$\forall~\eps>0,~\exists~\alpha>0,~\forall~x, |x-x_0|<\alpha \Rightarrow |f(x)-l|<\eps$$} 

\remarque{Il y a unicité de la limite. On peut donc bien parler de la limite en $x_0$.}

\exemple{Soit $f$ la fonction définie sur $\R$ par $f(x)=2x+1$. Soit $x_0=2$. Montrer que, quand $x$ tend vers $x_0$, $f(x)$ va tendre vers $5=f(2)$.}

\begin{prof}
\solution{
 On peut le démontrer rigoureusement :
$$|f(x)-5|=|2x+1-5|=|2x-4|=2|x-2|$$
Soit $\eps>0$ fixé. Alors $|f(x)-5|<\eps \Leftrightarrow 2|x-2|<\eps \Leftrightarrow |x-2|<\frac{\eps}{2}$. On pose alors $\alpha=\frac{\eps}{2}$.}
\end{prof}

\lignes{7}

    \subsection{Limite infinie en un point}
    
\definition{Soit $x_0$ un réel.\\
Si $f(x)$ est aussi grand que l'on veut dès lors que $x$ est proche de $x_0$, on dit que $f$ \textbf{a pour limite $+\infty$} quand $x$ tend vers $x_0$. On note
 $$\lim_{x \rightarrow x_0} f(x)=+\infty~~~~\textrm{ ou }~~~~\lim_{x_0} f =+\infty$$}
 
\begin{center}
    \includegraphics[width=8cm]{tex/Chap12/limitereelinfinie}
\end{center}

\remarque{Mathématiquement, on écrit donc 
$$\forall~A>0,~\exists~\alpha>0,~\forall~x, |x-x_0|<\alpha \Rightarrow f(x)>A$$} 

\remarque{Il y a unicité de la limite. On peut donc bien parler de la limite en $x_0$. \\On définit également $\displaystyle{\lim_{x\rightarrow x_0} f(x)=-\infty}$ de la même manière.}

    \subsection{Limite à gauche et à droite}

\definition{Soit $x_0$ un réel.
\begin{itemize}
    \item[$\bullet$] Si on s'intéresse à la limite en $x_0$, en imposant $x<x_0$, on parle de la \textbf{limite à gauche} en $x_0$, et on note $\displaystyle{\lim_{x\rightarrow x_0^-}}$ ou $\displaystyle{\lim_{\substack{x\rightarrow x_0\\ x<x_0}}}$.
    \item[$\bullet$] Si on s'intéresse à la limite en $x_0$, en imposant $x>x_0$, on parle de la \textbf{limite à droite} en $x_0$, et on note $\displaystyle{\lim_{x\rightarrow x_0^+}}$ ou $\displaystyle{\lim_{\substack{x\rightarrow x_0\\x>x_0}}}$.
\end{itemize}
}

\remarque{Une fonction peut avoir des limites à gauche et à droite en $x_0$ différentes ! Par exemple 
$$\lim_{x\rightarrow 0^-} \frac{1}{x}=-\infty \textrm{  et  } \lim_{x\rightarrow 0^+} \frac{1}{x}=+\infty$$}

\begin{proposition}
Soit $f$ une fonction et $x_0$ un réel. Alors $f$ admet une limite en $x_0$ si et seulement si $f$ admet des limites à gauche et à droite en $x_0$ et que ces limites sont les mêmes.\\Dans ce cas, $$\lim_{x\rightarrow x_0} f(x)=\lim_{x\rightarrow x_0^-} f(x) = \lim_{x\rightarrow x_0^+} f(x)$$	
\end{proposition}


\exemple{Ainsi, la fonction inverse n'admet pas de limite en $0$.}

\begin{methode}
On est souvent amené à étudier des limites à droite et à gauche lorsque la fonction est définie de deux manières différentes selon les intervalles.	
\end{methode}


\exemple{Etudier la limite en $0$ de la fonction $f$ définie sur $\R$ par 
$$f(x)=\left\{ \begin{array}{l} 1 \textrm{ si $x\geq 0$}\\
             e^x \textrm{ si $x<0$}
 \end{array}\right.$$}

\begin{prof}
\solution{On remarque que :
\begin{itemize}
    \item[$\bullet$] $\displaystyle{\lim_{x\rightarrow 0^+} f(x)=\lim_{x\rightarrow 0^+} 1= 1=f(0)}$
    \item[$\bullet$] $\displaystyle{\lim_{x\rightarrow 0^-} f(x)=\lim_{x\rightarrow 0^-} e^x=e^0=1}$
\end{itemize}
Puisque les limites à droite et à gauche sont égales, on en déduit que $f$ admet une limite en $0$, et $\displaystyle{\lim_{x\rightarrow 0} f(x)=1}$.
}
\end{prof}

\lignes{6}

\definition{\textit{(Asymptote verticale)}\\
 Si $\displaystyle{\lim_{x \rightarrow a^+ \textrm{(ou $a^-$)}} f(x) = +\infty \textrm{ ( ou $-\infty$)} }$, on dit que la droite d'équation $x=a$ est une \textbf{asymptote verticale} à la courbe de $f$.}

\begin{center}
\includegraphics[width=7cm]{tex/Chap12/asvert}
\end{center}


\section{Théorèmes d'existence et de comparaison}

        \subsection{Fonctions monotones}

Les fonctions monotones possèdent des propriétés intéressantes concernant les limites :

\begin{theoreme}
Soit $f$ une fonction monotone sur un segment $[a;b]$. Alors $f$ admet des limites à gauche et à droite en tout point.	
\end{theoreme}


\begin{theoreme}
Soit $f$ une fonction monotone sur $I=]a;b[$.
\begin{itemize}
    \item[$\bullet$] Si $f$ est croissante et majorée sur $I$, alors $f$ admet une limite finie en $b^-$. 
    \item[$\bullet$] Si $f$ est croissante et minorée sur $I$, alors $f$ admet une limite finie en $a^+$.
    \item[$\bullet$] Si $f$ est décroissante et minorée sur $I$, alors $f$ admet une limite finie en $b^-$.    
    \item[$\bullet$] Si $f$ est décroissante et majorée sur $I$, alors $f$ admet une limite finie en $a^+$.
\end{itemize}	
\end{theoreme}


\remarque{Si $f$ est croissante sur $I=]a;b[$ mais non majorée sur $I$, alors $f$ admet une limite en $b^-$ qui vaut $+\infty$.\\De manière générale, une fonction monotone sur $]a;b[$ admet toujours des limites en $a^+$ et en $b^-$, finie ou infinie.}

        \subsection{Limites et inégalités}
        
\begin{theoreme}
Soit $I$ un intervalle et $x_0\in I$. Soient $f$ et $g$ deux fonctions définies sur $I$, sauf éventuellement en $x_0$, mais possédant une limite en $x_0$. Alors, si pour tout $x$ de $I\backslash\{x_0\}$, $f(x)\geq g(x)$, on a $$\lim_{x\rightarrow x_0} f(x) \geq \lim_{x\rightarrow x_0} g(x)$$
En particulier, si $f(x)\geq 0$ pour tout $x\neq x_0$ alors $\displaystyle{\lim_{x\rightarrow x_0} f(x)\geq 0}$.
\end{theoreme}


\remarque{Attention : le passage à la limite ne conserve pas les inégalités strictes. Si $f(x)> g(x)$ pour tout $x \neq x_0$ alors $\displaystyle{\lim_{x\rightarrow x_0} f(x) \geq \lim_{x\rightarrow x_0} g(x)}$. Par exemple, pour tout $x$, $1+\frac{1}{x}>1$ mais $\displaystyle{\lim_{x\rightarrow +\infty} 1+\frac{1}{x} \geq 1}$.}

	\subsection{Théorème d'encadrement}
	
\begin{theoreme}
Soit $I$ un intervalle et $x_0\in I$. Soient $f,g,h$ trois fonctions définies sur $I$ sauf éventuellement en $x_0$. Si, pour tout $x$ de $I \backslash \{x_0\}$, on a $f(x) \leq g(x) \leq h(x)$ et si $f$ et $h$ ont la même limite $l$ en $x_0$, alors la limite de $g$ en $x_0$ existe, et \[\lim_{x\rightarrow x_0} g(x)=l\]	
\end{theoreme}


\preuve{Admis. Idée de démonstration dans le chapitre sur les suites.}

\exemple{Soit $g$ la fonction définie sur $]0;+\infty[$ par $g(x)=\frac{[x]}{x}$. Montrer que $\displaystyle{\lim_{x\rightarrow+\infty} g(x)=1}$.}

\solution[7]{
 On a, pour tout $x$ de $]0;+\infty[$, $x-1\leq [x] \leq x$, donc, en divisant par $x>0$, 
 \[\frac{x-1}{x} \leq g(x) \leq \frac{x}{x}=1\]
 Puisque $\displaystyle{\lim_{x\rightarrow +\infty} \frac{x-1}{x} =\lim_{x\rightarrow +\infty} 1=1}$, on en déduit donc, par encadrement, que la limite de $g$ en $+\infty$ existe, et 
\[\lim_{x\rightarrow +\infty} \frac{[x]}{x}=1\]
}

\exercice{Déterminer $\ds{\lim_{x\rightarrow +\infty} \frac{\sin(x)}{x}}$.}

\solution[7]{Par le même raisonnement, pour tout $x>0$, on a 
\begin{eqnarray*}
	-1\leq \sin(x)\leq 1 &\text{donc } -\frac{1}{x}\leq \frac{\sin(x)}{x}\leq \frac{1}{x} \text{ (car $x>0$)}
\end{eqnarray*}
Puisque on a $\ds{\lim_{x\rightarrow +\infty} -\frac{1}{x}=\lim_{x\rightarrow +\infty}\frac{1}{x}=0}$, par encadrement, on en déduit que la limite $\ds{\lim_{x\rightarrow +\infty} \frac{\sin(x)}{x}}$ existe et \[ \lim_{x\rightarrow +\infty} \frac{\sin(x)}{x}= 0 \]
}

\begin{theoreme}
Soit $I$ un intervalle, $x_0 \in I$ et $l$ un réel. Soient $f$ et $g$ deux fonctions définies sur $I$ sauf éventuellement en $x_0$. Si, pour tout réel $x \in I\backslash \{x_0\}$ on a $\displaystyle{|f(x)-l|\leq g(x)}$ et si $\displaystyle{\lim_{x\rightarrow x_0} g(x)=0}$ alors $\displaystyle{\lim_{x\rightarrow x_0} f(x)=l}$.	
\end{theoreme}


\preuve{C'est une conséquence directe du théorème d'encadrement. Voir le chapitre sur les suites.}

	\subsection{Comparaison à l'infini}
	
\begin{theoreme}
Soient $f$ et $g$ deux fonctions définies sur $I=]a;+\infty[$. Si pour tout $x$ de $I$ :
\begin{itemize}
	\item[$\bullet$] $f(x)\geq g(x)$ \textbf{et si} $\displaystyle{\lim_{x\rightarrow +\infty} g(x)=+\infty}$ alors $\displaystyle{\lim_{x\rightarrow +\infty} f(x)=+\infty}$.
	\item[$\bullet$] $f(x)\leq g(x)$ \textbf{et si} $\displaystyle{\lim_{x\rightarrow +\infty} g(x)=-\infty}$ alors $\displaystyle{\lim_{x\rightarrow +\infty} f(x)=-\infty}$.
\end{itemize}	
\end{theoreme}


\begin{prof}
\preuve{Soit $M$ un réel. Par définition, à partir d'un certain réel $b$, on a $g(x)>M$. Or $f(x)\geq g(x)$, donc $f(x) > M$ pour tout $x \geq b$ : par définition, $\displaystyle{\lim_{x\rightarrow +\infty} f(x)=+\infty}$.}
\end{prof}

\lignes{5}

\exemple{Déterminer $\displaystyle{\lim_{x\rightarrow +\infty} \frac{[x]+1}{\sqrt{x}}}$.}

\solution[7]{Pour tout réel $x$, on a $[x]\leq x\leq [x]+1$. Ainsi, pour tout réel $x>0$ (puisque $\sqrt{x}>0$) on a
$$\frac{x}{\sqrt{x}}\leq\frac{[x]+1}{\sqrt{x}}$$
soit
$$\sqrt{x}\leq \frac{[x]+1}{\sqrt{x}}$$
Puisque $\displaystyle{\lim_{x\rightarrow +\infty} \sqrt{x}=+\infty}$, par comparaison, $$\lim_{x\rightarrow +\infty} \frac{[x]+1}{\sqrt{x}}=+\infty$$} 


       \subsection{Asymptote oblique}
       
\definition{\textit{(Asymptote oblique)} Soit $\mathcal{C}_f$ la courbe représentative d'une fonction $f$ dans un repère donné. Soit $(d)$ une droite d'équation $y=ax+b ~(a\neq 0)$. On dit que la droite $(d)$ est une \textbf{asymptote oblique} à $\mathcal{C}_f$ au voisinage de $+\infty$ si
$$\lim_{x\rightarrow+\infty} [f(x)-(ax+b)] =0$$
}

\exemple{Soit $f$ la fonction définie sur $\R^*$ par $f(x)=\frac{x^2+1}{x}$. Montrer que la droite d'équation $y=x$ est asymptote oblique à la courbe de $f$ au voisinage de $+\infty$ et de $-\infty$.}

\solution[5]{En effet, pour tout réel $x\in \R^*$, 
$$f(x)-x=\frac{x^2+1}{x}-x=\frac{1}{x}$$
Or $\displaystyle{\lim_{x\rightarrow -\infty} \frac{1}{x}=\lim_{x\rightarrow +\infty} \frac{1}{x}=0}$.
Ainsi la droite d'équation $y=x$ est bien asymptote oblique à la courbe de $f$ au voisinage de $+\infty$ et $-\infty$}

\section{Opérations sur les limites et limites usuelles}

        \subsection{Opérations sur les limites}

On suppose connues les limites de deux fonctions $f$ et $g$.

            \subsubsection{Limite de $f+g$}

\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
 $\lim g$ / $\lim f$ & $l$ & $+\infty$ & $-\infty$ \\
 \hline
  $l'$ & $l+l'$ & $+\infty$ & $-\infty$ \\
  \hline
  $+\infty$ & $+\infty$ & $+\infty$ & IND \\
  \hline
  $-\infty$ & $-\infty$ & IND & $-\infty$\\
  \hline
\end{tabular}
\end{center}

\exemple{
 $$\lim_{x \rightarrow +\infty} (x+\sqrt{x}) = +\infty$$}

\begin{prof}
\solution{En effet, $\displaystyle{\lim_{x\rightarrow +\infty} x = \lim_{x\rightarrow +\infty} \sqrt{x} = +\infty}$. Par somme, $\displaystyle{\lim_{x\rightarrow +\infty} (x+\sqrt{x})=+\infty}$.}
\end{prof}

\lignes{5}


            \subsubsection{Limite de $f\times g$}

\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
 $\lim g$ / $\lim f$ & $l\neq 0$ & $+\infty$ & $-\infty$ \\
 \hline
  $l'\neq 0$ & $l.l'$ & signe($l'$).$\infty$ & -signe($l'$).$\infty$ \\
  \hline
  $+\infty$ & signe($l$).$\infty$ & $+\infty$ & $-\infty$ \\
  \hline
  $-\infty$ & -signe($l$).$\infty$ & $-\infty$ & $+\infty$\\
  \hline
\end{tabular}
\end{center}

\remarque{
Si $l=0$ (et/ou $l'=0$), seul le résultat $\lim(fg)=l.l'=0$ est déterminé. Toutes les autres limites (du type "$0\times \infty$") sont \textbf{indéterminées}. }

\exemple{
$$\lim_{x\rightarrow +\infty} (x\sqrt{x})=+\infty$$
$$\lim_{x\rightarrow 0}  3xe^x =0$$}

\begin{prof}
\solution{En effet, $\displaystyle{\lim_{x\rightarrow +\infty} x = \lim_{x\rightarrow +\infty} \sqrt{x} = +\infty}$. Par produit, $\displaystyle{\lim_{x\rightarrow +\infty} (x\sqrt{x})=+\infty}$.\\
De même, $\displaystyle{\lim_{x\rightarrow 0} 3x=0}$ et $\displaystyle{\lim_{x\rightarrow 0} e^x = e^0=1}$. Par produit, $\displaystyle{\lim_{x\rightarrow 0} 3xe^x = 0}$.
}
\end{prof}

\lignes{5}

            \subsubsection{Limite de $\frac{f}{g}$}

\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
 $\lim g$ / $\lim f$ & $l$ & $+\infty$ & $-\infty$ \\
 \hline
  $l'\neq 0$ & $\frac{l}{l'}$ & signe($l'$).$\infty$ & -signe($l'$).$\infty$ \\
  \hline
  $+\infty$ & $0$ & IND & IND \\
  \hline
  $-\infty$ & 0 & IND & IND\\
  \hline
\end{tabular}
\end{center}

Si $\lim g = 0$, il faut tout d'abord préciser si $\lim g = 0^+$ ($g$ tend vers $0$ en restant positif) ou si $\lim g=0^-$, et on applique :

\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
 $\lim g$ / $\lim f$ & $0$ & $l \neq 0$ & $+\infty$ & $-\infty$ \\
 \hline
  $0^+$ & IND & signe($l$).$\infty$ & $+\infty$ & $-\infty$ \\
  \hline
  $0^-$ & IND & -signe($l$).$\infty$ & $-\infty$ & $+\infty$ \\
  \hline
\end{tabular}
\end{center}

\exemple{
$$\lim_{x \rightarrow +\infty} \frac{1+\frac{1}{x}}{\sqrt{x}}=0$$
$$\lim_{x \rightarrow 0^+} \frac{x^2+1}{x}=+\infty$$
}

\begin{prof}
\solution{En effet, $\displaystyle{\lim_{x\rightarrow +\infty} 1+\frac{1}{x} = 1}$ par somme, et $\displaystyle{\lim_{x\rightarrow +\infty} \sqrt{x}=+\infty}$. Par quotient, $\displaystyle{\lim_{x\rightarrow +\infty} \frac{1+\frac{1}{x}}{\sqrt{x}}=0}$.\\
De même, $\displaystyle{\lim_{x\rightarrow 0^+} x^2+1=1}$ par somme et $\displaystyle{\lim_{x\rightarrow 0^+} x = 0^+}$. Par quotient, $\displaystyle{\lim_{x \rightarrow 0^+} \frac{x^2+1}{x}=+\infty}$.
}
\end{prof}

\lignes{5}

\subsection{Limite d'une fonction composée}

\begin{theoreme}
Soient $f,g,h$ trois fonctions telles que $f(x)=g(h(x))$ sur un intervalle $I$. Soient $a,b,c$ des éléments de $\R\cup \{+\infty;-\infty\}$.\\
Si $\displaystyle{\lim_{x\rightarrow a} h(x)=b}$ et $\displaystyle{\lim_{x\rightarrow b} g(x)=c}$, alors
$$\lim_{x\rightarrow a} f(x)=c$$	
\end{theoreme}


\preuve{Admis.}

\begin{methode}
Pour déterminer la limite d'une fonction composée $f(x)=g(h(x))$ en $x_0$ :
\begin{itemize}
    \item[$\bullet$] On pose $X=h(x)$.
    \item[$\bullet$] On détermine la limite $b$ de $X$ en $x_0$.
    \item[$\bullet$] On détermine la limite $c$ de $g$ en $b$, et on conclut : la limite de $f$ en $x_0$ vaut $c$.
\end{itemize}
\end{methode}

\exemple{Déterminer $\displaystyle{\lim_{x\rightarrow +\infty} \sqrt{x^2-x+1}}$.}

\begin{prof}
\solution{~
\begin{itemize}
    \item[$\bullet$] On pose $X=x^2-x+1$. On a 
    $$\lim_{x\rightarrow \textcolor{blue}{+\infty}} X = \textcolor{magenta}{+\infty}$$
    \item[$\bullet$] On a $$\lim_{X\rightarrow \textcolor{magenta}{+\infty}} \sqrt{X} = \textcolor{red}{+\infty}$$
\end{itemize}
Par composée, on a donc 
$$\lim_{x\rightarrow \textcolor{blue}{+\infty}} \sqrt{x^2-x+1} = \textcolor{red}{+\infty}$$
}
\end{prof}
 
\lignes{7}

\exercice{Montrer que $\displaystyle{\lim_{x\rightarrow \left(\frac{1}{3}\right)^+} \frac{1}{\sqrt{3x-1}} =+\infty}$.}

\begin{prof}
\solution{Posons $X=3x-1$. Alors :
\begin{itemize}
    \item[$\bullet$] On a $\displaystyle{\lim_{x\rightarrow \left(\frac{1}{3}\right)^+} 3x-1=0^+}$.
    \item[$\bullet$] De plus, $\displaystyle{\lim_{X\rightarrow 0^+} \frac{1}{\sqrt{X}} = +\infty} \textrm{ par quotient}$.
\end{itemize}
Par composée,
$$\lim_{x\rightarrow \left(\frac{1}{3}\right)^+} \frac{1}{\sqrt{3x-1}} =+\infty$$
}
\end{prof}

\lignes{7}


    
        \subsection{Limites usuelles}
        
            \subsubsection{Limites classiques}
            
On dispose d'un ensemble de limites usuelles.

\begin{proposition}[Fonctions usuelles]
$$\textrm{Pour tout entier $n\in \N$, } \lim_{x\rightarrow +\infty} x^n = +\infty~~~~~\lim_{x\rightarrow +\infty} \frac{1}{x^n} = \lim_{x\rightarrow -\infty} \frac{1}{x^n} =0$$
$$\textrm{Pour tout entier $n\in \N$, } \lim_{x\rightarrow -\infty} x^n = +\infty \textrm{ si $n$ est pair, } -\infty \textrm{ sinon.}$$
%$$\textrm{Pour tout entier $n\in \N$, }  \lim_{x\rightarrow +\infty} \frac{1}{x^n} = \lim_{x\rightarrow -\infty} \frac{1}{x^n} =0$$
$$\lim_{x\rightarrow +\infty} \sqrt{x} = \lim_{x\rightarrow +\infty} \ln(x)=\lim_{x\rightarrow +\infty} \E^x = +\infty$$
$$\lim_{x\rightarrow 0} \ln(x)=-\infty,~~~~\lim_{x\rightarrow -\infty} \E^x=0$$
$$\lim_{x\rightarrow -\infty} \eu{-x} = +\infty \textrm{  et  } \lim_{x\rightarrow +\infty} \eu{-x} = 0$$ 
$$\lim_{x\rightarrow +\infty} [x]=+\infty ~~~~~~\lim_{x\rightarrow -\infty} [x] = -\infty ~~~~\textrm{et}~~~~\lim_{x\rightarrow +\infty} |x|=\lim_{x\rightarrow -\infty} |x|=+\infty$$	
\end{proposition}

\begin{proposition}[Fonctions puissances]
$$\textrm{Pour tout }~\alpha>0,~\lim_{x\rightarrow +\infty} x^\alpha=+\infty \textrm{ et } \lim_{x\rightarrow 0} x^\alpha=0$$
$$\textrm{Pour tout }~\alpha<0,~\lim_{x\rightarrow +\infty} x^\alpha=0 \textrm{ et } \lim_{x\rightarrow 0} x^\alpha=+\infty$$
$$\textrm{Pour tout }~a>1,~~\lim_{x\rightarrow +\infty} a^x=+\infty \textrm{  et  } \lim_{x\rightarrow -\infty} a^x=0$$
$$\textrm{Pour tout $a$ tel que }~0<a<1,~~\lim_{x\rightarrow +\infty} a^x=0 \textrm{  et  } \lim_{x\rightarrow -\infty} a^x=+\infty$$	
\end{proposition}

            \subsubsection{Croissances comparées}
         
\begin{theoreme}
Pour tout $\alpha>0$, et $q>0$ 
$$\lim_{x\rightarrow +\infty} \frac{\E^x}{x^\alpha}=+\infty \textrm{ et } \lim_{x\rightarrow +\infty} \frac{x^\alpha}{\ln^q(x)} = +\infty$$	
\end{theoreme}


\consequence{Par passage à l'inverse,
$$\lim_{x\rightarrow +\infty} \frac{x^\alpha}{\E^x}=0 \textrm{ et } \lim_{x\rightarrow +\infty} \frac{\ln^q(x)}{x^\alpha} = 0$$
}

\begin{theoreme}
Pour tout $\alpha>0$, $$\lim_{x\rightarrow 0^+} x^\alpha \ln(x) =0$$	
\end{theoreme}


\preuve{Posons $X=\frac{1}{x}$. Alors $\displaystyle{\lim_{x\rightarrow 0^+} X =+\infty}$ et $\displaystyle{\lim_{x\rightarrow 0^+} x^\alpha \ln(x)=\lim_{X\rightarrow +\infty} -\frac{\ln(X)}{X^\alpha} = 0}$ d'après ce qui précède.}

\begin{theoreme}
Pour tout $n\in \N$, on a $$\lim_{x\rightarrow -\infty} x^n\E^x=0$$	
\end{theoreme}


\preuve{Se démontre de la même manière que précédemment (en posant $X=-x$).}

\begin{methode}
Pour utiliser les croissances comparées, il faut souvent faire un changement de variable pour s'y ramener.	
\end{methode}


\exemple{Déterminer $\displaystyle{\lim_{x\rightarrow +\infty} \frac{\eu{2x}}{x^3}}$.}

\begin{prof}
\solution{On pose $X=2x$. Alors \[\lim_{x\rightarrow +\infty} X = +\infty \textrm{ et } 
\lim_{x\rightarrow +\infty} \frac{\eu{2x}}{x^3}=\lim_{X\rightarrow +\infty} \frac{\E^{X}}{(X/2)^3}=\lim_{X\rightarrow +\infty} 2^3\frac{\E^{X}}{X^3}=+\infty \textrm{ par croissance comparée.}
\]}
\end{prof}

\lignes{4}

    \subsection{Autres limites}

\begin{theoreme}
On dispose des limites suivantes :
\[\lim_{x \rightarrow 0} \frac{\E^x-1}{x}=1~~~~\lim_{x\rightarrow 0}\frac{\ln(1+x)}{x}=1~~~~\lim_{x\rightarrow 1} \frac{\ln(x)}{x-1}=1\]
\[ \lim_{x\rightarrow 0} \frac{\sin(x)}{x}=1 \qeq \lim_{x\rightarrow 0} \frac{\cos(x)-1}{x}=0\]	
\end{theoreme}


\preuve{Nous verrons la démonstration de ces limites dans le chapitre dédié à la Dérivation.}

\exemple{Déterminer $\displaystyle{\lim_{x\rightarrow 0^+} \frac{\ln(1+x)}{x^3}}$.}

\begin{prof}
\solution{On remarque que, pour tout réel $x>0$ :
$$\frac{\ln(1+x)}{x^3} = \frac{\ln(1+x)}{x}\times \frac{1}{x^2}$$
Or $$\lim_{x\rightarrow 0^+} \frac{\ln(1+x)}{x} = 1 \textrm{  et  }\lim_{x\rightarrow 0^+} \frac{1}{x^2} = +\infty$$
Par produit,
$$\lim_{x\rightarrow 0^+} \frac{\ln(1+x)}{x^3}=+\infty$$
}
\end{prof}
 
\lignes{5}

       \subsection{Quelques indéterminations classiques}

        \subsubsection{Polynômes et fractions rationnelles}
        
\begin{methode}
En $+\infty$ ou en $-\infty$, il y a une méthode classique dite du terme du plus haut degré.
 \begin{itemize}
    \item[$\bullet$] Si $\displaystyle{f :x \mapsto a_nx^n+a_{n-1}x^{n-1}+\cdots +a_0}$ est un polynôme, alors $\displaystyle{\lim_{x\rightarrow +\infty} f(x)=\lim_{x\rightarrow +\infty} a_nx^n}$.
    \item[$\bullet$] Si $\displaystyle{g: x \mapsto \frac{a_nx^n+a_{n-1}x^{n-1}+\cdots +a_0}{b_kx^k+b_{k-1}x^{k-1}\cdots + b_0}}$ est une fraction rationnelle, alors $\displaystyle{\lim_{x\rightarrow +\infty} g(x)=\lim_{x\rightarrow +\infty} \frac{a_nx^n}{b_kx^k}}$.
\end{itemize}
\end{methode}

\exemple{Soient $f:x\mapsto 2x^3-3x^2+6x-1$ et $g:x\mapsto\frac{x^2+1}{2x^2-3x+1}$. Déterminer $\displaystyle{\lim_{x\rightarrow +\infty} f(x)}$ et $\displaystyle{\lim_{x\rightarrow -\infty} g(x)}$}

\begin{prof}
\solution{D'après la règle du terme du plus haut degré, 
$$\lim_{x\rightarrow +\infty} f(x)=\lim_{x\rightarrow +\infty} 2x^3=+\infty$$
De même,
$$\lim_{x\rightarrow -\infty} g(x)=\lim_{x\rightarrow -\infty} \frac{x^2}{2x^2}=\lim_{x\rightarrow -\infty} \frac{1}{2}= \frac{1}{2}$$
}
\end{prof}

\lignes{5}

\remarque{\danger{~Cette méthode ne s'applique qu'aux limites en $+\infty$ et $-\infty$.}
}

        \subsubsection{Racines}
       
\begin{methode}
Lorsqu'une fonction contient des radicaux, on utilise la quantité conjuguée.	
\end{methode}


\exemple{Soit $f:x\mapsto \sqrt{x^2+1}-x$. Déterminer $\displaystyle{\lim_{x\rightarrow +\infty} f(x)}$.}

\begin{prof}
\solution{On constate que la limite en $+\infty$ de $f$ est indéterminée. Alors,
 $$f(x)=(\sqrt{x^2+1}-x)\frac{\sqrt{x^2+1}+x}{\sqrt{x^2+1}+x}=\frac{1}{\sqrt{x^2+1}+x}$$ et donc, par composée et quotient,
 $$\lim_{x\rightarrow +\infty} f(x)=0$$ 
 }
\end{prof}

\lignes{6}

        \subsubsection{$\frac{\infty}{\infty}$ ou $\frac{0}{0}$}
     
\begin{methode}
Dans les cas $\frac{\infty}{\infty}$ ou $\frac{0}{0}$, on commence par mettre au numérateur et au dénominateur le terme prépondérant (en utilisant les croissances comparées).	
\end{methode}
 
\exemple{Soit $\displaystyle{f:x\mapsto \frac{x+1}{\E^x-1}}$. Déterminer $\displaystyle{\lim_{x\rightarrow +\infty} f(x)}$.}
 
\begin{prof}
\solution{Alors $$\frac{x+1}{\E^{x}-1}=\frac{x}{\E^x} \frac{1+\frac{1}{x}}{1-\E^{-x}}$$
Or $$\lim_{x\rightarrow +\infty} \frac{x}{\E^x}=0 \textrm{  et } \lim_{x\rightarrow +\infty} \frac{1+\frac{1}{x}}{1-\E^{-x}}=1\textrm{  par quotient}$$
Par produit $\displaystyle{\lim_{x\rightarrow +\infty} f(x)=0}$.}
\end{prof}

\lignes{6}

\section{Etude des limites d'une fonction}

    \subsection{1ère étape : limites}
    
Lorsqu'on se donne une fonction, on commencera toujours par déterminer ses limites au borne de l'intervalle de définition~:
\begin{itemize}
    \item[$\bullet$] Si $f$ est définie sur $\mathbb{R}$, on déterminera $\displaystyle{\lim_{x \rightarrow +\infty} f}$ et $\displaystyle{\lim_{x\rightarrow-\infty} f}$.
    \item[$\bullet$] Si $f$ est définie sur $]-\infty;a[\cup]a;+\infty[$, il faut déterminer $4$ limites : en $+\infty$, $-\infty$, $a^+$ et $a^-$.
\end{itemize}

On notera directement les asymptotes horizontales (limite finie en $+\infty$ ou $-\infty$) et les asymptotes verticales (limite infinie en $a^+$ ou $a^-$).

    \subsection{2ème étape : branches infinies}
    
Si les limites en $+\infty$ et/ou $-\infty$ sont infinies, on cherche une éventuelle asymptote oblique. Pour cela on détermine
$$\lim_{x\rightarrow +\infty} \frac{f(x)}{x}$$
\begin{itemize}
    \item[$\bullet$] Si cette limite est nulle, on dit que la courbe de $f$ possède une \textbf{branche parabolique de direction l'axe des abscisses} en $+\infty$.
    \begin{center}
    \includegraphics[width=5cm]{tex/Chap12/paraabs}
    \end{center}
    L'exemple classique est la fonction logarithme népérien.
    \item[$\bullet$] Si cette limite est infinie, on dit que la courbe de $f$ possède une \textbf{branche parabolique de direction l'axe des ordonnées} en $+\infty$.
    \begin{center}
    \includegraphics[width=5cm]{tex/Chap12/paraord}
    \end{center}
    L'exemple classique est la fonction exponentielle.
    \item[$\bullet$] Si cette limite est un nombre réel $a$, on dit que la courbe de $f$ admet une direction asymptotique d'équation $y=ax$. Il reste alors à calculer $\displaystyle{\lim_{x\rightarrow +\infty} (f(x)-ax)}$ .
        \begin{itemize}
                \item[$\circ$] Si cette limite est un réel $b$, la droite d'équation $y=ax+b$ est \textbf{asymptote oblique} à la courbe de $f$ en $+\infty$.
    \begin{center}
    \includegraphics[width=5cm]{tex/Chap12/asymptote}
    \end{center}
                \item[$\circ$] Si cette limite est infinie, on dit que la courbe de $f$ possède une \textbf{branche parabolique de direction la droite d'équation} $y=ax$.
    \begin{center}
    \includegraphics[width=5cm]{tex/Chap12/paradroite}
    \end{center}
        \end{itemize}
\end{itemize}
(bien sûr, tout ceci est valable en $-\infty$.)


