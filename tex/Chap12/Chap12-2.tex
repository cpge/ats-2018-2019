\section{Continuité}

	\subsection{Continuité en un point, sur un intervalle}
	
\definition{Soit $f$ une fonction, et $I$ un \textbf{intervalle} inclus dans l'ensemble de définition de $f$.
\begin{itemize}[label=\textbullet]
	\item On dit que la fonction $f$ est \textbf{continue à droite en un point $a$} de $I$ si $f$ admet une limite à droite en $a$, qui est égale à $f(a)$ : \[\lim_{x\rightarrow a^+} f(x)=f(a)\]
	\item On dit que la fonction $f$ est \textbf{continue à gauche en un point $a$} de $I$ si $f$ admet une limite à gauche en $a$, qui est égale à $f(a)$ : \[\lim_{x\rightarrow a^-} f(x)=f(a)\]
	\item On dit que la fonction $f$ est \textbf{continue en un point $a$} de $I$ si $f$ admet une limite en $a$, qui est égale à $f(a)$ : \[\lim_{x\rightarrow a} f(x)=f(a)\]
	\item On dit que $f$ est \textbf{continue sur l'intervalle I} si elle est continue en tout point de $I$.
\end{itemize}
}

\remarque{Ainsi, $f$ est continue en $a$ si et seulement si 
\[\forall~\eps>0,~\exists~\eta >0,~\forall~x\in I,~|x-a|<\eta \Rightarrow |f(x)-f(a)|<\eps\]}

\remarque{Il résulte des théorèmes sur les limites que la \textbf{somme}, le \textbf{produit}, et la \textbf{composée} de deux fonctions continues est continue.}

\exemple{La fonction carré est une fonction continue sur $\R$.
\begin{center}\includegraphics[width=5cm]{tex/Chap12/carree}\end{center}}

\exemple{La fonction \textbf{partie entière} n'est pas continue en $0,1,2,\ldots$ :
\begin{center}\includegraphics[width=5cm]{tex/Chap12/partie_ent}\end{center}}

\propriete{D'après un résultat vu sur les limites de fonctions, $f$ est continue en $a$ si et seulement si $f$ est continue à droite et à gauche en $a$ et si ces limites sont égales.}

\begin{methode}
Pour montrer qu'une fonction définie par morceaux est continue en un réel $a$, on calcule les limites à droite et à gauche en ce réel, et on montre que les deux limites valent $f(a)$.
\end{methode}

\exemple{Soit $f$ la fonction définie sur $\R$ par \[f(x)= \left \{ \begin{array}{cc} x+1 & \textrm{ si }x\geq 0 \\ \eu{-x} & \textrm{sinon}\end{array}\right. \]
Montrer que $f$ est continue en $0$.}

\begin{prof}
\solution{On a $f(0)=1$. Constatons alors que
\[\lim_{x\rightarrow 0^+} f(x)=\lim_{x\rightarrow 0^+} x+1 = 1 = f(0)\]
\[\lim_{x\rightarrow 0^-} f(x)=\lim_{x\rightarrow 0^-} \eu{-x}=\eu{-0}=1=f(0)\]
Ainsi, $f$ est continue à droite et à gauche en $0$. Elle est donc continue en $0$.
}	
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{5}{\textwidth}
\end{solu_eleve}
\end{eleve}


\begin{histoire}
\textbf{Karl Weierstrass} (encore lui) donna vers 1850 la première définition de la continuité d'une fonction. C'est également lui qui donna les définitions rigoureuses de la dérivée, que l'on verra plus tard.
\end{histoire}

	\subsection{Continuité des fonctions usuelles}
	
\begin{proposition}
~
\begin{itemize}[label=\textbullet]
\item Les fonctions polynômes sont continues sur $\R$.
\item Les fonctions rationnelles sont continues sur tout intervalle contenu dans leur domaine de définition.
\item La fonction valeur absolue est continue sur $\R$.
\item La fonction racine carré est continue sur $[0;+\infty[$.
\item La fonction exponentielle est continue sur $\R$, et la fonction $\ln$ sur $\R^{+*}$.
\item Les fonctions puissances $x\mapsto a^x$ ($a>0$) sont continues sur $\R$. Les fonctions puissances $x \mapsto x^a$ ($a \in \R$) sont continues sur $\R^{+*}$.
\item Les fonctions trigonométriques ($\cos, \sin, \tan$), trigonométriques réciproques ($\arccos, \arcsin, \arctan$) et hyperboliques ($\cosh, \sinh, \tanh$) sont continues sur leur domaine de définition.
\end{itemize}	
\end{proposition}


\preuve{Admis. On verra dans un chapitre ultérieure qu'une fonction dérivable est continue, ce qui nous permet de conclure rapidement.}

	\subsection{Prolongement par continuité}
	
\definition{Soit $f$ une fonction définie sur un intervalle $I$ sauf en un réel $a$. On suppose que $f$ est continue sur $I\backslash \{a\}$, et qu'il existe un réel $l$ tel que $\displaystyle{\lim_{x\rightarrow a} f(x)=l}$. On dit qu'on peut \textbf{prolonger par continuité} la fonction $f$ en posant $f(a)=l$. On définit ainsi une nouvelle fonction, définie sur $I$, qui est continue sur $I$ et coïncide avec $f$ sur $I\backslash \{a\}$.}

\exemple{Soit $f$ la fonction définie sur $\R^*_+$ par $f(x)=x\ln(x)$. Montrer que l'on peut prolonger par continuité $f$ en $0$.}

\begin{prof}
\solution{On constate que $f$ est continue sur $\R^*_+$. De plus, \[\lim_{x\rightarrow 0^+} f(x)=0 \textrm{ par croissance comparée}\]
Ainsi, on peut prolonger par continuité $f$ en $0$ en posant $f(0)=0$.
}	
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{3}{\textwidth}
\end{solu_eleve}
\end{eleve}

\remarque{Rigoureusement, on doit définir une nouvelle fonction $\tilde{f}$ qui est égale à $f$ sur $I\backslash \{a\}$, et telle que $\tilde{f}(a)=l$. En pratique, on confondra toujours $\tilde{f}$ et $f$.}

	\subsection{Tableau de variations et convention}
	
\remarque{Dans un tableau de variation, on convient que les flèches obliques indiquent que la fonction est \textbf{continue et strictement monotone}.}

    \subsection{Suites récurrentes et continuité}
    
\begin{theoreme}
Soit $f$ une fonction continue, et $l$ un réel. Soit $(u_n)$ une suite convergente, de limite $l$. Alors la suite $(f(u_n))$ converge également, et a pour limite $f(l)$.	
\end{theoreme}


\remarque{Si $f$ n'est pas continue en $x_0$ mais si la fonction $f$ a pour limite $l$ en $x_0$, alors quelque soit la suite $(u_n)$ de limite $x_0$, $(f(u_n))$ converge vers $l$.}

\begin{theoreme}[Théorème du point fixe] 
Soit $f$ une fonction continue, et $u$ une suite définie par $u_0$ donné, et pour tout $n$, $u_{n+1}=f(u_n)$. 
\\ Si la suite $u$ est convergente, alors par passage à la limite, en notant $l$ la limite de $u$, on en déduit que la limite $l$ vérifie $l=f(l)$ : c'est donc un \textbf{point fixe} de la fonction $f$.	
\end{theoreme}


\begin{methode}
Le théorème précédent permet souvent de déterminer la limite d'une suite définie par récurrence lorsqu'elle est convergente. 
\end{methode}

\exercice{Soit $u$ la suite définie par $u_0=\frac{1}{2}$ et, pour tout $n$, $u_{n+1}=\sqrt{u_n}$. 
\begin{enumerate}
    \item Montrer que la suite $u$ est croissante, et que pour tout $n$, $0\leq u_n \leq 1$.
    \item En déduire que la suite $u$ converge, et déterminer sa limite. 
\end{enumerate}
}

\begin{prof}
\solution{~
\begin{enumerate}
    \item Méthode classique : on montre par récurrence que pour tout $n$, $0\leq u_n\leq 1$ et que $u_n\leq u_{n+1}$. ~\\Pour tout entier $n$, soit $P_n$ la proposition définie par ``$0\leq u_n \leq 1$''.
    \begin{itemize}
        \item[$\bullet$] Initialisation : pour $n=0$, on a $u_0=\frac{1}{2}$ et on a bien $0\leq u_0 \leq 1$ : $P_0$ est vraie.
        \item[$\bullet$] Hérédité : supposons que la propriété $P_n$ est vraie pour un certain entier $n$, et montrons que $P_{n+1}$ est vraie. \\On a $0\leq u_n \leq 1$. La fonction racine étant croissante sur $\R^+$, on a donc
        \[\sqrt{0} \leq \sqrt{u_n} \leq 1\]
        et donc
        $0\leq u_{n+1} \leq 1$ : $P_{n+1}$ est donc vraie : la propriété est héréditaire.
        \item[$\bullet$] Conclusion : d'après le principe de récurrence, la proposition $P_n$ est vraie pour tout entier $n$ :
        \[\forall~n,~0\leq u_n \leq 1\]
    \end{itemize}
    ~\\Pour tout entier $n$, soit $Q_n$ la proposition définie par ``$u_n \leq u_{n+1}$''.
    \begin{itemize}
        \item[$\bullet$] Initialisation : pour $n=0$, on a $u_0=\frac{1}{2}$ et $u_1=\sqrt{\frac{1}{2}} = \frac{\sqrt{2}}{2} > u_0$. $u_0 \leq u_1$ : $Q_0$ est vraie.
        \item[$\bullet$] Hérédité : supposons que la propriété $Q_n$ est vraie pour un certain entier $n$, et montrons que $Q_{n+1}$ est vraie. \\On a $ u_n \leq u_{n+1}$. La fonction racine étant croissante sur $\R^+$, on a donc
        \[\sqrt{u_n} \leq \sqrt{u_{n+1}}\]
        et donc
        $u_{n+1} \leq u_{n+2}$ : $Q_{n+1}$ est donc vraie : la propriété est héréditaire.
        \item[$\bullet$] Conclusion : d'après le principe de récurrence, la proposition $Q_n$ est vraie pour tout entier $n$ :
        \[\forall~n,~u_n \leq u_{n+1}\]
        La suite $u$ est donc croissante.
    \end{itemize}
	\textbf{Remarque} : on aurait également pu démontrer, par récurrence, les deux propositions en une seule, en démontrant ``$0\leq u_n \leq u_{n+1}\leq 1$''.
    \item La suite $u$ est donc croissante et majorée. D'après le théorème de convergence monotone, celle-ci converge. Notons $l$ sa limite.
Puisque la fonction racine est continue sur $]0;+\infty[$, on en déduit par passage à la limite que que $l=\sqrt{l}$, c'est à dire $l=0$ ou $l=1$. Or, puisqu'elle est croissante, \[\textrm{Pour tout }~n, u_n\geq u_0=\frac{1}{2}\]
 La limite est donc $1$.
\end{enumerate}
}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{15}{\textwidth}
\end{solu_eleve}
\end{eleve}


    \subsection{Continuité par morceaux}
    
  \definition{Une fonction $f$ est dite \textbf{continue par morceaux} sur le segment $[a,b]$ s’il existe une \textbf{subdivision} $a_0=a<a_1 <\cdots<a_n=b$ (c'est-à-dire un découpage du segment $[a;b]$) telle que les restrictions de $f$ à chaque intervalle ouvert $]a_i , a_{i+1} [$ admettent un prolongement continu à l’intervalle fermé $[a_i, a_{i+1}]$.
  }

  \begin{center}
      \includegraphics[width=7cm]{tex/Chap12/continuemorceau}
  \end{center}
  
 \exemple{La fonction partie entière est continue par morceaux sur $\R$. En effet, sur tout segment de la forme $]n;n+1[$ (où $n \in \Z$) le fonction est continue (car constante), et prolongeable par continuité en $n$ et $n+1$.} 

\section{Fonctions continues et résolution d'équation}

	\subsection{Théorème des valeurs intermédiaires}
	
\begin{theoreme}
Soit $f$ une fonction définie et continue sur un intervalle $I$ et à valeurs réelles. Alors l'image $f(I)$ est un intervalle.	
\end{theoreme}


\remarque{On a mieux : si $I$ est un segment, $f(I)$ est également un segment. Ainsi, si $I$ est un segment $[a;b]$, le maximum et le minimum sont atteints : il existe deux réels $u$ et $v$ de $I$ tels que $\displaystyle{f(u)=\max_{x\in I} f(x)}$ et $\displaystyle{f(v)=\min_{x\in I} f(x)}$.
}

On utilisera la variante plus explicite suivante :	
	
\begin{theoreme}
Soit $f$ une fonction continue sur un intervalle $[a;b]$. Alors, pour tout réel $k$ pris entre $f(a)$ et $f(b)$, il existe \textbf{au moins} un réel $c$ de l'intervalle $[a;b]$, tel que $f(c)=k$.	
\end{theoreme}


\begin{center}
\includegraphics[width=7cm]{tex/Chap12/val_int}
\end{center}

\preuve{Théorème admis.}

\begin{histoire}
\textbf{Bernard Bolzano} donna en 1817 la première démonstration analytique de ce théorème. \textbf{Weierstrass} en donna une autre plus tard, vers 1850.
\end{histoire}

	\subsection{Théorème de la bijection}
	
Dans le cas d'une fonction continue strictement monotone sur un segment, on dispose d'un énoncé plus précis :	

\begin{theoreme}
Soit $f$ une fonction \textbf{continue strictement monotone} sur l'intervalle $I=[a;b]$. Alors
pour tout réel $k$ compris entre $f(a)$ et $f(b)$, l'équation $f(x)=k$ possède une \textbf{unique solution} dans $[a;b]$.	
\end{theoreme}


\begin{prof}
\preuve{Soit $k$ un réel compris entre $f(a)$ et $(b)$. $f$ étant une fonction continue, d'après le théorème des valeurs intermédiaires, il existe au moins un réel $c$ tel que $f(c)=k$. Supposons la fonction strictement croissante (le cas décroissant se montre de la même manière). Alors :
\begin{itemize}
	\item[$\bullet$] Pour tout $x>c$, on a $f(x)>f(c)=k$ donc il n'y a aucun réel $x>c$ vérifiant $f(x)=k$.
	\item[$\bullet$] Pour tout $x<c$, on a $f(x)<f(c)=k$ donc il n'y a aucun réel $x<c$ vérifiant $f(x)=k$.
\end{itemize} 
Donc le réel $c$ est le seul.}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{4}{\textwidth}
\end{solu_eleve}
\end{eleve}

\remarque{Ainsi, si $f:I\rightarrow \R$ est continue strictement monotone, alors $f$ est bijective de $I$ sur $f(I)$.}

\begin{methode}
On utilise le théorème de la bijection (ou \textit{corollaire du théorème des valeurs intermédiaires}) pour démontrer l'existence et l'unicité d'une solution à une équation. On vérifiera bien toutes les hypothèses : continuité, stricte monotonie, et déterminer l'intervalle image.	
\end{methode}


\exemple{	
On suppose que la fonction $f$ est continue, et que ses variations sont décrites dans le tableau suivant :

	\input{tex/Chap12/tabvar.cours.1}

Montrer que l'équation $f(x)=0$ possède une unique solution sur $[0;5]$.}

\begin{prof}
\solution[5]{~
\begin{itemize}
	\item[$\bullet$] $f$ est continue et strictement croissante sur $[0;5]$. 
	\item[$\bullet$] On a $f(0)=-3$, $f(5)=4$, donc $f([0;5])=[-3;4]$.
	\item[$\bullet$]  $0 \in [-3;4]$
\end{itemize}

D'après le corollaire du théorème des valeurs intermédiaires (\textit{ou théorème de la bijection}), l'équation $f(x)=0$ possède une unique solution sur l'intervalle $[0;5]$.
}
\end{prof}

	\subsection{Extension à un intervalle non borné}
	
Le théorème précédent peut être étendu dans le cas d'un intervalle $I$ quelconque. 

\begin{theoreme}
Soit $f$ une fonction continue strictement monotone sur un intervalle $I$. Alors l'image de $I$ par $f$ est encore un intervalle, $J$. De plus, pour tout réel $y$ de $J$, l'équation $f(x)=y$ possède une unique solution dans $I$.	
\end{theoreme}


\exemple{Si $f$ est une fonction continue et strictement croissante sur $]a;b]$, alors pour tout réel $k$ de l'intervalle $\displaystyle{\left]\lim_{x\rightarrow a^+} f(x);f(b)\right]}$, l'équation $f(x)=k$ possède une unique solution dans $]a;b]$.
}

\exercice{Prouver que l'équation $(E): x\sqrt{x}=1-x$ admet une unique solution sur $\R>$.}

\begin{prof}
\solution{On se ramène à une écriture $f(x)=k$ : $x+x\sqrt{x}=1$. Soit $f$ la fonction définie sur $\R^{+*}$ par $f(x)=x+x\sqrt{x}$. $f$  est dérivable sur $\R^{+*}$ et sa dérivée vaut 
\[f'(x)=1+\frac{3}{2}\sqrt{x} > 0\]
La fonction $f$ est donc strictement croissante, et continue sur $]0;+\infty[$. L'image de $]0;+\infty[$ est donc \[\left]\lim_{x\rightarrow 0} f(x); \lim_{x\rightarrow +\infty} f(x)\right[ = ]0;+\infty[\] qui contient $1$.\\
L'équation $f(x)=1$ possède donc une unique solution sur $]0;+\infty[$.
}

\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{8}{\textwidth}
\end{solu_eleve}
\end{eleve}

	\subsection{Méthode d'encadrement d'une solution par dichotomie}
	
Dans le cas d'une fonction continue, et strictement monotone sur un intervalle $[a;b]$, avec $f(a)$ et $f(b)$ de signe contraire, on peut déterminer une valeur approchée de la solution de l'équation $f(x)=0$ par \textbf{dichotomie} : on découpe au fur et à mesure l'intervalle en $2$ pour pouvoir cibler la solution.

\begin{center}
\includegraphics[width=7cm]{tex/Chap12/dichotomie}\\
\textit{Image disponible sur Wikipédia}
\end{center}


\begin{histoire}
Le mot dichotomie vient du grec \textit{dikha} (\textit{en deux}), et \textit{tomein} (\textit{couper}), c'est à dire ``couper en deux''.
\end{histoire}

\algorithme{Dans cet algorithme, $e$ représente la précision de la valeur approchée.

~\\
\begin{algorithm}[H]
\DontPrintSemicolon
\SetAlgoLined
\SetKwInOut{Initialisation}{Initialisation}
\SetKwFor{Tq}{Tant que}{}{FinTantque}
\Entree{Saisir $a$, $b$ et $e$}
\Tq{$b-a \geq e$}{
	$m \leftarrow \frac{a+b}{2}$\;
	\eSi{$f(a)\times f(m) \leq 0$}{$b\leftarrow m$}{$a\leftarrow m$}\; }
\Sortie{Afficher $a$ et $b$}
\caption{DICHOTOMIE}
\end{algorithm}
}


~\\En Scilab, cela donne (à condition que la fonction $f$ ait été définie, dans le cas $f$ croissante) la fonction suivante, où $[x;y]$ désigne l'intervalle de recherche de départ, et $eps$ la précision :

\begin{scilab}[Algorithme de dichotomie]
{\scriptsize \lstinputlisting[]{tex/Chap12/dicho.sci}}	
\end{scilab}
 
    \subsection{Continuité et sens de variation de $f^{-1}$}
  
 \begin{theoreme}
 Soit $f$ une fonction continue et strictement monotone sur un intervalle $I$. Alors $f^{-1}:f(I)\rightarrow I$ est continue sur $f(I)$, strictement monotone et de même monotonie que la fonction $f$.	
 \end{theoreme}


\begin{prof}
\preuve{Supposons $f$ strictement croissante (le raisonnement est le même dans le cas décroissant). Soient $x$ et $y$ deux éléments de $f(I)$ tels que $x<y$. Puisqu'ils sont dans $f(I)$, il existe $a$ et $b$ dans $I$ tels que $x=f(a)$ et $y=f(b)$.\\
Puisque $f$ est strictement croissante, $x=f(a)< f(b)=y \Rightarrow a < b$. Or, $a=f^{-1}(x)$ et $b=f^{-1}(y)$. On a donc montré 
\[\forall~(x,y)\in (f(I))^2,~~x<y \Rightarrow f^{-1}(x)< f^{-1}(y)\]
$f^{-1}$ est donc strictement croissante. \\
Etant strictement monotone, elle admet des limites à droite et à gauche en tout point de $f(I)$. Supposons  qu'il existe $y\in f(I)$ tel que les limites à droite et à gauche de $f^{-1}$ en $y$ soient différentes.  En repassant par $f$, par continuité de $f$, les limites à droite et à gauche en $f^{-1}(y)$ sont donc différentes, ce qui est absurde, puisque $f$ est continue sur $I$. 
}
\end{prof}

\begin{eleve} 
\begin{solu_eleve}
\notes[1.8ex]{7}{\textwidth}
\end{solu_eleve}
\end{eleve}
