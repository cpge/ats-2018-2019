%%%%%%%%%%%%%%%%%
%%% EXERCICES %%%
%%%%%%%%%%%%%%%%%
\newpage
\section*{Exercices}
\addcontentsline{toc}{section}{Exercices}%
%\protect\numberline{}`

\subsection*{Courbes paramétrées}

\exercice{[Bicorne] Étudier et représenter la courbe paramétrée par
\[\systeme*{x(t)=a\cos(t),y(t)=a\frac{\sin^2(t)}{2+\sin(t)}}, \quad t\in \R \]
}

\begin{prof}
\solution{Remarquons tout d'abord que $x$ et $y$ sont de classe $\CC^\infty$ sur $\R$ (par produit et quotient de fonctions trigonométriques, avec $2+\sin(t)\geq 1 \neq 0$.

Après calcul rapide de $x(-t)$ et $y(-t)$, on n'obtient pas, \textit{a priori}, de symétrie autre que la $2\pi$-périodicité des deux fonctions $x$ et $y$. On étudie alors la fonction $f$ sur $[-\pi;\pi]$.

On détermine $f'(t)$ pour tout réel $t$ :
\begin{align*}
	x'(t) &= -a\sin(t)\\
	\text{et }y'(t) &= a \frac{2\cos(t)\sin(t)(2+\sin(t))-\sin^2(t)\cos(t)}{(2+\sin(t))^2}\\
		  &= a \frac{\sin(t)\cos(t)(4+\sin(t))}{(2+\sin(t))^2}
\end{align*}
Remarquons ainsi que $x'(t)$ est du signe de $\sin(t)$ et $y'(t)$ est du signe de $\sin(t)\cos(t)$ (car $4+\sin(t)>0$). On obtient alors le tableau de signe suivant :

\[\begin{array}{|c|ccccccccc|}\hline
t & -\pi & &-\frac{\pi}{2}&&0&&\frac{\pi}{2}&&\pi\\\hline
\sin(t) & 0 & - & -1 & - & 0 & + & 1 & + & 0\\\hline
\cos(t) & -1 & - & 0 & + & 1 & + & 0 & - & -1\\\hline
x'(t) & 0 & + & a & + & 0 & - & -a & - & 0 \\\hline
y'(t) & 0 & + & 0 & - & 0 & + & 0 & - & 0\\\hline
\end{array}\]

En $-\frac{\pi}{2}$, un vecteur directeur de la tangente est $(a,0)$ et en $\frac{\pi}{2}$, un vecteur directeur est $(-a,0)$.
Remarquons que, pour $t\neq \dfrac{\pi}{2}[\pi]$, on a :
\begin{align*}
  \frac{y'(t)}{x'(t)} &= -\frac{2\cos(t)(4+\sin(t))}{(2+\sin(t))^2}	
\end{align*}
qui vaut $1$ en $-\pi$ et $\pi$, et $-1$ en $0$. Ainsi, $(1,1)$ est un vecteur directeur de la tangente en $-\pi$ et $\pi$, et $(1,-1)$ est un vecteur directeur de la tangente en $0$.

On obtient alors le tableau de variations conjointes suivant :
\begin{center}\input{tex/Chap21/tab/bicornetab}\end{center}

Puis on obtient la courbe suivante :
\begin{center}\includegraphics{tex/Chap21/mp/bicorne.mps}\end{center}
}	
\end{prof}


\exercice{[Deltoïde] Étudier et représenter la courbe paramétrée par
\[ \systeme*{x(t)=2\cos(t)+\cos(2t),y(t)=2\sin(t)-\sin(2t) },\quad t\in \R \]
}

\begin{prof}
\solution{Remarquons tout d'abord que $x$ et $y$ sont $2\pi$-périodiques. On peut restreindre l'étude à l'intervalle $[-\pi,\pi]$, symétrique par rapport à $0$. De plus, pour tout $t\in [-\pi;\pi]$ :
\begin{align*}
	x(-t)&= 2\cos(-t)+\cos(-2t)=x(t)	\\
	y(-t)&= 2\sin(-t)-\sin(-2t)=-y(t)
\end{align*}
Ainsi, la courbe est symétrique par rapport à l'axe des abscisses, et on peut restreindre l'étude à l'intervalle $[0;\pi]$.

$x$ et $y$ sont de classe $\CC^\infty$ sur $\R$. Pour tout $t\in [0;\pi]$ :
\begin{align*}
x'(t)&=-2\sin(t)-2\sin(2t) & \text{et } y'(t)&=2\cos(t)-2\cos(2t)\\
     &=-2(\sin(t)+\sin(2t))&  &=2(\cos(t) -\cos(2t))\\
     &=-4\sin\left(\frac{3t}{2}\right)\cos\left(\frac{t}{2}\right)	& &=-4\sin\left(\frac{3t}{2}\right)\sin\left(-\frac{t}{2}\right)\\& & &=4\sin\left(\frac{3t}{2}\right)\sin\left(\frac{t}{2}\right)
\end{align*}
Sur $[0;\pi]$, $\cos\left(\frac{t}{2}\right)\geq 0$ et $\sin\left(\frac{t}{2}\right)\geq 0$. On obtient le tableau de signe suivant :
\[\begin{array}{|c|ccccc|}\hline
t& 0 & & \frac{2\pi}{3}&& \pi\\\hline
x'(t) & 0 & - & 0 & + & 0 \\\hline
y'(t) & 0 & + & 0 & - & -4 \\\hline	
\end{array}
\]
Remarquons que, pour $t\in \R \setminus \{0, \frac{2\pi}{3}, \pi\}$ :
\begin{align*}
 \frac{y'(t)}{x'(t)} &= \frac{4\sin\left(\frac{3t}{2}\right)\sin\left(\frac{t}{2}\right)}{-4\sin\left(\frac{3t}{2}\right)\cos\left(\frac{t}{2}\right)}\\
 &= -\frac{\sin\left(\frac{t}{2}\right)}{\cos\left(\frac{t}{2}\right)}
\end{align*}
et alors 
\[ \lim_{t\rightarrow 0} \frac{y'(t)}{x'(t)} = 0  \qeq  \lim_{t\rightarrow \frac{2\pi}{3}} \frac{y'(t)}{x'(t)} = - \sqrt{3} \]
On obtient alors le tableau de variations conjointes suivant :
\begin{center}\input{tex/Chap21/tab/deltoidetab.tex}\end{center}
Et ainsi, la courbe suivante: 
\begin{center}\includegraphics[width=7cm]{tex/Chap21/mp/deltoide.mps}\end{center}	
}
\end{prof}


\exercice{[Tractrice] Étudier et représenter la courbe paramétrée par
\[ \systeme*{x(t)=t-\tanh(t),y(t)=\frac{1}{\cosh(t)}},\quad t\in \R \]
}

\begin{prof}
\solution{Remarquons ici que $x$ et $y$ sont de classe $\CC^\infty$ sur $\R$ (fonctions hyperboliques et polynômes, et $\cosh(t)\neq 0$ pour tout réel $t$).

Pour tout réel $t$, on a 
\begin{align*}
	x(-t)&= (-t)-\th(-t) &\text{et } y(-t)&= \frac{1}{\cosh(-t)}\\&=-(t-\th(t))=-x(t)&&=\frac{1}{\cosh(t)}=y(t)
\end{align*}
Ainsi, la courbe est symétrique par rapport à l'axe des ordonnées, et on peut restreindre l'intervalle d'étude à $\R^+$.

Puisque $\ds{\lim_{t\rightarrow +\infty} \th(t)=1}$ et $\ds{\lim_{t\rightarrow +\infty} \cosh(t)=+\infty}$, par somme et quotient : 
\[ \lim_{t\rightarrow +\infty} x(t)=+\infty \qeq \lim_{t\rightarrow +\infty} y(t)=0 \]
Ainsi, $\ds{\lim_{t\rightarrow +\infty} ||f(t)||=+\infty}$ et $\ds{\lim_{t\rightarrow +\infty} \frac{y(t)}{x(t)}=0}$ : l'axe des abscisses est asymptote à la courbe de $f$ au voisinage de $+\infty$.
Pour tout réel $t\in \R^+$, on a:
\begin{align*}
	x'(t) &= 1-(1-\th^2(t)) &\text{et } y'(t) &= -\frac{\sinh(t)}{\cosh^2(t)} \\ &= \th^2(t) 
\end{align*}
Ainsi, $x'(t)>0$ pour tout réel $t\in \R^+$, et $y'(t)$ est du signe de $-\sinh(t)$. 

Enfin, on constate que, pour $t>0$ :
\begin{align*}
 \frac{y'(t)}{x'(t)} &= \frac{-\frac{\sinh(t)}{\cosh^2(t)}}{\th^2(t)} \\
 &= -\frac{ \frac{\sinh(t)}{\cosh^2(t)}}{\frac{\sinh^2(t)}{\cosh^2(t)}} = -\frac{1}{\sinh(t)}	
\end{align*}
Mais alors, par quotient, \[ \lim_{t\rightarrow 0} \frac{y'(t)}{x'(t)} = -\infty \]
Ainsi, la tangente au point $M(0)$ est verticale.

\remarque{On aurait pu, ici, faire la dérivée seconde : \[f''(t)=\left(2(1+\th^2(t))\th(t); - \frac{\cosh^3(t)-2\sinh^2(t)\cosh(t)}{\cosh^4(t)}\right)\]
et alors $f(0)=(0; -1)$.}

On obtient de tableau de variations conjointes suivant :
\input{tex/Chap21/tab/tractricetab.tex}
et la courbe représentative suivante :
\begin{center}\includegraphics{tex/Chap21/mp/tractrice.mps}\end{center}
}	
\end{prof}


\subsection*{Sujet de concours}

\exercice{[ATS 2014] Soit la courbe $\mathcal{C}$ de représentation paramétrique dans le repère orthonormé $(O,\vv{i},\vv{j})$ :

\[ \systeme*{x(\alpha)=\alpha-\sin(\alpha),y(\alpha)=1-\cos(\alpha)}\quad \text{avec }\alpha\in \R \]
On note $M(\alpha)$ le point de paramètre $\alpha$ de $\mathcal{C}$.
\begin{enumerate}
	\item \begin{enumerate}
			\item Préciser la parité des fonctions $x$ et $y$. En déduire une symétrie pour la courbe $\mathcal{C}$.
			\item Donner les coordonnées du milieu du segment $[M(\alpha),M(2\pi-\alpha)]$. En déduire une symétrie pour la courbe $\mathcal{C}$.
			\item Donner les composantes du vecteur $\vv{M(\alpha)M(\alpha+2\pi)}$. Montrer qu'il existe des translations à préciser qui laissent la courbe $\mathcal{C}$ invariante.
			\item Déduire de ce qui précède toutes les symétries de la courbe $\mathcal{C}$.
		  \end{enumerate} 
	\item \begin{enumerate}
			\item Faire une étude conjointe des fonctions $x$ et $y$ sur $[0;2\pi]$. (dérivée, extrema, sens de variation, tableau de variation.)
			\item Déterminer la limite $\ds{\lim_{\alpha\rightarrow 0^+} \frac{y(\alpha)}{x(\alpha)}}$. On admettra qu'il en résulte que $\mathcal{C}$ admet une tangente verticale en $M(0)$.
			\item Donner une représentation graphique de $\mathcal{C}$ pour $\alpha \in [-2\pi,2\pi]$ dans le repère orthonormé $(0,\vv{i},\vv{j})$.
		  \end{enumerate} 
\end{enumerate}
On suppose maintenant que $\alpha \in ]0;\pi[$. On note $M$ le point $M(\alpha)$ de $\mathcal{C}$.
\begin{enumerate}
	\setcounter{enumi}{2}
	\item \begin{enumerate}
			\item Donner les composantes d'un vecteur directeur $\vv{t}$ de la tangente $\mathcal{T}$ à $\mathcal{C}$ en $M$, et d'un vecteur directeur $\vv{n}$ de la normale $\mathcal{N}$ à $\mathcal{C}$ en $M$.
			\item Donner une équation de la droite $\mathcal{N}$ normale à $\mathcal{C}$ en $M$. Déterminer les coordonnées du point $U$, intersection de $\mathcal{N}$ avec l'axe $(Ox)$.
			\item Donner une équation de la droite $\mathcal{T}$ tangente à $\mathcal{C}$ en $M$. Déterminer les coordonnées du point $V$, intersection de $\mathcal{T}$ avec la droite d'équation $y=2$.
		  \end{enumerate} 
\end{enumerate}
On appelle $I$ le milieu du segment $[UV]$.
\begin{center}
	\includegraphics[width=6cm]{tex/Chap21/mp/ats2014enonce.mps}
\end{center}
\begin{enumerate}
	\setcounter{enumi}{3}
	\item \begin{enumerate}
			\item Montrer que le cercle de centre $I$ et de rayon $1$ contient $U$, $V$ et $M$.
			\item Donner les composantes du vecteur $\vv{IM}$. En déduire une mesure de l'angle $\widehat{\vv{IU},\vv{IM}}$.
			\item Comparer la longueur de l'arc de cercle $\wideparen{UM}$ (situé sous la droite $(UM)$) et la longueur $[OU]$ ($O$ est l'origine du repère).
		 \end{enumerate} 
\end{enumerate}
}

\begin{prof}
\solution{~\remarque{L'objectif de cet exercice est multiple :
\begin{itemize}[label=\textbullet]
	\item Manipuler une courbe paramétrée, avec les méthodes classiques.
	\item Déterminer une tangente en utilisant un développement limité.
	\item Déterminer des propriétés concernant la courbe.
\end{itemize}
Aucune difficulté, il suffit d'écrire proprement les choses.}
\begin{enumerate}
	\item \begin{enumerate}
		\item Remarquons que $x$ et $y$ sont définies sur $\R$ qui est symétrique par rapport à $0$. On a, rapidement, $x(-\alpha)=-x(\alpha)$ et $y(-\alpha)=y(\alpha)$ pour $\alpha \in \R$. Ainsi, $x$ est impaire et $y$ est paire : la courbe $\mathcal{C}$ admet donc une symétrie par rapport à l'axe des ordonnées.
		\item Notons $I(\alpha)$ le milieu du segment $[M(\alpha)M(2\pi-\alpha)]$. Alors
			\[ I(\alpha)\left( \frac{x(\alpha)+x(2\pi-\alpha)}{2}; \frac{y(\alpha)+y(2\pi-\alpha)}{2}\right) \]
			et donc
			\[ \boxed{I(\alpha) \left( \pi; y(\alpha)\right)} \] 
			Puisque $y(2\pi-\alpha)=y(\alpha)$, on en déduit que la courbe $\mathcal{C}$ admet une symétrie d'axe $x=\pi$.
		\item En utilisant les définitions de $x$ et $y$, on obtient rapidement
		\[ \vv{M(\alpha)M(\alpha+2\pi)}(2\pi; 0) \]
		Ainsi, le point $M(\alpha+2\pi)$ s'obtient en effectuant au point $M(\alpha)$ une translation de vecteur $2\pi \vv{i}$.
		\item Ainsi, la courbe $\mathcal{C}$ possède l'axe des abscisses, et la droite d'équation $x=\pi$ comme axe de symétrie. Puisqu'elle est invariante par translation de vecteur $2\pi\vv{i}$, toutes les droites d'équation $x=k\pi$ (pour $k\in \Z)$ est donc un axe de symétrie de la courbe. 
		\end{enumerate}
	\item \begin{enumerate}
		\item $x$ et $y$ sont de classe $\CC^\infty$ sur $\R$ (fonctions trigonométriques et affines). Pour tout $x\in [0;2\pi]$ on a
			\[ x'(\alpha)=1-\cos(\alpha) \qeq y'(\alpha)=\sin(\alpha) \]
			Puisque $-1\leq \cos(\alpha) \leq 1$, on en déduit que $x'$ est positive sur $[0;2\pi]$. Enfin, $y'(\alpha)$ est positif si et seulement $\alpha \in [0;\pi]$. \\On obtient alors le tableau de variation suivant :
			\input{tex/Chap21/tab/ats2014tab}
			\item La limite étant indéterminée, effectuons un développement limité :
			\begin{align*}
				\frac{y(\alpha)}{x(\alpha)} &= \frac{1-(1-\frac{\alpha^2}{2}+o_0(\alpha^3))}{\alpha-(\alpha-\frac{\alpha^3}{3!} + o_0(\alpha^3))} \\
				&= \frac{\frac{\alpha^2}{2}+o_0(\alpha^3)}{\frac{\alpha^3}{3!}+o_0(\alpha^3}\\
				&\sim_0 \frac{3}{\alpha} \underset{\alpha\rightarrow 0^+}{\longrightarrow} +\infty
			\end{align*}
			Ainsi, $\boxed{\ds{\lim_{\alpha\rightarrow 0^+} \frac{y(\alpha)}{x(\alpha)} = +\infty }}$ : la courbe $\mathcal{C}$ admet donc une tangente verticale au point $M(0)$.
			\item En utilisant d'une part les tangentes (horizontale en $\pi$, verticale en $0$ et donc en $2\pi$) ainsi que les symétries précédentes, on obtient sur $[-2\pi;2\pi]$ la courbe suivante :
			\begin{center}\includegraphics{tex/Chap21/mp/ats2014.mps} \end{center}
		\end{enumerate}
	\item \begin{enumerate}
		\item Pour $\alpha \in ]0;\pi[$, on constate que $f'(\alpha)(x'(\alpha),y'(\alpha)$ n'est jamais nul. Un vecteur directeur de la tangente $\mathcal{T}$ à $\mathcal{C}$ en $M$ est donc
			\[ \vv{t}\left( x'(\alpha), y'(\alpha)\right)=(1-\cos(\alpha), \sin(\alpha)) \]
			Un vecteur directeur de la normale $\mathcal{N}$ est donc donné par un vecteur normal au vecteur directeur de la tangente. Ainsi, un vecteur directeur de la normale est
			\[ \vv{n}(-\sin(\alpha), 1-\cos(\alpha)) \]
		\item Puisque $\vv{n}$ est un vecteur directeur de $\mathcal{N}$, $\mathcal{N}$ a une équation de la forme $(1-\cos(\alpha))x+\sin(\alpha)y+c=0$. Puisque $M \in \mathcal{N}$, on a 
		\[ (1-\cos(\alpha))(\alpha-\sin(\alpha))+\sin(\alpha)(1-\cos(\alpha)+c=0 \]
		soit
		\[ c= -\alpha(1-\cos(\alpha)) \]
		$\mathcal{N}$ a donc pour équation $ (1-\cos(\alpha))x+\sin(\alpha)y-\alpha(1-\cos(\alpha))=0$.\\Le point d'intersection $U$ vérifie le système
		\[\systeme{(1\-\cos(\alpha))x+\sin(\alpha)y-\alpha(1\-\cos(\alpha))=0,y=0} \]
		soit, après résolution rapide
		\[ \boxed{U(\alpha, 0) } \]
		\item Puisque $\vv{t}$ est un vecteur directeur de $\mathcal{T}$, $\mathcal{T}$ a une équation de la forme $-\sin(\alpha)x+(1-\cos(\alpha)y+c=0$. Puisque $M \in \mathcal{T}$, on a 
		\[ -\sin(\alpha)(\alpha-\sin(\alpha))+(1-\cos(\alpha))(1-\cos(\alpha)+c=0 \]
		soit
		\[ c= \alpha\sin(\alpha)-2(1-\cos(\alpha)) \]
		$\mathcal{T}$ a donc pour équation $ -\sin(\alpha)x+(1-\cos(\alpha))y+\alpha\sin(\alpha)-2(1-\cos(\alpha))=0$.\\Le point d'intersection $V$ vérifie le système
	\[ \systeme{(\-\sin(\alpha)x+(1\-\cos(\alpha)y+\alpha\sin(\alpha)\-2(1\-\cos(\alpha))=0,y=2}\]
		soit, après résolution rapide ($\sin(\alpha)\neq 0$) :
		\[ \boxed{V(\alpha, 2) } \]
		\end{enumerate} 
	\item $I$ a donc pour coordonnées $I(\alpha, 1)$. 
		\begin{enumerate}
			\item \textbf{Première méthode} : On a rapidement $IV=IU=1$. Enfin
				\begin{align*}
					IM&=\sqrt{(x(\alpha)-\alpha)^2+(y(\alpha)-1)^2}\\
					  &= \sqrt{(-\sin(\alpha))^2+(\cos(\alpha)^2)}\\
					  &= \sqrt{\sin^2(\alpha)+\cos^2(\alpha)}=1
				\end{align*} 
				Ainsi $IM=1$ et $M$ est bien sur le cercle de centre $I$ et de rayon $1$.\\
				\textbf{Deuxième méthode} : on calcule $\vv{MU}\cdot \vv{MV}$ :
			\[ (\alpha-(\alpha-\sin(\alpha)))(\alpha-(\alpha-\sin(\alpha))) + (-(1-\cos(\alpha)))(2-(1-\cos(\alpha))) \]
			et après calcul, $\vv{MU}\cdot\vv{MV}=0$ : ainsi $M$ est sur le cercle de diamètre $[UV]$, et donc sur le cercle de centre $I$ et de rayon $1$.
			\item On a, rapidement
			\[ \vv{IM}(-\sin(\alpha), -\cos(\alpha)\]
			Ainsi,
			\[ \vv{IU}\cdot \vv{IM} = 0\times (-\sin(\alpha))+(-1)(-\cos(\alpha)) = \cos(\alpha) \]
			Or, par définition du produit scalaire
			\[ \vv{IU}\cdot \vv{IM} = IU\times IM \times \cos(\widehat{\vv{IU},\vv{IM}})=\cos(\widehat{\vv{IU},\vv{IM}}) \]
			Ainsi, $\cos(\widehat{\vv{IU},\vv{IM}})=\cos(\alpha)$ et puisque $\alpha \in ]0;\pi[$, on a
			\[ \boxed{\widehat{\vv{IU},\vv{IM}}=\alpha} \]
		\item Puisque $\widehat{\vv{IU},\vv{IM}}=\alpha$, et que le cercle est de rayon $1$ (et donc son périmètre vaut $2\pi$), l'arc de cercle $\wideparen{UM}$ a pour longueur $2\pi \times \alpha$. Puisque $OU=\alpha$, on en déduit que, pour tout $\alpha \in ]0;\pi[$, \[ \boxed{\wideparen{UM}=2\pi \times OU}\]
		\end{enumerate}
\end{enumerate}
}	
\end{prof}
