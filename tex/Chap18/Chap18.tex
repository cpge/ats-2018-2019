\objectif{Dans ce chapitre, on généralise la notion d'intégrale, vue sur un segment, au cas d'un intervalle quelconque. On verra des méthodes pour s'assurer que ces intégrales existent, et pour les calculer.}

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
	\item Connaître la définition d'intégrale sur un intervalle :
			\begin{itemize}[label=\textbullet]
				\item connaître la définition\dotfill $\Box$
				\item connaître les différentes propriétés usuelles\dotfill $\Box$
			\end{itemize}
	\item Concernant les théorèmes d'existence :
			\begin{itemize}[label=\textbullet]
				\item connaître le théorème de majoration des fonctions positives\dotfill $\Box$
				\item savoir utiliser les équivalents de fonctions de signe constant\dotfill $\Box$
				\item connaître les intégrales de référence (Riemann, exponentielle)\dotfill $\Box$
				\item savoir appliquer un changement de variable à une intégrale généralisée\dotfill $\Box$
			\end{itemize}
	\item Connaître la notion de convergence absolue :
			\begin{itemize}[label=\textbullet]
				\item connaître la définition\dotfill $\Box$
				\item l'inégalité triangulaire, et le cas d'une fonction continue dont l'intégrale converge absolument vers $0$\dotfill $\Box$
			\end{itemize}
\end{numerote}

\newpage 

\section{Intégrale sur un intervalle quelconque}

L'idée est d'étendre la notion d'intégrale, mais sur un intervalle infini, du type $[a;+\infty[$, $]-\infty; a[$ voire $]-\infty;+\infty[$; où sur un intervalle du type $]a;b]$ ou la fonction n'est pas définie en $a$. 

    \subsection{Définition}
    
\definition{Soit $f$ une fonction continue, définie sur $[a;b[$, où $a\in \R$ et $b\in \R\cup\{+\infty\}$. On dit que l'intégrale $\ds{\int_a^b f(t)\dd t}$ est \textbf{convergente} si et seulement si $\ds{\lim_{x\rightarrow b} \int_a^x f(t)\dd t}$ existe et est finie. Dans ce cas, on note \[ \int_{[a;b[} f(t)\dd t=\int_a^b f(t)\dd t = \lim_{x\rightarrow b} \int_a^x f(t)\dd t \]
}

\remarque{On définit de même le cas où $f$ est définie sur $]a;b]$, ou $]a;b[$ (dans ce cas, il y a deux limites à traiter).}    
    
%\exe{~\begin{itemize}
%    \item[$\bullet$] Soit $f$ une fonction continue sur $[a;+\infty[$. On dit que l'intégrale $\ds{\int_a^{+\infty} f(t)\dd t}$ est \textbf{convergente} si et seulement si $\ds{\lim_{x\rightarrow +\infty} \int_a^x f(t)\dd t}$ existe et est finie. Dans ce cas, on note
%\[\int_{[a;+\infty[} f(t)\dd t=\int_a^{+\infty} f(t)\dd t=\lim_{x\rightarrow +\infty} \int_a^x f(t)\dd t\]
%    On définit de même $\ds{\int_{]-\infty;a]}f(t)\dd t=\int_{-\infty}^a f(t)\dd t}$.
%    \item[$\bullet$] Soit $f$ une fonction continue sur $\R$. On dit que l'intégrale $\ds{\int_{-\infty}^{+\infty} f(t)\dd t}$ est \textbf{convergente} si et seulement si $\ds{\lim_{x\rightarrow +\infty} \int_a^x f(t)\dd t}$ et $\ds{\lim_{x\rightarrow -\infty} \int_{x}^a f(t)\dd t}$ existent et sont finies. Dans ce cas, on note
%\[\int_{\R} f(t)\dd t=\int_{-\infty}^{+\infty} f(t)\dd t=\lim_{x\rightarrow -\infty} \int_x^a f(t)\dd t+\lim_{x\rightarrow +\infty} \int_a^x f(t)\dd t\]
%\end{itemize}
%}

\remarque{On dit que l'intégrale $\ds{\int_a^{b} f(t)\dd t}$ est une intégrale \textbf{impropre} (puisque, rigoureusement, l'intégrale n'est définie que sur un segment).}


\exemple{Soit $f:t\mapsto e^{-t}$ sur $[0;+\infty[$. Calculer $\ds{\int_0^{+\infty} e^{-t}\dd t}$.}

\begin{prof}
\solution{Soit $x>0$. Alors
\[\int_0^x f(t)\dd t = [-e^{-t}]_0^x = 1-e^{-x}\]
et $\ds{\lim_{x\rightarrow +\infty} 1-e^{-x} = 0}$. Donc $\ds{\int_0^{+\infty} f(t)\dd t}$ existe, et \[\int_0^{+\infty} e^{-t}\dd t = 1\]
}
\end{prof}

\lignes{5}
   
    \subsection{Propriétés}

Toutes les propriétés de base se généralisent aux intégrales sur un intervalle quelconque.

		\subsubsection{Linéarité}

\propriete{[Linéarité]Soient $f$ et $g$ deux fonctions continues sur $[a;b[$, et $\lambda \in \R$.
\begin{itemize}[label=\textbullet]
    \item Si $\ds{\int_a^{b} f(t)\dd t}$ converge, alors $\ds{\int_a^{b} \lambda f(t)\dd t}$ converge, et 
    \[\int_a^{b} \lambda f(t)\dd t=\lambda\int_a^{b} f(t)\dd t\]
    \item Si $\ds{\int_a^{b} f(t)\dd t}$ et $\ds{\int_a^{b} g(t)\dd t}$ convergent, alors $\ds{\int_a^{b} (f(t)+g(t))\dd t}$ converge également, et 
    \[\int_a^{b} (f(t)+g(t))\dd t=\int_a^{b} f(t)\dd t+\int_a^{b} g(t)\dd t\]
\end{itemize}
}

\remarque{Attention : l'intégrale $\ds{\int_a^{b} (f(t)+g(t))\dd t}$ peut exister, sans pour autant que $\ds{\int_a^{b} f(t)\dd t}$ et $\ds{\int_a^{b} g(t)\dd t}$ n'existent.\\Par exemple, $\ds{\int_1^{+\infty} \frac{1}{t}\dd t}$  et $\ds{\int_1^{+\infty} \frac{1}{t^2}-\frac{1}{t}\dd t}$ ne convergent pas, et pourtant la somme $\ds{\int_1^{+\infty} \frac{1}{t^2}\dd t}$ converge. }

		\subsubsection{Relation de Chasles}
		
\propriete{[Relation de Chasles] Soient $-\infty \leq a < c < b \leq +\infty$ et $f$ une fonction continue sur $]a;b[$. Alors $\ds{\int_a^b f(t)\dd t}$ converge si et seulement si $\ds{\int_a^c f(t)\dd t}$ et $\ds{\int_c^b f(t)\dd t}$ convergent. Dans ce cas
\[\int_a^b f(t)\dd t=\int_a^c f(t)\dd t+\int_c^b f(t)\dd t\]}

		\subsubsection{Positivité et croissance de l'intégrale}
		
\propriete{[Positivité et croissance] Soient $f$ et $g$ deux fonctions continues sur l'intervalle $[a;b[$. On suppose que $\ds{\int_a^b f(t)\dd t}$ et $\ds{\int_a^b g(t)\dd t}$ convergent.
\begin{itemize}[label=\textbullet]
	\item Positivité : si $f$ est positive sur $[a;b[$ (avec $a\leq b$),  alors $\ds{\int_a^b f(t)\dd t\geq 0}$.
	\item Croissance : si, pour tout $t\in [a;b[$, $f(t)\leq g(t)$ (avec $a\leq b$) alors \[ \int_a^b f(t)\dd t \leq \int_a^b g(t)\dd t \]
\end{itemize}
}

\section{Théorèmes d'existence}

    \subsection{Intégrales de référence}
    
\begin{theoreme}[Intégrale de Riemann] 
La fonction $f:t\mapsto \frac{1}{t^\alpha}$ est intégrable sur $[1;+\infty[$ si et seulement si $\alpha >1$. Dans ce cas
\[\int_1^{+\infty} \frac{1}{t^\alpha}\dd t = \frac{1}{\alpha -1}\]	
\end{theoreme}


\begin{prof}
\preuve{Dans le cas où $\alpha \neq 1$, on a
\[\int_1^x \frac{1}{t^\alpha}\dd t = \left[ \frac{1}{1-\alpha}\frac{1}{t^{\alpha-1}}\right]_1^x = \frac{1}{(1-\alpha)x^{\alpha-1}}-\frac{1}{1-\alpha}\]
\begin{itemize}[label=\textbullet]
	\item Si $\alpha>1$ : quand $x$ tend vers $+\infty$, cette intégrale converge vers $\ds{\frac{1}{\alpha-1}}$
	\item Si $\alpha < 1$ : \[\lim_{x\rightarrow +\infty} \frac{1}{x^{\alpha - 1 }} = +\infty\]
	donc l'intégrale diverge.
	\item Si $\alpha=1$, on a \[\int_1^x \frac{1}{t}\dd t=\ln(x) \textrm{ et } \lim_{x\rightarrow +\infty} \ln(x)=+\infty\] L'intégrale diverge donc.
\end{itemize}
}
\end{prof}

\lignes{10}

\begin{theoreme}[Intégrale de Riemann - 2]
La fonction $f:t\mapsto \frac{1}{t^\alpha}$ est intégrable sur $]0;1]$ si et seulement si $\alpha<1$. Dans ce cas, 
\[ \int_0^1 \frac{1}{t^\alpha}\dd t = \frac{1}{1-\alpha} \]	
\end{theoreme}

\begin{prof}
\preuve{Pour $\alpha \neq 1$, on a, pour tout $u\in]0;1[$,
\[ \int_u^1 \frac{1}{t^\alpha}\dd t = \left[ \frac{1}{1-\alpha} \frac{1}{t^{\alpha-1}} \right]_u^1 = \frac{1}{1-\alpha}- \frac{1}{1-\alpha} \frac{1}{u^{\alpha-1}}\]
Si $\alpha>1$, $\ds{\lim_{u\rightarrow 0} \frac{1}{u^{\alpha-1}} = +\infty}$. L'intégrale diverge donc. Si $\alpha<1$, $\ds{\lim_{u\rightarrow 0} \frac{1}{u^{\alpha-1}} = 0}$ donc l'intégrale converge et \[\int_0^1 \frac{1}{t^\alpha}\dd t=\frac{1}{1-\alpha}\]
Si $\alpha=1$, pour $u\in ]0;1[$, \[ \int_u^1 \frac{1}{t}\dd t=\ln(1)-\ln(u)=-\ln(u) \stackrel{u\rightarrow 0}{\longrightarrow} +\infty \]
Donc l'intégrale diverge également. 
}
\end{prof}


\lignes{10}


\begin{theoreme}
La fonction $f:t\mapsto e^{-\alpha t}$ est intégrable sur $[0;+\infty[$ si et seulement si $\alpha >0$. Dans ce cas, 
\[\int_0^{+\infty} e^{-\alpha t}\dd t =\frac{1}{\alpha}\]	
\end{theoreme}

\begin{prof}
\preuve{Si $\alpha=0$, $e^{-\alpha t} = 1$ et la fonction constante égale à $1$ n'est pas intégrable sur $[0;+\infty[$. Sinon,
\[\int_0^x e^{-\alpha t}\dd t = \left[ \frac{e^{-\alpha t}}{-\alpha} \right]_0^x = -\frac{e^{-\alpha x}}{\alpha} +\frac{1}{\alpha}\]
Cette intégrale converge si et seulement si $\alpha > 0$, et dans ce cas
\[\lim_{x \rightarrow +\infty} \int_0^x e^{-\alpha t}\dd t = \frac{1}{\alpha}\]}
\end{prof}

\lignes{9}

\begin{theoreme}
La fonction $f:t\mapsto \ln(t)$ est intégrable sur $]0;1]$, et on a \[ \int_0^1 \ln(t)\dd t=-1 \] 	
\end{theoreme}


\begin{prof}
\preuve{
Soit $a\in ]0;1[$. Par intégration par parties, ou en utilisant une primitive de $\ln$, on a
\[ \int_a^1 \ln(t)\dd t = \left[ t\ln(t)-t \right]_a^1 = -1-a\ln(a)+a \]
Or, on a $\ds{\lim_{a\rightarrow 0} a\ln(a) = 0}$ (croissance comparée). Par somme, $\ds{\lim_{a\rightarrow 0} \int_a^1 \ln(t)\dd t=1}$. Ainsi, l'intégrale converge, et \[ \int_0^1 \ln(t)\dd t=-1 \]
}	
\end{prof}

\lignes{9}
%
%\theoreme{La fonction $f:x\mapsto e^{-x^2}$ est intégrable sur $\R$ et 
%\[\int_{-\infty}^{+\infty} e^{-t^2}\dd t =\sqrt{\pi}\]}
%
%\preuve{Théorème admis.}

	\subsection{Majoration}

\begin{theoreme}
Soient $f$ et $g$ deux fonctions continues sur l'intervalle $[a;b[$. On suppose que \[\forall~x\in [a;b[, 0\leq f(x)   \leq g(x)\]
Alors, si $\ds{\int_a^{b} g(t)\dd t}$ converge, $\ds{\int_a^{b} f(t)\dd t}$ est également convergente, et on a 
\[0\leq \int_a^{b} f(t)\dd t \leq \int_a^{b} g(t)\dd t\]	
\end{theoreme}


\remarque{Attention : il faut absolument que $f$ et $g$ soient positives.}

\remarque{Il suffit que l'inégalité $f(x)\leq g(x)$ soit vraie au voisinage de $b$ pour que le résultat puisse tout de même s'appliquer.}

\exemple{Montrer que $\ds{\int_1^{+\infty} \frac{1}{t+t^2}\dd t}$ converge.}

\begin{prof}
\solution{Tout d'abord, $t\mapsto \dfrac{1}{t+t^2}$ est continue sur $[1;+\infty[$. Remarquons que pour tout $t\geq 1$ on a $t+t^2\geq t^2$, soit
\[0\leq \frac{1}{t+t^2} \leq \frac{1}{t^2}\]
Or, l'intégrale $\ds{\int_1^{+\infty}\frac{1}{t^2}\dd t}$ converge (intégrale de Riemman avec $\alpha=2>1$). Par comparaison, l'intégrale $\ds{\int_1^{+\infty} \frac{1}{t+t^2}\dd t}$ converge.
}	
\end{prof}

\lignes{10}
    
    \subsection{Equivalences et négligeabilité}

\begin{theoreme}
Soient $f$ et $g$ deux fonctions continues, \textbf{positives} sur l'intervalle $[a;b[$. On suppose que \[\ f(x) \sim_{b} g(x)\]
~\\Alors, $\ds{\int_a^{b} g(t)\dd t}$ converge, si et seulement si $\ds{\int_a^{b} f(t)\dd t}$ est également convergente. 
%Dans ce cas on a 
%\[0\leq \int_a^{b} f(t)\dd t \leq \int_a^{b} g(t)\dd t\]	
\end{theoreme}


\remarque{Attention : il faut absolument que $f$ et $g$ soient positives, ou a minima, de signe constant (si $f$ et $g$ sont négatives, on peut raisonner sur $-f$ et $-g$).}

\exercice{Traiter le cas de l'intégrale $\ds{\int_1^{+\infty} \frac{1}{t+t^2}\dd t}$ à l'aide d'équivalents.}

\begin{prof}
\solution{Par équivalences usuelles :
\[ \frac{1}{t+t^2} \sim_{+\infty} \frac{1}{t^2} \]
Les deux fonctions sont continues et positives sur $[1;+\infty[$, et l'intégrale $\ds{\int_1^{+\infty} \frac{1}{t^2}\dd t}$ est convergente (Riemann avec $\alpha=2>1$). Par comparaison de fonctions positives, l'intégrale $\ds{\int_1^{+\infty} \frac{1}{t^2}\dd t}$ converge.

}	
\end{prof}


\lignes{5}

\begin{theoreme}
Soient $f$ et $g$ deux fonctions continues, \textbf{positives} sur l'intervalle $[a;b[$. On suppose que \[\ f(x) =o_{b}\left( g(x)\right)\]
~\\Si $\ds{\int_a^{b} g(t)\dd t}$ converge, alors $\ds{\int_a^{b} f(t)\dd t}$ est également convergente. 
%Dans ce cas on a 
%\[0\leq \int_a^{b} f(t)\dd t \leq \int_a^{b} g(t)\dd t\]	
\end{theoreme}

\remarque{Attention : il faut absolument que $f$ et $g$ soient positives.}
\begin{methode}
La plupart du temps, on essaiera de comparer à l'une des intégrales de références, et principalement les intégrales de Riemann convergentes.	
\end{methode}


\exemple{Montrer que l'intégrale $\ds{\int_1^{+\infty} t\eu{-t}\dd t}$ converge.}

\begin{prof}
\solution{$t\mapsto t\eu{-t}$ est continue sur $[1;+\infty[$. On constate que 
\[ \frac{t\eu{-t}}{1/t^2}=t^3\eu{-t}\underset{t\rightarrow +\infty}{\longrightarrow} 0 \]
Ainsi, $\ds{t\eu{-t} = o_{+\infty}\left(\frac{1}{t^2}\right)}$. Puisque les deux fonctions sont positives, et que l'intégrale $\ds{\int_1^{+\infty} \frac{\dd t}{t^2}}$ converge (intégrale de Riemann), par comparaison de fonctions positives, on en déduit que l'intégrale  $\ds{\int_1^{+\infty} t\eu{-t}\dd t}$ converge également.
}	
\end{prof}

\lignes{10}

\subsection{Méthode d'étude}
  
\begin{methode}
Pour montrer qu'une intégrale impropre $\ds{\int_a^b f(t)\dd t}$ converge :
\begin{numerote}
	\item On étudier tout d'abord la continuité de la fonction sur $]a;b[$.
	\item On vérifie en quel(s) point(s) $a$, $b$ ou les deux, l'intégrale est impropre.
	\item On utilise les théorèmes de majoration ou d'équivalence pour montrer que l'intégrale converge.
\end{numerote}	
\end{methode}


\exemple{Montrer que l'intégrale $\ds{\int_0^{+\infty} \eu{-t^2}\dd t}$ converge.}

\begin{prof}
\solution{La fonction $t\mapsto \eu{-t^2}$ est continue et positive sur $[0;+\infty[$. L'intégrale est donc impropre en $+\infty$. Or, au voisinage de $+\infty$ :
\[ \eu{-t^2} = o_{+\infty} \left(\frac{1}{t^2} \right)\]
En effet,
\[ \frac{\eu{-t^2}}{1/t^2} = t^2\eu{-t^2} \underset{t\rightarrow +\infty}{\longrightarrow} 0 \text{ par croissance comparée} \]
Puisque $t\mapsto \frac{1}{t^2}$ est positive, et que l'intégrale $\ds{\int_1^{+\infty} \frac{1}{t^2}\dd t}$ converge, par comparaison de fonctions positives, on en déduit que l'intégrale $\ds{\int_0^{+\infty} \eu{-t^2}\dd t}$ converge.
}	
\end{prof}

\lignes{8}


	\subsection{Changement de variable}
	
L'intégration par partie et le changement de variable, sous condition de convergence de chacune des intégrales, sont encore valables.

\begin{proposition}
Soit $f$ une fonction continue sur $]a;b[$. Soit $\varphi$ une fonction \textbf{strictement croissante} de classe $\CC^1$ sur $]\alpha;\beta[$, avec \[ \lim_{t\rightarrow \alpha} \varphi(t)=a \qeq \lim_{t\rightarrow \beta} \varphi(t)=b \]
Alors les intégrales $\ds{\int_a^b f(t)\dd t}$ et $\ds{\int_\alpha^\beta f(\varphi(u))\varphi'(u)\dd u}$ sont de même nature. Si elles sont convergentes, on a alors
\[ \int_a^b f(t)\dd t = \int_\alpha^\beta f(\varphi(u))\varphi'(u)du \]	
\end{proposition}


\remarque{Le résultat est valable si $\varphi$ est strictement décroissante, excepté que dans ce cas, les bornes $\alpha$ et $\beta$ sont inversées.}

\exemple{Soit $\ds{I=\int_0^{+\infty} \frac{1}{\sqrt{t}+t\sqrt{t}}\dd t}$. Calculer $I$ en effectuant le changement de variable $t=u^2$.}

\begin{prof}
\solution{Remarquons déjà que $f:t\mapsto \frac{1}{\sqrt{t}+t\sqrt{t}}$ est continue sur $]0;+\infty[$. De plus, 
\[ f(t) \sim_{+\infty} \frac{1}{t^{3/2}} \qeq f(t) \sim_{0} \frac{1}{\sqrt{t}} \]
Les trois fonctions sont positives, la fonction $t\mapsto \frac{1}{t^{3/2}}$ est intégrable sur $[1;+\infty[$ (Intégrale de Riemann, $\frac{3}{2}>1$) et  la fonction $t\mapsto \frac{1}{t^{1/2}}$ est intégrable sur $]0;1]$ (Intégrale de Riemann, $\frac{1}{2}<1$). Ainsi, $I$ est convergente.\\
Le changement de variable $t=u^2$ sur $[0;+\infty[$ est de classe $\CC^1$, strictement croissant sur $[0;+\infty[$. Par changement de variable, l'intégrale étant convergente :
\begin{eqnarray*}
	\int_0^{+\infty} \frac{1}{\sqrt{t}+t\sqrt{t}}\dd t &=& \int_0^{+\infty} \frac{1}{u+u^2\times u} (2udu) \\
	 &=& 2\int_0^{+\infty} \frac{1}{1+u^2}du
\end{eqnarray*}
Or, 
\[ \int_0^a \frac{1}{1+u^2} = [ \arctan(u)]_0^{a} = \arctan(a) \underset{a\rightarrow +\infty}{\longrightarrow} \frac{\pi}{2} \]
\textbf{Bilan} : $I=2\times \frac{\pi}{2}=\pi$.
}	
\end{prof}

\lignes{20}
  
 
\section{Convergence absolue}
    
    \subsection{Définition}
    
 \definition{Soit $f$ une fonction continue sur $[a;b[$. On dit que l'intégrale $\ds{\int_a^{b} f(t)\dd t}$ \textbf{converge absolument} sur $[a;b[$ si $\ds{\int_a^{b} |f(t)|\dd t}$ converge.}
     
 \remarque{Rigoureusement, on dit que $f$ est \textbf{intégrable} sur $[a;b[$ si $\ds{\int_a^{b} |f(t)|\dd t}$ converge.}
     
\begin{theoreme}
Soit $f$ une fonction continue sur $[a;b[$. Si $\ds{\int_a^{b} f(t)\dd t}$ est absolument convergente, alors elle est convergente, et on a
   \[\left|\int_a^{b} f(t)\dd t\right| \leq \int_a^{b} |f(t)|\dd t\]	
\end{theoreme}

    
\preuve{Admis.}

\remarque{\danger~La réciproque n'est pas vraie. Une intégrale peut être convergente, sans être absolument convergente. On dit alors que l'intégrale est \textbf{semi-convergente}. On pourra regarder l'exercice \ref{chap18exosin}.} 

	\subsection{Intégrale convergeant absolument vers $0$}

\begin{proposition}
Soit $f$ une fonction continue sur $]a;b[$. $f$ est nulle sur $]a;b[$ si et seulement si \[\int_a^b |f(t)|\dd t = 0\]	
\end{proposition}


\preuve{Si $f$ est nulle, alors son intégrale existe et est nulle. La réciproque se montre comme la proposition similaire du chapitre \textit{Intégration sur un segment}.}
