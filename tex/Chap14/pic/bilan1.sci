// Auteur : Crouzet
// Date : 10/02/2016
// Résumé : calcul de u_N dans le cas d'une suite u_(n+1)=f(u_n)
//          avec f:x -> x/ln(1+x)

// Rang voulu :
N = 5

// Valeur de u_0 :
U = exp(1)

// Boucle pour calculer u_N
for i=1:N
    U = U / log(1+U) // Rappel : la fonction ln s'écrit log en Scilab
end

// Affichage de la valeur de u_N
disp("U = ", U)

