\objectif{Dans ce chapitre, on étend la notion de produit scalaire vue dans $\R^2$ à $\R^3$ à des espaces plus généraux. On concentrera ensuite notre étude à $\R^n$, et des applications intéressantes : les isométries.}

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
	\item Sur les généralités  :
			\begin{itemize}[label=\textbullet]
				\item \hyperref[objectif-25-1]{connaître la définition d'un produit scalaire}\dotfill $\Box$
				\item \hyperref[objectif-25-2]{connaître norme et distance associées à un produit scalaire}\dotfill $\Box$
				\item \hyperref[objectif-25-3]{connaître l'inégalité de Cauchy-Schwarz}\dotfill $\Box$
			\end{itemize}
	\item Concernant l'orthogonalité :
			\begin{itemize}[label=\textbullet]
				\item \hyperref[objectif-25-4]{connaître la définition d'orthogonalité}\dotfill $\Box$
				\item \hyperref[objectif-25-5]{connaître la définition du groupe orthogonal}\dotfill $\Box$
				\item \hyperref[objectif-25-6]{connaître la définition d'orthogonalité pour une matrice}\dotfill $\Box$
			\end{itemize}
	\item \hyperref[objectif-25-7]{Savoir déterminer la nature d'une isométrie du plan}\dotfill $\Box$
	\item \hyperref[objectif-25-8]{Savoir déterminer la nature d'une isométrie de l'espace}\dotfill $\Box$
	\item \hyperref[objectif-25-9]{Connaître le théorème spectral}\dotfill $\Box$
\end{numerote}

\newpage 

\section{Produit scalaire et espace euclidien}

	\subsection{Produit scalaire}
	
\remarque{Si on s'intéresse au produit scalaire du plan (ou de l'espace), on avait plusieurs propriétés intéressantes :
\begin{itemize}[label=\textbullet]
	\item Pour tous vecteurs $\vv{u}$ et $\vv{v}$, on a $\vv{u}\cdot \vv{v}=\vv{v}\cdot \vv{u}$.
	\item Pour tous vecteurs $\vv{u},\vv{v}$ et $\vv{w}$, et réel $\lambda$, on a 
		\[ (\lambda \vv{u} + \vv{v}) \cdot \vv{w} = \lambda \vv{u}\cdot\vv{w}+\vv{v}\cdot \vv{w} \]
	\item Pour tout vecteur $\vv{u}$, $\vv{u}\cdot \vv{u}=\|\vv{u}^2\|\geq 0$.
	\item Enfin, si $\vv{u}\cdot \vv{u}=0$, alors $\vv{u}=\vv{0}$
\end{itemize} 
On part de ces propriétés pour définir la notion de produit scalaire général.}

\label{objectif-25-1}\definition{Soit $E$ un $\R$-espace vectoriel. On appelle \textbf{produit scalaire} sur $E$ une application \\$\langle \cdot,\cdot\rangle : E^2 \rightarrow \R$ vérifiant les propriétés suivantes :
\begin{itemize}[label=\textbullet]
	\item \textit{Symétrie} : pour tous vecteurs $u$ et $v$ de $E$, $\langle u,v\rangle = \langle v,u \rangle$.
	\item \textit{Bilinéarité} : pour tout réel $\lambda$, et pour tous vecteurs $u,v$ et $w$ :
	\[ \langle \lambda u+v, w\rangle = \lambda \langle u,w\rangle + \langle v,w\rangle \qeq \langle w,\lambda u+v\rangle = \langle\lambda w,  u\rangle + \langle w,v\rangle \]
	\item \textit{Positivité} : pour tout vecteur $u$ de $E$, $\langle u,u\rangle \geq 0$.
	\item \textit{Caractère définie ou séparation} : $\langle u,u\rangle =0$ si et seulement si $u=0$.
\end{itemize}
Le produit scalaire peut être noté $\langle u,v\rangle$, $(u,v)$ ou $u \cdot v$ selon les situations.
}

\remarque{Si on a démontré la symétrie, il n'est pas nécessaire de démontrer la bilinéarité, mais simplement une des deux linéarités.}

\exemple{Le produit scalaire usuel du plan est un produit scalaire sur $\R^2$.}

On peut étendre la définition du produit scalaire du plan directement à $\R^n$.

\exemple{[Produit scalaire canonique sur $\R^n$]
L'application $\langle\cdot,\cdot\rangle:\R^n \times \R^n\rightarrow \R$ définie pour tout vecteur $u=\matrice{u_1\\\vdots\\u_n}$ et $v=\matrice{v_1\\\vdots\\v_n}$ par 
\[ \langle u,v \rangle = \sum_{i=1}^n u_iv_i \]
est un produit scalaire, appelé produit scalaire canonique.
}

\begin{prof}
\preuve{A faire}
\end{prof}
\lignes{5}
\lignes{20}

\exercice{Soit $E$ l'ensemble des fonctions continues sur $\R$ et $2\pi$-périodiques. Montrer que l'application $\varphi:E\times E \rightarrow \R$ définie pour $(f,g)\in E^2$ par
\[ \varphi(f,g) = \frac{1}{\pi} \int_0^{2\pi} f(t)g(t)dt\]
est un produit scalaire.
}

\begin{prof}
\preuve{A faire}
\end{prof}

\lignes{20}
\lignes{10}

	\label{objectif-25-2}\subsection{Espace euclidien et norme}
	
\definition{On appelle \textbf{espace euclidien} un $\R$-espace vectoriel de dimension finie, muni d'un produit scalaire.}

\exemple{$\R^n$, muni du produit scalaire canonique, est un espace euclidien.}

\remarque{S'il n'est pas de dimension finie, on dit que $E$, muni d'un produit scalaire, est un espace \textbf{préhilbertien réel}.}

Tout comme dans le plan, où $\vv{u}\cdot \vv{u}=\|\vv{u}\|^2$, on va introduire la notion générale de norme d'un vecteur dans un espace euclidien.

\definition{Soit $E$ un espace euclidien, dont le produit scalaire est noté $\langle \cdot,\cdot \rangle$. On appelle \textbf{norme} de $u\in E$ associée au produit scalaire le nombre réel \[ \|u\|=\sqrt{\langle u,u\rangle} \]
}

\remarque{Sur $\R^n$ muni du produit scalaire canonique, on a ainsi, pour tout $x=\matrice{x_1\\\vdots\\x_n}\in \R^n$ :
\[ \| x\| = \sqrt{x_1^2+\cdots+x_n^2} \]
}

\exemple{Soit $\R^5$ muni du produit scalaire canonique. Déterminer la norme de $u=\matrice{1\\-1\\2\\-3\\-1}$.}

\begin{prof}
\solution{On a rapidement \[ \|u\|=\sqrt{1+1+4+9+1}=4 \]}
\end{prof}

\lignes{3}

\propriete{Soit $E$ un espace euclidien, dont le produit scalaire est noté $\langle \cdot,\cdot \rangle$, et $\|\cdot\|$ la norme associée. Alors, pour tous vecteurs $u$ et $v$ de $E$, et pour tout réel $\lambda$, on a
\begin{itemize}[label=\textbullet]
	\item $\|u\|\geq 0$.
	\item $\|u\|=0$ si et seulement si $u=0$.
	\item $\| \lambda u \| = |\lambda|\times \|u\|$
\end{itemize}
}

	\subsection{Distance associée}
	
De même que dans le plan, où $\|\vv{AB}\|$ représente la distance entre les points $A$ et $B$, on va introduire la distance entre deux vecteurs.

\definition{Soit $E$ un espace euclidien, dont le produit scalaire est noté $\langle \cdot,\cdot \rangle$. Soient $u$ et $v$ deux vecteurs de $E$. On appelle \textbf{distance} entre $u$ et $v$, et on note $d(u,v)$, le réel
	\[ d(u,v) = \|u-v\|\]
 }

\propriete{Soit $E$ un espace euclidien, dont le produit scalaire est noté $\langle \cdot,\cdot \rangle$, et $d$ la distance associée. Alors, pour tous vecteurs $u,v$ et $w$ de $E$, on a :
\begin{itemize}[label=\textbullet]
	\item $d(u,v)=d(v,u)$
	\item $d(u,v)\geq 0$.
	\item $d(u,v)=0$ si et seulement si $u=v$.
\end{itemize}
}

	\label{objectif-25-3}\subsection{Inégalité de Cauchy-Schwarz}
	
Un théorème assez important permet d'obtenir des inégalités entre produit scalaire et norme.

\begin{theoreme}[Inégalité de Cauchy-Schwarz]
 Soit $E$ un espace euclidien, dont le produit scalaire est noté $\langle \cdot,\cdot \rangle$, et $\|\cdot\|$ la norme associée. Alors, pour tous vecteurs $u$ et $v$ de $E$, on a
\[ \langle u,v\rangle^2 \leq \langle u,u\rangle \langle v,v\rangle \]
soit encore
\[ |\langle u,v\rangle| \leq \|u\|.\|v\| \]	
\end{theoreme}

\begin{prof}
\preuve{Soient $u$ et $v$ deux éléments de $E$ et $\lambda\in \R$. Constatons que 
\begin{align*}
	\norme{\lambda u + b} &= \lambda^2\norme{u}^2+2\lambda \langle u, v\rangle + \norme{v}^2 \geq 0
\end{align*}
Ce trinôme du second degré en $\lambda$ est donc de signe constant sur $\R$. Il n'a donc pas de racines réelles, et son discriminant doit être négatif. Or 
\[ \Delta = (2\langle u,v\rangle)^2-4\norme{u}^2\norme{v}^2 \]
Donc, puisque $\Delta\leq 0$, on en déduit que \[ (2\langle u,v\rangle)^2-4\norme{u}^2\norme{v}^2\leq 0 \]
puis
\[(\langle u,v\rangle)^2\leq \norme{u}^2\norme{v}^2\]
Par application de la fonction racine, croissante sur $\R^+$, on a bien \[ |\langle u,v\rangle | \leq \norme{u}\norme{v} \]
}
\end{prof}
\lignes{14}
\lignes{6}

Une conséquence importante de l'inégalité de Cauchy-Schwarz est l'inégalité triangulaire :

\begin{theoreme}[Inégalité triangulaire] 
Soit $E$ un espace euclidien, dont le produit scalaire est noté $\langle \cdot,\cdot \rangle$,  $\|\cdot\|$ la norme associée et $d$ la distance associée. Alors pour tout vecteurs $u$, $v$ et $w$ de $E$, on a
\[ \| u+v \| \leq \|u\|+\|v\| \qeq d(u,w)\leq d(u,v)+d(v,w) \]	
\end{theoreme}

\begin{prof}
\preuve{A faire}
\end{prof}
\lignes{17}

	\subsection{Identité du parallélogramme}
	
Comme dans le plan, on dispose de l'identité du parallélogramme :

\begin{proposition}
Soit $E$ un espace euclidien, dont le produit scalaire est noté $\langle \cdot,\cdot \rangle$, et $\|\cdot\|$ la norme associée. Pour tous vecteurs $u$ et $v$, on a
\[ \|u+v\|^2 = \|u\|^2+2\langle u,v\rangle + \|v\|^2 \]
En particulier, on a l'identité du parallélogramme :
\[ \|u+v\|^2+\|u-v\|^2 = 2(\|u\|^2+\|v\|^2) \]
et l'identité de polarisation :
\[ \langle u,v \rangle = \frac{\|u+v\|^2-\|u\|^2-\|v\|^2}{2} \]	
\end{proposition}


\section{Orthogonalité}

	\subsection{Définitions}
	
On prend appui sur les notions connues du plan pour introduire différents vocabulaires :	
\label{objectif-25-4}\definition{Soit $E$ un espace euclidien, dont le produit scalaire est noté $\langle \cdot,\cdot \rangle$, et $\|\cdot\|$ la norme associée.
\begin{itemize}[label=\textbullet]
	\item Un vecteur $u$ de $E$ est dit \textbf{unitaire} si $\|u\|=1$.
	\item Deux vecteurs $u$ et $v$ sont dits \textbf{orthogonaux} si et seulement si $\langle u,v\rangle = 0$. On notera en général $u \perp v$.
\end{itemize}
}

\exemple{\label{exempleortho} Soient $u=\matrice{\frac{\sqrt{2}}{2}\\0\\-\frac{\sqrt{2}}{2}}$ et $v = \matrice{1\\1\\1}$ deux éléments de $\R^3$ muni du produit scalaire canonique noté $\cdot$. Montrer que $u$ est unitaire et que $u$ et $v$ sont orthogonaux.}

\begin{prof}
\solution{On a en effet
\[ \|u\|^2 = u\cdot u = \frac{1}{2}+0+\frac{1}{2}=1 \]
et 
\[ u \cdot v = -\frac{\sqrt{2}}{2} + 0 + \frac{\sqrt{2}}{2}= 0 \]
}	
\end{prof}

\lignes{6}

On peut généraliser la notion d'orthogonalité à deux espaces :

\definition{Soient $F$ et $G$ deux sous-espaces vectoriels d'un espace euclidien $E$. On dit que $F$ et $G$ sont \textbf{orthogonaux} si tous les vecteurs de $F$ sont orthogonaux aux vecteurs de $G$ :
\[ \forall~x\in F,\quad \forall y\in G,\quad \langle x,y\rangle = 0\]}

\begin{proposition}[Egalité de Pythagore] 
Soit $E$ un espace euclidien, dont le produit scalaire est noté $\langle \cdot,\cdot \rangle$, et $\|\cdot\|$ la norme associée. Soient $u$ et $v$ deux vecteurs orthogonaux de $E$. Alors
\[ \|u+v\|^2=\|u\|^2+\|v\|^2 \]	
\end{proposition}


\preuve{En effet,
\[ \|u+v\|^2=\|u\|^2+2 \underbrace{\langle u,v \rangle}_{=0} + \|v\|^2 = \|u\|^2+\|v\|^2\]}

	\subsection{Famille orthogonale}
	
\definition{Soit $E$ un espace euclidien, dont le produit scalaire est noté $\langle \cdot,\cdot \rangle$, et $\|\cdot\|$ la norme associée.

Une famille $(u_i)$ de vecteurs non nuls de $E$ est dite \textbf{orthogonale} si et seulement si les vecteurs sont deux à deux orthogonaux : \[ \forall~i\neq j,\quad \langle u_i,u_j \rangle = 0 \]

Elle est dite \textbf{orthonormale} (ou \textbf{orthonormée}) si et seulement si elle est orthogonale et pour tout $i$, $\|u_i\|=1$.
}

\exemple{La famille $(u,v)$ de l'exemple $\ref{exempleortho}$ est une famille orthogonale.}

\exercice{Montrer que la base canonique de $\R^4$ est une famille orthonormale de $\R^4$, muni du produit scalaire canonique.}

\begin{prof}
\solution{Rapidement, si on note $(e_1,e_2,e_3,e_4)$ la base canonique de $\R^4$, on a bien $\|e_1\|=\|e_2\|=\|e_3\|=\|e_1\|=1$ et pour $i\neq j$, $e_i\cdot e_j=0$.}	
\end{prof}

\lignes{5}

\begin{proposition}
Soit $E$ un espace euclidien, et $(u_i)$ une famille orthogonale de $E$. Alors la famille $(u_i)$ est libre.

En particulier, une famille orthogonale de $n$ vecteurs d'un espace de dimension $n$ est une base.	
\end{proposition}




\preuve{En effet, prenons une combinaison linéaire nulle des $(u_i)$ : $\ds{\sum \lambda_i u_i = 0}$. Soit $j$ un indice. On a alors
\begin{eqnarray*}
	0=\langle u_j, \lambda_i u_i \rangle &=& \sum \lambda_i \langle u_j, u_i \rangle \\
	&=& \lambda_j \|u_j\|^2
\end{eqnarray*}
Or $\|u_j\|^2\neq 0$ (car $u_j$ n'est pas nul). Ainsi, $\lambda_j=0$. Ceci étant vrai pour tout indice $j$, on en déduit bien que la famille est libre.}

Ainsi, dans un espace euclidien, on peut être amené à chercher une base composée de vecteurs orthogonaux.

\definition{Soient $E$ un espace euclidien. Une base de $E$ est dite
\begin{itemize}[label=\textbullet]
	\item \textbf{orthogonale} si elle est composée de vecteurs deux à deux orthogonaux;
	\item \textbf{orthonormale} ou orthonormée si elle est composée de vecteurs unitaires et deux à deux orthogonaux. 
\end{itemize}
}

\remarque{Soit $E$ un espace euclidien dont le produit scalaire est noté $\langle \cdot,\cdot \rangle$, et $\mathcal{B}=(e_1,\hdots,e_n)$ une base orthonormée de $E$. Alors, pour tout vecteur $x\in E$, on a $x=x_1e_1+\cdots x_ne_n$ avec $x_i=\langle x, e_i\rangle$.

On obtient ainsi rapidement les coordonnées d'un vecteur dans une base orthonormée en calculant des produits scalaires.}

Dans le cas d'une famille orthogonale, l'égalité de Pythagore se généralise :

\begin{proposition}[Relation de Pythagore] 
Soit $E$ un espace euclidien, dont le produit scalaire est noté $\langle \cdot,\cdot \rangle$, et $\|\cdot\|$ la norme associée. Soit $(u_i)$ une famille orthogonale de $E$. Alors
\[ \left\|\sum u_i\right\|^2 = \sum \|u_i\|^2 \]	
\end{proposition}

\section{Groupe orthogonal}

Dans cette partie, on va s'intéresser aux applications qui conserve le produit scalaire et la norme.

Dans la suite, $(E, \langle \cdot,\cdot \rangle)$ désigne un espace euclidien.

	\label{objectif-25-5}\subsection{Définition}
	
\definition{Un endomorphisme $u$ de $E$ est dit \textbf{orthogonal} s'il conserve le produit scalaire :
\[ \forall~(x,y)\in E^2,\quad \langle u(x), u(y) \rangle = \langle x,y \rangle \]
On dit également que $u$ est une \textbf{isométrie vectorielle}.
}

\exemple{L'identité $\id:E\rightarrow E$ est un endomorphisme orthogonal.}

\notation{On note $\OO(E)$ l'ensemble des endomorphismes orthogonaux de $E$.}

\begin{proposition}
Un endomorphisme orthogonal $u$ est bijectif : il s'agit donc d'un automorphisme.	
\end{proposition}

\preuve{Montrons que $u$ est injectif. Soit $x\in \Ker(u)$. On a donc $u(x)=0$. Mais alors
\[ 0=\langle u(x),u(x)\rangle = \langle x,x\rangle =\| x\|^2 \]
On a donc $x=0$ et $u$ est injectif. Puisqu'on est en dimension finie, d'après le théorème du rang, $u$ est bijectif.
}

\begin{proposition}
Soit $u\in \OO(E)$. Alors $u$ conserve également la norme : 
\[ \forall~x\in E,\quad \|u(x)\|=\|x\| \]	
\end{proposition}


\preuve{En effet, \[ \|u(x)\|^2=\langle u(x),u(x)\rangle = \langle x,x\rangle = \|x\|^2 \]
et donc $\|u(x)\|=\|x\|$.}

	\label{objectif-25-6}\subsection{Matrices et orthogonalité}
	
\definition{Une matrice $A\in \MM_n(\R)$ est dite \textbf{orthogonale} si et seulement si $\,^tAA=I_n$.}

\notation{On note $\OO_n(\R)$ ou $\OO_n$ (voire $O(n)$) l'ensemble des matrices carrées de taille $n$ orthogonales.}

\exemple{Montrer que la matrice $A=\matrice{1&0&0\\0&\frac{1}{2}&-\frac{\sqrt{3}}{2}\\0&\frac{\sqrt{3}}{2}&\frac{1}{2}}$ est orthogonale.}

\begin{prof}
\solution{On a rapidement que $\,^tAA=I_3$.}	
\end{prof}
	
\lignes{4}

\begin{proposition}
Soit $A$ une matrice orthogonale. Alors la famille composée des $n$ colonnes de $A$ (respectivement des $n$ lignes de $A$) est une famille orthonormée de $\R^n$.	
\end{proposition}


\preuve{Admis}

\begin{methode}
Pour démontrer qu'une matrice est orthogonale, on peut soit calculer $\,^tAA$, soit remarquer que les colonnes de $A$ sont unitaires et orthogonales.	
\end{methode}


\exemple{Dans l'exemple précédent, on constate rapidement que la famille $\left ( \matrice{1\\0\\0}, \matrice{0\\\frac{1}{2}\\\frac{\sqrt{3}}{2}}, \matrice{0\\-\frac{\sqrt{3}}{2}\\\frac{1}{2}}\right)$ est bien orthonormée.}

Dans un espace euclidien, les matrices orthogonales apparaissent régulièrement :

\begin{proposition}
Soient $\mathcal{B}=(e_i)$ et $\mathcal{C}=(f_i)$ deux bases orthonormées de $E$. La matrice de passage $P$ de $\mathcal{B}$ à $\mathcal{C}$ est une matrice orthogonale.	
\end{proposition}


Plus important : il y a un lien très étroit entre endomorphisme orthogonal et matrice orthogonale :

\begin{theoreme}
Soient $\mathcal{B}=(e_i)$ une base orthonormée de $E$, et $u$ un endomorphisme de $E$. Alors $u$ est une isométrie vectorielle si et seulement si $\Mat_\mathcal{B}(u)$ est orthogonale.	
\end{theoreme}


\preuve{Admis.}

\exemple{Soit $f$ l'endomorphisme de $\R^2$ défini pour tout $\matrice{x\\y}\in \R^2$ par 
\[ f\left( \matrice{x\\y}\right) = \frac{\sqrt{2}}{2}\matrice{x-y\\x+y} \]
Déterminer la matrice de $f$ dans la base canonique de $\R^2$, et en déduire que $f$ est une isométrie du plan.}

\begin{prof}
\solution{Notons $A$ la matrice de $f$ dans la base canonique qui est orthonormée. Alors
\[ A=\frac{\sqrt{2}}{2}\matrice{1&-1\\1&1} \]
et on a
\[ \,^tAA = \frac{\sqrt{2}}{2}\matrice{1&1\\-1&1}\frac{\sqrt{2}}{2}\matrice{1&-1\\1&1} = I_2 \]
Ainsi, $A$ est orthogonale : $f$ est bien une isométrie du plan.
}	
\end{prof}

\lignes{7}
		
		\subsection{Matrice orthogonale et déterminant}
		
Les matrices orthogonales ont une propriété très importante : elles ont un déterminant valant $1$ ou $-1$.

\begin{proposition}
Soit $A\in \OO_n(\R)$. Alors $\det(A)\in \{ -1;1\}$.	
\end{proposition}


\preuve{Puisque $\det(\,^tA)=\det(A)$, et que $\,^tAA=I_n$, on a 
\[ \det(\,^tAA)=\det(I_n) \Longleftrightarrow \det(A)^2 = 1\]
et donc $\det(A) = \pm 1$.
}

\remarque{Par définition du déterminant d'un endomorphisme, on en déduit donc qu'une isométrie a également un déterminant valant $\pm 1$.}

On peut, tout comme on oriente l'espace, orienter un espace euclidien.

\definition{On appelle \textbf{espace euclidien orienté} la donnée d'un espace euclidien $(E, \langle \cdot,\cdot \rangle)$ et d'une base orthonormée $\mathcal{B_0}$ de $E$.

La base $\mathcal{C}$ est dite \textbf{directe} si $\det(\Mat_{\mathcal{B_0},\mathcal{C}}(\id_E))=1$, \textbf{indirecte} si le déterminant vaut $-1$.
}

\remarque{Rappelons que la matrice de passage $\Mat_{\mathcal{B_0},\mathcal{C}}(\id_E)$ est orthogonale, donc de déterminant $1$ ou $-1$.}

\exemple{Dans $\R^n$, on prendra comme orientation la base canonique.}

\section{Groupe orthogonal de dimension $2$ et $3$}

Dans cette section, on va expliciter toutes les isométries du plan vectoriel et de l'espace vectoriel.

	\subsection{Groupe orthogonal de dimension $2$}
	
On note $(i,j)$ la base canonique de $\R^2$.
	
On commence par identifier deux types de matrices orthogonales dans $\OO_2$ :

\begin{proposition}
Si $a$ et $b$ sont deux réels vérifiant $a^2+b^2=1$, alors les matrices $R=\matrice{a&b\\-b&a}$ et $S=\matrice{a&b\\b&-a}$ sont orthogonales.	
\end{proposition}


\preuve{En effet, on a
\[ \,^tRR = \matrice{a^2+b^2&0\\0&a^2+b^2} = I_2 \qeq \,^tSS=\matrice{a^2+b^2&0\\0&a^2+b^2}=I_2 \] 
}

Les matrices précédentes sont les seules matrices orthogonales de $\OO_2$ :

\begin{theoreme}
Soit $u$ une isométrie de $\R^2$. On note $M$ sa matrice dans la base canonique. Alors $M$ est soit de la forme $\matrice{a&b\\-b&a}$, soit de la forme $\matrice{a&b\\b&-a}$, avec $a^2+b^2=1$.	
\end{theoreme}


\preuve{On note $M=\matrice{a&b\\c&d}$. $\,^tMM=I_2$ nous donne donc
\[ \matrice{a^2+b^2 & ac+bd\\ ac+bd&c^2+d^2} = \matrice{1&0\\0&1}   \]
On en déduit donc que $a^2+b^2=1$, $c^2+d^2=1$ et que $ac+bd=0$, soit $ac=-bd$.
\begin{itemize}[label=\textbullet]
	\item Si $a=0$, $b= 1$ (car $a^2+b^2=1$) et donc $d=0$. On obtient la matrice $\matrice{0&1\\1&0}$ qui est bien orthogonale.
	\item Si $a\neq 0$, on a alors $c=-\frac{bd}{a}$, puis, puisque $c^2+d^2=1$
	\[ \frac{b^2d^2}{a^2}+d^2=1 \Leftrightarrow b^2d^2+a^2d^2=a^2 \]
	soit $d^2(a^2+b^2)=a^2$ et donc $d^2=a^2$ et puisque $a^2+b^2=c^2+d^2$, on a également $b^2=c^2$. Il y a donc $4$ cas possibles.
	\begin{itemize}
		\item Soit $a=d$ et $b=c$, ce qui est impossible, puisque $ac=-bd$.
		\item Soit $a=-d$ et $b=-c$, ce qui est impossible, puisque $ac=-bd$.
		\item Soit $a=d$ et $b=-c$, ce qui donne la matrice $\matrice{a&b\\-b&a}$, qui est bien orthogonale.
		\item Soit $a=-d$ et $b=c$, ce qui donne la materice $\matrice{a&b\\b&-a}$, qui est bien orthogonale.
	\end{itemize} 
\end{itemize}
}

Les deux matrices suivantes peuvent se ré-écrire différemment : en effet, puisque 
$a^2+b^2=1$, $a$ peut s'écrire $\cos(\theta)$ et $b$, $\sin(\theta)$ pour un réel $\theta$.

\begin{proposition}
Soit $r_\theta$ la rotation	 de centre $O$ et d'angle $\theta$. Alors $r_\theta$ est une isométrie vectorielle du plan, de matrice $R_\theta=\matrice{\cos(\theta)&-\sin(\theta)\\\sin(\theta)&\cos(\theta)}$ dans la base canonique.

Son déterminant vaut $1$.	
\end{proposition}


\preuve{Dans la base canonique, par définition de la rotation de centre $O$ et d'angle $\theta$, on a $r_\theta(i)=\cos(\theta)i + \sin(\theta)j$ et $r_\theta(j)=-\sin(\theta)i+\cos(\theta)j$. Ainsi, sa matrice s'écrit
\[ R_\theta = \matrice{\cos(\theta)&-\sin(\theta)\\\sin(\theta)&\cos(\theta)} \]
}	

\begin{proposition}
Soit $s_\theta$ la symétrie d'axe $u$ faisant un angle $\theta$ avec $i$. Alors $s_\theta$ est une isométrie vectorielle du plan, de matrice $S_\theta=\matrice{\cos(2\theta)&\sin(2\theta)\\\sin(2\theta)&-\cos(2\theta)}$ dans la base canonique.	
\end{proposition}


\preuve{Dans la base canonique, par définition de la symétrie, on a $s_\theta(i)=\cos(2\theta)i+\sin(2\theta)j$ et $s_\theta(j)=\sin(2\theta)i-\cos(\theta)j$. Ainsi, sa matrice s'écrit \[S_\theta=\matrice{\cos(2\theta)&\sin(2\theta)\\\sin(2\theta)&-\cos(2\theta)}\]}
	
Ainsi, les seules isométries du plan sont les rotations de centre $O$ et les réflexions d'un axe passant par l'origine.

\remarque{S'il s'agit d'une rotation, le déterminant vaut $1$; s'il s'agit d'une symétrie, le déterminant vaut $-1$. Enfin, on constate que , dans le cas d'une rotation, $\text{Tr} (R_\theta)=2\cos(\theta)$ et c'est donc un moyen rapide de déterminer l'angle de la rotation au signe près.}

\label{objectif-25-7}\begin{methode}
Pour déterminer la nature d'une isométrie du plan, on calcule le déterminant de la matrice associée $A$.
\begin{itemize}[label=\textbullet]
	\item Si celui-ci vaut $1$, il s'agit d'une rotation, et on peut utiliser la trace par exemple pour déterminant l'angle de la rotation au signe près. Pour déterminer le signe, on prend un vecteur $X=\matrice{a\\b}$ quelconque, et on calcule $\det(X,AX)$ pour obtenir le signe du sinus.
	\item Si celui-ci vaut $-1$, il s'agit d'une symétrie, et il faut déterminer l'angle $\theta$ de l'axe. On fera attention au $2\theta$ apparaissant dans la matrice.
\end{itemize} 	
\end{methode}

\exercice{Déterminer la nature des isométries de matrices respectives dans la base canonique $A=\frac{1}{2}\matrice{1&-\sqrt{3}\\\sqrt{3}&1}$ et $B=\frac{1}{2}\matrice{\sqrt{2}&\sqrt{2}\\\sqrt{2}&-\sqrt{2}}$.}

\begin{prof}
\solution{Le déterminant de $A$ vaut $1$. En notant $\theta$ l'angle de la rotation, la trace nous donne $\cos(\theta)=\dfrac{1}{2}$, soit $\theta=\pm \dfrac{\pi}{3}$.\\En prenant $X=\matrice{1\\0}$, on a \[ \det(X, AX)=\determinant{1&\frac{1}{2}\\0&\frac{1}{2}}=\frac{1}{2}>0\]
Ainsi, $A$ est la matrice de la rotation autour de l'origine, d'angle $\dfrac{\pi}{3}$.

De même, le déterminant de $B$ vaut $-1$. Remarquons alors que \[ B=\matrice{\cos\left(\frac{\pi}{4}\right)&\sin\left(\frac{\pi}{4}\right)\\\sin\left(\frac{\pi}{4}\right)&-\cos\left(\frac{\pi}{4}\right)}\]
Ainsi, $B$ est la matrice de la symétrie par rapport à la droite faisant un angle $\dfrac{\frac{\pi}{4}}{2}=\dfrac{\pi}{8}$ avec l'axe $(O;\vv{i})$.

}	
\end{prof}

\lignes{14}

	\subsection{Groupe orthogonal de dimension 3}
	
La description du groupe orthogonal de dimension $3$ est plus difficile, car la base intéressante n'est pas connue.

\begin{theoreme}
Soit $A$ une matrice de $\OO_3$. Il existe une base orthonormée direct $\mathcal{B}=(e_1,e_2,e_3)$ telle que, en notant $P$ la matrice de passage de la base canonique à $\mathcal{B}$, on ait
\[ \,^tPAP = \matrice{\pm 1&0&0\\0&\cos(\theta)&-\sin(\theta)\\0&\sin(\theta)&\cos(\theta)} \text{ avec } \theta \in \R \]
\begin{itemize}[label=\textbullet]
	\item Si le coefficient en haut à gauche vaut $1$, $\det(A)=1$ et il s'agit d'une rotation, dirigé par $e_1$, et d'angle $\theta$.
	\item Si le coefficient en haut à gauche vaut $-1$, $\det(A)=-1$ et il s'agit de la composée d'une symétrie orthogonale par rapport au plan de vecteur normal $e_1$, et de la rotation d'axe dirigé par $e_1$ et d'angle $\theta$.
\end{itemize}	
\end{theoreme}


\preuve{Admis}

\remarque{Soit $\lambda$ une valeur propre de $A$, associée à un vecteur propre $X$. Alors $\| AX \| = \|X\|$ mais également $\|AX\|=\|\lambda X\|=|\lambda|\|X\|$ et donc  $|\lambda|=1$. Mais puisque le polynôme caractéristique de $A$ est de degré $3$, le polynôme admet au moins une racine réelle, à savoir $1$ ou $-1$. C'est un vecteur de l'espace propre associé à cette valeur propre qui donnera $a_1$.}

\remarque{Constatons que $\text{Tr}(\,^tPAP)=\pm 1 + 2\cos \theta$. Il nous manque le signe de $\sin(\theta)$ pour déterminer $\theta$. Pour cela, on prend un vecteur orthogonal à $e_1$, qu'on appelle $e_2$. Par définition, le signe du déterminant $\det(e_1, e_2, Ae_2)$ nous donnera le signe du $\sin(\theta)$ (si c'est une rotation) ou de $-\sin(\theta)$ s'il s'agit une symétrie composée à une rotation.}

\label{objectif-25-8}\begin{methode}
Pour déterminer la nature d'une matrice orthogonale $A$.
\begin{numerote}
	\item On détermine le déterminant pour savoir s'il s'agit d'une rotation ou de la composée d'une rotation et d'une symétrie orthogonale.
\end{numerote}
La suite diffère selon la nature de la matrice.
\begin{itemize}
	\item S'il s'agit d'une rotation :
	\begin{numerote}[resume]
		\item On détermine l'espace propre associé à la valeur propre $1$, et on prend un des vecteurs non nuls de norme $1$ : cela représentera $e_1$.	
		\item On détermine la trace pour obtenir la valeur du $\cos(\theta)$.
		\item On détermine un vecteur orthogonal à $e_1$, qu'on note $e_2$, et on calcule le déterminant de $(e_1, e_2, Ae_2)$.  Son signe donne le signe de $\sin(\theta)$.
		\item On conclut on donnant l'axe de la rotation ($e_1$) et l'angle $\theta$.
	\end{numerote}
	\item S'il s'agit d'une réflexion suivi d'une rotation
	\begin{numerote}[resume]
		\item On détermine l'espace propre associé à la valeur propre  $-1$, et on prend un des vecteurs non nuls de norme $1$ : cela représentera $e_1$.	
		\item On détermine la trace pour obtenir la valeur du $\cos(\theta)$.
		\item On détermine un vecteur orthogonal à $e_1$, qu'on note $e_2$, et on calcule le déterminant de $(e_1, e_2, Ae_2)$. Son signe donne l'opposé du signe de $\sin(\theta)$. 
		\item On conclut on donnant l'axe de la symétrie et de la rotation ($e_1$) et l'angle $\theta$.
	\end{numerote}
\end{itemize}	
\end{methode}


\exercice{Identifier la nature de l'isométrie donnée par la matrice $A=\matrice{0&0&1\\1&0&0\\0&1&0}$ et celle donnée par $B=\frac{1}{3}\matrice{2&1&2\\-2&2&1\\-1&-2&2}$.}

\begin{prof}
\solution{A faire}	
\end{prof}

\lignes{42}
\lignes{20}

\section{Matrices symétriques} 

On termine par quelques résultats sur les matrice symétriques, qui repose sur les espaces euclidiens (mais dont la démonstration est hors programme).

	\subsection{Matrices symétriques et espaces propres}
	
\begin{proposition}
Soit $A$ une matrice réelle symétrique de $\MM_n(\R)$. Alors les espaces propres de $A$ sont deux à deux orthogonaux pour le produit scalaire canonique de $\R^n$.	
\end{proposition}


\exemple{Soit $A=\matrice{1&1\\1&1}$. Déterminer les espaces propres de $A$ et montrer qu'ils sont orthogonaux.}

\begin{prof}
\solution{Le polynôme caractéristique de $A$ est $\chi_A=(1-X)^2-1=X(X-2)$. On a, rapidement :
\[ E_0=\Vect\left(\matrice{1\\-1}\right)\qeq E_2=\Vect\left(\matrice{1\\1}\right) \]
Et on constate que, quel que soit $X_1=\matrice{x\\-x} \in E_0$ et $X_2=\matrice{y\\y} \in E_2$ :
\[ \langle X_1,X_2\rangle = xy+(-x)y=0 \]
Ainsi, les deux espaces propres sont orthogonaux.
}	
\end{prof}

\lignes{12}
\lignes{12}

	\subsection{Théorème spectral}

Le théorème suivant est fondamental.

\label{objectif-25-9}\begin{theoreme}[Théorème spectral] 
Soit $A$ une matrice réelle symétrique. Alors $A$ est diagonalisable.	
\end{theoreme}


\preuve{Admis.}

\remarque{Ainsi, dès lors que la matrice $A$ est symétrique, on est sûr qu'elle est diagonalisable.}