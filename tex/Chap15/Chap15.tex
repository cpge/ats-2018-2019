\objectif{Dans ce chapitre, on revient sur la notion d'intégrale sur un segment. Après un retour sur la définition, on verra différente méthodes de calcul pratiques (intégration par parties, changement de variable). On revient également sur la notion de primitive.}

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
	\item Connaître la définition de l'intégrale :
			\begin{itemize}[label=\textbullet]
				\item dans le cas des fonctions positives\dotfill $\Box$
				\item dans le cas des fonctions en escalier\dotfill $\Box$
			\end{itemize}
	\item Concernant les primitives :
			\begin{itemize}[label=\textbullet]
				\item connaître les primitives usuelles\dotfill $\Box$
				\item savoir déterminer des primitives dans les cas de dérivation calssique\dotfill $\Box$
				\item connaître les opérations sur les primitives\dotfill $\Box$
			\end{itemize}
	\item Connaître les différentes propriétés de l'intégrale :
			\begin{itemize}[label=\textbullet]
				\item linéarité et relation de Chasles\dotfill $\Box$
				\item encadrement et inégalité de la moyenne\dotfill $\Box$
				\item positivité et croissance de l'intégrale\dotfill $\Box$
				\item fonction positive et intégrale nulle\dotfill $\Box$
			\end{itemize}
	\item Concernant les méthodes de calcul d'intégrales :
			\begin{itemize}[label=\textbullet]
				\item l'intégration par partie\dotfill $\Box$
				\item le changement de variable\dotfill $\Box$
				\item les fonctions définies par une intégrale\dotfill $\Box$
			\end{itemize}
\end{numerote}

\newpage 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Intégrale sur un segment}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Cas des fonctions positives}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
\definition{Soit $f$ une fonction continue et positive sur $[a;b]$. On appelle \textbf{intégrale} de $a$ à $b$ de la fonction $f$, et on note $\ds{\int_a^b f(t)\dd t}$, l'aire du domaine délimité par l'axe des abscisses, la courbe de $f$ et les droites d'équations $x=a$ et $x=b$.
\begin{center}
	\input{tex/Chap15/pic/int1}
\end{center}
}

\remarque{Dans l'écriture $\displaystyle{\int_a^b f(t)\dd t}$, $t$ est une variable muette. Ainsi, \[\int_a^b f(t)\dd t= \int_a^b f(u)\dd u = \int_a^b f(x)\dd x\]}

\exemple{Si $f$ est la fonction constante égale à $2$, alors le domaine est un rectangle, et
\[ \int_2^4 f(t)\dd t = 2\times (4-2) = 4 \]}

\exercice{Calculer $\ds{\int_{1/2}^2 f(t)\dd t}$ dans le cas suivant :
\begin{center}
	\input{tex/Chap15/pic/int2}
\end{center}
}

\begin{prof}
\solution{Le domaine délimité par la courbe de $f$, l'axe des abscisses et les droites d'équation $x=1/2$ et $x=2$ est un triangle. Ainsi
\[ \int_{1/2}^2 f(t)\dd t = \frac{1.5 \times 1.5}{2} = 1.125 = \frac{9}{8} \]
}	
\end{prof}

\lignes{5}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Premières propriétés}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
On dispose des résultats suivants :
\begin{proposition}
Soit $f$ une fonction continue et positive sur $[a;b]$. Alors
\begin{itemize}[label=\textbullet]
	\item $\ds{\int_a^a f(t)\dd t = 0}$.
	\item Relation de Chasles : pour tout réel $c\in [a;b]$, on a
		\[ \int_a^c f(t)\dd t + \int_c^b f(t)\dd t = \int_a^b f(t)\dd t \]
\end{itemize}	
\end{proposition}


\preuve{Dans le premier cas, le domaine est vide, donc d'aire nulle. Dans le second cas,  il suffit de voir que le domaine délimité par les droites $x=a$ et $x=b$ peut être coupé en deux domaines : celui délimité par $x=a$ et $x=c$, et celui délimité par $x=c$ et $x=b$.}
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Cas des fonctions en escalier}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
\definition{Une fonction $f:[a;b]\rightarrow \R$ est dites en escalier s'il existe une subdivision $a=a_0<a_1<\cdots<a_{n-1}<a_n=b$ telle que $f$ est constante sur les intervalles $]a_i;a_{i+1}[$.
\input{tex/Chap15/pic/esc1}	

On note $\EE([a;b])$ l'ensemble des fonctions en escaliers sur $[a;b]$, qui est un sous-espace vectoriel de l'espace des fonctions définies sur $[a;b]$.
}

\exemple{Par exemple, la fonction partie entière est en escalier sur $[-1;2]$.}

\definition{Soit $f:[a;b]\rightarrow \R$ une fonction en escalier, et $a=a_0<a_1<\cdots<a_n=b$ une subdivision adaptée. On note $c_i$ la valeur de $f$ sur $]a_i;a_{i+1}[$. On définit l'intégrale de $f$ de $a$ à $b$ le nombre 
\[ \int_a^b f(t)\dd t = \sum_{k=0}^{n-1} c_i (a_{i+1}-a_i) \]
	\input{tex/Chap15/pic/esc2}
}

\exemple{En utilisant cette définition,
\[ \int_{-1}^2 [x]\dd x = -1(0-(-1)) + 0 (1-0) + 1 (2-1) = 0 \]
}


\remarque{Dans le cas des fonctions en escalier, l'intégrale de $f$ correspond donc à l'aire algébrique du domaine compris entre la courbe de $f$, l'axe des abscisses et les droites d'équation $x=a$ et $x=b$. L'aire est ainsi comptée positivement si $f$ est positive, négativement sinon.}

	%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Cas général}
    %%%%%%%%%%%%%%%%%%%%%%%%
    
On admettra que l'on peut généraliser la définition précédente à toutes les fonctions continue.
    
\definition{Soit $f:[a;b]\rightarrow \R$ une fonction continue. On appelle intégrale de $f$ de $a$ à $b$, et on note  $\displaystyle{\int_a^b f(t)\dd t}$, l'aire \textbf{algébrique} (ou aire \textbf{signée}) en unité d'aire du domaine délimité par $\mathcal{C}_f$, courbe représentative de $f$, l'axe des abscisses, et les droites d'équations $x=a$ et $x=b$.}

\begin{center}
\includegraphics[width=7cm]{tex/Chap15/mp/cas_fonction_continue.mps}
\end{center}
 

\remarque{Par convention, si $a>b$, on notera
\[ \int_a^b f(t)\dd t = -\int_b^a f(t)\dd t \]}
 
On conserve la propriété suivante : 

\propriete{Soit $f$ une fonction continue sur un intervalle $[a;b]$, et $c\in [a;b]$. Alors  $\displaystyle{\int_c^cf(t)\dd t=0}$
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Propriétés générales}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Relation de Chasles}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
\begin{theoreme}
Soit $f$ une fonction continue sur un intervalle $I$. Soient $a,b,c$ trois réels quelconques de $I$. Alors 
\[\int_a^b f(t)\dd t+\int_b^c f(t)\dd t=\int_a^c f(t)\dd t\]	
\end{theoreme}


\exemple{[Calcul de l'intégrale d'une fonction affine par morceaux] Soit $f$ la fonction donnée ci-dessous. Déterminer $\ds{I(f)=\int_{-1}^5 f(t)\dd t}$.

\begin{center}\includegraphics[width=9cm]{tex/Chap15/mp/exercice_chasles}\end{center}}

\begin{prof}
\solution{
D'après la relation de Chasles :
\[\int_{-1}^5f(t)\dd t=\int_{-1}^0f(t)\dd t+\int_0^2 f(t)\dd t+\int_2^3 f(t)\dd t+\int_3^5f(t)\dd t=2+3-1-2=2\]
}
\end{prof}

\lignes{5}    

\consequence{~
\begin{itemize}
	\item[$\bullet$] Si $f$ est paire sur $[-a;a]$, alors \[\int_{-a}^a f(t)\dd t=2\int_0^a f(t)\dd t\]
	\item[$\bullet$] Si $f$ est impaire sur $[-a;a]$, alors \[\int_{-a}^a f(t)\dd t=0\]
\end{itemize}}

	%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Linéarité}
	%%%%%%%%%%%%%%%%%%%%%%
	
\begin{theoreme}
Soient $f$ et $g$ deux fonctions continues sur un intervalle $I$, soit $\lambda$ un réel quelconque, et soient $a,b$ deux réels quelconques de l'intervalle $I$. Alors 
\[\int_a^b \lambda f(t)\dd t=\lambda \int_a^b f(t)\dd t\]
\[\int_a^b (f+g)(t)\dd t=\int_a^b f(t)\dd t+\int_a^b g(t)\dd t\]	
\end{theoreme}

%%%%%%%%%%%%%%%%%%%%
\section{Primitives}
%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Définition}
	%%%%%%%%%%%%%%%%%%%%%%%
	
\definition{Soient $f$ et $F$ deux fonctions définies sur un intervalle $I$, avec $F$ dérivable. Si $F'=f$, on dit que $F$ est une \textbf{primitive} de $f$ sur l'intervalle $I$.}

\exemple{~\begin{itemize}
	\item[$\bullet$] Soit $f$ la fonction définie sur $\R$ par $f(x)=2x$. Alors $f$ admet comme primitive sur $\R$ la fonction $F:x\mapsto x^2$, mais également les fonctions $G:x\mapsto x^2+1$, $H:x\mapsto x^2+l,~~~l\in \R$. 
	\item[$\bullet$] La fonction $x\mapsto \E^x$ admet comme primitive sur $\R$ la fonction $x\mapsto \E^x$.
\end{itemize}}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Différentes primitives d'une fonction}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
\begin{theoreme}
Les seules primitives de la fonction nulle sur un intervalle $I$ sont les fonctions constantes.	
\end{theoreme}


\preuve{En effet, si $F'=0$ sur l'intervalle $I$, nous avons vu dans le chapitre sur la dérivation, que cela implique que $F$ est constante. Réciproquement, les fonctions constantes ont une dérivée nulle.}

\consequence{Soit $F$ une primitive d'une fonction $f$ sur l'intervalle $I$. Alors toutes les primitives de $f$ sur $I$ s'écrivent $F+\lambda$, avec $\lambda$ constante réelle.}

\preuve{En effet, en notant $F$ et $G$ deux primitives de $f$ sur $I$, on a $(F-G)'=F'-G'=f-f=0$. D'après le résultat précédent, $F-G$ est une fonction constante sur l'intervalle $I$, c'est à dire $F=G+\lambda$ avec $\lambda$ constante réelle.}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Fonction continue et primitive}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
\begin{theoreme}[Théorème fondamental de l'analyse] 
Une fonction continue sur un intervalle $I$ admet des primitives sur $I$ : la fonction $\ds{\mathcal{A}:x\mapsto \int_{x_0}^x f(t)\dd t}$ est la primitive de $f$ nulle en $x_0$.	
\end{theoreme}


\begin{prof}
\preuve{Démontrons, pour simplifier, le théorème dans le cas où $f$ est une fonction croissante et positive. Soit $f$ une fonction continue croissante sur $[a;b]$, et $\mathcal{C}_f$ sa courbe représentative dans un repère orthonormé.
Pour tout $x\in [a;b]$, on note $\mathcal{A}(x)$ l'aire de la surface délimitée par l'axe des abscisses, la courbe $\mathcal{C}_f$, entre les abscisses $a$ et $x$. 
\begin{center}
\includegraphics{tex/Chap15/mp/dem_theoreme_fondamental_1.mps}
\end{center}

Cette fonction est bien définie sur $[a;b]$. On va montrer que $\mathcal{A}$ est dérivable sur $[a;b]$, et que $\mathcal{A}'=f$.\\
Soit $h>0$ et $x\in[a;b]$. Le nombre $\mathcal{A}(x+h)-\mathcal{A}(x)$ représente l'aire de la surface délimitée par l'axe des abscisses, la courbe de $f$, et les abscisses $x$ et $x+h$. 
\begin{center}
\includegraphics{tex/Chap15/mp/dem_theoreme_fondamental_2.mps}
\end{center}
Puisque $f$ est croissante, on peut encadrer cette aire par deux rectangles :
\[(x+h-x)f(x)\leq \mathcal{A}(x+h)-\mathcal{A}(x) \leq (x+h-x)f(x+h)\]
donc
\[f(x) \leq \frac{\mathcal{A}(x+h)-\mathcal{A}(x)}{h} \leq f(x+h)\]
Par continuité de $f$, $\displaystyle{\lim_{h\rightarrow 0} f(x+h)=f(x)}$. Par encadrement, on a donc
\[\lim_{h\rightarrow 0^+} \frac{\mathcal{A}(x+h)-\mathcal{A}(x)}{h} = f(x)\]
Donc $\mathcal{A}$ est bien dérivable à droite en $x$, et $\mathcal{A}'_d(x)=f(x)$.
\\On montre de même que $\mathcal{A}$ est dérivable à gauche en $x$, et que $\mathcal{A}'_g(x)=f(x)$. Les dérivées à droite et à gauche de $\mathcal{A}$ étant égales, on en déduit que $\mathcal{A}$ est dérivable et que sa dérivée est $f$.
}
\end{prof}

\lignes{40}

On retiendra le résultat sous la forme suivante :

\begin{theoreme}
Soit $f$ une fonction continue sur un intervalle $I$, $x_0\in I$. La fonction \[F:x\mapsto \int_{x_0}^x f(t)\dd t\] est l'unique primitive de $f$ sur $I$ qui s'annule en $x_0$. Ainsi, $F$ est de classe $\mathcal{C}^1$ sur $I$, et $F'=f$.	
\end{theoreme}


\begin{prof}
\preuve{Soit $G$ une primitive de $f$ sur $I$ (qui existe parce qu'elle est continue). Alors, pour tout $x$ de $I$, \[F(x)=\int_{x_0}^x f(t)\dd t=G(x)-G(x_0)\]
Donc $F(x_0)=0$, $F$ est dérivable sur $I$ et on a, pour tout $x$ de $I$, $F'(x)=G'(x)=f(x)$. Donc $F$ est une primitive de $f$, et puisque $F(x_0)=0$, c'est la primitive de $f$ nulle en $0$.}
\end{prof}

\lignes{8}
    
\exemple{Soit $f$ la fonction définie sur $\R$ par \[f(x)=\int_0^x \eu{-t^2}\dd t\] Déterminer $f'$.}

\begin{prof}
\solution{$f$ est la primitive de la fonction $x\mapsto \eu{-x^2}$ (fonction continue sur $\R$) nulle en $0$. Ainsi, $f$ est dérivable sur $\R$ et 
\[\forall~x,~f'(x)=\eu{-x^2}\]}
\end{prof}

\lignes{9}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Fonction primitive et condition initiale}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
\begin{theoreme}
Soit $f$ une fonction admettant des primitives sur un intervalle $I$. Soit $x_0$ un réel de l'intervalle $I$, et $y_0$ un réel donné. Alors il existe une, et une seule, primitive $F$ de $f$ sur $I$ telle que $F(x_0)=y_0$.\\
En particulier, $f$ admet une unique primitive s'annulant en un $x_0$ donné.	
\end{theoreme}


\begin{prof}
\preuve{Soit $F$ une primitive de $f$. Alors, la fonction $G$ définie par $G=F-F(x_0)+y_0$ est également une primitive de $f$, et $G(x_0)=y_0$. \\
Si $G$ et $H$ sont deux primitives de $f$ telles que $G(x_0)=H(x_0)=y_0$, alors, puisqu'on peut écrire $G=H+l$, on a $G(x_0)=H(x_0)+l = G(x_0)+l$, donc $l=0$ et $G=H$.}
\end{prof}

\lignes{8}    
    
\exemple{La fonction $F$ définie sur $\R$ par $F(x)=x^2-1$ est une primitive de $f:x\mapsto  2x$ vérifiant $F\left(1\right)=0$.}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Calcul intégral et primitive}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Le résultat précédent permet de calculer une intégrale à l'aide des primitives :

\begin{proposition}
Soit $f$ une fonction continue sur $[a;b]$. L'intégrale de $a$ à $b$ de la fonction $f$ est égal au nombre réel $F(b)-F(a)$, où $F$ est une primitive quelconque de $f$ sur $[a;b]$. On note $\displaystyle{\int_a^b f(t)\dd t=F(b)-F(a)}$.	
\end{proposition}


\remarque{Par convention de notation, on note $[F(t)]_a^b = F(b)-F(a)$, de sorte que \[\int_a^b f(t)\dd t=[F(t)]_a^b = F(b)-F(a)\]}

\exemple{$\displaystyle{\int_1^2 x\dd x = \left[ \frac{x^2}{2} \right]_1^2 = 2-\frac{1}{2}=\frac{3}{2}}$. On peut également prendre une autre primitive : 
 \[\int_1^2 x\dd x = \left[ \frac{x^2}{2}+1 \right]_1^2 = 3-\frac{3}{2}=\frac{3}{2}\]}
 
\remarque{La définition de l'intégrale ne dépend pas de la primitive choisie. En effet, soient $F$ et $G$ deux primitives de $f$. D'après un résultat précédent, il existe un réel $\lambda$ tel que $G=F+\lambda$. Mais alors 
\[G(b)-G(a)=(F(b)+\lambda) - (F(a)+\lambda)= F(b)-F(a)\]}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Recherche de primitives}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Pour rechercher des primitives, on utilise les formules connues pour la dérivation, et les dérivées connues.

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Fonctions usuelles}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
\begin{center}\begin{tabular}{|c|c|c|}\hline
\textbf{Fonction $f$} & \textbf{Primitive $F$} &  \textbf{Intervalle $I$} \\ \hline
 $x\mapsto a$ (constante non nulle) & $x \mapsto ax$ & $\R$ \\ \hline
 &&\\ $x\mapsto x^n (n\geq 1)$ & $\displaystyle{x\mapsto \frac{x^{n+1}}{n+1}}$ & $\R$ \\ &&\\ \hline
 &&\\
 $\displaystyle{x\mapsto \frac{1}{x^n}}$ ($n>1$) & $ x\mapsto -\frac{1}{(n-1)x^{n-1}}$ & 
$]-\infty;0[ \textrm{ ou} ]0;+\infty[ $\\  &&\\\hline  
&& \\$x\mapsto x^\alpha ~~(\alpha \neq -1)$ & $x\mapsto \frac{x^{\alpha+1}}{\alpha +1}$ & $\R^*_+$ \\ && \\\hline 
  &&\\ $\displaystyle{x\mapsto \frac{1}{\sqrt{x}}}$ & $x\mapsto 2\sqrt{x}$ & $\R^*_+$\\  &&\\ \hline
  &&\\$\displaystyle{x\mapsto \frac{1}{x}}$ & $\ln x$ & $\R^*_+$ \\ &&\\ \hline
 $x\mapsto \E^x$ & $x \mapsto \E^x$ & $\R$ \\ \hline
\end{tabular}\end{center}
\begin{center}\begin{tabular}{|c|c|c|}\hline
\textbf{Fonction $f$} & \textbf{Primitive $F$} &  \textbf{Intervalle $I$} \\ \hline
 $x\mapsto \cos(x)$ & $x\mapsto \sin(x)$ & $\R$ \\\hline
 $x\mapsto \sin(x)$ & $x\mapsto -\cos(x)$ & $\R$ \\\hline
 $x\mapsto \cosh(x)$ & $x\mapsto \sinh(x)$ & $\R$ \\\hline
 $x\mapsto \sinh(x)$ & $x\mapsto \cosh(x)$ & $\R$ \\\hline
&&\\  $\ds{x\mapsto \frac{1}{1+x^2}}$ & $x\mapsto \arctan(x)$ & $\R$\\ &&\\ \hline
&&\\  $\ds{x\mapsto \frac{1}{\sqrt{1-x^2}}}$ & $x\mapsto \arcsin(x)$ & $]-1;1[$\\ &&\\ \hline
&&\\  $\ds{x\mapsto -\frac{1}{\sqrt{1-x^2}}}$ & $x\mapsto \arccos(x)$ & $]-1;1[$\\ &&\\ \hline
\end{tabular}\end{center}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Utilisation des formules de dérivation}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
On peut utiliser les formules suivantes :

\remarque{\[\frac{u'}{\sqrt{u}}=(2\sqrt{u})'~~~~~~~~\frac{u'}{u^2}=\left(-\frac{1}{u}\right)'\]
\[2u'u=(u^2)'~~~~~~~~u'u^n=\left(\frac{1}{n+1}u^{n+1}\right)'~~~~~~~~u'u^\alpha = \left( \frac{u^{\alpha+1}}{\alpha+1}\right)' (\alpha \neq -1)\]
\[\frac{u'}{u}=(\ln |u|)'~~~~~~~~u'\E^u=(\E^u)'\]}

\exemple{Soit $f$ la fonction définie sur $\R^*_+$ par $f(x)=\frac{\ln(x)}{x}$. Déterminer une primitive de $f$.}

\begin{prof}
\solution{On pose $u(x)=\ln x$. Alors $f(x)=u'(x)u(x)$ et admet donc comme primitive $F:x \mapsto \frac{1}{2}u^2(x)=\frac{1}{2}\ln(x)^2$.}
\end{prof}

\lignes{5}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Opération sur les primitives}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{theoreme}
Soient $F$ et $G$ deux primitives respectives des fonctions $f$ et $g$ sur un meme intervalle $I$, et $\lambda \in \R$.
\begin{itemize}
    \item[$\bullet$] $\lambda F$ est une primitive de $\lambda f$ sur $I$.
    \item[$\bullet$] $F+G$ est une primitive de $f+g$ sur $I$.
\end{itemize}	
\end{theoreme}


\preuve{Assez immédiate : $(\lambda F)'=\lambda F'=\lambda f$ et $(F+G)'=F'+G'=f+g$ en exploitant la linéarité de la dérivation.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Propriétés d'encadrement et valeur moyenne}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Encadrement}
	%%%%%%%%%%%%%%%%%%%%%%%%
	
\begin{theoreme}
Soient $f$ et $g$ deux fonctions continues sur un intervalle $I$, et soient $a,b$ deux réels quelconques de $I$.
\begin{itemize}
	\item[$\bullet$](\textbf{Positivité} de l'intégrale) Si $a\leq b$ et si, pour tout réel $t$ de $[a;b]$, $f(t)\geq 0$, alors \[\int_a^b f(t)\dd t\geq 0\]
	\item[$\bullet$](\textbf{Croissance} de l'intégrale) Si $a\leq b$ et si, pout tout réel $t$ de $[a;b]$ $f(t)\leq g(t)$ alors \[\int_a^b f(t)\dd t\leq \int_a^b g(t)\dd t\]
\end{itemize}	
\end{theoreme}

\remarque{\danger{~Il faut absolument que $a\leq b$ !}}

\begin{prof}
\preuve{Soit $F$ une primitive de $f$ sur $I$, et $G$ une primitive de $g$ sur $I$.
\begin{itemize}
	\item[$\bullet$] Si $f$ est positive sur $I$, alors $F$ est croissante sur $I$ (puisque $F'=f$). Mais alors, si $a\leq b$, \[\int_a^b f(t)\dd t=F(b)-F(a)\geq 0 \textrm{ par croissance de } F\]
	\item[$\bullet$] Si $f\leq g$ sur $I$, alors $g-f\geq 0$ sur $I$. D'après le résultat précédent, $\displaystyle{\int_a^b (g(t)-f(t))\dd t \geq 0}$. Par linéarité, on obtient bien $\displaystyle{\int_a^b f(t)\dd t \leq \int_a^b g(t)\dd t}$.
	
\end{itemize}}
\end{prof}

\lignes{10}
    
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Inégalité de la moyenne}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{theoreme}[Inégalité de la moyenne] 
Soit $f$ une fonction continue sur un intervalle $I$. Soient $m$ et $M$ deux réels, et $a,b$ deux réels de l'intervalle $I$ tels que $a\leq b$.\\
Si $m\leq f\leq M$ sur $[a;b]$ alors 
\[m(b-a)\leq \int_a^b f(t)\dd t\leq M(b-a)\]
Et si $a\neq b$ :
\[m \leq \frac{1}{b-a}\int_a^b f(t)\dd t\leq M\]	
\end{theoreme}

\begin{prof}
\preuve{Pour tout $t$ dans $[a;b]$, on a $m\leq f(t)\leq M$. D'après le théorème précédent, puisque $a\leq b$, on a alors 
\[\int_a^b m\dd t\leq \int_a^b f(t)\dd t \leq \int_a^b M\dd t\]
Or $\int_a^b m\dd t=m(b-a)$ et $\int_a^b M\dd t=M(b-a)$ (car les fonctions $t\mapsto M$ et $t\mapsto m$ sont constantes sur $[a;b]$), ce qui donne le résultat.

On peut également le prouver à l'aide de l'inégalité des accroissements finis appliquée à $F$, une primitive de $f$.}
\end{prof}
\lignes{8}
    
    
\exercice{Montrer que la fonction $\displaystyle{f:x\mapsto \frac{1}{\E^x+\eu{-x}}}$ est décroissante sur $[0;+\infty[$. Pour tout entier naturel $n$, on pose \[I_n=\int_n^{n+1}f(x)\dd x\]
Prouver que pour tout entier naturel $n$, $f(n+1)\leq I_n \leq f(n)$, puis en déduire que la suite $(I_n)$ est convergente.}


\begin{prof}
\solution{$f$ est dérivable sur $\R^+$ comme quotient de fonctions dérivables dont le dénominateur ne s'annule pas. Par dérivation :
\[ \forall~x\geq 0,\quad f'(x)=-\frac{\E^x-\eu{-x}}{(\E^x+\eu{-x})^2}=-2\frac{\sinh(x)}{(\E^x+\eu{-x})^2} \]
Puisque $\sinh$ est positive sur $\R^+$, on en déduit que $f'$ est négative sur $\R^+$, et donc que $f$ est décroissante sur $[0;+\infty[$.\\ 
Soit $n$ un entier. Puisque $f$ est décroissante sur $\R^+$, elle l'est sur $[n;n+1]$. Ainsi, pour tout réel $t\in [n;n+1]$, on a
	\[f(n+1)\leq f(t) \leq f(n)\]
	D'après l'inégalité de la moyenne, on a alors
	\[f(n+1)(n+1-n) \leq \int_n^{n+1} f(t)\dd t \leq f(n)(n+1-n)\]
	soit
	\[f(n+1) \leq I_n \leq f(n)\]
	Constatons enfin que \[\lim_{n\rightarrow +\infty} f(n)=\lim_{n\rightarrow +\infty} f(n+1)=\lim_{x\rightarrow +\infty} f(x)=0 \textrm{  par quotient.}\]
	D'après le théorème d'encadrement, on en déduit que la suite $(I_n)$ converge, et que sa limite vaut $0$.
}	
\end{prof}

\lignes{20}


\begin{theoreme}[Inégalité triangulaire] 
Soient $f$ une fonction continue sur $[a;b]$. Alors
\[\left| \int_a^b f(t)\dd t \right| \leq \int_a^b |f(t)|\dd t\]	
\end{theoreme}

\preuve{Théorème admis.}
  
 	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Valeur moyenne d'une fonction}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
\definition{Soit $f$ une fonction continue sur un intervalle $I$, et soient $a,b$ deux réels distincts de $I$. Le nombre réel \[\frac{1}{b-a}\int_a^b f(t)\dd t\] est appelé \textbf{valeur moyenne} de $f$ entre $a$ et $b$.}

\begin{theoreme}
Dans les conditions précédentes, il existe un réel $c$ situé entre $a$ et $b$, tel que 
\[\frac{1}{b-a}\int_a^b f(t)\dd t=f(c)\]	
\end{theoreme}


\begin{prof}
\preuve{Soit $f$ une fonction continue sur le segment $[a;b]$. Puisqu'elle est continue, l'image du segment $[a;b]$ est un segment $[m;M]$. Mais alors, pour tout $x$ de $[a;b]$ :
\[m\leq f(x)\leq M \Leftrightarrow \int_a^b m\dd t\leq \int_a^b f(t)\dd t \leq \int_a^b M\dd t\]
Soit
\[m\leq \frac{1}{b-a}\int_a^b f(t)\dd t \leq M\]
Le réel $\frac{1}{b-a}\int_a^b f(t)\dd t$ est donc compris entre $m$ et $M$. D'après le théorème des valeurs intermédiaires, cette valeur est atteinte en un réel $c\in[a;b]$.}
\end{prof}

\lignes{15}
  
  	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Fonctions positive et intégrale nulle}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    

\begin{proposition}
Soit $f$ une fonction \underline{continue} sur $[a;b]$. Si
\[\forall~t \in [a;b], f(t)\geq 0 \textrm{ et } \int_a^b f(t)\dd t=0\]
alors 
$f(t)=0$ pour tout $t\in [a;b]$.	
\end{proposition}


\begin{prof}
\preuve{Supposons par l'absurde qu'il existe $\alpha \in ]a;b[$ tel que $f(\alpha)>0$. Par continuité de $f$, il existe un intervalle $J=]\alpha-\eps; \alpha+\eps[$ tel que, pour tout $x\in J$, $f(x)\geq \frac{f(\alpha)}{2}$.
Mais alors,
\[\int_a^b f(t)\dd t \geq \int_J f(t)\dd t \geq \frac{f(\alpha)}{2}\times 2\eps > 0\]
ce qui est absurde.}
\end{prof}

\lignes{10}
       
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Intégration d'une fonction continue par morceaux}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
\remarque{Rappel : une fonction $f$ est dite continue par morceaux sur le segment $[a,b]$ s’il existe une subdivision $a_0=a<a_1 <\cdots<a_n=b$ telle que les restrictions de $f$ à chaque intervalle ouvert $]a_i , a_{i+1} [$ admettent un prolongement continu à l’intervalle fermé $[a_i, a_{i+1}]$.    }

\definition{Pour tout $i\in\llbracket 0;n-1\rrbracket$, notons $\widehat{f_i}$ le prolongement par continuité de $f_i$ sur l'intervalle $[a_i;a_{i+1}]$. On appelle alors intégrale de $f$ sur $[a;b]$ le nombre
\[\int_a^b f(x)\dd x= \sum_{i=0}^{n-1} \int_{a_i}^{a_{i+1}} \widehat{f_i}(x)\dd x\]
}

\begin{center}\includegraphics[width=8cm]{tex/Chap15/mp/integration_continue_morceau.mps}\end{center}



\remarque{Ainsi, pour calculer l'intégrale d'une fonction continue par morceaux, on calcule l'intégrale sur chacun des intervalles $[a_i;a_{î+1}]$ de la subdivision, puis on additionne les différentes valeurs.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
\section{Méthode de calcul d'intégrales}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Intégration par partie}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{theoreme}
Soient $u$ et $v$ deux fonctions de classe $\mathcal{C}^1$ sur $I$. Alors, pour tous réels $a$ et $b$ de $I$ :
\[\int_a^bu(t)v'(t)\dd t=[u(t)v(t)]_a^b-\int_a^b u'(t)v(t)\dd t\]	
\end{theoreme}

\begin{prof}
\preuve{La fonction $uv$ est dérivable sur $I$ et on a $(uv)'=u'v+uv'$. Donc $uv'=uv-u'v$, et toutes ces fonctions sont continues sur $I$. On en déduit donc :
\[\int_a^b (uv')(t)\dd t=\int_a^b [(uv)'(t)- (u'v)(t)]\dd t\]
Par linéarité de l'intégrale, et puisque $uv$ est une primitive de $(uv)'$, on a
\[\int_a^b u(t)v'(t)\dd t=[u(t)v(t)]_a^b-\int_a^b u'(t)v(t)\dd t\]}
\end{prof}

\lignes{13}

\begin{methode}Pour calculer une intégrale par intégration par partie, on détermine les fonctions $u$ et $v$ qui interviennent et on vérifie qu'elles sont de classe $\mathcal{C}^1$ sur l'intervalle considéré.	
\end{methode}


\exemple{Calculer $\displaystyle{\int_0^1 t\E^t\dd t}$.}

\begin{prof}
\solution{Pour tout $t \in [0;1]$, posons $u(t)=t$ et $v'(t)=\E^t$. $u$ et $v$ sont de classe $\CC^1$ sur $[0;1]$. Alors, $u'(t)=1$ et $v(t)=\E^t$. On a donc
\[\int_0^1 t\E^t\dd t = [t\E^t]_0^1 - \int_0^1 1\E^t\dd t = e - [\E^t]_0^1=1\]}
\end{prof}

\lignes{8}

\exemple{Calculer $\displaystyle{\int_1^e \ln(x)\dd x}$.}

\begin{prof}
\solution{ Puisque $\ln(t)=1\times \ln(t)$, pour tout $t \in [1;e]$, on pose $u(t)=\ln(t)$ et $v'(t)=1$. Les fonctions $u$ et $v$ sont bien de classe $\mathcal{C}^1$ sur $[1;e]$. On a donc $u'(t)=\frac{1}{t}$ et $v(t)=t$. Alors
\[\int_1^e \ln(t)\dd t= [t\ln(t)]_1^e - \int_1^e t\frac{1}{t}\dd t\]
et donc
\[\int_1^e \ln(t)\dd t = e - [t]_1^e = 1\]
  }
\end{prof}

\lignes{9}

\remarque{Ainsi, une primitive de la fonction $\ln$ sur $]0;+\infty[$ est $x\mapsto x\ln(x)-x$.}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Changement de variable}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

L'idée du changement de variable est de se ramener à une intégrale que l'on sait calculer.

\begin{theoreme}
Soit $f$ une fonction continue sur $[a;b]$, et $u$ une fonction $\mathcal{C}^1$ sur $[\alpha; \beta]$, telle que $u([\alpha; \beta])\subset [a;b]$. Alors
\[\int_\alpha^\beta f(u(t))u'(t)\dd t = \int_{u(\alpha)}^{u(\beta)} f(x)\dd x\]
\end{theoreme}

\remarque{Il faut donc reconnaitre une forme $f(u)u'$ pour pouvoir effectuer un changement de variable. On n'oubliera pas de remplacer également les bornes d'intégration.}

\begin{prof}
\preuve{Soit $F$ une primitive de $f$, et  $g=F\circ u$. $g$ est $C^1$ sur $[\alpha; \beta]$ (puisque $F$ et $u$ sont $C_1$) et on a $g'= F'(u) \times u'=f(u)u'$. Donc $g$ est une primitive de $f(u)u'$. Donc
\[\int_\alpha^\beta f(u(t))u'(t)\dd t=[g(t)]_\alpha^\beta = F(u(\beta))-F(u(\alpha))=[F(t)]_{u(\alpha)}^{u(\beta)} =\int_{u(\alpha)}^{u(\beta)} f(x)\dd x\]
}
\end{prof}

\lignes{8}

\exemple{Calculer $\displaystyle{I=\int_0^1 t\sqrt{1-t^2}\dd t}$ en posant $u=1-t^2$.}

\begin{prof}
\solution{Pour tout $t\in [0;1]$, $u(t)=1-t^2$. $u$ est de classe $\CC^1$ sur $[0;1]$. Alors
\[I=\int_0^1 -\frac{1}{2}u'(t)\sqrt{u(t)}\dd t = -\frac{1}{2} \int_{u(0)}^{u(1)} \sqrt{x}\dd x = \frac{1}{2}\int_0^1 \sqrt{t}\dd t = \frac{1}{2}\left[\frac{2x\sqrt{x}}{3}\right]_0^1=\frac{1}{3}\]}
\end{prof}

\lignes{8}

\begin{methode}
Soit $\ds{I=\int_a^b f(t)\dd t}$. Pour effectuer un changement de variable :
\begin{itemize}
    \item[$\bullet$] On pose $x=u(t)$ où $u$ est une fonction de classe $\mathcal{C}^1$ sur $[a;b]$. On écrit alors $t=u^{-1}(x)$ puis $\dd t=(u^{-1})'(x)\dd x$.
    \item[$\bullet$] On s'occupe des bornes : lorsque $t=a$, $x=u(a)$ et lorsque $t=b$, $x=u(b)$.
    \item[$\bullet$] Enfin, on exprime $f(t)$ et $\dd t$ uniquement avec $x$ et $\dd x$.
\end{itemize}
On a alors $\ds{\int_a^b f(t)\dd t = \int_{u(a)}^{u(b)} g(x)\dd x}$, cette intégrale étant a priori plus simple à calculer.
\end{methode}

\exemple{Calculer $\displaystyle{I=\int_0^1 \frac{\ln(1+\E^t)}{1+\eu{-t}} \dd t}$ en effectuant le changement de variable $x=1+\E^t$.}

\begin{prof}
\solution{~
\begin{itemize}
    \item[$\bullet$] La fonction $u:t\mapsto 1+\E^t$ est de classe $\mathcal{C}^1$ sur $[0;1]$.
    \item[$\bullet$] $u(0)=2$ et $u(1)=1+e$.
    \item[$\bullet$] Puisque $x=1+\E^t$, alors $t=\ln(x-1)$. La fonction $x\mapsto \ln(x-1)$ est dérivable sur $[2;1+e]$, et donc 
    \[\dd t=  \frac{1}{x-1} \dd x\]
\end{itemize}
Ainsi,
\[I=\int_2^{e+1} \frac{\ln(u)}{1-\eu{-\ln(u-1)}} \frac{\dd u}{u-1}=\int_2^{e+1} \frac{\ln u }{u}\dd u\]
Ainsi,
\[I=\left[ \frac{1}{2} (\ln u)^2\right]_2^{e+1} = \frac{1}{2}\left((\ln(e+1))^2-\ln(2)^2\right)\]}
\end{prof}

\lignes{15}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsection{Fonctions définies par une intégrale}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
On peut être amené à étudier une fonction du type $\displaystyle{F: x\mapsto \int_{x_0}^x f(t)\dd t}$    
ou $\displaystyle{G:x\mapsto \int_{u(x)}^{v(x)} f(t)\dd t}$.

\begin{methode}
Pour étudier ce genre de fonctions :
\begin{itemize}
    \item[$\bullet$] Dans le premier cas, on reconnait, après avoir étudié la continuité de $f$, la primitive de $f$ nulle en $x_0$, que l'on étudiera en tant que telle.
    \item[$\bullet$] Dans le deuxième cas, on introduira systématiquement une primitive $H$ de $f$ si $f$ est continue. Dans ce cas, $G(x)=H(v(x))-H(u(x))$ et on étudiera ensuite (dérivation par exemple, si $u$ et $v$ sont dérivables).
\end{itemize}
\end{methode}

\exemple{Soit $\displaystyle{g:x\mapsto \int_x^{2x} f(t)\dd t}$ où $f$ est une fonction continue sur $\R$. Déterminer $g'$.}

\begin{prof}
\solution{En notant $F$ une primitive de $f$ sur $\R$, on a pour tout réel $x$, $g(x)=F(2x)-F(x)$ qui est de classe $\mathcal{C}^1$ puisque $f$ est continue. On a donc
\[\forall~x\in \R,~g'(x)=2F'(2x)-F'(x)=2f(2x)-f(x)\]}
\end{prof}

\lignes{8}

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsection{Méthode des rectangles}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
Pour déterminer une valeur approchée d'une intégrale sur un segment, on peut utiliser la méthode des rectangles, qui consiste à calculer non pas l'aire sous la courbe, mais l'aire d'une somme de rectangles de largeur ``petite''.

\begin{center}
	\includegraphics{tex/Chap15/riemann}	
\end{center}

\begin{methode}{Méthode des rectangles} 
Lorsqu'une fonction est croissante ou décroissante sur un segment $I=[a,b]$, on décompose $I$ en subdivision de longueur $\frac{b-a}{n}$ : \[\left[ a, a+\frac{b-a}{n}\right ], \left[ a+\frac{b-a}{n}, a+2\frac{b-a}{n}\right]\hdots,\left[ a+k\frac{b-a}{n}, a+(k+1)\frac{b-a}{n}\right],\hdots \left[ a+(n-1)\frac{b-a}{n}, b\right]\]
On calcule alors la somme des aires des rectangles ``inférieurs'' et ``supérieurs'' : pour l'intervalle \[\left[ a+k\frac{b-a}{n}, a+(k+1)\frac{b-a}{n}\right],\] on prend le rectangle de hauteur $f\left(a+k\frac{b-a}{n}\right)$ (rectangle inférieur), ou $f\left(a+(k+1)\frac{b-a}{n}\right)$.
\begin{center}\includegraphics{tex/Chap15/rectangle}\end{center}
Lorsque $n$ tend vers $+\infty$, l'aire obtenue, si la fonction est continue, tend vers l'intégrale de la fonction sur le segment.
\end{methode}

\begin{scilab}[Méthode des rectangles]
{\small \lstinputlisting[]{tex/Chap15/rectangle.sce}}
\end{scilab}
