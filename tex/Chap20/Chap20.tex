\objectif{Dans ce chapitre, on va reprendre l'algèbre linéaire, et on va essayer de trouver une base d'un espace vectoriel dans laquelle la matrice d'un endomorphisme est diagonale, ou, à défaut, triangulaire supérieure.}

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
	\item Connaître la notion de valeur propre  :
			\begin{itemize}[label=\textbullet]
				\item connaître la définition de valeur propre, vecteur propre et espace propres\dotfill $\Box$
				\item connaître le lien entre inversibilité et valeur propre $0$\dotfill $\Box$
				\item connaître les propriétés des sous-espaces vectoriels\dotfill $\Box$
			\end{itemize}
	\item Concernant la diagonalisation :
			\begin{itemize}[label=\textbullet]
				\item connaître la définition du polynôme caractéristique\dotfill $\Box$
				\item connaître le lien entre valeur propre et polynôme caractéristique\dotfill $\Box$
				\item savoir utiliser les conditions de diagonalisabilité\dotfill $\Box$
				\item connaître la méthode générale pour diagonaliser un endomorphisme ou une matrice\dotfill $\Box$
			\end{itemize}
	\item Concernant la trigonalisation :
			\begin{itemize}[label=\textbullet]
				\item connaître la définition\dotfill $\Box$
				\item savoir utiliser la conditions de trigonalisabilité\dotfill $\Box$
			\end{itemize}
	\item Savoir appliquer la diagonalisation et la trigonalisation :
			\begin{itemize}[label=\textbullet]
				\item pour calculer la puissance $n$-ième d'une matrice\dotfill $\Box$
				\item pour résoudre des systèmes récurrents linéaires homogènes\dotfill $\Box$
			\end{itemize}
\end{numerote}

\newpage 

Nous avons vu dans les chapitres précédentes d'algèbre linéaire qu'un endomorphisme, en dimension finie, peut être représenté par une matrice, et que toutes les matrices d'un même endomorphisme sont semblables.

L'idée de ce chapitre est de pouvoir trouver une base de l'espace vectoriel dans laquelle la matrice de l'endomorphisme est diagonale, ou bien triangulaire si cela n'est pas possible : c'est ce qu'on appelle la \textbf{réduction d'endomorphisme}.

Dans l'ensemble de ce chapitre, $\K$ désignera $\C$ ou $\R$.

\section{Eléments propres}

	\subsection{Valeurs propres, vecteurs propres}

Dans cette partie, on se donne $E$ un $\K$-espace vectoriel, et $f$ un endomorphisme de $E$.
\definition{On dit qu'un vecteur \emph{non nul} $u$ est un \textbf{vecteur propre} de $f$, associé à la \textbf{valeur propre} $\lambda\in \K$ si et seulement si $f(u)=\lambda u$.
}

\definition{Si $\lambda$ est une valeur propre de $f$, on notera $E_\lambda=\Ker(f-\lambda \id_E)$ l'ensemble appelé \textbf{sous-espace propre} de $f$ associé à la valeur propre $\lambda$.

L'ensemble des valeurs propres d'un endomorphisme $f$ est appelé le \textbf{spectre} de $f$, et est noté $\Sp(f)$.
}

\begin{proposition}
$\lambda$ est une valeur propre de $f$ si et seulement si $E_\lambda$ n'est pas réduit à $0$.	
\end{proposition}


\preuve{En effet, s'il existe $u\in E$ non nul tel que $f(u)=\lambda u$, alors $(f-\lambda \id_E)(u) = 0_E$ et donc $u\in E_\lambda$. \\Réciproquement, si $E_\lambda \neq \{0\}$, soit $u\in E_\lambda$ non nul. Alors, $(f-\lambda \id_E)(u)=0_E$ soit $f(u)=\lambda u$.}

\begin{methode}
Pour vérifier qu'un vecteur $u$ donné est un vecteur propre, il suffit de calculer $f(u)$ et de remarquer que $f(u)=\lambda u$ pour un certain élément $u\in \K$.	
\end{methode}


\exemple{\label{ex1}On considère $f$ l'endomorphisme de $\R^3$ dont la matrice dans la base canonique est $A=\matrice{5&-1&1\\1&3&1\\2&2&4}$. Soit $u=\matrice{1\\1\\-2}\in \R^3$. Montrer que $u$ est un vecteur propre de $f$.}

\begin{prof}
\solution{On constate rapidement que
\[ \matrice{5&-1&1\\1&3&1\\2&2&4}\matrice{1\\1\\-2} = \matrice{2\\2\\-4} = 2\matrice{1\\1\\-2} \]
Ainsi, $u$ est un vecteur propre de $f$, associé à la valeur propre $2$. 
 }	
\end{prof}

\lignes{4}

\begin{methode}
Pour montrer que $\lambda \in \K$ est une valeur propre de $f$, on détermine $E_\lambda=\Ker(f-\lambda \id_E)$ et on montre que celui-ci n'est pas réduit à $0$. On en profitera pour déterminer une base de $E_\lambda$.	
\end{methode}


\exemple{Montrer que $4$ est valeur propre de l'endomorphisme de l'exemple \ref{ex1}.}

\begin{prof}
\solution{Soit $X=\matrice{x\\y\\z} \in E_4=\Ker(f-4\id_{\R^3})$. Alors, $X$ vérifie
\begin{eqnarray*}
	f(X)=4X &\Leftrightarrow& \matrice{5x-y+z\\x+3y+z\\2x+2y+4z}=\matrice{4x\\4y\\4z}\\
	&\Leftrightarrow& \left\{\begin{array}{ccccccc}
 		x&-&y&+&z&=&0\\
 		x&-&y&+&z&=&0\\
 		2x&+&2y&&&=&0
	 \end{array}\right.\\
	 &\Leftrightarrow& \left\{
	 \begin{array}{ccc}
	 		x&=&-\frac{1}{2}z\\
	 		y&=&\frac{1}{2}z\\z&=&z	 		
	 \end{array}\right.,\quad z\in \R
\end{eqnarray*}
Ainsi, $E_4$ n'est pas réduit à $0$ : $4$ est une valeur propre de $f$ et \[ E_4=\Vect \left( \matrice{-1\\1\\2}\right) \]
}	
\end{prof}

\lignes{12}

\exercice{Soit $f:\R_3[X]\rightarrow \R_3[X]$ définie par $f(P)=XP'$. Montrer que $1, X, X^2$ et $X^3$ sont des vecteurs propres de $f$ associés à des valeurs propres à déterminer. Quelle est la matrice de $f$ dans la base $(1,X,X^2,X^3)$ ?}

\begin{prof}
\solution{On constate rapidement que
\[ f(1)=0,\quad f(X)=X,\quad f(X^2)=2X^2 \qeq f(X^3)=3X^3 \]
Ainsi, $1$ est associé à la valeur propre $0$, $X$ à la valeur propre $1$, $X^2$ à la valeur propre $2$ et enfin $X^3$ à la valeur propre $3$. Dans la base canonique, $f$ a pour matrice
\[ \matrice{0&0&0&0\\0&1&0&0\\0&0&2&0\\0&0&0&3} \]
}	
\end{prof}

\lignes{10}

\remarque{Si $u$ est un vecteur propre de $f$, associé à la valeur propre $\lambda$, alors, pour tout $\alpha \in \K$, on a $f(\alpha u)=\alpha\lambda u \in \Vect(u)$. \\On a donc $f(\Vect(u))\subset \Vect(u)$ : on dit que la droite $\Vect(u)$ est \textbf{stable} par $f$.}

	\subsection{Valeur propre et inversibilité}

\begin{proposition}
Soit $E$ un $\K$-espace vectoriel, et $f$ un endomorphisme de $E$. $0$ est valeur propre de $f$ si et seulement si $f$ n'est pas injective.	
\end{proposition}


\preuve{En effet, $0$ est valeur propre si et seulement si $\Ker(f-0\id_E)$ n'est pas réduit à $0$, c'est-à-dire si $\Ker(f)$ n'est pas réduit à $0$, c'est-à-dire si $f$ n'est pas injective.}

\remarque{Ainsi, si, en dimension finie, $f$ n'est pas bijective, alors $0$ est nécessairement valeur propre de $f$.}

\exemple{Soit $f$ l'endomorphisme de $\R^3$ dont la matrice dans la base canonique est $A=\matrice{1&1&1\\1&1&1\\1&1&1}$. Montrer que $0$ est valeur propre de $f$, et déterminer $E_0$.}

\begin{prof}
\solution{Remarquons que $A$ n'est pas inversible (en effet, elle possède deux lignes égales). D'après le résultat précédent, on en déduit que $0$ est valeur propre de $f$. $X=\matrice{x\\y\\z}$ est un élément de $E_0$ si et seulement si $x+y+z=0$, soit $x=-y-z$, avec $(y,z)\in \R^2$. Ainsi
\[ \boxed{E_0=\Vect \left( \matrice{-1\\1\\0},\matrice{-1\\0\\1}\right) }\]
}	
\end{prof}
 
\lignes{7}

		\subsection{Sous-espaces propres en somme directe}

\begin{proposition}
Soit $E$ un $\K$-espace vectoriel de \emph{dimension finie}, et $f$ un endomorphisme de $E$. Soient $\lambda_1,\cdots, \lambda_n$ des valeurs propres deux à deux distinctes de $f$. Alors $E_{\lambda_1}, \cdots, E_{\lambda_n}$ sont en sommes directes.	
\end{proposition}

\preuve{Soient $(\lambda, \mu)$ deux valeurs propres distinctes de $f$. Soit $u\in E_\lambda \cap E_\mu $. Alors
\begin{itemize}[label=\textbullet]
	\item $u\in E_\lambda$ donc $(f-\lambda \id_E)(u)=0_E$, c'est-à-dire $f(u)=\lambda u$.
	\item De même, $u\in E_\mu$ donc $f(u)=\mu u$.
\end{itemize}
Mais alors, $f(u)=\lambda u=\mu u \Leftrightarrow (\lambda-\mu)u=0_E$. Or $\lambda \neq \mu$ donc nécessairement $u=0_E$. \\Ainsi, $E_\lambda \cap E_\mu = \{ 0_E \}$ : $E_\lambda$ et $E_\mu$ sont en somme directe.
}

\consequence{Soit $E$ un $\K$-espace vectoriel de \emph{dimension finie}, et $f$ un endomorphisme de $E$. Soient $\lambda_1,\cdots, \lambda_n$ des valeurs propres deux à deux distinctes de $f$. Alors :
\begin{itemize}[label=\textbullet]
	\item $E_{\lambda_1}\oplus\cdots\oplus E_{\lambda_n}$ est un sous-espace vectoriel de $E$.
	\item $\dim(E_{\lambda_1} \oplus \cdots \oplus E_{\lambda_n}) =\dim(E_1)+\cdots+\dim(E_n)$.
	\item Si on note $\mathcal{B}_1, \cdots, \mathcal{B}_n$ des bases respectives de $E_{\lambda_1},\cdots,E_{\lambda_n}$, alors la famille réunissant les bases $\mathcal{B}_1,\cdots, \mathcal{B}_n$ est une base de $E_{\lambda_1} \oplus \cdots \oplus E_{\lambda_n}$.
\end{itemize}
}

	\subsection{Diagonalisation}
	
\remarque{Remarquons que, d'après le résultat précédent, nous avons \[\dim(E_{\lambda_1} \oplus \cdots \oplus E_{\lambda_n}) \leq \dim(E) \]
puisque $E_{\lambda_1} \oplus \cdots \oplus E_{\lambda_n}$ est un sous-espace vectoriel de $E$. On s'intéresse au cas où la dimension $\dim(E_{\lambda_1} \oplus \cdots \oplus E_{\lambda_n})$ est égale à $\dim(E)$, c'est-à-dire au cas particulier où $E_{\lambda_1} \oplus \cdots \oplus E_{\lambda_n}=E$.}

\definition{Soit $E$ un $\K$-espace vectoriel de \emph{dimension finie}, et $f$ un endomorphisme de $E$. On dit que $f$ est \textbf{diagonalisable} s'il existe une base de $E$ constituée de vecteurs propres de $f$.}

\remarque{Ainsi, diagonaliser un endomorphisme, c'est trouver une base de vecteurs propres et les valeurs propres associées.}

\exercice{Montrer que $6$ est valeur propre de l'endomorphisme $f$ de l'exemple \ref{ex1}, puis diagonaliser $f$.}

\begin{prof}
\solution{Nous avons vu d'une part que $2$ et $4$ sont valeurs propres, et $E_4=\Vect\left( \matrice{-1\\1\\2}\right)$. Par le même calcul, on peut montrer que $E_2=\Vect\left( \matrice{1\\1\\-2}\right)$. Enfin, en résolvant le système $f(X)=6X$, on obtient
\begin{eqnarray*}
	X=\matrice{x\\y\\z}\in E_6 &\Leftrightarrow& \left\{\begin{array}{ccccccc}-x&-&y&+&z&=&0\\x&-&3y&+&z&=&0\\2x&+&2y&-&2z&=&0\end{array}\right.\\
	&\Leftrightarrow& \left\{\begin{array}{ccc}x&=&\frac{1}{2}z\\y&=&\frac{1}{2}z\\z&=&z \end{array}\right.,\quad z\in \R
\end{eqnarray*} 	
Ainsi, $E_6=\Vect\left( \matrice{1\\1\\2} \right)$.\\
Remarquons finalement que $\mathcal{B}=\left( \matrice{1\\1\\-2},\matrice{-1\\1\\2}, \matrice{1\\1\\2} \right)$ est une famille libre (puisque composée de vecteurs propres associées à des valeurs propres distinctes) de cardinal $3$, égal à la dimension de $\R^3$. Ainsi, $\mathcal{B}$ est une base de $\R^3$, et dans cette base, la matrice de $f$ est 
\[ \Mat_{\mathcal{B}}(f) = \matrice{2&0&0\\0&4&0\\0&0&6} \]
}
\end{prof}

\lignes{12}

\begin{proposition}
Soit $E$ un $\K$-espace vectoriel de \emph{dimension finie}, et $f$ un endomorphisme de $E$. $f$ est diagonalisable si et seulement s'il existe une base de $E$ dans laquelle la matrice de $f$ est diagonale.	
\end{proposition}


\preuve{Soit $E$ un $\K$-espace vectoriel de \emph{dimension finie}, et $f$ un endomorphisme de $E$, \emph{diagonalisable}. On note $\mathcal{B}=(e_1,\cdots, e_n)$ une base de vecteurs propres de $f$, dont les valeurs propres associées sont respectivement notées $(\lambda_1,\cdots, \lambda_n)$. Alors, dans la base $\mathcal{B}$, la matrice de $f$ est 
\[ \Mat_{\mathcal{B}}(f)=\matrice{\lambda_1 & 0 & \hdots & 0 \\ 0 & \lambda 2 & \ddots & \vdots\\\vdots & \ddots & \ddots & 0 \\ 0 & \hdots & 0 & \lambda_n} \]
}

	\subsection{Matrice diagonalisable}
	
\remarque{L'ensemble du vocabulaire énoncé pour les endomorphismes se transpose aux matrices carrées. En effet, si $A$ est une matrice carré de $\MM_n(\K)$, on peut lui associer l'endomorphisme de $\K^n$ canoniquement associé à $A$.

Par exemple, $X\in \K^n$ est un vecteur propre de $A$, associé à la valeur propre $\lambda \in \K$, si et seulement si $AX=\lambda X$.}

\begin{proposition}
$A\in \MM_n(\K)$ est inversible si et seulement si $0$ n'est pas dans le spectre de $A$.	
\end{proposition}


\preuve{En effet, $A$ est inversible si et seulement si $A$ est injective (puisqu'on est en dimension finie). Or $A$ est injective si et seulement si $\Ker(f)=\{0\}$, où $f$ est l'endomorphisme canoniquement associé à $A$.}

\definition{Soient $A\in \MM_n(\K)$. On dit que $A$ est \textbf{diagonalisable} si et seulement si elle est semblable à une matrice diagonale, c'est-à-dire s'il existe $P\in GL_n(\K)$ telle que $P^{-1}AP$ est diagonale.}

\remarque{Cette définition est bien sûr cohérente avec la définition de diagonalisabilité d'un endomorphisme. Ainsi, $A$ est diagonalisable si et seulement si l'endomorphisme canoniquement associé est diagonalisable.}

\remarque{Diagonaliser une matrice, c'est donc donner une matrice $P$ inversible, et une matrice $D$ diagonale, telle que $A=PDP^{-1}$.}

\exercice{En reprenant l'exemple \ref{ex1}, montrer que la matrice $A=\matrice{5&-1&1\\1&3&1\\2&2&4}$ est diagonalisable. On pourra s'intéresser à 
\[P=\matrice{1&-1&1\\1&1&1\\-2&2&2} \]
constituée des vecteurs propres trouvés précédemment, et calculer $P^{-1}AP$.}

\begin{prof}
\solution{$P$ est inversible, et par les méthodes classiques, on obtient
\[ P^{-1}=\matrice{0&\frac{1}{2}&-\frac{1}{4}\\-\frac{1}{2}&\frac{1}{2}&0\\\frac{1}{2}&0&\frac{1}{4}} \]
Puis
\[ P^{-1}AP=\matrice{2&0&0\\0&4&0\\0&0&6} \]
Ainsi, $A$ est diagonalisable, et $A=P\matrice{2&0&0\\0&4&0\\0&0&6}P^{-1}$
}	
\end{prof}

\lignes{12}

\section{Diagonalisation}

L'exemple précédent a permis de donner une intuition sur la méthode que nous allons appliquer pour diagonaliser, si c'est possible, un endomorphisme.

Il faut cependant une méthode pour déterminer les valeurs propres d'un endomorphisme ou d'une matrice.

	\subsection{Polynôme caractéristique}

\definition{Soit $E$ un $\K$-espace vectoriel de dimension finie. Soit $f\in \mathcal{L}(E)$.  On appelle \textbf{polynôme caractéristique} de $f$, et on note $\chi_f$ ou $P_f$, le déterminant
\[ \chi_f(X)=\det(f-X \id_E) \in \K_n[X] \]
On définit de même le polynôme caractéristique d'une matrice $A\in \MM_n(\R)$ comme $\chi_A(X)= \det(A-X I_n)$.
}

\remarque{Le polynôme caractéristique est un polynôme de degré $n=\dim(E)$. En effet, en notant $A=\matrice{a_{11}&a_{12}&\hdots&a_{1n}\\a_{21}&a_{22}&\hdots&a_{2n}\\\vdots&\vdots&&\vdots\\a_{n1}&a_{n2}&\hdots&a_{nn}}$, on a
\[ \chi_A(X)=\determinant{a_{11-X}&a_{12}&\hdots&a_{1n}\\a_{21}&a_{22}-X&\hdots&a_{2n}\\\vdots&\vdots&&\vdots\\a_{n1}&a_{n2}&\hdots&a_{nn}-X}\]
qui va bien donner un polynôme de degré $n$ (en utilisant la définition par récurrence du déterminant).
} 

\exemple{En reprenant l'exemple \ref{ex1}, déterminer le polynôme caractéristique de $f$. Calculer l'image par $\chi_f$ de chacune des valeurs propres. Que constate-t-on ? }

\begin{prof}
\solution{Par définition, on a
\begin{eqnarray*}
	\chi_f(X) &=& \det(f-X \id_{\R^3})\\
		&=& \det(A-XI_3) \\
		&=& \determinant{5-X&-1&1\\1&3-X&1\\2&2&4-X} \\
		&=& -\determinant{1&-1&5-X\\1&3-X&1\\4-X&2&2}\\
		&=& -\determinant{1&-1&5-X\\0&4-X&-4+X\\0&6-X&-18+9X-X^2} \\
		&=& -\determinant{4-X&-4+X\\6-X&-18+9X-X^2}\\
		&=& -((4-X)(-18+9X-X^2)-(6+X)(-4+X)-\\
		&=& -(4-X)(-18+9X-X^2+6+X) \\
		&=& (4-X)(12-8X+X^2)
\end{eqnarray*}
Ainsi, $\chi_f(X)=(4-X)(12-8X+X^2)$.
 On constate alors que $\chi_f(2)=\chi_f(4)=\chi_f(6)=0$.
}
\end{prof}

\lignes{10}
	
	\subsection{Valeur propre et polynôme caractéristique}

Le résultat vu dans l'exemple précédent est général : $\lambda\in \K$ est valeur propre de $f$ si et seulement si $\lambda$ est une racine de $\chi_f$.

\begin{theoreme}
Soient $E$ un $\K$-espace vectoriel de dimension finie, et $f$ un endomorphisme de $E$.\\
$\lambda \in \K$ est une valeur propre de $f$ si et seulement si $\lambda$ est une racine du polynôme caractéristique de $f$. \\
On appelle \textbf{multiplicité} de la valeur propre $\lambda$ la multiplicité de la racine $\lambda$ du polynôme caractéristique.	
\end{theoreme}

\begin{methode}
Pour déterminer les valeurs propres d'un endomorphisme $f$ on peut 
\begin{itemize}[label=\textbullet]
	\item soit résoudre le système $f(u)=\lambda u$, et chercher pour quelle(s) valeur(s) de $\lambda$ le système admet une infinité de solution.
	\item soit déterminer le polynôme caractéristique de $f$, puis déterminer les racines du polynôme. 
\end{itemize}	
\end{methode}

\exercice{Soit $f$ l'endomorphisme de $\R^2$ dont la matrice dans la base canonique est $A=\matrice{9&-4\\12&-5}$. Déterminer les valeurs propres de $f$.}

\begin{prof}
\solution{Déterminons son polynôme caractéristique :
\begin{eqnarray*}
	\chi_f(X) &=& \det(f-XI_2) \\
		&=& \determinant{9-X&-4\\12&-5-X}\\
		&=&(9-X)(-5-X)-(-4)*(12) \\
		&=& 3 - 4X +X^2
\end{eqnarray*}
On constate que $X^2-4X+3=(X-1)(X-3)$. Ainsi, les valeurs propres de $f$ sont $1$ et $3$.
}	
\end{prof}
  
\lignes{8}

En dimension finie, nous avons également un lien entre dimension du sous-espace propre, et multiplicité d'une valeur propre, qui vous nous servir :

\begin{proposition}
Soient $E$ un $\K$-espace vectoriel de dimension finie, et $f$ un endomorphisme de $E$. Soit $\lambda$ une valeur propre de $f$, de multiplicité $m_\lambda$, et soit $E_\lambda=\Ker(f-\lambda \id_E)$ l'espace propre associé. Alors
\[ 1 \leq \dim(E_\lambda) \leq m_\lambda \]	
\end{proposition}


\preuve{Par définition, $\lambda$ est valeur propre de $f$ si $E_\lambda$ n'est pas réduit à $\{0\}$, donc si $\dim(E_\lambda)\geq 1$.\\
On note $d=\dim(E_\lambda)$, et on prend une base $(e_1,\cdots,e_d)$ de $E_\lambda$. On complète cette base en une base de $E$ noté $(e_1,\cdots, e_d,e_{d+1},\cdots, e_n)$. Dans cette base, la matrice de $f$ s'écrit
\[ \Mat_\mathcal{B}(f)=\matrice{\lambda I_d & B \\0_{n-d,d}&A} \]
où $A \in \MM_{n-d}(\K)$ et $B\in \MM_d(\K)$. Mais alors, on a
\[ \Mat_{\mathcal{B}}(f-\lambda \id_E) = \matrice{(\lambda-X) I_d & B \\0_{n-d,d}&A-XI_{n-d}} \]
et donc
\[ \chi_f(X)=\det(f-\lambda \id_E) = (\lambda-X)^d \det(A-XI_{n-d}) \]
Ainsi, le polynôme $(\lambda-X)^d$ divise $\chi_f(X)$, ce qui signifie que la multiplicité de $\lambda$ dans le polynôme $\chi_f(X)$ est supérieure ou égale à $d$ : $m_\lambda \geq d$.
}


	\subsection{Condition de diagonalisabilité}


Nous avons vu que les sous-espaces propres étaient en somme directe. Ainsi, en notant $\lambda_1,\cdots, \lambda_n$ les valeurs propres de $f$, on a \[ \dim(E_{\lambda_1})+\cdots +\dim(E_{\lambda_n}) \leq \dim(E) \]

\consequence{$f$ est diagonalisable si et seulement si la somme des dimensions des sous-espaces propres est égale à la dimension de $E$.}

\preuve{En effet, dans ce cas, $\dim(E_{\lambda_1}\oplus \cdots \oplus E_{\lambda_n})=\dim(E)$ implique, puisque $E_{\lambda_1}\oplus \cdots \oplus E_{\lambda_n}$ est un sous-espace vectoriel de $E$, que $E_{\lambda_1}\oplus \cdots \oplus E_{\lambda_n}=E$.}

Ce résultat va nous servir de base pour déterminer une condition de diagonalisabilité : $f$ sera diagonalisable si la multiplicité des valeurs propres coïncide avec la dimension du sous-espace propre.

\begin{theoreme}[Condition nécessaire et suffisante de diagonalisabilité] 
Soient $E$ un $\K$-espace vectoriel de dimension finie, et $f$ un endomorphisme de $E$.\\
$f$ est diagonalisable si et seulement si son polynôme caractéristique est scindé (c'est-à-dire que ses facteurs irréductibles sont de degré $1$), et si, pour chaque valeur propre $\lambda$ de $f$, la dimension du sous-espace propre est égale à la multiplicité de $\lambda$.	
\end{theoreme}


\preuve{Admis.}

On a un cas particulier qui nous garantit la diagonalisabilité :

\begin{proposition}[Condition suffisante de diagonalisabilité] 
Soient $E$ un $\K$-espace vectoriel de dimension finie, et $f$ un endomorphisme de $E$. Si $\chi_f(X)$ est scindé et à racines simples, alors $f$ est diagonalisable.	
\end{proposition}


\remarque{\danger~La réciproque est fausse : ce n'est pas parce que $f$ est diagonalisable que son polynôme caractéristique sera scindé à racine simple.}

\exemple{Montrer que la matrice $A_\theta=\matrice{\cos(\theta)&-\sin(\theta)\\\sin(\theta)&\cos(\theta)}$ est diagonalisable dans $\MM_2(\C)$. Est-elle diagonalisable dans $\MM_2(\R)$ ?}

\begin{prof}
\solution{Déterminons le polynôme caractéristique de $A_\theta$ :
\begin{eqnarray*}
	\chi_A(X)&=& \det(A-XI_2) \\
	 		 &=& \determinant{\cos(\theta)-X&-\sin(\theta)\\\sin(\theta)&\cos(\theta)-X} \\
	 		 &=& (\cos(\theta)-X)(\cos(\theta)-X) -\sin(\theta)(-\sin(\theta))\\
	 		 &=& \cos(\theta)^2-2X\cos(\theta)+X^2+\sin(\theta)^2\\
	 		 &=& X^2-2X\cos(\theta)+1
\end{eqnarray*}
Nous avons déjà vu (voir chapitre \emph{Nombres complexes}) que les racines de ce polynôme sont $\eu{i\theta}$ et $\eu{-i\theta}$. Ainsi
\[ \chi_A(X) = (X-\eu{i\theta})(X-\eu{-i\theta}) \]
est scindé, à racines simples : $A$ est diagonalisable dans $\MM_2(\C)$.\\
Remarquons que $A$ n'a pas de valeur propre réelle, d'après ce qui précède (sauf si $\theta=0$). Elle ne peut donc pas être diagonalisable dans $\MM_2(\R)$. 
}	
\end{prof}

\lignes{10}

\remarque{Ainsi, une matrice ou un endomorphisme peut être diagonalisable sur $\C$, mais pas sur $\R$. La condition de diagonalisabilité dépend donc fortement du corps de base.}

Pour terminer, un théorème sur les matrices diagonalisables ayant une seule valeur propre :

\begin{proposition}
Les seules matrices diagonalisables n'ayant qu'une seule valeur propre sont les matrices d'homothétie $\lambda I_n$.	
\end{proposition}


\begin{prof}
\solution{En effet, si $M$ est diagonalisable avec une seule valeur propre $\lambda$, il existe, d'après ce qui précède, une matrice $P$ inversible telle que \[ M=P\matrice{\lambda&0&\cdots&0\\0&\lambda&\cdots&0\\\vdots & & \ddots & 0\\0&0&\cdots&\lambda} P^{-1} = P(\lambda I_n)P^{-1} =\lambda PP^{-1}=\lambda I_n\]}
\end{prof}

\lignes{3}

	\subsection{Méthode générale}
	
La méthode générale de diagonalisation d'une matrice dépend de la matrice (ou de l'endomorphisme) et de l'exercice.	

\begin{methode}
Pour montrer qu'un endomorphisme $f$ est diagonalisable, on utilise les résultats précédents.\\~\\
\textbullet\quad\textbf{1ère étape} : on détermine les valeurs propres.\\
Pour cela, plusieurs possibilités :
\begin{itemize}
	\item On résout le système $f(u)=\lambda u$, d'inconnue $u$, et on détermine les valeurs de $\lambda$ pour lesquelles ce système n'est pas de Cramer.
	\item On détermine le polynôme caractéristique $\chi_f$ et on détermine les racines de $\chi_f$.
\end{itemize}
~\\
\textbullet\quad\textbf{2ème étape} : on détermine les sous-espaces propres associés.\\
Pour cela, pour chacune des valeurs propres $\lambda \in \Sp(f)$, on résout le système $f(u)=\lambda u$ et on détermine l'ensemble des solutions pour en déduire $E_\lambda$.\\~\\
\textbullet\quad\textbf{3ème étape} : on conclut.\\
On détermine la dimension de chacun des sous-espaces propres, et 
\begin{itemize}
	\item on vérifie que la dimension est égale à la multiplicité de la racine $\lambda$ du polynôme caractéristique;
	\item ou on vérifie que la somme des dimensions des sous-espaces propres est égale à la dimension de l'espace $E$.
\end{itemize}	
\end{methode}

\remarque{Si, dans la troisième étape, on constate que la somme des dimensions ne fait pas $\dim(E)$, ou bien que la dimension d'un sous-espace propre n'est pas égale à la multiplicité de la valeur propre associée, on peut conclure que $f$ n'est pas diagonalisable.}

\exercice{Soit $A=\matrice{-14&12&3\\-20&17&4\\0&0&1}$ et $f$ l'endomorphisme de $\MM_{3,1}(\R)$ dont la matrice dans la base canonique est $A$. Montrer que $f$ est diagonalisable dans $\MM_{3,1}(\R)$, et déterminer deux matrices $P$ inversible et $D$ diagonale telle que $A=PDP^{-1}$.}

\begin{prof}
\solution{On applique la méthode.~\\
\textbullet\quad\textbf{1ère étape} : déterminons les valeurs propres de $A$. Pour cela, on détermine $\chi_A(X)=\det(A-\lambda XI_3)$.
\begin{eqnarray*}
	\chi_A(X) &=& 
		\determinant{(-14-X)&12&3\\-20&(17-X)&4\\0&0&(1-X)}\\
	  &=& 
		-\determinant{-20&(17-X)&4\\(-14-X)&12&3\\0&0&(1-X)}\\
		&=& 
		-\frac{1}{20}\determinant{-20&(17-X)&4\\0&X^2-3X+2&4-4X \\0&0&(1-X)}\\
		&=& (1-X)(X^2-3X+2)=-(X-1)^2(X-2)
\end{eqnarray*}
Ainsi, les valeurs propres sont les racines du polynôme caractéristique : $1$ de multiplicité $2$, et $2$ de multiplicité $1$.\\\textbf{Bilan} : \[ \boxed{\Sp(f) = \{ 1; 2 \}} \]
\textbullet\quad\textbf{2ème étape} : on détermine les sous-espaces propres.\\
On résout tout d'abord le système $AX=X$ pour la valeur propre $\lambda=1$. En reprenant le travail précédent,
\begin{eqnarray*}
	X=\matrice{x\\y\\z} \in E_1 &\Leftrightarrow&
	 \left \{ \begin{array}{ccccccc}
          -20x &+& 16y &+&4z&=&0 \\
           &&  && 0 &=&0 \\
           &&. && 0 &=&0\end{array}\right.  	\\
      &\Leftrightarrow&
      \left \{ \begin{array}{ccc}
      		x&=& \frac{4}{5}y+\frac{1}{5}z\\
            y&=&y\\
            z&=&z \end{array}\right.,\quad (y,z)\in \R^2
\end{eqnarray*}
Ainsi, \[ \boxed{E_1=\Vect\left( \matrice{\frac{4}{5}\\1\\0},\matrice{\frac{1}{5}\\0\\1}\right)=\Vect\left( \matrice{4\\5\\0},\matrice{1\\0\\5}\right)} \]
De même, on résout $AX=2X$ :
\begin{eqnarray*}
	X=\matrice{x\\y\\z} \in E_1 &\Leftrightarrow&
	 \left \{ \begin{array}{ccccccc}
          -20x &+& 15y &+&4z&=&0 \\
           &&  && -4z &=&0 \\
           &&. && -z &=&0\end{array}\right.  	\\
      &\Leftrightarrow&
      \left \{ \begin{array}{ccc}
      		x&=& \frac{3}{4}y\\
            y&=&y\\
            z&=&0 \end{array}\right.,\quad (y,z)\in \R^2
\end{eqnarray*}
Ainsi, \[ \boxed{E_2=\Vect\left(\matrice{\frac{3}{4}\\1\\0}\right)=\Vect\left(\matrice{3\\4\\0}\right) }\]
\textbullet\quad\textbf{3ème étape} : on conclut. \\
On remarque que $\dim(E_1)=2$ (les vecteurs ne sont pas colinéaires) et $\dim(E_2)=1$. Ainsi
\[ \dim(E_1)+\dim(E_2)=3=\dim(\MM_{3,1}(\R)) \]
Ainsi, \fbox{$A$ est diagonalisable}.\\
Posons $\mathcal{B}$ la base canonique, et $\mathcal{C}=\left(\matrice{4\\5\\0},\matrice{1\\0\\5},\matrice{3\\4\\0}\right)$ la base de vecteur propre. Par construction, on obtient
\[ D=\Mat_{\mathcal{C}}(f) = \matrice{1&0&0\\0&1&0\\0&0&2} \]
En notant $P=\Mat_{\mathcal{B},\mathcal{C}} = \matrice{4&1&3\\5&0&4\\0&5&0}$, d'après la formule du changement de bases :
\[ A=\Mat_{\mathcal{B}}(f) = \Mat_{\mathcal{B},\mathcal{C}} \times \Mat_{\mathcal{C}}(f)\times \Mat_{\mathcal{C},\mathcal{B}} = PDP^{-1} \]
\remarque{On peut vérifier (avec SciLab par exemple) que $P^{-1}=\matrice{4&-3&-\frac{4}{5}\\0&0&\frac{1}{5}\\-5&4&1}$ et $PDP^{-1}=A$.}
}	
\end{prof}

\lignes{27}
\lignes{40}

\section{Trigonalisation}

Toute matrice (ou tout endomorphisme) n'est pas nécessairement diagonalisable. On peut alors essayer de la rendre non pas diagonale, mais triangulaire.

	\subsection{Définition}

\definition{On dit qu'une matrice $A\in \MM_n(\K)$ est \textbf{trigonalisable} si et seulement si elle est semblable à une matrice triangulaire supérieure.}

\definition{Soient $E$ un $\K$-espace vectoriel de dimension finie, et $f$ un endomorphisme de $E$. On dit que $f$ est \textbf{trigonalisable} si et seulement si sa matrice dans une base est triangulaire supérieure.} 

	\subsection{Caractérisation des endomorphismes trigonalisables}
	
\begin{theoreme}[Condition nécessaire et suffisante de trangularisation] 
Soient $E$ un $\K$-espace vectoriel de dimension finie, et $f$ un endomorphisme de $E$. $f$ est trigonalisable si et seulement si son polynôme caractéristique est scindé.	
\end{theoreme}

\remarque{Ainsi, pour montrer qu'un endomorphisme est trigonalisable, on détermine son polynôme caractéristique et on vérifie qu'il ne possède que des facteurs irréductibles de degré $1$.}

\exemple{Montrer que la matrice $A=\matrice{\frac{1}{2}&\frac{1}{2}\\-\frac{1}{2}&\frac{3}{2}}$ est trigonalisable.}

\begin{prof}
\solution{En effet
\begin{eqnarray*}
	\chi_A(X)&=&\determinant{\frac{1}{2}-X&\frac{1}{2}\\-\frac{1}{2}&\frac{3}{2}-X}\\
	&=& \left(\frac{1}{2}-X\right)\left(\frac{3}{2}-X\right) +\frac{1}{4}\\
	&=& X^2-2X+1=(X-1)^2
\end{eqnarray*} 
qui est bien scindé.
}	
\end{prof}

\lignes{5}

\begin{proposition}
Le théorème de d'Alembert-Gauss garantit que tout polynôme sur $\C$ est scindé. Ainsi, toute \textbf{matrice carré complexe est trigonalisable}.	
\end{proposition}


	\subsection{Méthode pratique en basse dimension}
	
\remarque{Dans un exercice, si on indique ``réduire'' la matrice $A$, c'est trouver une matrice diagonale ou triangulaire supérieure semblable à $A$.}
	
\begin{methode}%[Matrice carré d'ordre 2]
Pour montrer qu'une matrice carré de taille $2$ est trigonalisable, il suffit de trouver une valeur propre et un vecteur propre, puis de rajouter un vecteur quelconque non colinéaire à ce vecteur propre. Dans cette base, la matrice sera triangulaire.	
\end{methode}


\exemple{Réduire l'endomorphisme de $\R^2$ défini par 
\[ f:\matrice{x\\y} \mapsto \matrice{3x+2y\\-2x-y}\]}


\begin{prof}
\solution{On pose $A=\matrice{3&2\\-2&-1}$. Soit $\chi_A(X)=\determinant{3-X&2\\-2&-1-X} = (3-X)(-1-X)+4 = X^2 -2X+1=(X-1)^2$. Ainsi, $1$ est valeur propre. En résolvant $AX=X$ avec $X=\matrice{x\\y}$, on constate que $\matrice{1\\-1}$ est vecteur propre associé à la valeur propre $1$. On constate que puisque $\dim(E_1)=1\neq \dim(\R^2)$, $f$ n'est pas diagonalisable.\\On pose alors $\mathcal{B}=\left(\matrice{1\\-1}, \matrice{1\\0} \right)$ qui est une base de $\R^2$ (car les vecteurs ne sont pas colinéaire). Par construction $f\left(\matrice{1\\-1}\right)=\matrice{1\\-1}$. De plus,
\[ f\left(\matrice{1\\0}\right)=\matrice{3\\-2} = 2\matrice{1\\-1} + \matrice{1\\0} \]
On en déduit que $A$ est semblable à 
\[ T = \matrice{1 & 2\\0&1} \]

}	
\end{prof}

\lignes{30}
	
\section{Applications}

	\subsection{SciLab}
	
SciLab permet de déterminer le spectre d'une matrice, et obtenir également la matrice $P$ composée de vecteur propre. Nous verrons dans un chapitre ultérieur que SciLab renvoie une base de vecteurs propres de norme $1$.

\begin{mdframed}[style=algostyle]
{\small \lstinputlisting[]{tex/Chap20/spec1.sce}}
\end{mdframed}
\begin{scilabsession}
 ans  =
 
    6.  
    4.  
    2.
 \end{scilabsession}
\begin{mdframed}[style=algostyle]
{\small \lstinputlisting[]{tex/Chap20/spec2.sce}}
\end{mdframed}
\begin{scilabsession}
 val  =
 
    6.    0     0   
    0     4.    0   
    0     0     2.  
 vec  =
 
    0.4082483    0.4082483  - 0.4082483  
    0.4082483  - 0.4082483  - 0.4082483  
    0.8164966  - 0.8164966    0.8164966 
 \end{scilabsession}

	\subsection{Trace, déterminant et valeurs propres}
	
Il y a un lien profond entre les valeurs propres et deux éléments caractéristiques d'une matrice ou d'un endomorphisme : sa trace et son déterminant.

\begin{proposition}
Soit $E$ un $\K$-espace vectoriel de dimension finie, et $f$ un endomorphisme de $E$ \textbf{trigonalisable} ou \textbf{diagonalisable}. On note $\lambda_1,\cdots, \lambda_n$ ses valeurs propres, comptées avec leur multiplicité. Alors
\[ \det(f)=\prod_{k=1}^n \lambda_k \qeq \text{Tr}(f)=\sum_{k=1}^n \lambda_k \]	
\end{proposition}

\preuve{Puisque $f$ est trigonalisable, sa matrice dans une certaine base de $E$ est triangulaire et s'écrit
\[ T=\matrice{\lambda_1&*&\cdots&*\\0&\lambda_2&\cdots&*\\\vdots&\vdots&&\vdots\\0&0&\cdots&\lambda_n} \]
Ainsi, par définition de la trace et du déterminant, on en déduit bien que $\text{Tr}(f)=\lambda_1+\cdots+\lambda_n$ et $\det(f)=\lambda_1\times\cdots\times \lambda_n$.
}

\remarque{Ce résultat s'étend aux matrices trigonalisables.}

	\subsection{Puissances de matrices}
	
Dans le cas où une matrice $A$ est diagonalisable, on peut calculer $A^n$ assez facilement.

\begin{methode}
Soit $A$ une matrice diagonalisable de $\MM_n(\K)$. On note $\mathcal{B}$ la base canonique de $K^n$.
\begin{numerote}
	\item On détermine une base de vecteurs propres $\mathcal{C}$ de $\K^n$.
	\item On pose $P=\Mat_{\mathcal{B},\mathcal{C}}$ la matrice de passage de la base canonique à la base $\mathcal{C}$. Puisque $A$ est diagonalisable, la matrice $D=P^{-1}AP$ est diagonale.
	\item On calcule $D^n$ pour tout entier $n$.
	\item On revient à $A^n$ en montrant que, pour tout entier $n$, $A^n=PD^nP^{-1}$. 
\end{numerote}	
\end{methode}

\exemple{Déterminer $A^n$ pour tout entier $n$, avec $A=\matrice{1&0&1\\0&2&0\\1&0&1}$.}

\begin{prof}
\solution{On commence par déterminer les valeurs propres de $A$. Le polynôme caractéristique est, en développant par rapport à la deuxième colonne :
\begin{eqnarray*}
	\chi_A(X) &=& \det(A-XI_3)\\
			  &=& \determinant{1-X&0&1\\0&2-X&0\\1&0&1-X} \\
			  &=& (-1)^{2+2}\times (2-X) \times \determinant{1-X&1\\1&1-X}\\
			  &=& (2-X)((1-X)^2-1)\\
			  &=&(2-X)X(X-2)=-X(X-2)^2
\end{eqnarray*}
Les valeurs propres sont donc $0$ et $2$. Déterminons les sous-espaces propres.\\
\textbullet\quad Déterminons $E_0$ : 
\begin{eqnarray*}
	\matrice{x\\y\\z} \in E_0 &\Leftrightarrow& AX=0 \\
	  &\Leftrightarrow& \left\{ \begin{array}{ccccccc} x&&&+&z&=&0\\&&2y&&&=&0\\x&&&+&z&=&0\end{array}\right. \\
	  &\Leftrightarrow& \left\{ \begin{array}{ccc} x&=&-z\\y&=&0\\z&=&z\end{array}\right.,\quad z\in \R
\end{eqnarray*}
Ainsi, \[ \boxed{E_0=\Vect\left( \matrice{-1\\0\\1}\right) } \]
\textbullet\quad Déterminons $E_2$ : 
\begin{eqnarray*}
	\matrice{x\\y\\z} \in E_0 &\Leftrightarrow& AX=2X \\
	  &\Leftrightarrow& \left\{ \begin{array}{ccccccc} x-2x&&&+&z&=&0\\&&2y-2y&&&=&0\\x&&&+&z-2z&=&0\end{array}\right. \\
	  &\Leftrightarrow& \left\{ \begin{array}{ccc} x&=&z\\y&=&y\\z&=&z\end{array}\right.,\quad (y,z)\in \R^2
\end{eqnarray*}
Ainsi, \[ \boxed{E_2=\Vect\left(\matrice{0\\1\\0}, \matrice{1\\0\\1}\right) } \]
On constate que $\dim(E_0)+\dim(E_2)=1+2=3=\dim(\R^3)$ (les vecteurs propres de $E_2$ sont bien libres car non colinéaires). Ainsi, $A$ est diagonalisable.\\~\\
\textbullet\quad \textbf{Diagonalisation} :\\
On pose $\mathcal{C}=\left ( \matrice{-1\\0\\1},\matrice{0\\1\\0},\matrice{1\\0\\1}\right)$. $\mathcal{C}$ est une base de vecteurs propres de $\R^3$. Posons $P$ la matrice de passage de la base canonique à $\mathcal{C}$. Alors
$P=\matrice{-1&0&1\\0&1&0\\1&0&1}$, 
puis, après calcul, 
$P^{-1}=\matrice{-\frac{1}{2}&0&\frac{1}{2}\\0&1&0\\\frac{1}{2}&0&\frac{1}{2}}$. Enfin, par construction, on a
\[ A=P \matrice{0&0&0\\0&2&0\\0&0&2}P^{-1} \]~\\
\textbullet\quad \textbf{Conclusion} : \\
Pour tout entier $n\geq 0$, on constate que 
\[ A^n = PDP^{-1}PDP^{-1}\cdots PDP^{-1} = PD^nP^{-1} \]
que l'on peut prouver rapidement par récurrence. Puisque $D$ est diagonale, pour tout entier $n\geq 1$ :
\[ D^n = \matrice{0&0&0\\0&2^n&0\\0&0&2^n} \]
puis
\[ \forall~n\geq 1,\quad A^n=\matrice{-1&0&1\\0&1&0\\1&0&1}\matrice{0&0&0\\0&2^n&0\\0&0&2^n} \matrice{-\frac{1}{2}&0&\frac{1}{2}\\0&1&0\\\frac{1}{2}&0&\frac{1}{2}} \]
et donc
\[ \boxed{\forall~n\geq 1,\quad A^n = \matrice{2^{n-1}&0&2^{n-1}\\0&2^n&0\\2^{n-1}&0&2^{n-1}}}\]
}	

\end{prof}

\lignes{42}
\lignes{25}
	
	\subsection{Systèmes récurrents linéaires homogènes}
	
On s'intéresse dans cette partie à des systèmes d'équations linéaires homogènes dont les inconnues sont des suites à déterminer.

\exemple{\label{ex2} Soient $(u_n)$ et $(v_n)$ deux suites complexes vérifiant 
\[ \left \{ \begin{array}{ccccc} u_{n+1}&=&-3u_n&+&4v_n\\v_{n+1}&=&-2u_n&+&3v_n\end{array}\right.\]
}

\begin{methode}
Lorsqu'on dispose d'un système récurrent linéaire homogène :
\begin{numerote}
	\item On pose $U_n$ le vecteur colonne des inconnues (par exemple, $U_n=\matrice{u_n\\v_n}$), et on introduit la matrice $A$ vérifiant $U_{n+1}=AU_n$.
	\item On montre par récurrence sur $n$ que $U_n=A^n U_0$.
	\item On détermine $A^n$ (par exemple, par la méthode précédente).
	\item On conclut.	
\end{numerote}	
\end{methode}

\exemple{Déterminer l'expression de $u_n$ et $v_n$ en fonction de $n$, de $u_0$ et de $v_0$ en reprenant l'exemple \ref{ex2}.}

\begin{prof}
\solution{On pose $U_n=\matrice{u_n\\v_n}$ et $A=\matrice{-3&4\\-2&3}$. On a bien, pour tout entier $n$, $U_{n+1}=AU_n$.\\
On montre par récurrence sur $n$ que pour tout entier $n$, $P_n$ : ``$U_n=A^n U_0$'' :
\begin{itemize}[label=\textbullet]
	\item Pour $n=0$, on a $A^0U_0=I_2U_0=U_0$. $P_0$ est donc vraie.
	\item Supposons la proposition $P_n$ vraie pour un certaine entier $n$. Montrons alors que $P_{n+1}$ est vraie. En effet, par définition de $A$ :
	\[ U_{n+1} = AU_n \underset{\text{H.R.}}{=} A (A^n U_0) = A^{n+1} U_0 \]
	$P_{n+1}$ est donc vraie.
\end{itemize}
D'après le principe de récurrence, on a donc bien démontré que $U_n=A^nU_0$ pour tout entier $n$.\\~\\
\textbullet\quad \textbf{Détermination de $A^n$} :\\
Le polynôme caractéristique de $A$ est 
\[ \chi_A(X)=\determinant{-3-X&4\\-2&3-X} = (-3-X)(3-X)+8= X^2-1=(X-1)(X+1) \]
Les valeurs propres sont donc $1$ et $-1$. Le polynôme caractéristique étant scindé à racines simples, $A$ est diagonalisable.\\
On détermine alors les sous-espaces propres. Après calcul :
\[ E_1=\Vect\left(\matrice{1\\1} \right) \qeq E_{-1} = \Vect\left(\matrice{2\\1} \right)\]
En posant $\mathcal{C}=\left( \matrice{1\\1},\matrice{2\\1}\right)$, et $P$ la matrice de passage de la base canonique à $\mathcal{C}$, on a respectivement
\[ P=\matrice{1&2\\1&1},\quad P^{-1}=\matrice{-1&2\\1&-1} \qeq A=P\matrice{1&0\\0&-1}P^{-1} \]
Mais alors, pour tout entier $n$ :
\[ A^n =PD^nP^{-1}=\matrice{1&2\\1&1}\matrice{1&0\\0&(-1)^n}\matrice{-1&2\\1&-1} \]
et donc
\[ \boxed{A^n = \matrice{-1+2(-1)^n&2+2(-1)^{n+1}\\-1+(-1)^n&2+(-1)^{n+1}} } \]
~\\\textbullet\quad\textbf{Conclusion} :\\
On revient à $U_n$ : $U_n=A^nU_0$ et donc
\[ \boxed{\forall~n,\quad u_n=(-1+2(-1)^n)u_0+(2+2(-1)^{n+1})v_0 \qeq v_n=(-1+(-1)^n)u_0+(2+(-1)^{n+1})v_0}\]

}
	
\end{prof}


\lignes{50}
\lignes{20}