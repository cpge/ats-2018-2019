
\objectif{Dans ce chapitre, on introduit un objet très important, qui sera utilisé régulièrement et qui fera l'objet d'étude approfondie plus tard dans l'année.}

\large \textbf{Objectifs} \normalsize

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
				\item Savoir calculer avec les matrices (sommes, produits, transposés)\dotfill $\Box$
				\item Connaître définition et propriétés des matrices inversibles\dotfill $\Box$ 
				\item Savoir déterminer le rang d'une matrice\dotfill $\Box$
				\item Savoir faire le lien entre système et matrice associée\dotfill $\Box$
				\item Savoir déterminer la puissance $n^\text{ième}$ d'une matrice...
				\begin{itemize}[label=\textbullet]
					\item par récurrence, en conjecturant l'allure générale\dotfill $\Box$
					\item par la formule du binôme de Newton\dotfill $\Box$
					\item par diagonalisation, lorsque celle-ci est donnée\dotfill $\Box$
				\end{itemize}
\end{numerote}


\newpage
\section{Matrices}

    \subsection{Définition}
    
\definition{Soient $n$ et $p$ deux entiers non nuls, et $\K$ représentant $\R$ ou $\C$. On appelle \textbf{matrice} à $n$ lignes et $p$ colonnes à coefficients dans $\K$ un tableau rectangulaire de nombres dans $\K$ comportant $n$ lignes et $p$ colonnes.

\[\underbrace{\matrice{ ~&~\\~&~}}_{p \textrm{ colonnes}}\left.\begin{array}{c}~\\~\end{array}\right\} n \textrm{ lignes}\] 

En général, lorsque $A$ est une matrice à $n$ lignes et $p$ colonnes, le coefficient situé à l'intersection de la $i\up{ème}$ ligne et de la $j\up{ème}$ colonne se note $a_{i,j}$. On écrit alors
\[A=
\matrice{
a_{1,1}& a_{1,2} & \hdots & a_{1,j} & \hdots & a_{1,p}\\
a_{2,1}& a_{2,2} & \hdots & a_{2,j} & \hdots & a_{2,p}\\
\vdots & \vdots & & \vdots & & \vdots \\
a_{i,1}& a_{i,2} & \hdots & a_{i,j} & \hdots & a_{i,p}\\
\vdots & \vdots & & \vdots & & \vdots \\
a_{n,1}& a_{n,2} & \hdots & a_{n,j} & \hdots & a_{n,p}
}
 \textrm{ ou } A = (a_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq p}}\]    }
    
\notation{L'ensemble des matrices à $n$ lignes et $p$ colonnes à coefficients dans $\K$ est noté $\MM_{n,p}(\K)$.}    

\definition{~ \begin{itemize}[label=\textbullet]
     \item On appelle \textbf{matrice ligne} $\matrice{.&.&.}$ un élément de $\mathcal{M}_{1,p}(\K)$.
     \item On appelle \textbf{matrice colonne} $\matrice{.~\\.~\\.~}$ un élément de $\mathcal{M}_{n,1}(\K)$. 
     \item On appelle \textbf{matrice nulle} de $\mathcal{M}_{n,p}(\K)$, notée $0_{n,p}$ (ou $0$ quand il n'y a pas d'ambiguïté), la matrice de $\mathcal{M}_{n,p}(\K)$ dont tous les coefficients sont nuls.
 \end{itemize}
}

    \subsection{L'algèbre des matrices}
    
        \subsubsection{Addition de matrices}

\definition{Soient $A=(a_{i,j})_{\substack{1\leq i \leq n\\1 \leq j \leq p}}$ et $B=(b_{i,j})_{\substack{1\leq i \leq n\\1 \leq j \leq p}}$ deux matrices de $\mathcal{M}_{n,p}(\K)$. On appelle \textbf{somme} de la matrice $A$ et de la matrice $B$ la matrice de $\mathcal{M}_{n,p}(\K)$, notée $A+B$ définie par 
\[A+B=(c_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq p}} \textrm{ où } \forall~i\in \llbracket 1,n \rrbracket,~\forall~j\in \llbracket 1,p \rrbracket,~ c_{i,j}=a_{i,j}+b_{i,j}\]
}

\exemple{Si $A=\matrice{1 & 2\\2 & -1\\ 1& 0}$ et $B=\matrice{0 & 1\\-1 & 1 \\ 2& 1}$, alors
 $A+B=\matrice{1 & 3 \\ 1 & 0 \\ 3 & 1}$
 }

        \subsubsection{Multiplication par un scalaire}
        
\definition{Soient $A=(a_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq p}}$ une matrice de $\mathcal{M}_{n,p}(\K)$ et $\lambda \in \K$. On appelle \textbf{produit} de la matrice $A$ par le nombre $\lambda$ la matrice de $\mathcal{M}_{n,p}(\K)$, notée $\lambda A$, définie par
\[\lambda A=(c_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq p}} \textrm{ où } \forall~i\in \llbracket 1,n \rrbracket,~\forall~j\in \llbracket 1,p \rrbracket,~ c_{i,j}=\lambda a_{i,j}\]
}

\exemple{Si $A=\matrice{1 & 2\\2 & -1\\ 1& 0}$ alors $2A=\matrice{2 & 4\\4 & -2\\ 2& 0}$}


        \subsubsection{Premières propriétés}
        
\propriete{Soient $A, B$ et $C$ trois éléments de $\mathcal{M}_{n,p}(\K)$, et $\lambda, \mu$ deux nombres de $\K$.
\begin{itemize}[label=\textbullet]
    \item $A+B=B+A$ (commutativité de l'addition).
    \item $0+A=A+0=A$ ($0$ est le neutre de l'addition)
    \item $A+(-A)=(-A)+A=A-A=0$ ($-A$ est l'opposé de la matrice $A$.)
    \item $(\lambda+\mu)A=\lambda A +\mu A$, $\lambda(A+B)=\lambda A+ \lambda B$ et $\lambda (\mu A) = (\lambda \mu)A = \lambda \mu A$ (distributivités)
\end{itemize}
}        

\remarque{Les différentes propriétés précédentes font de l'ensemble $\mathcal{M}_{n,p}(\K)$, muni de l'addition et la multiplication par un réel, un \textbf{espace vectoriel}. Nous y reviendrons plus tard dans l'année.}

\remarque{On peut manipuler, pour ces opérations, ainsi les matrices comme les nombres réels ou complexes. Par exemple,  l'équation $X+A=B$ d'inconnue la matrice $X$, admet comme unique solution $X=B-A$. De même, l'équation $2X=A$ d'inconnue la matrice $X$ admet comme unique solution $X=\frac{1}{2}A$.}
 ~\\~\\
        \subsubsection{Produit matriciel}
     
\definition{Soient $A=(a_{i,j})_{\substack{1\leq i \leq n\\1 \leq j \leq p}}$ une matrice $\mathcal{M}_{n,p}(\K)$ et $B=(b_{i,j})_{\substack{1\leq i \leq p\\1 \leq j \leq m}}$ une matrice $\mathcal{M}_{p,m}(\K)$.
On appelle \textbf{produit} de la matrice $A$ par la matrice $B$ la matrice de $\mathcal{M}_{n,m}(\K)$, notée $A\times B$ ou $AB$, définie par
\[AB=(c_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq m}} \textrm{ où } \forall~i\in \llbracket 1,n \rrbracket,~\forall~j\in \llbracket 1,m \rrbracket,~ c_{i,j}=a_{i,1}b_{1,j}+a_{i,2}b_{2,j}+\cdots +a_{i,p}b_{p,j}=\sum_{k=1}^p a_{i,k}b_{k,j}\]
}
%
%Par exemple :
\begin{center}
\input{tex/Chap9/pic/mult}
\end{center}

\remarque{~\begin{itemize}[label=\textbullet]
    \item \danger{~Pour multiplier deux matrices, il faut qu'elles soient compatibles : lorsque l'on calcule $AB$ il faut que le nombre de colonnes de $A$ soit égal au nombre de lignes de $B$.}
    \item \danger{~La multiplication des matrices n'est pas \textbf{commutative} : en général, $AB \neq BA$. Par exemple
\[\matrice{ 1&1\\ 0&0} \matrice{1 & 0 \\ 1 & 0} = \matrice{2 & 0 \\ 0 & 0} \textrm{ et } \matrice{ 1 & 0 \\ 1 & 0}\matrice{1&1\\ 0&0}=\matrice{1 & 1 \\ 1 & 1}\]}
    \item \danger{~Contrairement à ce qui se passe dans $\R$ ou $\C$, on peut avoir $AB=0$ sans pour autant que  $A$ et $B$ soient nuls. Par exemple
\[\matrice{1&0\\ 0&0}\matrice{0 & 0 \\ 1 & 1} = \matrice{0 & 0 \\ 0 & 0}\]
On dit que $\mathcal{M}_{n,p}(\K)$ n'est pas \textbf{intègre}.}
\end{itemize}
}

\exemple{Soient $\displaystyle{A=\matrice{1&0\\1&1\\-1&2}}$ et $\displaystyle{B=\matrice{1&1&0\\1&-1&1}}$. Alors 
\[AB=\matrice{ 1 & 1 & 0 \\ 2 & 0 & 1\\1 & -3 & 2} \textrm{ et } BA = \matrice{ 2 & 1 \\-1 & 1}\]
}

\propriete{Soient $A,B,C$ trois matrices (que l'on considère compatibles pour les multiplications envisagées), et $\lambda$ un élément de $\K$.
\begin{enumerate}[label=\textbullet]
    \item $A(BC)=(AB)C=ABC$ (associativité de la multiplication)
    \item $(\lambda A)B=A(\lambda B) = \lambda AB$
    \item $(A+B)C=AC+BC$ et $C(A+B)=CA+CB$ (distributivités)
\end{enumerate}
}

%% Ajouter interprétation colonne de AB en fonction de la colonne de B... cf programme

~\\
        \subsubsection{Transposition}
        
\definition{Soit $A=(a_{i,j})_{\substack{1\leq i \leq n\\1 \leq j \leq p}}$ une matrice $\mathcal{M}_{n,p}(\K)$.
On appelle \textbf{transposée} de la matrice $A$ la matrice de $\mathcal{M}_{p,n}(\K)$, notée $\,^tA$ , définie par
\[\,^tA=(c_{i,j})_{\substack{1\leq i \leq p\\1\leq j \leq n}} \textrm{ où } \forall~i\in \llbracket 1,p \rrbracket,~\forall~j\in \llbracket 1,n \rrbracket,~ c_{i,j}=a_{j,i}\]
Ainsi, la matrice $\,^t A$ est la matrice obtenue à partir de $A$ par symétrie, en échangeant les lignes et les colonnes.
}

\exemple{Si $A=\matrice{ 1&2\\-1&-3\\0&4}$, alors 
$\,^tA=\matrice{1 & -1 & 0 \\ 2 & -3 & 4}$.}

\propriete{Soient $A$ et $B$ deux matrices (que l'on considère compatibles pour les multiplications envisagées) et $\lambda$ un élément de $\K$.
\begin{itemize}[label=\textbullet]
    \item $\,^t (A+B) = \,^t A+\,^t B$ et $\,^t (\lambda A)=\lambda \,^t A$. 
    \item $\,^t (\,^t A)=A$ et $\,^t (AB)= \,^t B \,^t A$.
\end{itemize}
}

\section{Matrices carrées}

    \subsection{Définitions}

        \subsubsection{Matrices carrées}
        
\definition{Une \textbf{matrice carrée} d'ordre $n$ est une matrice à $n$ lignes et à $n$ colonnes. On notera $\mathcal{M}_n(\K)$ l'ensemble des matrices carrées d'ordre $n$, plutôt que $\mathcal{M}_{n,n}(\K)$. De même, on notera $0_n$ la matrice nulle de $\mathcal{M}_n(\K)$.}

\exemple{La matrice $\matrice{ 1&2\\-1&-3}$ est une matrice carrée d'ordre $2$.}

\definition{Si $A=(a_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq n}}$ est une matrice carrée, on appelle \textbf{diagonale} de $A$ les coefficients $(a_{i,i})_{1\leq i \leq n}$.}

\exemple{Dans l'exemple précédent, la diagonale est $(1, -3)$.}

        \subsubsection{Matrices diagonales et triangulaires}
        
\definition{~\begin{itemize}[label=\textbullet]
    \item Une \textbf{matrice diagonale} d'ordre $n$ est une matrice carrée d'ordre $n$ où tous les coefficients sont nuls sauf éventuellement ceux de la diagonale :
    \[ \matrice{ a_{1,1} & 0 & \hdots & 0 \\0 & \ddots & \ddots & \vdots\\\vdots & \ddots & \ddots & 0 \\ 0 & \hdots & 0 & a_{n,n} }\]
    \item La \textbf{matrice identité} de $\mathcal{M}_n(\K)$, notée $I_n$, est la matrice diagonale avec des $1$ sur la diagonale :
     \[I_n=\matrice{1 & 0 & \hdots & 0 \\0 & \ddots & \ddots & \vdots\\\vdots & \ddots & \ddots & 0 \\ 0 & \hdots & 0 & 1 }\]
     \item Une \textbf{matrice triangulaire supérieure} $(a_{i,j})$ est une matrice telle que 
     \[\forall~i\in \llbracket 1,n \rrbracket, \forall~j\in \llbracket 1,n \rrbracket, i>j \implies a_{i,j}=0\]
     Ainsi, elle est de la forme
     \[\matrice{
          a_{1,1} & a_{1,2} & \hdots & a_{1,n-1} & a_{1,n} \\
          0 & a_{2,2} & \hdots & \vdots & \vdots \\
          \vdots & 0 & \ddots & \vdots & \vdots \\
          \vdots & \vdots & \ddots & a_{n-1,n-1} & a_{n-1,n} \\
          0 & \hdots & 0 & 0 & a_{n,n}
      }\]
     \item Une \textbf{matrice triangulaire inférieure} $(a_{i,j})$ est une matrice telle que 
     \[\forall~i\in \llbracket 1,n \rrbracket, \forall~j\in \llbracket 1,n \rrbracket, i<j \implies a_{i,j}=0\]
     Ainsi, elle est de la forme
     \[\matrice{
          a_{1,1} & 0 & \hdots & 0 & 0 \\
          a_{2,1} & a_{2,2} & 0 & \hdots & \vdots \\
          \vdots & a_{3,2} & \ddots & \ddots & \vdots \\
          \vdots & \vdots & \ddots & a_{n-1,n-1} & 0\\
          a_{n,1} & \hdots & \hdots & a_{n,n-1} & a_{n,n}
      }\]
\end{itemize}
}

\remarque{La matrice identité $I_n$ est \textbf{neutre} pour la multiplication : quelle que soit la matrice $A$ de $\mathcal{M}_n(\R)$, on a $AI_n=I_nA=A$.}

            \subsubsection{Matrices symétriques et antisymétriques}
            
\definition{Soit $A=(a_{i,j})_{\substack{1\leq i \leq n\\1\leq j \leq n}}$ une matrice carrée. 
\begin{itemize}[label=\textbullet]
	\item $A$ est dite \textbf{symétrique} si 
\[\forall~(i,j)\in \llbracket 1,n\rrbracket^2,~a_{i,j}=a_{j,i}\]
Ainsi, une matrice est symétrique si et seulement si $\,^tA=A$.
	\item $A$ est dite \textbf{antisymétrique} si $\,^tA=-A$.
\end{itemize}
On note $\mathcal{S}_n(\K)$ l'ensemble des matrices d'ordre $n$ symétriques, et $\mathcal{A}_n(\K)$ l'ensemble des matrices d'ordre $n$ antisymétiques.
}

\exemple{La matrice $A=\matrice{ 1 & 2 \\ 2 & 3 }$ est symétrique.}             

    \subsection{Puissances d'une matrice carrée}
    
Si $A$ et $B$ sont deux matrices de $\mathcal{M}_n(\K)$, on peut alors calculer $AB$ et $BA$ (elles sont compatibles) et le produit est encore dans $\mathcal{M}_n(\K)$. On peut alors définir la puissance $n^{\textrm{ième}}$ d'une matrice.

\definition{Soit $A$ une matrice carrée d'ordre $n$. Soit $k$ un entier. On définit $A^k$ de la manière suivante :
\begin{itemize}[label=\textbullet]
    \item Si $k=0$, $A^0=I_n$.
    \item Si $k>0$, $A^k = \underbrace{A\times A \times \cdots \times A}_{k \textrm{ fois}}$.
\end{itemize}
}

\propriete{Par définition, pour tout matrice $A \in \mathcal{M}_n(\K)$, et pour tous entiers $p$ et $q$, $A^{p}\times A^q = A^{p+q}$.}

\remarque{Si $A$ et $B$ sont deux matrices carrées d'ordre $n$ diagonales, alors le produit $AB$ est facile à calculer; en effet, si $A=\displaystyle{\matrice{ a_{1,1} & 0 & \hdots & 0 \\0 & \ddots & \ddots & \vdots\\\vdots & \ddots & \ddots & 0 \\ 0 & \hdots & 0 & a_{n,n} }}$ et $B=\displaystyle{\matrice{ b_{1,1} & 0 & \hdots & 0 \\0 & \ddots & \ddots & \vdots\\\vdots & \ddots & \ddots & 0 \\ 0 & \hdots & 0 & b_{n,n} }}$, alors 
\[ AB = \matrice{ a_{1,1}\times b_{1,1} & 0 & \hdots & 0 \\0 & \ddots & \ddots & \vdots\\\vdots & \ddots & \ddots & 0 \\ 0 & \hdots & 0 & a_{n,n}\times b_{n,n} } \]
Ainsi, si $A$ est une matrice diagonale $A=\displaystyle{\matrice{ a_{1,1} & 0 & \hdots & 0 \\0 & \ddots & \ddots & \vdots\\\vdots & \ddots & \ddots & 0 \\ 0 & \hdots & 0 & a_{n,n} }}$, alors pour tout entier $p$, on a 
\[ A^p = \matrice{ a_{1,1}^p & 0 & \hdots & 0 \\0 & \ddots & \ddots & \vdots\\\vdots & \ddots & \ddots & 0 \\ 0 & \hdots & 0 & a_{n,n}^p } \]
}

\remarque{\danger{~Puisque la multiplication des matrices n'est pas commutative, on n'a pas $(AB)^k=A^kB^k$. En effet, $(AB)^k=(AB)(AB)\cdots (AB)$ et il faut que $AB=BA$ pour pouvoir obtenir $A^kB^k$.}}

\definition{Soient $A$ et $B$ deux matrices de $\mathcal{M}_n(\K)$. On dit que $A$ et $B$ \textbf{commutent} si $AB=BA$.}

\exemple{La matrice $I_n$ commutent avec toutes les matrices de $\mathcal{M}_n(\K)$. En effet, $AI_n=I_nA=A$.}

\begin{theoreme}[Formule du binôme de Newton] 
Soient deux matrices $A$ et $B$ de $\mathcal{M}_n(\K)$ qui \underline{commutent}. Alors, pour tout entier $n$, on a
\[(A+B)^n = \sum_{k=0}^n \binom{n}{k} A^k B^{n-k}\]	
\end{theoreme}


\begin{methode}
Pour calculer $A^p$, on peut parfois utiliser la formule du binôme de Newton, en décomposant $A$ sous la forme $I_n+B$ avec $B$ une matrice dont les puissances sont faciles à calculer.	
\end{methode}


\exemple{Soit $A=\matrice{1 & 0 & 1 \\ 0 & 1 & 0 \\ 0 & 0 & 1 }$. Calculer $A^n$ pour tout entier $n$.}

\begin{prof}
\solution{On constate que $A=I_3+ B$ avec \[B=\matrice{ 0 & 0 & 1 \\ 0 & 0 & 0 \\ 0 & 0 & 0 }\]
et $B^2=0$. Puisque $I_3$ et $B$ commutent (\textit{car $I_3$ commute avec toutes les matrices}), on en déduit que, pour tout entier $n\geq 2$ 
\[A^n = (I_3+B)^n = \sum_{k=0}^n \binom{n}{k} B^k I_3^{n-k} = \binom{n}{0} B^0 + \binom{n}{1} B^1 + \underbrace{\binom{n}{2} B^2 +\cdots + \binom{n}{n}B^n}_{=0}\] 
Ainsi, pour tout entier $n\geq 2$,
\[A^n = I_3 + nB = \matrice{ 1 & 0 & n \\ 0 & 1 & 0 \\ 0 & 0 & 1 }\] résultat qui est également vrai pour $n=0$ et $n=1$.
}
\end{prof}

\lignes{11}

\begin{methode}
Pour calculer $A^p$, on peut également essayer de calculer les premières puissances, puis en déduire le résultat par récurrence sur $p$.	
\end{methode}


\exemple{Soit $A=\matrice{1 & 1 \\0 & 1}$.  Calculer $A^n$ pour tout entier $n$.}

\begin{prof}
\solution{On constate que \[A^0=I_2~~~~A^1=\matrice{ 1 & 1 \\0 & 1 }~~~~A^2=\matrice{1 & 2 \\0 & 1}~~~~A^3=\matrice{1 & 3 \\0 & 1 }\]
Soit alors $P_n$ la proposition définie pour tout entier $n$ par \[P_n: A^n = \matrice{ 1 & n \\0 & 1}\]
que l'on démontre par récurrence sur $n$ :
\begin{itemize}[label=\textbullet]
    \item Initialisation : puisque $A^0=I_2 = \matrice{ 1 & 0 \\0 & 1 }$, $P_0$ est vraie.
    \item Hérédité : supposons que la proposition $P_n$ est vraie pour un certain entier $n$ fixé. Montrons alors $P_{n+1}$ :
    \[A^{n+1} = A^n \times A \underbrace{=}_{\textrm{par H.R.}} \matrice{1 & n \\0 & 1 } \matrice{ 1 & 1 \\0 & 1} = \matrice{1 & n+1\\0 & 1}\]
    La proposition $P_{n+1}$ est donc vraie.
\end{itemize}
On a ainsi démontré par récurrence que 
\[\forall~n\geq 0,~~A^n=\matrice{1 & n \\0 & 1}\]
}
\end{prof}

\lignes{11}

\section{Matrices inversibles}

    \subsection{Définition}
   
\definition{Soit $A$ une matrice carrée d'ordre $n$. On dit que $A$ est \textbf{inversible} s'il existe une matrice $B$ de $\mathcal{M}_n(\K)$ vérifiant \[AB=BA=I_n\]
Dans ce cas, $B$ est appelée \textbf{matrice inverse} de $A$, et est notée $B=A^{-1}$.}   
 
 \notation{On note $GL_n(\K)$ l'ensemble des matrices carrée d'ordre $n$ inversibles à coefficients dans $\K$.}   
  
  \exemple{La matrice $\matrice{ 1 & 1 \\ 2 & 3}$ est inversible. En effet,
    \[\matrice{ 1 & 1 \\ 2 & 3 } \matrice{ 3 & -1 \\ -2 & 1 }=
    \matrice{ 3 & -1 \\ -2 & 1 }
    \matrice{ 1 & 1 \\ 2 & 3 }
    =I_2\]}
    
 \remarque{La matrice $I_n$ est inversible et $I_n^{-1}=I_n$. En effet, $I_nI_n=I_nI_n=I_n$.}   
    
    \subsection{Propriétés}
    
\propriete{Soient $A$ et $B$ deux matrices carrées de $\mathcal{M}_n(\K)$.  
    \begin{itemize}[label=\textbullet]
        \item Si $A\in GL_n(\K)$, alors $A^{-1}\in GL_n(\K)$ et $(A^{-1})^{-1}=A$.
        \item Si $A$ et $B$ sont inversibles, alors $AB$ est également inversible, et on a $(AB)^{-1}=B^{-1}A^{-1}$.
    \end{itemize}
}   

\begin{prof}
\preuve{Pour le premier point, on a en effet $A^{-1}A=AA^{-1}=I_n$ donc $A^{-1}$ est inversible et son inverse est $A$.\\
Pour le second point, on a $AB(B^{-1}A^{-1})=AI_nA^{-1}=AA^{-1}=I_n$ et $(B^{-1}A^{-1})AB=B^{-1}I_nB=B^{-1}B=I_n$. Donc $AB$ est inversible et son inverse est $B^{-1}A^{-1}$.}
\end{prof}

\lignes{6}

Pour démontrer qu'une matrice n'est pas inversible, on peut utiliser le théorème suivant :

\begin{theoreme}
Soit $A \in \mathcal{M}_n(\K)$ une matrice non nulle. S'il existe une matrice $B\in \mathcal{M}_n(\K)$ non nulle telle que $AB=0_n$ alors $A$ n'est pas inversible.	
\end{theoreme}


\begin{prof}
\preuve{Faisons un raisonnement par l'absurde, et supposons que $A$ soit inversible, d'inverse $A^{-1}$. Alors
\[AB=0_n \Rightarrow A^{-1}(AB)=O_n \Rightarrow B=0_n\]
ce qui est absurde, puisque $B$ n'est pas nulle.}  
\end{prof}

\lignes{4}

\remarque{Si $A$ et $B$ sont toutes les deux non nulles, telles que $AB=0_n$ alors ni $A$ ni $B$ ne sont inversibles.}
    
    \subsection{Règles de calcul}
    
\propriete{Soient $A$ et $B$ deux matrices de $\mathcal{M}_n(\K)$, et $C \in GL_n(\K)$. Alors 
\[AC=B \Leftrightarrow A=BC^{-1}~~~~~~~~CA=B \Leftrightarrow A=C^{-1}B\]
\[AC=BC \Leftrightarrow A=B~~~~~~~~CA=CB \Leftrightarrow A=B\]}

\remarque{\danger{~Cela n'est valable que si $C$ est inversible ! Ce n'est pas forcément vrai si $C$ n'est pas inversible. Par exemple, si $A=\matrice{1 & 3 \\ -1 & 1 }$, $B=\matrice{ 1 & -4 \\ -1 & 2 }$, et $C=\matrice{ 1 & 1 \\ 0 & 0 }$, on a $AC=BC$ et pourtant $A\neq B$.}}

\begin{theoreme}
~\begin{itemize}[label=\textbullet]
    \item Soit $A \in M_n(\K)$. S'il existe une matrice $B\in M_n(\K)$ telle que $AB=I_n$, alors $A$ est inversible, d'inverse $B$.
    \item Soit $A \in M_n(\K)$. S'il existe une matrice $B\in M_n(\K)$ telle que $BA=I_n$, alors $A$ est inversible, d'inverse $B$.
\end{itemize}	
\end{theoreme}


Ainsi, il n'est pas nécessaire de vérifier $AB=I_n$ ET $BA=I_n$. Seul un des sens est nécessaire.

\begin{methode}
Pour montrer qu'une matrice $A$ est inversible, on peut chercher une matrice $B$ telle que $AB=I_n$. On pourra conclure que $A$ est inversible, et que $A^{-1}=B$.	
\end{methode}


\exemple{Soit $A=\matrice{ 1&1\\0&1}$. On note également $B=\matrice{ 0 & -1\\0 & 0}$. 
\begin{enumerate}
    \item Calculer $A(I_2+B)$.
    \item En déduire que $A$ est inversible, et calculer $A^{-1}$.
\end{enumerate}
}

\begin{prof}
\solution{~
\begin{enumerate}
    \item On constate que \[A(I_2+B)=\matrice{1&1\\0&1}\matrice{1&-1\\0&1}=\matrice{1&0\\0&1}=I_2\]
    \item D'après ce qui précède, $A$ est inversible, et $A^{-1}=I_2+B=\matrice{1&-1\\0&1}$.
\end{enumerate}
}
\end{prof}

\lignes{4}

    \subsection{Ensemble $GL_2(\R)$}
    
\begin{theoreme}
Soit $A=\matrice{ a & b \\ c & d} \in \mathcal{M}_2(\K)$. Alors $A$ est inversible si et seulement si $ad-bc \neq 0$. On a alors
\[A^{-1}=\frac{1}{ad-bc}\matrice{ d & -b \\ -c & a}\]
Le réel $ad-bc$ est appelé \textbf{déterminant} de la matrice $A$ et est noté $\det(A)$.	
\end{theoreme}


\begin{prof}
\preuve{Notons $A=\matrice{ a&b\\c&d}$ et $B=\matrice{d & -b \\ -c & a }$. On suppose que $A\neq 0$ et donc $B\neq 0$. Alors
\[AB=\matrice{ ad-bc & 0 \\ 0 & ad-bc} = (ad-bc)I_2\]
\begin{itemize}[label=\textbullet]
  \item Si $ad-bc=0$, alors $AB=0_2$. Puisque $B\neq 0$, d'après un résultat précédent, $A$ ne peut pas être inversible.
  \item Si $ad-bc\neq 0$, alors $A \times \left( \frac{1}{ad-bc} B \right) = I_2$. D'après un résultat précédent, $A$ est donc inversible, et 
  		\[A^{-1}=\frac{1}{ad-bc}B = \frac{1}{ad-bc} \matrice{ d & -b\\-c&a}\]
\end{itemize}
}
\end{prof}

\lignes{9}


\remarque{Nous verrons plus tard dans l'année la notion de déterminant de manière plus générale.}

\section{Systèmes linéaires et matrices}

    \subsection{Ecriture matricielle d'un système linéaire}

\exemple{On s'intéresse au système 
\[(S) \left \{ \begin{array}{ccccccc}
  2x & - & 3y & + & z &=&1\\
  x & + & y & - & 2z &=& 2\\
  -x & - & 2y & + & z & =& -1
 \end{array}
\right.\]

Notons alors $A=\matrice{ 2 & -3 & 1 \\ 1 & 1 & -2 \\ -1 & -2 & 1}$, $X=\matrice{ x\\y\\z}$ et $Y=\matrice{1 \\ 2 \\ -1 }$.
On a alors
\[(S) \Leftrightarrow AX=Y\]

La matrice $A$ est appelée \textbf{matrice associée} au système $(S)$. Résoudre le système $(S)$, c'est donc trouver le vecteur colonne $X$.}

\definition{Soit $(S)$ un système $n\times p$ de la forme
\[\left \{ \begin{array}{ccccccccc} a_{11}x_1&+&a_{12}x_2&+&\cdots &+& a_{1p}x_p &=& b_1 \\ \vdots & & \vdots & &\vdots & & \vdots & & \vdots\\a_{n1}x_1&+&a_{n2}x_2&+&\cdots &+& a_{np}x_p&=&b_n\end{array}\right.\]
On appelle \textbf{matrice associée} à $(S)$ la matrice 
\[A=\matrice{ a_{11} & a_{12} & \cdots & a_{1p}\\\vdots & \vdots & \cdots & \vdots\\a_{n1}&a_{n2}&\cdots &a_{np} }\]
Le système $(S)$ s'écrit alors $AX=Y$, avec $X=\matrice{x_1\\\vdots\\x_n}$ et $Y=\matrice{b_1\\\vdots\\b_n}$.}

    \subsection{Inverse d'une matrice et système}
    
\begin{theoreme}
Soit $A \in \mathcal{M}_n(\K)$. Soit $B \in \mathcal{M}_{n,1}(\K)$ une matrice colonne. Le système $(S)~~AX=B$ admet une unique solution si, et seulement si, la matrice $A$ est inversible.\\Dans ce cas, $X=A^{-1}B$.	
\end{theoreme}


\remarque{Ainsi, pour résoudre un système $(S)$, on peut introduire la matrice associée et résoudre une équation matricielle $AX=B$. Cela permet en général de simplifier les notations.}

\consequence{Une matrice triangulaire supérieure $A$ est inversible si et seulement si tous les termes de la diagonale sont non nuls.}

\begin{prof}
\preuve{En effet, un système triangulaire est de Cramer si et seulement si tous ses pivots sont non nuls.}
\end{prof}

\lignes{3}

\begin{methode}
Pour montrer qu'une matrice $A$ est, ou n'est pas inversible, sans calculer son inverse, on résout matriciellement l'équation $AX=0$ en appliquant la méthode du pivot de Gauss. Si on obtient une diagonale sans terme nul, la matrice sera inversible. On peut simplifier les écritures en écrivant $(A | 0)$ pour ne pas s'encombrer des inconnues.	
\end{methode}


\exemple{Montrer que la matrice
\[ 
    \matrice{
        1 & 2 & 0 \\
        2 & 1 & 0 \\
        0 & 1 & 0
    }
\]
n'est pas inversible. 
Montrer que la matrice \[A= 
	\matrice{
        1 & 7 & 2 \\
        2 & 4 & 1 \\
        -1 & 1 & 0
	}
\]
est inversible.
}

\begin{prof}
\solution{On résout :
\[\left(\begin{array}{ccc|c}1 & 2 & 0 & 0\\2 & 1 & 0 & 0\\0 & 1 & 0 & 0 \end{array}\right)~\sim
\left(\begin{array}{ccc|c}1 & 2 & 0 & 0\\0 & -3 & 0 & 0\\0 & 1 & 0 & 0 \end{array}\right) \begin{array}{c}\textrm{ligne pivot} \\ L_2\leftarrow L_2-2L_1\\~\end{array}~\sim
\left(\begin{array}{ccc|c}1 & 2 & 0 & 0\\0 & -3 & 0 & 0\\0 & 0 & 0 & 0 \end{array}\right) \begin{array}{c}\\~\textrm{ligne pivot} \\ L_3\leftarrow 3L_3+L_2\end{array} \]
Puisqu'un des termes sur la diagonale est nul, la matrice n'est pas inversible.\\Pour $A$ :
\[\left( 
    \begin{array}{ccc|c}
        1 & 7 & 2 & 0 \\
        2 & 4 & 1 & 0\\
        -1 & 1 & 0 & 0
    \end{array}
\right) \sim 
\left( 
    \begin{array}{ccc|c}
        1 & 7 & 2 & 0 \\
        0 & -10 & -3  & 0\\
        0 & 8 & 2 & 0
    \end{array}
\right) \sim
\left( 
    \begin{array}{ccc|c}
        1 & 7 & 2 & 0 \\
        0 & -10 & -3  & 0\\
        0 & 0 & -4 & 0
    \end{array}
\right) 
\]
Les termes sur la diagonale étant non nuls, la matrice $A$ est bien inversible.
}
\end{prof}

\lignes{16}
    
\begin{methode}
Pour déterminer l'inverse d'une matrice, on peut utiliser la méthode du pivot de Gauss, mais en simplifiant les écritures. On écrit $(M | I_n)$ et on cherche à remplacer, par des opérations sur les lignes, $M$ par $I_n$. A la place du $I_n$ de départ, on aura alors $M^{-1}$.	
\end{methode}


\exemple{Déterminer l'inverse de 
\[\matrice{
     -3 & 5 & 6 \\
     -1 & 2 & 2 \\
     1 & -1 & -1
} \]
}

\begin{prof}
\solution{On a : 
\[\left( \begin{array}{ccc|ccc}
     -3 & 5 & 6 & 1 & 0 & 0\\
     -1 & 2 & 2 & 0 & 1 & 0\\
     1 & -1 & -1 & 0 & 0 & 1
 \end{array}
\right) \sim
\left( \begin{array}{ccc|ccc}
     1 & -1 & -1 & 0 & 0 & 1\\
     -1 & 2 & 2 & 0 & 1 & 0\\
     -3 & 5 & 6 & 1 & 0 & 0
 \end{array}
\right) L_1 \leftrightarrow L_3 
\] 
\[
\sim\left( \begin{array}{ccc|ccc}
     1 & -1 & -1 & 0 & 0 & 1\\
     0  & 1 & 1 & 0 & 1 & 1\\
     0  & 2 & 3 & 1 & 0 & 3
 \end{array}\right)
  \begin{array}{c}~\\ L_2 \leftarrow L_2+L_1\\ L_3\leftarrow L_3+3L_1\end{array}
\]
\[
\sim\left( \begin{array}{ccc|ccc}
     1 & -1 & -1 & 0 & 0 & 1\\
     0  & 1 & 1 & 0 & 1 & 1\\
     0  & 0 & 1 & 1 & -2 & 1
 \end{array}\right)
  \begin{array}{c}~\\ ~\\ L_3\leftarrow L_3-2L_2\end{array}
\]
\[
\sim\left( \begin{array}{ccc|ccc}
     1 & -1 & -1 & 0 & 0 & 1\\
     0  & 1 & 0 & -1 & 3 & 0\\
     0  & 0 & 1 & 1 & -2 & 1
 \end{array}\right)
  \begin{array}{c}~\\ L_2\leftarrow L_2-L_3\\ ~\end{array}
\]
\[
\sim\left( \begin{array}{ccc|ccc}
     1 & 0 & 0 & 0 & 1 & 2\\
     0  & 1 & 0 & -1 & 3 & 0\\
     0  & 0 & 1 & 1 & -2 & 1
 \end{array}\right)
  \begin{array}{c}L_1\leftarrow L_1+L_2+L_3\\ ~\\ ~\end{array}
\]
Ainsi, $\displaystyle{
  \matrice{
     -3 & 5 & 6 \\
     -1 & 2 & 2 \\
     1 & -1 & -1
}}$ est inversible, et son inverse est 
\[\matrice{
  0 & 1 & 2\\
  -1 & 3 & 0\\
  1 & -2 & 1
}\]

}
\end{prof}    

\lignes{16}

\begin{histoire}
Cette méthode de détermination de l'inverse d'une matrice est appelée \textbf{Réduction de Gauss-Jordan}, en hommage à  \textit{Carl Friedrich Gauss} et \textit{Wilhelm Jordan}, mais était connue des Chinois au $1^{\textrm{er}}$ siècle de notre ère, sous le nom \textit{Fang cheng} dans \textit{Les Neuf Chapitres sur l'art mathématique}.	
\end{histoire}


\definition{On appelle \textbf{rang} d'une matrice $A$, et on note $\rg(A)$ le rang du système associée $AX=0$, où $X$ représente le vecteur colonne des inconnues.}

\propriete{On dispose des propriétés suivantes :
	\begin{itemize}[label=\textbullet]
		\item $\rg(A)=0$ si et seulement si la la matrice est nulle.
		\item $\rg(A)=1$ si et seulement si toutes les colonnes de $A$ sont colinéaires.
		\item Si la matrice $A$ est carrée d'ordre $n$, $\rg(A)=n$ si et seulement si la matrice est inversible.
	\end{itemize}
}
