// Auteur : Crouzet
// Date : 21/06/2015
// Résumé : programme déterminant le premier entier n tel que  
// un > a, où a est un réel donné et u une suite de limite +oo

n=0;
u=1;
// Valeur de seuil
seuil=100;

while(u<=seuil)
  // On n'a pas dépassé le seuil.
  // On passe au rang suivant
  n=n+1;
  // On calcule le terme suivant de la suite
  u=1+u^2;
end

// Ainsi, n contient le premier rang à partir duquel u_n > seuil
