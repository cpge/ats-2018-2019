\objectif{Dans ce chapitre, on introduit un concept important quant à ses applications (sur les équations différentielles, par exemple). Cet outil permettra de résoudre des équations qu'on ne peut pas résoudre autrement.}

\textit{La liste ci-dessous représente les éléments à maitriser absolument. Pour cela, il faut savoir refaire les exemples et exercices du cours, ainsi que ceux de la feuille de TD.}

\begin{numerote}
	\item Sur les généralités  :
			\begin{itemize}[label=\textbullet]
				\item \hyperref[objectif-24-1]{savoir déterminer le rayon de convergence via borne ou limite}\dotfill $\Box$
				\item \hyperref[objectif-24-2]{savoir déterminer le rayon de convergence avec le critère de d'Alembert}\dotfill $\Box$
				\item \hyperref[objectif-24-3]{connaître les propriétés des opérations sur les séries entières}\dotfill $\Box$
			\end{itemize}
	\item Concernant les propriétés des séries entières :
			\begin{itemize}[label=\textbullet]
				\item \hyperref[objectif-24-4]{savoir déterminer la série dérivée ou primitive d'une série entière}\dotfill $\Box$
				\item \hyperref[objectif-24-5]{connaître les séries de référence}\dotfill $\Box$
			\end{itemize}
	\item \hyperref[objectif-24-6]{Savoir trouver une solution d'une équation différentielle sous forme d'une série entière}\dotfill $\Box$
	\item \hyperref[objectif-24-7]{Savoir déterminer le développement en série entière d'une fonction non usuelle}\dotfill $\Box$
	\item \hyperref[objectif-24-8]{Savoir calculer la somme d'une série à l'aide d'une série entière}\dotfill $\Box$
\end{numerote}

\newpage 

%%
%%  Vérifier exercice équa diff
%% Faire exercices manquants

Dans l'ensemble de ce chapitre, $\K$ désigne $\R$ ou $\C$.
\section{Série entière}

	\subsection{Définition}

\definition{On appelle \textbf{série entière} à coefficients dans $\K$ la donnée d'une suite $(a_n) \in \K^{\N}$, que l'on note $\ds{\sum_{n\geq 0} a_nX^n}$.}

\notation{L'ensemble des séries entières à coefficients dans $\K$ est noté $\mathbb{K}[[X]]$.}

\exemple{En prenant pour tout $n\geq 1$, $a_n=\frac{1}{n}$, la série $\ds{\sum_{n\geq 1} \frac{1}{n}X^n}$ est une série entière.}
	
\remarque{Une série entière est une notation formelle. Il n'y a aucune raison pour que la série converge en un élément $x\in K$ donné.

En revanche, toute série entière converge pour $x=0$.

Par abus de notation, on écrira régulièrement $\ds{\sum_{n\geq 0} a_nx^n}$ voire $\ds{\sum a_nx^n}$ au lieu de $\ds{\sum_{n\geq 0} a_nX^n}$.}

\definition{Soit $\ds{\sum_{n\geq 0} a_nX^n}$ une série entière, et $\mathcal{D}$ l'ensemble des éléments de $\K$ pour lesquels la série converge. Alors la fonction $f:\mathcal{D}\rightarrow \K$, définie par $\ds{x\mapsto \sum_{n=0}^{+\infty} a_nx^n}$ est appelée \textbf{somme} de la série entière, de somme partielle $\ds{\left(x\mapsto \sum_{n=0}^N a_nx^n\right)_N}$.}

	\subsection{Rayon de convergence}

\remarque{On peut se demander sur quel domaine la série entière va converger. On va introduire pour cela la notion de rayon de convergence.} 	

\lemme{[Lemme d'Abel] Soit $\ds{\sum_{n\geq 0} a_nX^n}$ une série entière à coefficients dans $\K$. Soit un réel $r>0$ pour lequel la suite $(a_nr^n)$ est bornée. Alors, pour tout nombre $z$ vérifiant $|z|<r$, la série $\ds{\sum_{n\geq 0} a_nz^n}$ converge absolument.} 
	
\preuve{On suppose donc que la suite $(a_nr^n)$ est bornée : on a alors, pour tout entier $n$, $|a_nr^n|<M_0$ où $M_0\in \R$. Mais alors, remarquons que pour tout entier $n$ on a
\begin{eqnarray*}
	|a_nz^n| &=& \left|a_nr^n \frac{z^n}{r^n}\right |\\
			 &=&   |a_nr^n| \left | \frac{z}{r}\right|^n \\
			 &\leq&  M_0 \left | \frac{z}{r}\right|^n
\end{eqnarray*} 	
Si $|z|<r$, alors la série $\ds{\sum_{n\geq 0} \left| \frac{z}{r}\right|^n}$ converge (série géométrique). Par comparaison de séries à termes positifs, la série $\ds{\sum_{n\geq 0} a_nz^n}$ converge absolument.
}


\exemple{Soit la série entière $\ds{\sum_{n\geq 1} \frac{1}{n}x^n}$. Alors, on remarque que pour $x=1$, la suite $(\frac{1}{n})$ est bornée. D'après le lemme d'Abel, la série $\ds{\sum_{n\geq 1} \frac{x^n}{n}}$ converge pour tout $x\in ]-1;1[$.}
	
\definition{Soit $\ds{\sum_{n\geq 0} a_nX^n}$ une série entière. Alors on appelle \textbf{rayon de convergence} de la série la borne supérieure de l'ensemble non vide $\left \{ r \in \R^{+},~(a_nr^n) \text{ est bornée} \right \}$.}

\remarque{Cette borne supérieure peut être nulle (si la suite n'est bornée que pour $r=0$), mais également infinie (si pour tout réel positif $r$, $(a_nr^n)$ est bornée).}

Le lemme d'Abel permet d'en déduire le résultat important suivant :

\begin{proposition}
Soit $\ds{\sum_{n\geq 0} a_nX^n}$ une série entière et  $R\in \R^+\cup \{\infty\}$ son rayon de convergence, qu'on suppose non nul.
\begin{itemize}[label=\textbullet]
	\item Si $|x|<R$, la série $\ds{\sum_{n\geq 0} a_nx^n}$ converge.
	\item Si $|x|>R$, la série diverge grossièrement.
	\item Si $|x|=R$, la série peut converger ou diverger (cas limite)
\end{itemize}	
\end{proposition}


Ainsi, connaissant le rayon de convergence, on sait globalement où la série converge.

\definition{Soit $\ds{\sum_{n\geq 0} a_nX^n}$ une série entière  à coefficients complexes et  $R\in \R^+\cup \{\infty\}$ son rayon de convergence. L'ensemble $\ds{B_R=\left \{z \in \K,\quad |z|<R \right \}}$ est appelé \textbf{disque de convergence}.
\begin{center}
	\input{tex/Chap24/pic/disque}
\end{center}
Si la série est à coefficients réels, le disque de convergence est l'intervalle $]-R;R[$. 
}

Sur l'ensemble des $z$ vérifiant $|z|=R$, la série peut converger ou diverger. On appelle cet ensemble le \textbf{cercle d'incertitude}.

\begin{methode}
\label{objectif-24-1}Pour déterminer le rayon de convergence $R$ de la série entière $\ds{\sum_{n\geq 0} a_nz^n}$. 
\begin{itemize}[label=\textbullet]
	\item On cherche un réel $r$ pour lequel la suite $(a_nr^n)$ est bornée. Ainsi, le rayon de convergence $R\geq r$.
	\item On cherche un réel $r$ pour lequel la série $\ds{\sum_{n\geq 0} a_nr^n}$ converge absolument. Dans ce cas, $R\geq r$.
	\item On cherche un réel $r$ pour lequel la suite $(|a_nr^n|)$ diverge ou n'est pas bornée. Dans ce cas, $R\leq r$.
\end{itemize}	
\end{methode}


\exercice{Soient les trois séries entières suivantes définies sur $\R$ : 
\begin{numerote}
	\item $\ds{\sum_{n=1}^{+\infty} nx^n}$
	\item $\ds{\sum_{n=1}^{+\infty} \frac{x^n}{n^2}}$
	\item $\ds{\sum_{n=1}^{+\infty} 	\frac{x^n}{n}}$.
\end{numerote}
Déterminer le rayon de convergence de chacune des séries, ainsi que le comportement aux bornes de l'intervalle de convergence.}

\begin{prof}
\solution{Pour les séries $2$ et $3$, on constate que pour $x=1$, les suites $\left(\frac{1}{n}\right)_{n\geq 1}$ et $\left(\frac{1}{n^2}\right)_{n\geq 1}$ sont bornées. Donc leur rayon de convergence $R$ est supérieur ou égal à $1$. Mais pour $r>1$, les suites $\left(\frac{r^n}{n}\right)_{n\geq 1}$ et $\left(\frac{r^n}{n^2}\right)_{n\geq 1}$ divergent vers $+\infty$ (croissances comparées). Donc $R\leq 1$ et donc $R=1$.

Pour la série $1$, remarquons que pour $x=1$, la suite $(n)$ diverge, donc le rayon de convergence $R\leq 1$. Pour $r\in ]0,1[$, la suite $\ds{(nr^n)}$ converge (par croissances comparées) donc est bornée. Ainsi $R=1$.

Concernant les bornes :
\begin{itemize}[label=\textbullet]
	\item Pour la série $1$, elle diverge en $x=1$ et $x=-1$ car les suites $(n)$ et $((-1)^n n)$ ne tendent pas vers $0$. Ainsi, la série ne converge que sur $]-1;1[$.
	\item Pour la série $2$, la série est absolument convergente pour $x=1$ et $x=-1$ (série de Riemann). Ainsi, la série converge sur $[-1;1]$.
	\item Pour la série $3$, elle ne converge pas pour $x=1$ (série harmonique) mais elle converge pour $x=-1$ (série alternée). Ainsi, celle-ci converge sur $[-1;1[$.
\end{itemize}
}	
\end{prof}

\lignes{30}

\remarque{L'exercice précédent permet de voir qu'au niveau des bornes, il peut tout arriver.}

	\subsection{Critère de d'Alembert}

Dans le cas des séries générales, il y a un critère permettant de déterminer si une série est convergente ou non, dans le cas où son terme général ne s'annule jamais.

\begin{proposition}[Règle de d'Alembert] 
Soit $\ds{\sum_{n\geq 0} a_n}$ une série à \textbf{termes strictement positifs}. On suppose que la limite 
\[ \ell=\lim_{n\rightarrow +\infty} \frac{a_{n+1}}{a_n} \]
existe.
\begin{enumerate}
	\item Si $\ell<1$, la série $\ds{\sum_{n\geq 0} a_n}$ converge.
	\item Si $\ell>1$, la série $\ds{\sum_{n\geq 0} a_n}$ diverge.
	\item Si $\ell=1$, on ne peut pas conclure
\end{enumerate}	
\end{proposition}

\exemple{La série $\ds{\sum_{n\geq 0} \frac{1}{n!}}$ converge. En effet, on a
\[ \lim_{n\rightarrow +\infty} \frac{\frac{1}{(n+1)!}}{\frac{1}{n!}}=\frac{1}{n+1} \underset{n\rightarrow +\infty}{\longrightarrow} 0 \]}

On peut appliquer cette règle au cas des séries entières.

\begin{methode}
\label{objectif-24-2} Soit $\ds{\sum_{n\geq 0} a_nX^n}$ une série entière  à coefficients complexes. On pose $u_n=a_nz^n$ pour $z\in \K$. Si la limite 
\[ \ell(z)=\lim_{n \rightarrow +\infty} \left | \frac{u_{n+1}}{u_n}\right| \]
existe, alors la série converge si $\ell(z)<1$ et diverge si $\ell(z)>1$.	
\end{methode}


\exemple{On reprend la série $\ds{\sum_{n\geq 1} \frac{x^n}{n}}$. On constate que, pour $n\geq 1$ 
\[ \frac{ \frac{x^{n+1}}{n+1}}{\frac{x^n}{n}} = x \frac{n+1}{n} \underset{n\rightarrow +\infty}{\longrightarrow} x \]
Ainsi, si $|x|<1$, la série converge, et si $|x|>1$, la série diverge. On obtient bien que le rayon de convergence vaut $1$.}

\exercice{Déterminer le rayon de convergence sur $\C$ de $\ds{\sum_{n\geq 0} a^nz^n}$ (avec $a\in \C^*$) et de $\ds{\sum_{n\geq 0} \frac{z^n}{n!}}$.}

\begin{prof}
\solution{On constate que 
\[ \module{\frac{ a^{n+1}z^{n+1}}{a^nz^n}} = \module{az} \]
Ainsi, si $\module{az}<1$, $\ds{\sum_{n\geq 0} a^nz^n}$ converge, et si $|az|>1$, $\ds{\sum_{n\geq 0} a^nz^n}$ diverge. Le rayon de convergence est donc $R=\frac{1}{|a|}$.

De même, après calcul, on a
\[ \module{\frac{ \frac{z^{n+1}}{(n+1)!}}{\frac{z^n}{n!}}} = \frac{\module{z}}{n+1}\underset{n\rightarrow +\infty}{\longrightarrow} 0 \]
La série $\ds{\sum_{n\geq 0} \frac{z^n}{n!}}$ converge donc pour tout $z\in \C$, et donc $R=\infty$.
}	
\end{prof}

\lignes{10}
\lignes{15}

	\label{objectif-24-3}\subsection{Séries entières et opérations}

L'ensemble des séries entières $\K[[X]]$ est un espace vectoriel (puisqu'il ne s'agit que d'une ré-écriture de l'ensemble des suites). On dispose alors du résultat suivant quant au rayon de convergence.

\begin{proposition}
Soient $\ds{\sum a_nx^n}$ et $\ds{\sum b_nx^n}$ deux séries entières de $\R[[X]]$, de rayon de convergence respectifs $R_a$ et $R_b$. Soit $\lambda$ un réel. Alors
\begin{itemize}[label=\textbullet]
	\item la série $\ds{\sum (a_n+b_n)x^n}$ a un rayon de convergence $R\geq \min(R_a,R_b)$, avec égalité si $R_a\neq R_b$.
	\item la série $\ds{\sum \lambda a_nx^n}$ est de rayon de convergence $R=R_a$ (sauf si $\lambda=0$).
\end{itemize}		
\end{proposition}

\exercice{Déterminer le rayon de convergence de $\ds{\sum \cosh(n)x^n}$.}

\begin{prof}
\solution{On sait que $\cosh(n)=\frac{\E^n+\eu{-n}}{2}$. Or 
\begin{itemize}[label=\textbullet]
	\item le rayon de convergence de la série $\ds{\sum \E^nx^n}$ est de $R_1=\frac{1}{e}$.
	\item le rayon de convergence de la série $\ds{\sum \eu{-n}x^n}$ est de $R_2=\frac{1}{\eu{-1}}=\E$.
\end{itemize}
Les rayons de convergence étant différents, on en déduit que le rayon de convergence de  $\ds{\sum \cosh(n)x^n}$ est $R=\min(R_1,R_2)=\frac{1}{\E}$.
}	
\end{prof}

\lignes{20}

	
\section{Propriétés d'une série entière}

	\subsection{Continuité}

Les séries entières réelles sont très régulières sur leur intervalle de convergence : elles y sont continues.

\begin{proposition}[Continuité]
Soit $\ds{\sum_{n\geq 0} a_nX^n}$ une série entière  à coefficients réels, et $R$ son rayon de convergence. Alors la somme de cette série $S\colon]-R;R[\rightarrow \R$ définie par $\ds{S\colon x\mapsto \sum_{n=0}^{+\infty} a_nx^n}$ est continue sur $]-R;R[$.	
\end{proposition}

\preuve{On va utiliser la définition de continuité. Soit $a\in ]-r;r[$. On note $S_N:]-R;R[\rightarrow \R$ définie par $\ds{S_N:x\mapsto \sum_{n=0}^N a_nx^n}$. D'après l'inégalité triangulaire, on a pour $x\in ]-R;R[$ :
\[ |S(x)-S(a)| \leq |S(x)-S_N(x)| + |S_N(x)-S_N(a)|+|S_N(a)-S(a) | \]
\begin{enumerate}
	\item Le terme $|S(x)-S_N(x)|$ tend vers $0$ quand $N$ tend vers $+\infty$ (puisque la série $\ds{\sum_{n\geq 0} a_nx^n}$ converge).
	\item De même, le terme $|S_N(a)-S(a)|$ tend vers $0$ quand $N$ tend vers $+\infty$.
	\item A $N$ fixé, le terme $|S_N(x)-S_N(a)|$ tend vers $0$ quand $x$ tend vers $a$, car la fonction $S_N$ est un polynôme (car une somme finie).
\end{enumerate}
Ainsi, si on se donne $\eps>0$, il existe un rang $N_0$ tel que $|S(x)-S_N(x)|\leq \eps$ et $|S_N(a)-S(a)|\leq \eps$ pour $N\geq N_0$; et un réel $\delta$ tel que $|S_{N_0}(x)-S_{N_0}(a)|\leq \eps$ pour $x\in ]a-\delta; a+\delta[$. Mais alors, pour $N\geq N_0$ et $x\in ]a-\delta; a+\delta[$, on a alors
\[ |S(x)-S(a)| \leq 3\eps \]
Par définition, $S(x)$ tend bien vers $S(a)$ et $S$ est continue en $a$.
}

\remarque{[Cas au bord] Si le rayon de la série entière est $R$, et que la série entière converge pour $x=R$ (respectivement $x=-R$), alors la somme sera également continue en $R$ (resp. en $-R$). Ainsi, si la série entière converge en $R$ et en $-R$, la somme est continue sur $[-R;R]$.}

	\subsection{Dérivation, intégration}
	
	
Mieux, on peut dériver facilement une série entière :

\begin{proposition}[Dérivation terme à terme] \label{objectif-24-4}
Soit $\ds{\sum_{n\geq 0} a_nX^n}$ une série entière  à coefficients réels, et $R$ son rayon de convergence. Alors la somme de cette série $S\colon]-R;R[\rightarrow \R$ définie par $\ds{S\colon x\mapsto \sum_{n=0}^{+\infty} a_nx^n}$ est de classe $\CC^\infty$ sur $]-R;R[$, et on a
	\[ \forall~x\in]-R;R[,\quad S'(x)= \sum_{n=1}^{+\infty} na_nx^{n-1} \]
	et la série entière $S'$ a également $R$ comme rayon de convergence.	
\end{proposition}



On peut également intégrer terme à terme une série entière, sans changer le rayon de convergence.

\begin{proposition}[Intégration terme à terme] 
Soit $\ds{\sum_{n\geq 0} a_nx^n}$ une série entière  à coefficients réels, et $R$ son rayon de convergence. Alors la série entière $\ds{\sum_{n=0}^{+\infty} \frac{a_n}{n+1}x^{n+1}}$ est la primitive de la série entière $\ds{\sum_{n\geq 0} a_nx^n}$ nulle en $0$, et son rayon de convergence est également $R$.	
\end{proposition}

	
\begin{methode}
Pour déterminer le rayon de convergence d'une série, on peut déterminer le rayon de convergence de sa dérivée ou de sa primitive nulle en $0$, qui est des fois plus facile à obtenir.	
\end{methode}


\exemple{Ainsi, la série $\ds{\sum_{n=0}^{+\infty} (n+1)x^n = \sum_{n=1}^{+\infty} nx^{n-1}}$ est la série dérivée de la série entière $\ds{\sum_{n=0}^{+\infty} x^n}$, de rayon de convergence $1$. Ainsi, la série $\ds{\sum_{n=0}^{+\infty} (n+1)x^n}$ a également un rayon de convergence $1$. Et puisque, pour $x\in ]-1;1[$, $\ds{\sum_{n=0}^{+\infty} x^n=\frac{1}{1-x}}$, on en déduit que pour tout $x\in ]-1;1[$ :
\[ \sum_{n=0}^{+\infty} (n+1)x^n = \frac{1}{(1-x)^2}\]
}

\exercice{Déterminer le rayon de convergence de la série $\ds{\sum_{n=0}^{+\infty} \frac{(-1)^n}{2n+1}x^{2n+1}}$.}

\begin{prof}
\solution{Cette série est la primitive nulle en $0$ de la série $\ds{\sum_{n=0}^{+\infty} (-1)^nx^{2n}}$ dont le rayon de convergence est $1$. Ainsi, la série de départ a pour rayon de convergence $1$.
}	
\end{prof}

\lignes{23}

\begin{methode}
Pour faire apparaitre la série dérivée (première, seconde, $\hdots$), on essaiera d'exprimer les coefficients polynomiaux suivant la base $(1, (n+1), (n+1)(n+2), \cdots )$.	
\end{methode}

\exercice{Déterminer le rayon de convergence et la somme de la série $\ds{\sum_{n=0}^{+\infty} nx^n}$ et de la série $\ds{\sum_{n=0}^{+\infty} (n^2-2n-1)x^n}$.}

\begin{prof}
\solution{Pour la première, on constate que $nx^n = x \times nx^{n-1}$. Ainsi, par linéarité
\[ \sum_{n=0}^{+\infty} nx^n = x \sum_{n=0}^{+\infty} nx^{n-1} \]
La deuxième série ayant un rayon de convergence de $1$, la première également par linéarité, et pour tout $x\in]-1;1[$ 
\[ \sum_{n=0}^{+\infty} nx^n =\frac{x}{(1-x)^2} \]
Pour la deuxième, on note $R$ son rayon de convergence et on constate que :
\begin{eqnarray*}
	(n^2-2n-1)x^n &=& \left ( (n+1)(n+2) -5n -3 \right)x^n \\
		&=& \left( (n+1)(n+2)-5(n+1)+2\right)x^n\\
		&=& (n+1)(n+2)x^{n} - 5(n+1)x^n +2x^n  
\end{eqnarray*} 
Chacune des séries entières $\ds{\sum_{n=0}^{+\infty} (n+1)(n+2)x^{n}}$, $\ds{\sum_{n=0}^{+\infty} (n+1)x^{n}}$ et $\ds{\sum_{n=0}^{+\infty}x^n}$ a pour rayon de convergence $1$ (car il s'agit d'une série de référence et de ses deux premières dérivées). Par linéarité, $R\geq 1$. Puisque la série $\ds{\sum_{n=0}^{+\infty} n^2-2n-2}$ diverge grossièrement, on en déduit que $R=1$. En dérivant la série entière $\ds{\sum_{n=0}^{+\infty} x^n}$ deux fois, on obtient
\[ \sum_{n=0}^{+\infty} (n+1)x^n = \frac{1}{(1-x)^2} \qeq \sum_{n=0}^{+\infty} (n+2)(n+1)x^n = \frac{2}{(1-x)^3} \]
et donc
\[ \sum_{n=0}^{+\infty} (n^2-2n-1)x^n = \frac{2}{(1-x)^3}-5\frac{1}{(1-x)^2}+2\frac{1}{1-x}= \frac{2x^2+x+4}{(1-x)^3} \]
}	
\end{prof}

\lignes{40}

	\subsection{Unicité du développement en série entière}
	
\begin{proposition}
Soit $\ds{\sum_{n\geq 0} a_nx^n}$ une série entière  à coefficients réels, et $R$ son rayon de convergence. On note $S\colon ]-R;R[\rightarrow \R$ la somme de la série entière $\ds{S\colon x\mapsto \sum_{n=0}^{+\infty} a_nx^n}$.

Alors, pour tout entier $n$, $a_n=\frac{S^{(n)}(0)}{n!}$. Ainsi, deux séries entières qui coïncident sur un intervalle ouvert autour de $0$ auront les mêmes coefficients.	
\end{proposition}

\remarque{\danger~ On remarque ainsi que les coefficients du développement en série entière correspond au développement limité au voisinage de $0$. Mais attention : un développement en série entière est valable de manière globale sur tout un intervalle, alors qu'un développement limité n'est valable qu'au voisinage de $0$.}

\consequence{En utilisant l'unicité d'un développement en série entière, on en déduit que :
\begin{enumerate}
	\item si la somme $S$ est paire, alors pour tout $n$, $a_{2n+1}=0$.
	\item si la somme $S$ est impaire, alors pour tout $n$, $a_{2n}=0$.
\end{enumerate}}

	\label{objectif-24-5}\subsection{Séries entière de référence}
	
%En appliquant les résultats précédents, on obtient les développements en série entière (DSE) suivant :

\begin{center}
\renewcommand{\arraystretch}{2}
\begin{tabular}{|ccm{5cm}|}\hline
		Rayon de convergence & Fonction &  \\[5pt]\hline
		$R=1$ (si $\alpha \not \in \N$) & $\ds{x\mapsto (1+x)^\alpha}$ & $\ds{=\sum_{n=0}^{+\infty} \frac{\alpha(\alpha-1)\cdots(\alpha-n+1)}{n!}x^n}$\\[5pt]\hline \hline
		$R=1$ & $\ds{x\mapsto \frac{1}{1-x}}$ & $\ds{=\sum_{n=0}^{+\infty} x^n}$\\[5pt]\hline
		$R=1$ & $\ds{x\mapsto \frac{1}{1+x}}$ & $\ds{=\sum_{n=0}^{+\infty} (-1)^nx^n}$\\[5pt]\hline  
		$R=1$ & $\ds{x\mapsto \ln(1+x)}$ & $\ds{=\sum_{n=1}^{+\infty} \frac{(-1)^{n-1}}{n}x^n}$\\[5pt]\hline 
		$R=1$ & $\ds{x\mapsto \frac{1}{1+x^2}}$ & $\ds{=\sum_{n=0}^{+\infty} (-1)^nx^{2n}}$ \\[5pt]\hline 
		$R=1$ & $\ds{x\mapsto \arctan(x)}$ & $\ds{=\sum_{n=0}^{+\infty} \frac{(-1)^n}{2n+1}x^{2n+1}}$ \\[5pt]\hline \hline
		$R=\infty$ & $x\mapsto \E^x$ & $\ds{=\sum_{n=0}^{+\infty}\frac{x^n}{n!} }$ \\[5pt]\hline 
		$R=\infty$ & $x\mapsto \cosh(x)$ & $\ds{=\sum_{n=0}^{+\infty} \frac{x^{2n}}{(2n)!}}$ \\[5pt]\hline
		$R=\infty$ & $x\mapsto \sinh(x)$ & $\ds{=\sum_{n=0}^{+\infty} \frac{x^{2n+1}}{(2n+1)!}}$ \\[5pt]\hline
		$R=\infty$ & $x\mapsto \cos(x)$ & $\ds{=\sum_{n=0}^{+\infty} \frac{(-1)^nx^{2n}}{(2n)!}}$ \\	[5pt]\hline
		$R=\infty$ & $x\mapsto \sin(x)$ & $\ds{=\sum_{n=0}^{+\infty} \frac{(-1)^n x^{2n+1}}{(2n+1)!}}$ \\[5pt]	\hline
\end{tabular}
\end{center}

\remarque{Le développement en série entière de l'exponentielle sur $\R$ est en réalité valable sur le plan complexe tout entier : pour tout $z\in \C$, on a alors
\[ \forall~z\in \C, \quad \E^z = \sum_{n=0}^{+\infty} \frac{z^n}{n!} \]
Ainsi, on récupère les développements en série entière de $\cos$  en calculant $\dfrac{\eu{iz}+\eu{-iz}}{2}$.}

\section{Applications}

	\label{objectif-24-6}\subsection{Equations différentielles}
	
Lorsqu'on ne sait pas résoudre une équation différentielle, on peut essayer d'en chercher une sous la forme d'une série entière, et en vérifiant que son rayon de convergence est strictement positif.

\begin{methode}
Pour résoudre une équation différentielle à l'aide des séries entières :
\begin{numerote}
	\item On cherche une solution de la forme $S(x)=\sum a_nx^n$ sans se préoccuper de son existence.
	\item Après changement d'indice, identification, et unicité du développement en série entière, on obtient une relation de récurrence entre les coefficients $a_n$.
	\item On utilise les conditions initiales pour obtenir le(s) premier(s) terme(s).
	\item On étudie alors le rayon de convergence du résultat obtenu.	
\end{numerote}	
\end{methode}

\exercice{Soit $(E)$ l'équation différentielle sur $]0;\frac{\pi}{2}[$ définie par \[ (E): xy''+2y'+xy=0 \]
 \begin{enumerate}
 	\item Soit $S(x)=\sum a_nx^n$ solution de $(E)$. Déterminer une relation de récurrence entre les $a_n$.
 	\item Déterminer la dérivée de $g:x\mapsto \frac{1}{\tan(x)}$. 
 	\item En déduire l'expression d'une solution $S$ sur l'intervalle considéré.
 	\item Déterminer alors toutes les solutions de l'équation différentielle.
 \end{enumerate}
}
\begin{prof}
\solution{~
\begin{enumerate}
	\item On injecte la solution dans l'équation différentielle, et on effectue des changements de variables. On a alors
	\begin{eqnarray*}
		xS''(x)+2S'(x)+xS(x) &=&  x\sum_{n=2}^{+\infty} n(n-1)a_nx^{n-2} + 2\sum_{n=1}^{+\infty} na_nx^{n-1} + x\sum_{n=0}^{+\infty} a_nx^n \\
		&=& \sum_{n=2}^{+\infty} n(n-1)a_nx^{n-1} + \sum_{n=1}^{+\infty} 2na_nx^{n-1}+\sum_{n=0}^{+\infty} a_nx^{n+1} \\
		&=& \sum_{n=1}^{+\infty} (n+1)na_{n+1}x^n + \sum_{n=0}^{+\infty} 2(n+1)a_{n+1}x^n  + \sum_{n=1}^{+\infty} a_{n-1}x^{n} \\
		&=& \sum_{n=1}^{+\infty} (n+1)na_{n+1}x^n + 2a_1 + \sum_{n=1}^{+\infty} 2(n+1)a_{n+1}x^n +\sum_{n=1}^{+\infty} a_{n-1}x^{n} \\
		&=& 2a_1 + \sum_{n=1}^{+\infty} ((n+1)na_{n+1} + 2(n+1)a_{n+1}+a_{n-1})x^n\\
		&=& 2a_1 + \sum_{n=1}^{+\infty} ((n+1)(n+2)a_{n+1} +a_{n-1})x^n
	\end{eqnarray*}
$S$ est solution de $(E)$ si et seulement si $xS''(x)+2S'(x)+xS(x)$, c'est-à-dire si et seulement si
\[2a_1=0 \qeq \forall~n\geq 1,\quad (n+1)(n+2)a_{n+1}+a_{n-1}=0 \]
	\item On en déduit donc que $a_1=0$ et par la relation de récurrence, pour tout entier $n$, $a_{2n+1}=0$. Enfin, on a $a_2=-\frac{1}{2\times 3}a_0=-2\frac{1}{3!}a_0$, $a_4=-\frac{1}{4\times 5}a_3=\frac{2}{5!} a_0$ et on peut montrer par récurrence sur $n$ que
		\[ \forall~n\in \N,\quad a_{2n}=\frac{(-1)^n 2}{(2n+1)!}a_0 \]
		Ainsi, \[ S(x)= \sum_{n=0}^{+\infty} \frac{(-1)^n 2}{(2n+1)!}a_0 x^{2n} \]
		ce qui se ré-écrit, pour $x\neq 0$ (puisque sur $\left]0;\frac{\pi}{2}\right[$) :
		\begin{eqnarray*}
			S(x) &=& 2a_0\sum_{n=0}^{+\infty} \frac{1}{x}\frac{(-1)^n}{(2n+1)!}x^{2n+1}\\
			 &=& \frac{2a_0}{x} (\sin(x)) 
		\end{eqnarray*}
	et le rayon de convergence de la série obtenue est donc infini. Une solution particulière de l'équation $(E)$ est donc
	\[ \boxed{f:x\mapsto \frac{\sin(x)}{x} } \]  
	\item Cette fonction est dérivable sur $\left]0;\frac{\pi}{2}\right[$ par quotient de fonctions dérivables dont le dénominateur ne s'annule pas et on a, pour $x\in \left]0;\frac{\pi}{2}\right[$ :
		\[ g'(x)= \frac{-\sin^2(x)-\cos^2(x)}{\sin^2(x)}=-\frac{1}{\sin^2(x)} \]
	\item Connaissant une solution, on cherche les autres de la forme $g=f\times k$, où $k$ est une fonction inconnue de classe $\CC^2$. On a alors $g'=f'k+fk'$, et $g''=f''k+2f'k'+fk''$ et en injectant dans l'équation $(E)$ :
		\begin{eqnarray*}
			xg''+2g'+xg &=& x(f''k+2f'k'+fk'')+2(f'k+fk')+xfk \\
			&=& k(\underbrace{xf''+2f'+xf}_{=0}) + 2xf'k'+xfk''+2fk' \\
			&=& k'(2xf'+2f)+xfk''
		\end{eqnarray*}
		En posant $K=k'$, on obtient l'équation différentielle
		\[ 2K \cos(x) + \sin(x)K'= 0 \]
		soit, sur $]0;\pi[$, $K' = -2\frac{\cos(x)}{\sin(x)}$, dont les solutions s'écrivent sur $]0;\pi[$ :
		\[ \mathcal{S}_K = \left \{ x\mapsto \alpha\eu{-\ln |\sin(x)|^2}, \alpha \in \R\right \} = \left \{ x\mapsto \frac{\alpha}{\sin^2(x)},\quad \alpha \in \R \right \} \]
		On revient à la fonction de départ : $K=k'$, et on obtient l'équation différentielle $\ds{k'(x) = \frac{\alpha}{\sin^2(x)}}$ ce qui, d'après la question $2$, donne
		\[ \mathcal{S}_k = \left \{ x\mapsto -\alpha \frac{1}{\tan(x)} + \beta,\quad (\alpha, \beta)\in \R^2 \right \} \] 
		On revient finalement à l'équation différentielle de départ (on avait posé $g=f\times k$) : l'ensemble des solutions de $(E)$ s'écrit donc
		\[ \mathcal{S} = \left \{ x\mapsto \frac{\sin(x)}{x}\left(-\alpha \frac{1}{\tan(x)} + \beta\right),\quad (\alpha, \beta)\in \R^2 \right \} \]
		ce qui s'écrit finalement :
		\[ \boxed{\mathcal{S} = \left \{ x\mapsto -\alpha\frac{\cos(x)}{x}+\beta\frac{\sin(x)}{x},\quad (\alpha, \beta)\in \R^2 \right \}} \]		
\end{enumerate}
}

\end{prof}
\lignes{15}
\lignes{50}
\lignes{33}	
	\label{objectif-24-7}\subsection{Déterminer un développement en série entière}
	
Pour déterminer un développement en série entière d'une fonction non usuelle, on peut chercher une équation différentielle dont elle est solution, puis en déduire son développement en série entière via l'équation différentielle.

\exercice{Soit $f$ la fonction définie sur $]-1;1[$ par \[ f:x\mapsto \frac{\arcsin(x)}{\sqrt{1-x^2}}\]
\begin{enumerate}
	\item Montrer que $f$ est solution du problème de Cauchy $(1-x^2)y'-xy=1$, avec $y(0)=0$.
	\item Déterminer le développement en série entière des solutions de l'équation différentielle.
	\item En déduire le DSE de $f$.
\end{enumerate}
}

\lignes{50}
\lignes{50}

	\label{objectif-24-8}\subsection{Calcul de somme de série}
	
Connaissant le développement en série entière d'une fonction, on peut trouver des valeurs particulières de séries en choisissant des réels bien choisis dans l'intervalle de convergence.

\exemple{Puisque, pour $x\in \R$, on a $\ds{\E^x = \sum_{n=0}^{+\infty} \frac{x^n}{n!}}$, en prenant $x=1$, on en déduit que 
\[ \sum_{n=0}^{+\infty} \frac{1}{n!} = \E \]}

\exercice{Déterminer la somme de la série $\ds{\sum_{n=0}^{+\infty} \frac{n+1}{2^n}}$.}

\begin{prof}
\solution{Remarquons que, pour tout $x\in ]-1;1[$ 
\[ \sum_{n=0}^{+\infty} (n+1)x^n = \frac{1}{(1-x)^2} \]
Pour $x=\frac{1}{2} \in ]-1;1[$, on a alors
\[ \sum_{n=0}^{+\infty} \frac{n+1}{2^n} = \frac{1}{(1-1/2)^2} =4 \]

}	
\end{prof}

\lignes{15}